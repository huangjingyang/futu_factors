#-*- coding: utf-8 -*-

# 对stock list 做切片处理，为跑批服务

import pandas as pd
import math
import csv
import numpy as np

def cut_stock_list():
    stock_info = pd.read_csv('./stock_info.csv', dtype="str")
    #stock_info = pd.read_csv('./6.csv', dtype="str")
    stock_list= stock_info['SecuCode'].tolist()
    # 计算数量
    num = 6
    run_len = math.ceil(len(stock_list) / num)
    # run_len = len(stock_list)
    for i in range(num):
        print(i * run_len, (i + 1) * run_len)
        calculate_list = stock_list[i * run_len: (i + 1) * run_len]
        calculate_list_array = np.array(calculate_list)
        temp_df = pd.DataFrame(calculate_list_array, columns=['SecuCode'])
        temp_df.to_csv("./files/"+ str(i) + ".csv", index=False)
        # calculate_list = [stock_list[i]]HK_Growthability



if __name__=="__main__":
    cut_stock_list()