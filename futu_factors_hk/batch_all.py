#-*- coding: utf-8 -*-

# 统一跑批

from multiprocesses import *
import configparser

class Batch():
    def __init__(self, start_date, now_date):
        self.start_time = start_date
        self.end_time = now_date
        pass

    def batch_run(self):
        run_start = datetime.datetime.now()

        #start_time = "2005-01-01"
        #end_time = "2018-09-28"
        config = configparser.ConfigParser()
        config.read("config/config.ini")
        start_time = str(config['global']['start_date'])
        end_time = str(config['global']['end_date'])
        num = int(config['global']['threads'])
        stock_list = self.get_stock_list()
        run_len = math.ceil(len(stock_list) / num)
        # run_len = len(stock_list)
        for i in range(run_len):
            calculate_list = stock_list[i * num: (i + 1) * num]
            # calculate_list = [stock_list[i]]HK_Growthability
            obj_main = Multiprocess(start_date, now_date)
            obj_main.calcualte_factors_stock_list(calculate_list)

        run_end = datetime.datetime.now()
        run_time = (run_end - run_start).seconds
        print("All Batch Run End", run_end, "cost time", run_time)

    # 获取港股所有股票
    def get_stock_basic_info(self):
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_stmt = text(
            "SELECT InnerCode, CompanyCode, SecuCode, ListedState, ListedDate, DelistingDate FROM HK_SecuMain " \
            "where SecuCategory in (3, 51, 52, 53) and ListedDate <= Date('{end_time}');".format(
                start_time=self.start_time, end_time=self.end_time))
        engine = create_engine(engine_str)
        df_result = pd.read_sql(sql_stmt, engine)
        return df_result

    # 获取跑批列表
    def get_stock_list(self):
        stock_info = self.get_stock_basic_info()
        stock_list = stock_info['SecuCode'].tolist()
        return stock_list

if __name__ == "__main__":
    now = datetime.datetime.now()
    now_date = now.strftime("%Y-%m-%d")
    start_time = now - datetime.timedelta(days=15)
    start_date = start_time.strftime("%Y-%m-%d")
    obj = Batch(start_date, now_date)
    obj.batch_run()