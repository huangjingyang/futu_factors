#-*- coding: utf-8 -*-

# 为基本面数据服务，输入市场（目前是香港市场），开始时间，结束时间
# 根据股票获得相关信息
# 输出一个Dataframe，基本信息有市场，Innercode,comanyCode, FutuSeccode,Start_time, end_time

from sqlalchemy import create_engine, text
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.exc import DisconnectionError, OperationalError, SQLAlchemyError
import pandas as pd
import numpy as np

class MarketStockBasicInfo():
    def __init__(self, market_code, start_time, end_time):
        self.market_code = market_code
        self.start_time = start_time
        self.end_time = end_time

    def get_stock_basic_info(self):
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_stmt = text("SELECT InnerCode, CompanyCode, SecuCode, ListedState, ListedDate, DelistingDate FROM HK_SecuMain " \
                       "where SecuCategory in (3, 51, 52, 53) and ListedDate <= Date('{end_time}');".format(start_time=self.start_time, end_time=self.end_time))
        engine = create_engine(engine_str)
        df_result = pd.read_sql(sql_stmt, engine)
        return df_result

if __name__=="__main__":
    market_code = 'HK'
    start_time = "2005-01-01"
    end_time = "2018-12-24"
    obj = MarketStockBasicInfo(market_code, start_time, end_time)
    result_df = obj.get_stock_basic_info()
    result_df.to_csv("../../input/stock_info.csv", index=False)
