#-*- coding: utf-8 -*-
import math
import numpy as np
import pandas as pd

class BasicOperations(object):
    def __init__(self, val):
        if type(val) is BasicOperations:
            val=val.val
        if pd.isnull(np.array([val], dtype=float))[0]:
            val = None
        self.val = val


    def format_value(self, value):
        try:
            temp_value_float = float(value)
        except:
            temp_value_float = 0
        if math.isnan(temp_value_float):
            temp_value_float = 0
        return temp_value_float

    def __sub__(self, other):
        if self.val == None and other.val == None:
            return BasicOperations(None)
        else:
            return BasicOperations(self.format_value(self.val) - self.format_value(other.val))

    def __add__(self, other):
        if self.val == None and other.val == None:
            return BasicOperations(None)
        else:
            return BasicOperations(self.format_value(self.val) + self.format_value(other.val))

    def __mul__(self, other):
        if self.val == None or other.val == None:
            return BasicOperations(None)
        else:
            return BasicOperations(self.format_value(self.val) * self.format_value(other.val))

    def __truediv__(self, other):
        if self.val == None or other.val == None:
            return BasicOperations(None)
        else:
            if other.val == 0:
                return BasicOperations(None)
            result = None
            try:
                result = self.val / other.val
            except:
                result = None
            return BasicOperations(result)

    def abs_value(self):
        try:
            temp_value_abs = abs(self.val)
        except:
            temp_value_abs = None
        return BasicOperations(temp_value_abs)


if __name__=="__main__":
    k1 = BasicOperations(34)
    k2 = BasicOperations(2)
    k1k = k1.abs_value()

    print(k1k.val)