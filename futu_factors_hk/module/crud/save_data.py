#-*- coding: utf-8 -*-

# 每一次都必须要注意删除以前的重复数据

class SaveDataToDB():
    def __init__(self):
        self.fundamental_table_dict = dict()
        pass

    # 保存基本面数据, 根据type来选择要存储的数据
    def save_fundamental_data(self, dataframe, type):
        pass

    # 保存量价数据
    # 日、周、月、年
    def save_non_fundamental_data(self, dataframe, type):
        pass


if __name__=="__main__":
    pass