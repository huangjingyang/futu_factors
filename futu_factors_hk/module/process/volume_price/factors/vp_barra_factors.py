#-*- coding: utf-8 -*-

# Barra量价因子
from module.process.volume_price.basic_factor import *

class VPBarraFactors(BasicFactor):
    __name__ = "VPBarraFactors"

    def __init__(self, stock_info_dict, time_info_dict, data_source_dict):
        BasicFactor.__init__(self, stock_info_dict, time_info_dict, data_source_dict)

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Hbeta" in columns_list:
            hbeta = self.get_hbeta(time_info)
            result_dict['Hbeta'] = hbeta
        if "Halpha" in columns_list:
            halpha = self.get_alpha(time_info)
            result_dict['Halpha'] = halpha
        if "Hsigma" in columns_list:
            hsigma = self.get_hsigma(time_info)
            result_dict['Hsigma'] = hsigma
        if "Dmsens" in columns_list:
            dmsens = self.get_dmsens(time_info)
            result_dict['Dmsens'] = dmsens
        if "Oilsen" in columns_list:
            oilsen = self.get_oilsen(time_info)
            result_dict['Oilsen'] = oilsen
        if "Dsbeta" in columns_list:
            dsbeta = self.get_dsbeta(time_info)
            result_dict['Dsbeta'] = dsbeta
        if "Cmra" in columns_list:
            cmra = self.get_cmra(time_info)
            result_dict['Cmra'] = cmra
        if "Stom" in columns_list:
            stom = self.get_stom(time_info)
            result_dict['Stom'] = stom
        if "Stoq" in columns_list:
            stoq = self.get_stoq(time_info)
            result_dict['Stoq'] = stoq
        if "Stoa" in columns_list:
            stoa = self.get_stoa(time_info)
            result_dict['Stoa'] = stoa
        if "Dastd" in columns_list:
            dastd = self.get_dastd(time_info)
            result_dict['Dastd'] = dastd
        if "Season" in columns_list:
            season = self.get_season(time_info)
            result_dict['Season'] = season
        if "Rstr" in columns_list:
            rstr = self.get_rstr(time_info)
            result_dict['Rstr'] = rstr
        if "Strev" in columns_list:
            strev = self.get_strev(time_info)
            result_dict['Strev'] = strev
        return result_dict


    def get_hbeta(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        hbeta = GetVPGlobalVariable().get_volume_price_data_point(self.data_source_dict["WLS"], trady_day_stamp, 'Beta')
        return hbeta


    def get_alpha(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        before_halpha_df = GetVPGlobalVariable().get_stock_dataframe_data(self.data_source_dict["WLS"],trady_day_stamp, 20)
        before_halpha_list = before_halpha_df['Alpha'].values[:11].tolist()
        if not before_halpha_list or before_halpha_list == len(before_halpha_list) * [None]:
        #if not before_halpha_list or np.isnan(before_halpha_list).sum() == len(before_halpha_list):
            return None
        else:
            halpha = np.mean(before_halpha_list)
            return halpha


    def get_hsigma(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        residual = GetVPGlobalVariable().get_volume_price_data_point(self.data_source_dict["WLS"], trady_day_stamp, 'Residual')
        if not isinstance(residual, np.ndarray):
            return None
        hsigma = np.std(residual)
        return hsigma


    def get_dmsens(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        residual = GetVPGlobalVariable().get_volume_price_data_point(self.data_source_dict["WLS"], trady_day_stamp, 'Residual')
        if not isinstance(residual, np.ndarray):
            return None
        residual_length = len(residual)
        index_df = GetVPGlobalVariable().get_index_dataframe_data(self.data_source_dict["SP"],trady_day_stamp, residual_length)
        index_yield_rate_array = index_df['yield_rate'].values
        weights = GetVPGlobalVariable().half_life_weight_list(63, residual_length)
        endog = residual
        exog = sm.add_constant(index_yield_rate_array)
        wls_model = sm.WLS(endog, exog, weights=weights)
        wls_results = wls_model.fit()
        beta = wls_results.params[1]
        dmsens = beta
        return dmsens

    def get_oilsen(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        residual = GetVPGlobalVariable().get_volume_price_data_point(self.data_source_dict["WLS"], trady_day_stamp, 'Residual')
        if not isinstance(residual, np.ndarray):
            return None
        residual_length = len(residual)
        oil_df = GetVPGlobalVariable().oil_market_data(self.data_source_dict["OIL"], trady_day_stamp,residual_length)
        oil_yield_rate_array = oil_df['yield_rate'].values
        weights = GetVPGlobalVariable().half_life_weight_list(63, residual_length)
        endog = residual
        exog = sm.add_constant(oil_yield_rate_array)
        wls_model = sm.WLS(endog, exog, weights=weights)
        wls_results = wls_model.fit()
        beta = wls_results.params[1]
        oilsen = beta
        return oilsen

    def get_dsbeta(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        stock_index_two_day_negative_tuple = GetVPGlobalVariable().stock_yield_rate_compare_index_tow_day_negative(self.data_source_dict["WLS"],
                                                                                                                   self.data_source_dict["HS"])
        stock_two_day_negative_df = stock_index_two_day_negative_tuple[0]
        index_tow_day_negative_df = stock_index_two_day_negative_tuple[1]

        stock_two_day_negative_df_252 = GetVPGlobalVariable().get_stock_dataframe_data(stock_two_day_negative_df,trady_day_stamp, 252)
        if stock_two_day_negative_df_252.empty:
            return None
        stock_yield_rate_array = stock_two_day_negative_df_252['Yield_Rate'].values
        stock_yield_rate_array_length = len(stock_yield_rate_array)
        index_two_day_negative_df_252 = GetVPGlobalVariable().get_index_dataframe_data(index_tow_day_negative_df,\
                                        trady_day_stamp, stock_yield_rate_array_length)
        index_yield_rate_array = index_two_day_negative_df_252['yield_rate'].values

        weights = GetVPGlobalVariable().half_life_weight_list(63, stock_yield_rate_array_length)
        endog = stock_yield_rate_array
        exog = sm.add_constant(index_yield_rate_array)
        wls_model = sm.WLS(endog, exog, weights=weights)
        wls_results = wls_model.fit()
        beta = wls_results.params[1]
        dsbeta = beta
        return dsbeta

    def get_cmra(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        risk_free_rate = .03
        #monthly_risk_free_rate = [risk_free_rate/k for k in range(12, 0 , -1)]
        monthly_risk_free_rate = [(1 + risk_free_rate)**(k/12) -1 for k in range(1, 13)] #对数收益率
        monthly_yield_rate_list = [] #应该为对数收益率
        for i in range(1, 13):
            monthly_yield_rate_list.append(GetVPGlobalVariable().get_num_monthly_yield_rate(self.data_source_dict['WLS'], trady_day_stamp, i))
        monthly_excess_return = pd.Series(monthly_yield_rate_list) - pd.Series(monthly_risk_free_rate)
        monthly_excess_return = monthly_excess_return.dropna()
        if len(monthly_excess_return) <= 1:
            return None
        elif 1 < len(monthly_excess_return) <= 5:
            monthly_excess_return_max = monthly_excess_return[-2:].max()
            monthly_excess_return_min = monthly_excess_return[-2:].min()
        elif 5 < len(monthly_excess_return) <= 11:
            monthly_excess_return_max = monthly_excess_return[-6:].max()
            monthly_excess_return_min = monthly_excess_return[-6:].min()
        else:
            monthly_excess_return_max = monthly_excess_return.max()
            monthly_excess_return_min = monthly_excess_return.min()
        cmra = monthly_excess_return_max - monthly_excess_return_min
        return cmra

    def get_stom(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        monthly_turnover_rate, num_days = GetVPGlobalVariable().get_num_monthly_turnover_rate(
            self.data_source_dict['Market'], trady_day_stamp, 1)
        if num_days == 0:
            return None
        stom = np.log(monthly_turnover_rate / (num_days / 21))
        return stom

    def get_stoq(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        quarterly_turnover_rate, num_days = GetVPGlobalVariable().get_num_monthly_turnover_rate(
            self.data_source_dict['Market'], trady_day_stamp, 3)
        if num_days == 0:
            return None
        stoq = np.log(quarterly_turnover_rate / (num_days / 21))
        return stoq

    def get_stoa(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        yearly_turnover_rate, num_days = GetVPGlobalVariable().get_num_monthly_turnover_rate(
            self.data_source_dict['Market'], trady_day_stamp, 12)
        if num_days == 0:
            return None
        stoa = np.log(yearly_turnover_rate / (num_days / 21))
        return stoa


    def get_dastd(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        stock_df = GetVPGlobalVariable().get_stock_dataframe_data(self.data_source_dict["WLS"], trady_day_stamp, 252)
        if stock_df.empty:
            return None
        stock_yield_rate_array = stock_df['Yield_Rate'].values
        stock_yield_rate_array_length = len(stock_yield_rate_array)
        index_df = GetVPGlobalVariable().get_index_dataframe_data(self.data_source_dict["HS"], trady_day_stamp, stock_yield_rate_array_length)
        index_yield_rate_array = index_df['yield_rate'].values
        excess_return_array = stock_yield_rate_array - index_yield_rate_array
        weights = GetVPGlobalVariable().half_life_weight_list(42, len(excess_return_array))
        excess_return_weightes_array = excess_return_array * np.array(weights)
        dastd = np.std(excess_return_array)
        return dastd


    def get_season(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        monthly_yield_rate_df = GetVPGlobalVariable().get_yearly_monthes_yield_rate(self.stock_info_dict['InnerCode'], trady_day_stamp, 5)
        monthly_yield_rate_df['Month'] = pd.DatetimeIndex(monthly_yield_rate_df['Date']).month
        monthly_yield_rate_df_samemonth = monthly_yield_rate_df[(monthly_yield_rate_df['Month'] == trady_day_stamp.month)&(monthly_yield_rate_df['Date'] != trady_day_stamp.replace(day = 1))]
        season = monthly_yield_rate_df_samemonth['yield_rate'].mean()
        return season


    def get_rstr(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        before_stock_df_21 = GetVPGlobalVariable().get_stock_dataframe_data(self.data_source_dict["WLS"], trady_day_stamp, 21)
        trady_day_previous11_previous21 = before_stock_df_21['Date'][-21+1:(-11+1)+1].tolist()
        rstr_list = []
        for date in trady_day_previous11_previous21:
            before_stock_df = GetVPGlobalVariable().get_stock_dataframe_data(self.data_source_dict["WLS"], date, 252)
            if before_stock_df.empty:
                return None
            stock_yield_rate_array = before_stock_df['Ln_Yield_Rate'].values
            stock_yield_rate_array_length = len(stock_yield_rate_array)
            index_yield_rate_df = GetVPGlobalVariable().get_index_dataframe_data(self.data_source_dict["HS"], date, stock_yield_rate_array_length)
            index_yield_rate_array = index_yield_rate_df['ln_yield_rate'].values
            excess_return_array = stock_yield_rate_array - index_yield_rate_array
            weights = GetVPGlobalVariable().half_life_weight_list(126, stock_yield_rate_array_length)
            excess_return = np.sum(weights * excess_return_array)
            rstr_list.append(excess_return)
        rstr = np.mean(rstr_list)
        return rstr
        
    def get_strev(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        before_stock_df_3 = GetVPGlobalVariable().get_stock_dataframe_data(self.data_source_dict["WLS"], trady_day_stamp, 3)
        trady_day_previous1_previous3 = before_stock_df_3['Date'][-3+1:].tolist()
        trady_day_previous1_previous3.append(trady_day_stamp)
        strev_list = []
        for date in trady_day_previous1_previous3:
            before_stock_df = GetVPGlobalVariable().get_stock_dataframe_data(self.data_source_dict["WLS"], date, 63)
            if before_stock_df.empty:
                return None
            stock_yield_rate_array = before_stock_df['Ln_Yield_Rate'].values
            stock_yield_rate_array_length = len(stock_yield_rate_array)
            index_yield_rate_df = GetVPGlobalVariable().get_index_dataframe_data(self.data_source_dict["HS"], date, stock_yield_rate_array_length)
            index_yield_rate_array = index_yield_rate_df['ln_yield_rate'].values
            excess_return_array = stock_yield_rate_array - index_yield_rate_array
            weights = GetVPGlobalVariable().half_life_weight_list(10, stock_yield_rate_array_length)
            excess_return = np.sum(weights * excess_return_array)
            strev_list.append(excess_return)
        rstr = np.mean(strev_list)
        return rstr


