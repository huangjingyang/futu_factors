#-*- coding: utf-8 -*-

# 基础量价因子
from module.process.volume_price.basic_factor import *

class VPCommonFactorsCN(BasicFactor):
    __name__ = "VPCommonFactors"

    def __init__(self, stock_info_dict, time_info_dict, data_source_dict):
        BasicFactor.__init__(self, stock_info_dict, time_info_dict, data_source_dict)

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Ps" in columns_list:
            result_dict['Ps'] = self.get_ps(time_info)
        if "Ps_ttm" in columns_list:
            result_dict['Ps_ttm'] = self.get_ps_ttm(time_info)
        if "Ps_lyr" in columns_list:
            result_dict['Ps_lyr'] = self.get_ps_lyr(time_info)
        if "Pcf_nfl" in columns_list:
            result_dict['Pcf_nfl'] = self.get_pcf_nfl(time_info)
        if "Pcf_nfl_ttm" in columns_list:
            result_dict['Pcf_nfl_ttm'] = self.get_pcf_nfl_ttm(time_info)
        if "Pcf_nfl_lyr" in columns_list:
            result_dict['Pcf_nfl_lyr'] = self.get_pcf_nfl_lyr(time_info)
        if "Pcf_ocf" in columns_list:
            result_dict['Pcf_ocf'] = self.get_pcf_ocf(time_info)
        if "Pcf_ocf_ttm" in columns_list:
            result_dict['Pcf_ocf_ttm'] = self.get_pcf_ocf_ttm(time_info)
        if "Pcf_ocf_lyr" in columns_list:
            result_dict['Pcf_ocf_lyr'] = self.get_pcf_ocf_lyr(time_info)
        if "Btop" in columns_list:
            result_dict['Btop'] = self.get_btop(time_info)
        if "Lncap" in columns_list:
            result_dict['Lncap'] = self.get_lncap(time_info)
        if "Midcap" in columns_list:
            result_dict['Midcap'] = self.get_midcap(time_info)
        if "Etop" in columns_list:
            result_dict['Etop'] = self.get_etop(time_info)
        if "Cetop" in columns_list:
            result_dict['Cetop'] = self.get_cetop(time_info)
        """
        if "Yield" in columns_list:
            result_dict['Yield'] = self.get_yield(time_info)
        """
        if "Yield_ttm" in columns_list:
            result_dict['Yield_ttm'] = self.get_yield_ttm(time_info)
        if "Mlev" in columns_list:
            result_dict['Mlev'] = self.get_mlev(time_info)
        if "Blev" in columns_list:
            result_dict['Blev'] = self.get_blev(time_info)
        if "HS_Industry" in columns_list:
            result_dict['HS_Industry'] = self.get_hs_industry(time_info)
        if "GICS_Industry" in columns_list:
            result_dict['GICS_Industry'] = self.get_gics_industry(time_info)
        if "HS_Industry_name" in columns_list:
            result_dict['HS_Industry_name'] = self.get_hs_industry_name(time_info)
        if "GICS_Industry_name" in columns_list:
            result_dict['GICS_Industry_name'] = self.get_gics_industry_name(time_info)
        if "Turnover" in columns_list:
            result_dict['Turnover'] = self.get_turnover(time_info)
        if "Shares_float" in columns_list:
            result_dict['Shares_float'] = self.get_shares_float(time_info)
        if "Shares_total" in columns_list:
            result_dict['Shares_total'] = self.get_shares_total(time_info)
        if "MV_float" in columns_list:
            result_dict['MV_float'] = self.get_mv_float(time_info)
        if "MV_total" in columns_list:
            result_dict['MV_total'] = self.get_mv_total(time_info)
        if "PE_ttm" in columns_list:
            result_dict['PE_ttm'] = self.get_PE_ttm(time_info)
        if "PB" in columns_list:
            result_dict['PB'] = self.get_PB(time_info)
        if "FPE" in columns_list:
            result_dict['FPE'] = self.get_FPE(time_info)
        if "DividendRatioFY" in columns_list:
            result_dict['DividendRatioFY'] = self.get_DividendRatioFY(time_info)
        if "DividendRatioRW" in columns_list:
            result_dict['DividendRatioRW'] = self.get_DividendRatioRW(time_info)
        if "PCF" in columns_list:
            result_dict['PCF'] = self.get_PCF(time_info)
        if "PERatio" in columns_list:
            result_dict['PERatio'] = self.get_PERatio(time_info)
        return result_dict

    def get_ps(self, time_info):
        # 取市值
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                         trady_day_stamp, 'MarketValue')
        # 取销售收入
        OperatingIncome = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"],
                                                                       trady_day_stamp, "ICC", "TotOpeRev")

        market_value = BasicOperations(market_value)
        OperatingIncome = BasicOperations(OperatingIncome)
        result = market_value / OperatingIncome
        return result.val

    def get_ps_ttm(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        OperatingIncome_ttm = GetVPGlobalVariable().get_finance_data_point_ttm(
            self.data_source_dict["FIN"], trady_day_stamp, "ICC",
            "TotOpeRev")

        market_value = BasicOperations(market_value)
        OperatingIncome_ttm = BasicOperations(OperatingIncome_ttm)
        result = market_value / OperatingIncome_ttm
        return result.val

    def get_ps_lyr(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        OperatingIncome_lyr = GetVPGlobalVariable().get_finance_data_point_lyr(
            self.data_source_dict["FIN"], trady_day_stamp, "ICC",
            "TotOpeRev")

        market_value = BasicOperations(market_value)
        OperatingIncome_lyr = BasicOperations(OperatingIncome_lyr)
        result = market_value / OperatingIncome_lyr
        return result.val

    def get_pcf_nfl(self, time_info):
        # 取市值
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                trady_day_stamp, 'MarketValue')
        # 取销售收入
        NetCashFlow = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"],
                                                                   trady_day_stamp, "FI", "NetCashFlow")

        market_value = BasicOperations(market_value)
        NetCashFlow = BasicOperations(NetCashFlow)
        result = market_value / NetCashFlow
        # print('NetCashFlow', NetCashFlow)
        return result.val

    def get_pcf_nfl_ttm(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        NetCashFlow_ttm = GetVPGlobalVariable().get_finance_data_point_ttm(self.data_source_dict["FIN"],
                                                                           trady_day_stamp, "FI", "NetCashFlow")

        market_value = BasicOperations(market_value)
        NetCashFlow_ttm = BasicOperations(NetCashFlow_ttm)
        result = market_value / NetCashFlow_ttm
        # print('NetCashFlow_ttm', NetCashFlow_ttm)
        return result.val

    def get_pcf_nfl_lyr(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        NetCashFlow_lyr = GetVPGlobalVariable().get_finance_data_point_lyr(self.data_source_dict["FIN"],
                                                                           trady_day_stamp, "FI",
                                                                           "NetCashFlow")

        market_value = BasicOperations(market_value)
        NetCashFlow_lyr = BasicOperations(NetCashFlow_lyr)
        result = market_value / NetCashFlow_lyr
        # print('NetCashFlow_lyr', NetCashFlow_lyr)
        return result.val

    def get_pcf_ocf(self, time_info):
        # 取市值
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                trady_day_stamp, 'MarketValue')
        # 取销售收入
        NetCashFromOperating = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"],
                                                                            trady_day_stamp, "CSC",
                                                                            "NetOpeCFlow")

        market_value = BasicOperations(market_value)
        NetCashFromOperating = BasicOperations(NetCashFromOperating)
        result = market_value / NetCashFromOperating
        return result.val

    def get_pcf_ocf_ttm(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        NetCashFromOperating_ttm = GetVPGlobalVariable().get_finance_data_point_ttm(
            self.data_source_dict["FIN"], trady_day_stamp, "CSC",
            "NetOpeCFlow")

        market_value = BasicOperations(market_value)
        NetCashFromOperating_ttm = BasicOperations(NetCashFromOperating_ttm)
        result = market_value / NetCashFromOperating_ttm
        #
        return result.val

    def get_pcf_ocf_lyr(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        NetCashFromOperating_lyr = GetVPGlobalVariable().get_finance_data_point_lyr(
            self.data_source_dict["FIN"], trady_day_stamp, "CSC",
            "NetOpeCFlow")

        market_value = BasicOperations(market_value)
        NetCashFromOperating_lyr = BasicOperations(NetCashFromOperating_lyr)
        result = market_value / NetCashFromOperating_lyr
        return result.val



    def get_btop(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        PB = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['QT'], trady_day_stamp, 'PB')
        PB = BasicOperations(PB)
        BTOP = BasicOperations(1)/PB
        return BTOP.val


    def get_lncap(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                trady_day_stamp, 'MarketValue')
        if market_value == None:
            return None
        lncap = np.log(market_value)
        return lncap

    def get_midcap(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                trady_day_stamp, 'MarketValue')
        if market_value == None:
            return None
        midcap = np.log(market_value)**3
        return midcap

    def get_etop(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        PETTM = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['QT'], trady_day_stamp, 'PETTM')
        PETTM = BasicOperations(PETTM)
        ETOP = BasicOperations(1) / PETTM
        return ETOP.val


    def get_cetop(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue')
        NetOpeCFlow = GetVPGlobalVariable().get_finance_data_point_ttm(self.data_source_dict["FIN"], trady_day_stamp, "CSC", "NetOpeCFlow")
        if NetOpeCFlow == None or str(NetOpeCFlow) == 'nan':
            return None
        NetOpeCFlow = BasicOperations(NetOpeCFlow)
        market_value = BasicOperations(market_value)
        cetop = NetOpeCFlow / market_value
        return cetop.val


    def get_yield(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        close_price = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                               trady_day_stamp, 'ClosePrice')
        CashDividendPS = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"], trady_day_stamp, "DV", "CashDividendPS")
        SpecialDividendPS = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"], trady_day_stamp, "DV", "SpecialDividendPS")

        if (str(CashDividendPS) == 'nan' or CashDividendPS == None) and (str(SpecialDividendPS) == 'nan' or SpecialDividendPS == None):
            return None
        elif str(CashDividendPS) == 'nan' or CashDividendPS == None:
            CashDividendPS = 0.0
        elif str(SpecialDividendPS) == 'nan' or SpecialDividendPS == None:
            SpecialDividendPS = 0.0
        yield_ratio = (CashDividendPS+SpecialDividendPS)/close_price
        return yield_ratio

    def get_yield_ttm(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        close_price = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                               trady_day_stamp, 'ClosePrice')

        CashDividendPS = GetVPGlobalVariable().get_hk_divident_ttm(self.data_source_dict["FIN"], trady_day_stamp, "CashDividendPS")
        SpecialDividendPS = GetVPGlobalVariable().get_hk_divident_ttm(self.data_source_dict["FIN"], trady_day_stamp, "SpecialDividendPS")
        if (str(CashDividendPS) == 'nan' or CashDividendPS == None) and (str(SpecialDividendPS) == 'nan' or SpecialDividendPS == None):
            return None
        elif str(CashDividendPS) == 'nan' or CashDividendPS == None:
            CashDividendPS = 0.0
        elif str(SpecialDividendPS) == 'nan' or SpecialDividendPS == None:
            SpecialDividendPS = 0.0
        yield_ratio = (CashDividendPS+SpecialDividendPS)/close_price
        return yield_ratio

    def get_mlev(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],trady_day_stamp, 'MarketValue')
        TotalNonCurLia_num = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"],trady_day_stamp, "BLC", "TotalNonCurLia")
        TotalNonCurLia_lyp = GetVPGlobalVariable().get_finance_data_point_lyp(self.data_source_dict["FIN"], trady_day_stamp, "BLC", "TotalNonCurLia")
        gics_industry = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                 trady_day_stamp, 'GICSIndustry')
        if gics_industry in set([40, 336, 363, 284, 254, 293, 212, 334, 171, 184, 150, 210, 154, 357]):
            return 0.0
        if TotalNonCurLia_num == None or str(TotalNonCurLia_num) == 'nan':
            return None
        elif TotalNonCurLia_lyp == None or str(TotalNonCurLia_lyp) == 'nan':
            TotalNonCurLia = TotalNonCurLia_num
        else:
            TotalNonCurLia = (TotalNonCurLia_num+TotalNonCurLia_lyp)/2
        mlev = TotalNonCurLia/(TotalNonCurLia + market_value)
        return mlev


    def get_blev(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        TotalNCurrentLiability_num = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"],trady_day_stamp, "BLC", "TotalNonCurLia")
        TotalNCurrentLiability_lyp = GetVPGlobalVariable().get_finance_data_point_lyp(self.data_source_dict["FIN"], trady_day_stamp, "BLC", "TotalNonCurLia")
        TotalEquity_num = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"], trady_day_stamp,"BLC", "TotalShEquity")
        TotalEquity_lyp = GetVPGlobalVariable().get_finance_data_point_lyp(self.data_source_dict["FIN"], trady_day_stamp, "BLC", "TotalShEquity")

        gics_industry = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],trady_day_stamp, 'GICSIndustry')
        if gics_industry in set([40, 336, 363, 284, 254, 293, 212, 334, 171, 184, 150, 210, 154, 357]):
            return 0.0
        TotalNCurrentLiability_num = BasicOperations(TotalNCurrentLiability_num)
        TotalNCurrentLiability_lyp = BasicOperations(TotalNCurrentLiability_lyp)
        TotalEquity_num = BasicOperations(TotalEquity_num)
        TotalEquity_lyp = BasicOperations(TotalEquity_lyp)

        if TotalNCurrentLiability_lyp.val == None or str(TotalNCurrentLiability_lyp.val) == 'nan':
            TotalNCurrentLiability = TotalNCurrentLiability_num
        else:
            TotalNCurrentLiability = BasicOperations((TotalNCurrentLiability_num.val + TotalNCurrentLiability_lyp.val) / 2)
        if TotalEquity_lyp.val == None or str(TotalEquity_lyp.val) == 'nan':
            TotalEquity = TotalEquity_num
        else:
            TotalEquity = BasicOperations((TotalEquity_num.val + TotalEquity_lyp.val) / 2)
        result = TotalNCurrentLiability / TotalEquity
        return result.val




    def get_hs_industry(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        hs_industry = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'HSIndustry')
        return hs_industry


    def get_gics_industry(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        gics_industry = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'GICSIndustry')
        return gics_industry



    def get_hs_industry_name(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        hs_industry = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'HSIndustry_name')
        hs_industry_name = None
        if hs_industry == "能源业":
            hs_industry_name = "Energy"
        elif hs_industry == "原材料业":
            hs_industry_name = "Material"
        elif hs_industry in ("工业制品业", "工业"):
            hs_industry_name = "IndustrialGoods"
        elif hs_industry == "消费品制造业":
            hs_industry_name = "ConsumerGoods"
        elif hs_industry in ("消费者服务业", "服务业"):
            hs_industry_name = "Services"
        elif hs_industry == "电讯业":
            hs_industry_name = "Telecommunications"
        elif hs_industry == "公用事业":
            hs_industry_name = "Utilities"
        elif hs_industry == "金融业":
            hs_industry_name = "Financials"
        elif hs_industry == "地产建筑业":
            hs_industry_name = "PropertiesConstruction"
        elif hs_industry == "资讯科技业":
            hs_industry_name = "InformationTechnology"
        elif hs_industry == "综合企业":
            hs_industry_name = "Conglomerates"
        return hs_industry_name

    def get_gics_industry_name(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        gics_industry = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'GICSIndustry_name')
        gics_industry_name = None
        if gics_industry == "能源":
            gics_industry_name = "Energy"
        elif gics_industry == "原材料":
            gics_industry_name = "Materials"
        elif gics_industry ==  "工业":
            gics_industry_name = "Industry"
        elif gics_industry == "非日常生活消费品":
            gics_industry_name = "ConsumerDiscretionary"
        elif gics_industry == "日常消费品":
            gics_industry_name = "ConsumerStaples"
        elif gics_industry == "金融":
            gics_industry_name = "Finance"
        elif gics_industry == "医疗保健":
            gics_industry_name = "MedicalHealth"
        elif gics_industry == "信息技术":
            gics_industry_name = "InformationTechnology"
        elif gics_industry == "电信业务":
            gics_industry_name = "Telecom"
        elif gics_industry == "公用事业":
            gics_industry_name = "Utilities"
        elif gics_industry == "房地产":
            gics_industry_name = "RealEstate"
        return gics_industry_name


    def get_turnover(self, time_info):
        # 换手率
        trady_day_stamp = time_info['Trading_Day']
        turnover_rate = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['Market'], trady_day_stamp, 'TurnoverRate')
        return turnover_rate

    def get_shares_float(self, time_info):
        # 流通股数
        trady_day_stamp = time_info['Trading_Day']
        ListedShares = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['Market'], trady_day_stamp, 'ListedShares')
        return ListedShares

    def get_shares_total(self, time_info):
        # 总股数
        trady_day_stamp = time_info['Trading_Day']
        TotalShares = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['Market'], trady_day_stamp, 'TotalShares')
        return TotalShares

    def get_mv_float(self, time_info):
        # 流通市值
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['Market'], trady_day_stamp, 'MarketValue')
        return market_value

    def get_mv_total(self, time_info):
        # 总市值
        trady_day_stamp = time_info['Trading_Day']
        market_value_total = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['Market'], trady_day_stamp, 'MarketValue_total')
        return market_value_total

    def get_PE_ttm(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        PE_ttm = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['QT'], trady_day_stamp, 'PETTM')
        return PE_ttm

    def get_PB(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        PB = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['QT'], trady_day_stamp, 'PB')
        return PB

    def get_FPE(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        FPE = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['QT'], trady_day_stamp, 'FPE')
        return FPE

    def get_DividendRatioFY(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        DividendRatioFY = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['QT'], trady_day_stamp, 'DividendRatioFY')
        return DividendRatioFY

    def get_DividendRatioRW(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        DividendRatioRW = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['QT'], trady_day_stamp, 'DividendRatioRW')
        return DividendRatioRW

    def get_PCF(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        PCF = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['QT'], trady_day_stamp, 'PCF')
        return PCF

    def get_PERatio(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        PERatio = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['QT'], trady_day_stamp, 'PERatio')
        return PERatio