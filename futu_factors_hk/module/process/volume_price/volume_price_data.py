#-*- coding: utf-8 -*-

# 输入股票信息，跑批开始时间，结束时间获得每日跑批数据，以日为单位

#from module.process.volume_price.vp_global_variable import *
from module.process.volume_price.data_function import *
from module.process.volume_price.basic_factor import *
import pandas as pd
import datetime
from sqlalchemy import create_engine, text
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.exc import DisconnectionError, OperationalError, SQLAlchemyError
import numpy as np
import statsmodels.api as sm
from module.process.volume_price.factors.vp_common_factors import *
from module.process.volume_price.factors.vp_common_factors_cn import *
from module.process.volume_price.factors.vp_barra_factors import *
import threading


class VolumePriceData():
    def __init__(self, stock_info_df, start_time, end_time):
        self.stock_info_df = stock_info_df
        self.start_time = start_time
        self.end_time = end_time
        self.start_time_format = self.convert_string_to_datetime(start_time)
        self.end_time_format = self.convert_string_to_datetime(end_time)
        # 日志功能
        #self.logger = self.get_logger()
        # 获取输入值
        self.stock_info_dict = self.stock_info_df.to_dict('records')[0]
        self.time_info_dict = self.get_time_info()
        self.recall_start_time = self.convert_datetime_to_string(self.time_info_dict['Recall_Start_Time'])
        self.cal_start_time = self.convert_datetime_to_string(self.time_info_dict['Cal_Start_Time'])
        self.cal_end_time = self.convert_datetime_to_string(self.time_info_dict['Cal_End_Time'])
        self.index_start_time = self.convert_datetime_to_string(self.time_info_dict['Index_Start_Time'])
        company_code = self.stock_info_dict['CompanyCode']
        #futu_secucode = self.stock_info_dict['Futu_SecuCode']
        inner_code = self.stock_info_dict['InnerCode']
        #stock_info_dict['End_Time'] = search_end_time_str
        self.obj_GetVPGlobalVariable = GetVPGlobalVariable()
        get_VPGlobalVariable_obj = GetVPGlobalVariable()
        self.get_VPGlobalVariable_obj = get_VPGlobalVariable_obj
        #self.VPGlobalVariable = VPGlobalVariable()

        self.data_source_dict = {}
        self.data_source_dict["RUN"] = 1
        self.data_source_dict["FIN"] = get_VPGlobalVariable_obj.get_stock_financial_data(company_code, inner_code, self.recall_start_time, self.cal_end_time)
        self.data_source_dict["HS"] = get_VPGlobalVariable_obj.get_hs_index_data(self.index_start_time, self.cal_end_time)
        self.data_source_dict["SP"] = get_VPGlobalVariable_obj.get_sp_index_data(self.index_start_time, self.cal_end_time)
        stock_data_wq_info_df = get_VPGlobalVariable_obj.get_stock_wq_data_info(inner_code, self.recall_start_time, self.cal_end_time)
        if stock_data_wq_info_df.empty or (self.data_source_dict["FIN"]['BalanceSheet'].empty and self.data_source_dict["FIN"]['CNBalanceSheet'].empty):
            self.data_source_dict["RUN"] = 0
            return

        # 处理股票数据,以及WLS的情况
        self.data_source_dict["WLS"] = get_VPGlobalVariable_obj.get_stock_data_info_add_wls(stock_data_wq_info_df, self.data_source_dict["HS"],self.cal_start_time)

        # 取市场量价数据：换手率、市值
        self.data_source_dict['Market'] = get_VPGlobalVariable_obj.get_stock_market_data_info(inner_code, company_code, self.recall_start_time, self.cal_end_time)
        # 石油数据
        self.data_source_dict["OIL"] = get_VPGlobalVariable_obj.load_all_oil_market_data()
        # 会计标准
        self.account_standard = self.data_source_dict["FIN"]['Standard']

        # 取QT_HKDailyQuoteIndex中的数据
        self.data_source_dict['QT'] = get_VPGlobalVariable_obj.get_QT_HKDailyQuoteIndex_data(inner_code, self.recall_start_time, self.cal_end_time)


    def get_time_info(self):
        time_info_dict = dict()
        listed_state_str = self.stock_info_dict['ListedState']
        listed_date_stamp = self.stock_info_dict['ListedDate']
        delisting_date_stamp = self.stock_info_dict['DelistingDate']
        listed_date = listed_date_stamp.to_pydatetime()
        if listed_date >= self.start_time_format:
            calculate_start_time = listed_date
        else:
            calculate_start_time = self.start_time_format
        if listed_state_str == 1:
            calculate_end_time = self.end_time_format
        else:
            delisting_date = delisting_date_stamp.to_pydatetime()
            if delisting_date >= self.end_time_format:
                calculate_end_time = self.end_time_format
            else:
                calculate_end_time = delisting_date

        # 获取回溯时间，满足计算用的
        temp_recall_start_time = calculate_start_time - relativedelta(years=2)
        index_start_time = self.start_time_format - relativedelta(years=2)
        if temp_recall_start_time < listed_date:
            temp_recall_start_time = listed_date
        time_info_dict['Recall_Start_Time'] = temp_recall_start_time
        time_info_dict['Cal_Start_Time'] = calculate_start_time
        time_info_dict['Cal_End_Time'] = calculate_end_time
        time_info_dict['Index_Start_Time'] = index_start_time
        return time_info_dict



    # 计算各大类因子
    def calculate_factors(self):
        if self.data_source_dict["RUN"] == 0:
            return
        t_common = threading.Thread(target=self.calculate_factors_common)
        t_barra = threading.Thread(target=self.calculate_factors_barra)
        t_common.start()
        t_barra.start()
        t_common.join()
        t_barra.join()

    def save_error_code(self, code, error):
        day_str = datetime.datetime.now().strftime('%Y-%m-%d')
        file_name = os.getcwd() + r"/log_files/error_log/" + day_str +"_" +code +"_vp_" + ".log"
        hs = open(file_name, "a+")
        hs.write("\n"+ str(code)+ "\terror\t" + error)
        hs.close()

    def calculate_factors_common(self):
        try:
            obj_common = VPCommonFactors(self.stock_info_dict, self.time_info_dict, self.data_source_dict)
            obj_common.calculate_factors_result()
        except Exception as err:
            self.save_error_code(self.stock_info_dict['SecuCode'], "Common: " + str(err))
            #raise Exception("Stock Code: " + self.stock_info_dict['SecuCode'] + "Common: " + str(err))
            #self.logger.error("Stock Code: " + self.stock_info_dict['SecuCode'] + "Common: " + str(err))



    def calculate_factors_barra(self):
        try:
            obj_barra = VPBarraFactors(self.stock_info_dict, self.time_info_dict, self.data_source_dict)
            obj_barra.calculate_factors_result()
        except Exception as err:
            self.save_error_code(self.stock_info_dict['SecuCode'], "Barra: " + str(err))
            #raise Exception("Stock Code: " + self.stock_info_dict['SecuCode'] + "Common: " + str(err))
            #self.logger.error("Stock Code: " + self.stock_info_dict['SecuCode'] + "Common: " + str(err))


    # 日期转化为字符
    def convert_datetime_to_string(self, datetime_foramt):
        datetime_str = datetime_foramt.strftime('%Y-%m-%d')
        return datetime_str

    # 字符转化为日期
    def convert_string_to_datetime(self, datetime_str):
        datetime_foramt = datetime.datetime.strptime(datetime_str, '%Y-%m-%d')
        return datetime_foramt

if __name__=="__main__":
    stock_info_df = pd.read_csv("../../test_files/stock_info_tencent.csv", engine='python')
    stock_info_dict = stock_info_df.to_dict('records')[0]
    print(type(stock_info_dict['ListedDate']))
    #print(stock_info_df)
    #start_time = "2006-01-01"
    #end_time = "2018-01-01"
    #obj = VolumePriceData(stock_info_df, start_time, end_time)
    #obj.traverse_date()
    #obj.get_ps()
    #obj.test()
    #print(VPGlobalVariable.hs_data_info_df)
    #print(VPGlobalVariable.stock_financial_data_dict)
    #obj.init_VPGlobalVariable()
    #print(VPGlobalVariable.hs_data_info_df)
