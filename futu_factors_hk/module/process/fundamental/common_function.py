#-*- coding: utf-8 -*-

import datetime
import pickle
import json
from sqlalchemy import create_engine, text
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.exc import DisconnectionError, OperationalError, SQLAlchemyError
import pandas as pd
import numpy as np
import math
#from module.basic.currency_convert_bak import *
from dateutil.relativedelta import relativedelta
from module.basic.basic_operations import *
import configparser


# 静态类
class CommonFunction():
    def __init__(self):
        pass

    # 日期转化为字符
    @classmethod
    def convert_datetime_to_string(self, datetime_foramt):
        datetime_str = ""
        datetime_str = datetime_foramt.strftime('%Y-%m-%d')
        return datetime_str

    # 字符转化为日期
    @classmethod
    def convert_string_to_datetime(self, datetime_str):
        datetime_foramt = datetime.datetime.min
        datetime_foramt = datetime.datetime.strptime(datetime_str, '%Y-%m-%d')
        return datetime_foramt

    # 取相对值
    @classmethod
    def get_data_point_lastest_date(self, stock_info, data_dict, time_info, table_name, data_name):
        inner_code = stock_info['InnerCode']
        data_point_value = np.nan
        end_time = self.convert_datetime_to_string(time_info['EndDate'])
        with open("config/formula.json") as f:
            config_data = json.load(f)
        # print(config_data)
        data_dict_key = config_data['fundamental']['table_names'][table_name]
        # print(data_dict_key)
        table_data = data_dict[data_dict_key]
        EndDate = self.convert_string_to_datetime(end_time)
        temp = table_data[table_data['EndDate'] <= EndDate]
        temp = temp.tail(1)
        temp = temp.reset_index(drop=True)
        if temp.empty:
            return None
        else:
            return temp[data_name].iloc[0]

    # 输入，名称，时间点，输出具体的值
    # C: current
    # Y: Year end
    # L: last year period
    @classmethod
    def get_data_point(self, stock_info, data_dict, time_info, table_name, data_name, date_type):
        inner_code = stock_info['InnerCode']
        data_point_value = np.nan
        end_time = self.convert_datetime_to_string(time_info['EndDate'])
        date_type_code = time_info['DateTypeCode']
        with open("config/formula.json") as f:
            config_data = json.load(f)
        #print(config_data)
        data_dict_key = config_data['fundamental']['table_names'][table_name]
        #print(data_dict_key)
        table_data = data_dict[data_dict_key]
        EndDate = self.convert_string_to_datetime(end_time)
        before_end_data = EndDate - relativedelta(years=2)
        if date_type == "C":
            temp = table_data[table_data['EndDate'] == EndDate]
        elif date_type == "Y":
            temp = table_data[(table_data['EndDate'] < EndDate) & (table_data['EndDate'] >= before_end_data) & (table_data['DateTypeCode'] == 12)]
        elif date_type == "L":
            before_end_data = EndDate - relativedelta(years=1)
            temp = table_data[(table_data['EndDate'] < EndDate) & (table_data['EndDate'] >= before_end_data) & (table_data['DateTypeCode'] == date_type_code)]
        elif date_type =="PRE":
            temp = table_data[(table_data['EndDate'] < EndDate) & (table_data['EndDate'] >= before_end_data)]
        temp = temp.tail(1)
        temp = temp.reset_index(drop=True)
        #temp_series = temp[data_name].copy()
        #temp_series  = temp_series.reset_index(drop=True)
        #temp_value = temp_series[data_name].get(0)
        #data_point_value = temp_series.get(0)
        if temp.empty:
            return None, None
        else:
            return temp['EndDate'].iloc[0], temp[data_name].iloc[0]

    # 取Full表中的数值
    # 取值规则：
    # 先取

    # 取行情数据
    @classmethod
    def get_daily_quote(self, inner_code, data_name, date_time_str):
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        engine = create_engine(engine_str)
        quote_stmt = text("""select * from QT_HKDailyQuoteIndex where InnerCode ='{inner_code}' and TradingDay = Date('{date_time_str}');""".format(inner_code=inner_code, date_time_str=date_time_str))
        quote_df = pd.read_sql(quote_stmt, engine)
        temp_series = quote_df[data_name].copy()
        temp_series = temp_series.reset_index(drop=True)
        # temp_value = temp_series[data_name].get(0)
        data_point_value = temp_series.get(0)
        return data_point_value

    # 取上一期数据
    @classmethod
    def get_data_point_pre(self, stock_info, data_dict, time_info, table_name, data_name, num=1):
        time_info_temp = time_info.copy()
        end_time = time_info_temp['EndDate']
        end_time_year_ago = end_time - pd.DateOffset(years=num - 1)
        time_info_temp['EndDate'] = end_time_year_ago
        date_type = "PRE"
        end_date, data_point_value = self.get_data_point_num_and_date(stock_info, data_dict, time_info, table_name,
                                                                      data_name, date_type)
        return data_point_value

    # 取上一年年末数据
    @classmethod
    def get_data_point_lyr(self, stock_info, data_dict, time_info, table_name, data_name, num=1):
        time_info_temp = time_info.copy()
        end_time = time_info_temp['EndDate']
        end_time_year_ago = end_time - pd.DateOffset(years=num-1)
        time_info_temp['EndDate'] = end_time_year_ago
        date_type = "Y"
        end_date, data_point_value = self.get_data_point_num_and_date(stock_info, data_dict, time_info, table_name, data_name, date_type)
        return data_point_value

    # 取上一年年末数据
    @classmethod
    def get_data_point_lyr_for_ttm(self, stock_info, data_dict, time_info, table_name, data_name, num=1):
        time_info_temp = time_info.copy()
        end_time = time_info_temp['EndDate']
        end_time_year_ago = end_time - pd.DateOffset(years=num - 1)
        time_info_temp['EndDate'] = end_time_year_ago
        date_type = "Y"
        end_date, data_point_value = self.get_data_point_num_and_date(stock_info, data_dict, time_info, table_name,
                                                                      data_name, date_type)
        return end_date, data_point_value

    # 去上一年的同期
    @classmethod
    def get_data_point_lyp(self, stock_info, data_dict, time_info, table_name, data_name, num=1):
        time_info_temp = time_info.copy()
        end_time = time_info_temp['EndDate']
        # end_time_last_year_period = end_time.replace(year=end_time.year - num)
        # - pd.DateOffset(years=years_num)
        end_time_last_year_period = end_time - pd.DateOffset(years=num-1)
        time_info_temp['EndDate'] = end_time_last_year_period
        date_type="L"
        data_point_value = self.get_data_point_num(stock_info, data_dict, time_info_temp, table_name, data_name, date_type)
        return data_point_value

    # 去上一年的同期
    @classmethod
    def get_data_point_lyp_for_ttm(self, stock_info, data_dict, time_info, table_name, data_name, num=1):
        time_info_temp = time_info.copy()
        end_time = time_info_temp['EndDate']
        #end_time_last_year_period = end_time.replace(year=end_time.year - num)
        # - pd.DateOffset(years=years_num)
        end_time_last_year_period = end_time - pd.DateOffset(years=num-1)
        time_info_temp['EndDate'] = end_time_last_year_period
        date_type = "L"
        end_date, data_point_value = self.get_data_point_num_and_date(stock_info, data_dict, time_info_temp, table_name, data_name, date_type)
        return end_date, data_point_value

    # 取TTM数据
    # 计算逻辑
    # 年报：直接取
    # 其他： 原始数据 + 上一年年报数据 - 上一期数据
    @classmethod
    def get_data_point_ttm(self, stock_info, data_dict, time_info, table_name, data_name, num=1):
        result_value = None
        current_end_date, current_value = self.get_data_point_num_and_date(stock_info, data_dict, time_info, table_name, data_name, "C")
        year_ago_end_date, year_ago_value = self.get_data_point_lyr_for_ttm(stock_info, data_dict, time_info, table_name, data_name, num)
        year_ago_period_end_date, year_ago_period_value = self.get_data_point_lyp_for_ttm(stock_info, data_dict, time_info, table_name, data_name, num)
        check_current_value = pd.isnull(np.array([current_value], dtype=float))[0]
        check_year_ago_value = pd.isnull(np.array([year_ago_value], dtype=float))[0]
        check_year_ago_period_value = pd.isnull(np.array([year_ago_period_value], dtype=float))[0]
        try:
            step_one_period = int((current_end_date - year_ago_end_date).days/30)
            if step_one_period == 12:
                return current_value
            if check_current_value == True and check_year_ago_value==False and check_year_ago_period_value== True:
                return year_ago_value
            if check_current_value == False and check_year_ago_value==False and check_year_ago_period_value== False:
                result_value = current_value + year_ago_value - year_ago_period_value
                return result_value
            step_one_mean = None
            step_two_mean = None
            if check_current_value==False:
                step_one_period = int((current_end_date - year_ago_end_date).days / 30)
                step_one_mean= current_value / step_one_period
            if check_year_ago_value==False and check_year_ago_period_value==False:
                step_two_period = int((year_ago_end_date - year_ago_period_end_date).days/30)
                if step_two_period == 0:
                    return year_ago_value
                step_two_mean = (year_ago_value - year_ago_period_value)/step_two_period
            check_step_one_mean = pd.isnull(np.array([step_one_mean], dtype=float))[0]
            check_step_two_mean = pd.isnull(np.array([step_two_mean], dtype=float))[0]
            if check_step_one_mean==False and check_step_two_mean==False:
                result_value = (step_one_mean + step_two_mean) / 2 * 12
            elif check_step_one_mean==False and check_step_two_mean==True:
                result_value = step_one_mean * 12
            elif check_step_one_mean==True and check_step_two_mean==False:
                result_value = step_two_mean * 12

        #result_value = current_value + year_ago_value - year_ago_period_value
        except:
           result_value = None
        return result_value

    # 获取当时当时时间点的汇率
    @classmethod
    def get_currency_data(self, stock_info, data_dict, time_info, table_name):
        currency_unit_code = time_info['CurrencyUnitCode']
        convert_time = time_info['EndDate']
        currency_value = self.get_currency_info(stock_info, currency_unit_code, convert_time)
        return currency_value


    # 货币转换取值
    @classmethod
    def get_data_point_num(self, stock_info, data_dict, time_info, table_name, data_name, date_type="C"):
        result = None
        _, data_point = self.get_data_point(stock_info, data_dict, time_info, table_name, data_name, date_type)
        if data_name == None:
            return result
        if table_name in ["BL", "IC", "CS", "ICC", "CSC", "BLC"]:
            currency_value = self.get_currency_data(stock_info, data_dict, time_info, table_name)
            try:
                result = data_point * currency_value
            except:
                result = None
        else:
            result = data_point
        return result

    # 获取数据值加上日期，用于TTM计算
    @classmethod
    def get_data_point_num_and_date(self, stock_info, data_dict, time_info, table_name, data_name, date_type):
        result = None
        end_date, data_point = self.get_data_point(stock_info, data_dict, time_info, table_name, data_name, date_type)
        if data_name == None:
            return result
        if table_name in ["BL", "IC", "CS", "ICC", "CSC", "BLC"]:
            currency_value = self.get_currency_data(stock_info, data_dict, time_info, table_name)
            try:
                result = data_point * currency_value
            except:
                result = None
        else:
            result = data_point
        return end_date, result

    # 取hs行业
    @classmethod
    def get_hs_industry(self, stock_info, data_dict, time_info, data_name):
        inner_code = stock_info['InnerCode']
        end_date = time_info['EndDate']
        table_data = data_dict["HSIndustry"]
        temp = table_data[(table_data['TradingDay'] <= end_date)]
        temp = temp.tail(1)
        temp = temp.reset_index(drop=True)
        if temp.empty:
            return None
        else:
            temp[data_name].iloc[0]

    @classmethod
    def get_currency_info(self, stock_info, currency_unit_code, convert_time):
        result_value = 1
        temp_currency_dict = stock_info['Currency_Temp']
        if currency_unit_code in temp_currency_dict.keys():
            if convert_time in temp_currency_dict[currency_unit_code].keys():
                result_value = temp_currency_dict[currency_unit_code][convert_time]
                return result_value
        config = configparser.ConfigParser()
        config.read("config/config.ini")
        currency = str(config["global"]["currency"])
        if str(currency) == "0":
            return result_value
        if str(currency_unit_code) == "1100":
            return result_value
        currency_data_dict = stock_info['Currency_Data']
        try:
            currency_code = ''
            path_currency = "config/Currency.csv"
            currency_df = pd.read_csv(path_currency)
            currency_code_series = currency_df.loc[
                currency_df['CurrencyUnitCode'] == int(currency_unit_code), 'CurrencyCode']
            if currency_code_series.shape[0] > 0:
                currency_code = currency_code_series.iloc[0]
            else:
                raise Exception('Error!')

            # currency_code = currency_code.reset_index(drop=True)
            path_currency_data = "config/CurrencyData.xlsx"
            currency_code_convert = currency_code + "HKD"
            if currency_code_convert in currency_data_dict.keys():
                target_currency_data_df = currency_data_dict[currency_code_convert]
            else:
                target_currency_data_df = pd.read_excel(path_currency_data, sheet_name=currency_code_convert,
                                                        header=None,
                                                        names=['Date', 'Value'])
                currency_data_dict[currency_code_convert] = target_currency_data_df

            result_value = target_currency_data_df.loc[target_currency_data_df['Date'] <= convert_time, 'Value'].iloc[
                -1]
        except Exception as err:
            raise Exception(currency_unit_code, convert_time, str(err))
        stock_info['Currency_Temp'] = {currency_unit_code: {convert_time: result_value}}
        return result_value