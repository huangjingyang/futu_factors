#-*- coding: utf-8 -*-

# 成长能力因子
from module.process.fundamental.basal_factor import *
import math

class FactorGrowthabilityCN(BasalFactor):
    __name__ = "Growthability_CN"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Growth_ocf" in columns_list:
            result_dict['Growth_ocf'] = self.get_growth_ocf(time_info)
        if "Yoyocfps" in columns_list:
            result_dict['Yoyocfps'] = self.get_yoyocfps(time_info)
        if "Yoy_or" in columns_list:
            result_dict['Yoy_or'] = self.get_yoy_or(time_info)
        if "Yoynetprofit_deducted" in columns_list:
            result_dict['Yoynetprofit_deducted'] = self.get_yoynetprofit_deducted(time_info)
        if "Yoyocf" in columns_list:
            result_dict['Yoyocf'] = self.get_yoyocf(time_info)
        if "Yoyroe" in columns_list:
            result_dict['Yoyroe'] = self.get_yoyroe(time_info)
        if "Growth_roe" in columns_list:
            result_dict['Growth_roe'] = self.get_growth_roe(time_info)
        if "Yoyroic" in columns_list:
            result_dict['Yoyroic'] = self.get_yoyroic(time_info)
        if "Yoyroic_3" in columns_list:
            result_dict['Yoyroic_3'] = self.get_yoyroic_3(time_info)
        if "Yoyeps_basic" in columns_list:
            result_dict['Yoyeps_basic'] = self.get_yoyeps_basic(time_info)
        if "Yoyop2" in columns_list:
            result_dict['Yoyop2'] = self.get_yoyop2(time_info)
        if "Yoyebt" in columns_list:
            result_dict['Yoyebt'] = self.get_yoyebt(time_info)
        if "Yoynetprofit" in columns_list:
            result_dict['Yoynetprofit'] = self.get_yoynetprofit(time_info)
        if "Growth_or" in columns_list:
            result_dict['Growth_or'] = self.get_growth_or(time_info)
        if "Growth_op" in columns_list:
            result_dict['Growth_op'] = self.get_growth_op(time_info)
        if "Growth_ebt" in columns_list:
            result_dict['Growth_ebt'] = self.get_growth_ebt(time_info)
        if "Growth_netprofit" in columns_list:
            result_dict['Growth_netprofit'] = self.get_growth_netprofit(time_info)
        if "Growth_assets" in columns_list:
            result_dict['Growth_assets'] = self.get_growth_assets(time_info)
        return result_dict

    # 解决中国会计标准的控制问题
    def get_yoynetprofit_deducted(self, time_info):
        return None

    def get_yoyocfps(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperCashFlowPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                            "OperCashFlowPS")
        OperCashFlowPS_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI",
                                                                         "OperCashFlowPS")

        OperCashFlowPS = BasicOperations(OperCashFlowPS)
        OperCashFlowPS_LYP = BasicOperations(OperCashFlowPS_LYP)
        OperCashFlowPS_LYP_abs = BasicOperations(OperCashFlowPS_LYP).abs_value()
        const_100 = BasicOperations(100)
        # OperCashFlowPS_LYP_abs.val = abs(OperCashFlowPS_LYP_abs.val)

        result = (OperCashFlowPS - OperCashFlowPS_LYP) / OperCashFlowPS_LYP_abs * const_100
        return result.val

    def get_yoyocf(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetOpeCFlow = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CSC",
                                                                            "NetOpeCFlow")
        NetOpeCFlow_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "CSC",
                                                                         "NetOpeCFlow")

        NetOpeCFlow = BasicOperations(NetOpeCFlow)
        NetOpeCFlow_LYP = BasicOperations(NetOpeCFlow_LYP)
        NetOpeCFlow_LYP_abs = BasicOperations(NetOpeCFlow_LYP).abs_value()
        #NetOpeCFlow_LYP_abs.val = abs(NetOpeCFlow_LYP_abs.val)
        const_100 = BasicOperations(100)

        result = (NetOpeCFlow - NetOpeCFlow_LYP) / NetOpeCFlow_LYP_abs*const_100
        return result.val

    def get_yoyroe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        ROEWeighted = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                            "ROEWeighted")
        ROEWeighted_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI",
                                                                         "ROEWeighted")

        ROEWeighted = BasicOperations(ROEWeighted)
        ROEWeighted_LYP = BasicOperations(ROEWeighted_LYP)
        ROEWeighted_LYP_abs = BasicOperations(ROEWeighted_LYP).abs_value()
        const_100 = BasicOperations(100)
        # ROEWeighted_LYP_abs.val = abs(ROEWeighted_LYP_abs.val)
        result = (ROEWeighted - ROEWeighted_LYP) / ROEWeighted_LYP_abs*const_100
        return result.val

    def get_growth_ocf(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetOpeCFlow = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CSC",
                                                                            "NetOpeCFlow")
        NetOpeCFlow_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "CSC",
                                                                         "NetOpeCFlow",num=3)

        NetOpeCFlow = BasicOperations(NetOpeCFlow)
        NetOpeCFlow_LYP3 = BasicOperations(NetOpeCFlow_LYP3)
        NetOpeCFlow_LYP3_abs = BasicOperations(NetOpeCFlow_LYP3).abs_value()
        const_100 = BasicOperations(100)
        # NetOpeCFlow_LYP3_abs.val = abs(NetOpeCFlow_LYP3_abs.val)

        result = (NetOpeCFlow - NetOpeCFlow_LYP3) / NetOpeCFlow_LYP3_abs*const_100

        return result.val

    def get_growth_roe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        ROEWeighted = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                            "ROEWeighted")
        ROEWeighted_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI",
                                                                         "ROEWeighted",num=3)

        ROEWeighted = BasicOperations(ROEWeighted)
        ROEWeighted_LYP3 = BasicOperations(ROEWeighted_LYP3)
        ROEWeighted_LYP3_abs = BasicOperations(ROEWeighted_LYP3).abs_value()
        const_100 = BasicOperations(100)
        # ROEWeighted_LYP3_abs.val = abs(ROEWeighted_LYP3_abs.val)
        result = (ROEWeighted - ROEWeighted_LYP3) / ROEWeighted_LYP3_abs*const_100

        return result.val

    def get_yoyroic(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPPCompOwners = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                   "NPPCompOwners")
        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "SEWithoutMI")
        ShortTermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "ShortTermLoan")
        NonCurLiaIn1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "NonCurLiaIn1Y")
        NotesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "NotesPayable")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "BondsPayable")
        BorFromCB = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "BorFromCB")
        NPPCompOwners_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "ICC",
                                                                   "NPPCompOwners")
        SEWithoutMI_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "SEWithoutMI")
        ShortTermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "ShortTermLoan")
        NonCurLiaIn1Y_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "NonCurLiaIn1Y")
        NotesPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "NotesPayable")
        LongtermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "LongtermLoan")
        BondsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "BondsPayable")
        BorFromCB_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                       "BorFromCB")
        SEWithoutMI_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "SEWithoutMI", num=2)
        ShortTermLoan_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "ShortTermLoan", num=2)
        NonCurLiaIn1Y_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "NonCurLiaIn1Y", num=2)
        NotesPayable_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "NotesPayable", num=2)
        LongtermLoan_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "LongtermLoan", num=2)
        BondsPayable_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "BondsPayable", num=2)
        BorFromCB_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                       "BorFromCB", num=2)

        NPPCompOwners = BasicOperations(NPPCompOwners)
        const_2 = BasicOperations(2)
        SEWithoutMI = BasicOperations(SEWithoutMI)
        ShortTermLoan = BasicOperations(ShortTermLoan)
        NonCurLiaIn1Y = BasicOperations(NonCurLiaIn1Y)
        NotesPayable = BasicOperations(NotesPayable)
        LongtermLoan = BasicOperations(LongtermLoan)
        BondsPayable = BasicOperations(BondsPayable)
        BorFromCB = BasicOperations(BorFromCB)
        NPPCompOwners_LYP = BasicOperations(NPPCompOwners_LYP)
        SEWithoutMI_LYP = BasicOperations(SEWithoutMI_LYP)
        ShortTermLoan_LYP = BasicOperations(ShortTermLoan_LYP)
        NonCurLiaIn1Y_LYP = BasicOperations(NonCurLiaIn1Y_LYP)
        NotesPayable_LYP = BasicOperations(NotesPayable_LYP)
        LongtermLoan_LYP = BasicOperations(LongtermLoan_LYP)
        BondsPayable_LYP = BasicOperations(BondsPayable_LYP)
        BorFromCB_LYP = BasicOperations(BorFromCB_LYP)
        SEWithoutMI_LYP2 = BasicOperations(SEWithoutMI_LYP2)
        ShortTermLoan_LYP2 = BasicOperations(ShortTermLoan_LYP2)
        NonCurLiaIn1Y_LYP2 = BasicOperations(NonCurLiaIn1Y_LYP2)
        NotesPayable_LYP2 = BasicOperations(NotesPayable_LYP2)
        LongtermLoan_LYP2 = BasicOperations(LongtermLoan_LYP2)
        BondsPayable_LYP2 = BasicOperations(BondsPayable_LYP2)
        BorFromCB_LYP2 = BasicOperations(BorFromCB_LYP2)

        ProfitToShareholders_abs = NPPCompOwners_LYP*const_2/(SEWithoutMI_LYP + ShortTermLoan_LYP + NonCurLiaIn1Y_LYP\
                                                                 + NotesPayable_LYP + LongtermLoan_LYP + BondsPayable_LYP + BorFromCB_LYP + SEWithoutMI_LYP2 + ShortTermLoan_LYP2 + NonCurLiaIn1Y_LYP2\
                                                                 + NotesPayable_LYP2 + LongtermLoan_LYP2 + BondsPayable_LYP2 + BorFromCB_LYP2)
        ProfitToShareholders_abs = BasicOperations(ProfitToShareholders_abs).abs_value()
        const_100 = BasicOperations(100)
        # ProfitToShareholders_abs.val = abs(ProfitToShareholders_abs.val)
        ROIC = NPPCompOwners*const_2/(SEWithoutMI + ShortTermLoan + NonCurLiaIn1Y\
                                                                 + NotesPayable + LongtermLoan + BondsPayable + BorFromCB + SEWithoutMI_LYP + ShortTermLoan_LYP + NonCurLiaIn1Y_LYP\
                                                                 + NotesPayable_LYP + LongtermLoan_LYP + BondsPayable_LYP + BorFromCB_LYP)
        ROIC_LYP = NPPCompOwners_LYP*const_2/(SEWithoutMI_LYP + ShortTermLoan_LYP + NonCurLiaIn1Y_LYP\
                                                                 + NotesPayable_LYP + LongtermLoan_LYP + BondsPayable_LYP + BorFromCB_LYP + SEWithoutMI_LYP2 + ShortTermLoan_LYP2 + NonCurLiaIn1Y_LYP2\
                                                                 + NotesPayable_LYP2 + LongtermLoan_LYP2 + BondsPayable_LYP2 + BorFromCB_LYP2)
        result = (ROIC - ROIC_LYP)/ProfitToShareholders_abs*const_100

        return result.val


    def get_yoyroic_3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPPCompOwners = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                   "NPPCompOwners")
        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "SEWithoutMI")
        ShortTermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "ShortTermLoan")
        NonCurLiaIn1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "NonCurLiaIn1Y")
        NotesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "NotesPayable")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "BondsPayable")
        BorFromCB = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "BorFromCB")

        SEWithoutMI_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "SEWithoutMI")
        ShortTermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "ShortTermLoan")
        NonCurLiaIn1Y_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "NonCurLiaIn1Y")
        NotesPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "NotesPayable")
        LongtermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "LongtermLoan")
        BondsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "BondsPayable")
        BorFromCB_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                       "BorFromCB")
        NPPCompOwners_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "ICC",
                                                                   "NPPCompOwners", num=3)
        SEWithoutMI_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "SEWithoutMI", num=3)
        ShortTermLoan_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "ShortTermLoan", num=3)
        NonCurLiaIn1Y_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "NonCurLiaIn1Y", num=3)
        NotesPayable_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "NotesPayable", num=3)
        LongtermLoan_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "LongtermLoan", num=3)
        BondsPayable_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "BondsPayable", num=3)
        BorFromCB_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                       "BorFromCB", num=3)
        SEWithoutMI_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "SEWithoutMI", num=4)
        ShortTermLoan_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "ShortTermLoan", num=4)
        NonCurLiaIn1Y_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "NonCurLiaIn1Y", num=4)
        NotesPayable_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "NotesPayable", num=4)
        LongtermLoan_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "LongtermLoan", num=4)
        BondsPayable_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                   "BondsPayable", num=4)
        BorFromCB_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                       "BorFromCB", num=4)


        NPPCompOwners = BasicOperations(NPPCompOwners)
        const_2 = BasicOperations(2)
        SEWithoutMI = BasicOperations(SEWithoutMI)
        ShortTermLoan = BasicOperations(ShortTermLoan)
        NonCurLiaIn1Y = BasicOperations(NonCurLiaIn1Y)
        NotesPayable = BasicOperations(NotesPayable)
        LongtermLoan = BasicOperations(LongtermLoan)
        BondsPayable = BasicOperations(BondsPayable)
        BorFromCB = BasicOperations(BorFromCB)
        NPPCompOwners_LYP3 = BasicOperations(NPPCompOwners_LYP3)
        SEWithoutMI_LYP = BasicOperations(SEWithoutMI_LYP)
        ShortTermLoan_LYP = BasicOperations(ShortTermLoan_LYP)
        NonCurLiaIn1Y_LYP = BasicOperations(NonCurLiaIn1Y_LYP)
        NotesPayable_LYP = BasicOperations(NotesPayable_LYP)
        LongtermLoan_LYP = BasicOperations(LongtermLoan_LYP)
        BondsPayable_LYP = BasicOperations(BondsPayable_LYP)
        BorFromCB_LYP = BasicOperations(BorFromCB_LYP)
        SEWithoutMI_LYP3 = BasicOperations(SEWithoutMI_LYP3)
        ShortTermLoan_LYP3 = BasicOperations(ShortTermLoan_LYP3)
        NonCurLiaIn1Y_LYP3 = BasicOperations(NonCurLiaIn1Y_LYP3)
        NotesPayable_LYP3 = BasicOperations(NotesPayable_LYP3)
        LongtermLoan_LYP3 = BasicOperations(LongtermLoan_LYP3)
        BondsPayable_LYP3 = BasicOperations(BondsPayable_LYP3)
        BorFromCB_LYP3 = BasicOperations(BorFromCB_LYP3)
        SEWithoutMI_LYP4 = BasicOperations(SEWithoutMI_LYP4)
        ShortTermLoan_LYP4 = BasicOperations(ShortTermLoan_LYP4)
        NonCurLiaIn1Y_LYP4 = BasicOperations(NonCurLiaIn1Y_LYP4)
        NotesPayable_LYP4 = BasicOperations(NotesPayable_LYP4)
        LongtermLoan_LYP4 = BasicOperations(LongtermLoan_LYP4)
        BondsPayable_LYP4 = BasicOperations(BondsPayable_LYP4)
        BorFromCB_LYP4 = BasicOperations(BorFromCB_LYP4)


        ProfitToShareholders_abs = NPPCompOwners_LYP3*const_2/(SEWithoutMI_LYP3 + ShortTermLoan_LYP3 + NonCurLiaIn1Y_LYP3\
                                                                 + NotesPayable_LYP3 + LongtermLoan_LYP3 + BondsPayable_LYP3 + BorFromCB_LYP3 + SEWithoutMI_LYP4 + ShortTermLoan_LYP4 + NonCurLiaIn1Y_LYP4\
                                                                 + NotesPayable_LYP4 + LongtermLoan_LYP4 + BondsPayable_LYP4 + BorFromCB_LYP4)
        ProfitToShareholders_abs = BasicOperations(ProfitToShareholders_abs).abs_value()
        const_100 = BasicOperations(100)
        # ProfitToShareholders_abs.val = abs(ProfitToShareholders_abs.val)
        ROIC = NPPCompOwners*const_2/(SEWithoutMI + ShortTermLoan + NonCurLiaIn1Y\
                                                                 + NotesPayable + LongtermLoan + BondsPayable + BorFromCB + SEWithoutMI_LYP + ShortTermLoan_LYP + NonCurLiaIn1Y_LYP\
                                                                 + NotesPayable_LYP + LongtermLoan_LYP + BondsPayable_LYP + BorFromCB_LYP)
        ROIC_LYP3 = NPPCompOwners_LYP3*const_2/(SEWithoutMI_LYP3 + ShortTermLoan_LYP3 + NonCurLiaIn1Y_LYP3\
                                                                 + NotesPayable_LYP3 + LongtermLoan_LYP3 + BondsPayable_LYP3 + BorFromCB_LYP3 + SEWithoutMI_LYP4 + ShortTermLoan_LYP4 + NonCurLiaIn1Y_LYP4\
                                                                 + NotesPayable_LYP4 + LongtermLoan_LYP4 + BondsPayable_LYP4 + BorFromCB_LYP4)
        result = (ROIC - ROIC_LYP3)/ProfitToShareholders_abs*const_100

        return result.val

    def get_yoyeps_basic(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        BasicEPS1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "BasicEPS1Y")
        result = BasicEPS1Y
        return result

    def get_yoy_or(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingRevenueGR1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "OperatingRevenueGR1Y")
        result = OperatingRevenueGR1Y
        return result

    def get_yoyop2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperProfitGR1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "OperProfitGR1Y")
        result = OperProfitGR1Y
        return result

    def get_yoyebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        EBTGR1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "EBTGR1Y")
        result = EBTGR1Y
        return result

    def get_yoynetprofit(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NPParentCompanyGR1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "NPParentCompanyGR1Y")
        result = NPParentCompanyGR1Y
        return result

    def get_growth_or(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingRevenueGR3Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "OperatingRevenueGR3Y")
        result = OperatingRevenueGR3Y
        return result

    def get_growth_op(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperProfitGR3Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "OperProfitGR3Y")
        result = OperProfitGR3Y
        return result

    def get_growth_ebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        EBTGR3Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "EBTGR3Y")
        result = EBTGR3Y
        return result

    def get_growth_netprofit(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetProfitGR3Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "NetProfitGR3Y")
        result = NetProfitGR3Y
        return result

    def get_growth_assets(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        TotalAssetGR3Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "TotalAssetGR3Y")
        result = TotalAssetGR3Y
        return result