#-*- coding: utf-8 -*-

# 营运能力因子
from module.process.fundamental.basal_factor import *

class FactorOperationabilityCN(BasalFactor):
    __name__ = "Operationability_CN"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Arturn" in columns_list:
            result_dict['Arturn'] = self.get_arturn(time_info)
        if "Netaccountsreceivable_lyr" in columns_list:
            result_dict['Netaccountsreceivable_lyr'] = self.get_netaccountsreceivable_lyr(time_info)
        if "Faturn" in columns_list:
            result_dict['Faturn'] = self.get_faturn(time_info)
        if "Assetsturn" in columns_list:
            result_dict['Assetsturn'] = self.get_assetsturn(time_info)
        if "Caturn" in columns_list:
            result_dict['Caturn'] = self.get_caturn(time_info)
        if "Invturn" in columns_list:
            result_dict['Invturn'] = self.get_invturn(time_info)
        if "Operatingincome_ttm" in columns_list:
            result_dict['Operatingincome_ttm'] = self.get_operatingincome_ttm(time_info)
        if "Operatingincome_lyr" in columns_list:
            result_dict['Operatingincome_lyr'] = self.get_operatingincome_lyr(time_info)
        if "Netcashflow_ttm" in columns_list:
            result_dict['Netcashflow_ttm'] = self.get_netcashflow_ttm(time_info)
        if "Netcashflow_lyr" in columns_list:
            result_dict['Netcashflow_lyr'] = self.get_netcashflow_lyr(time_info)
        if "Netcashfromoperating_ttm" in columns_list:
            result_dict['Netcashfromoperating_ttm'] = self.get_netcashfromoperating_ttm(time_info)
        if "Netcashfromoperating_lyr" in columns_list:
            result_dict['Netcashfromoperating_lyr'] = self.get_netcashfromoperating_lyr(time_info)
        if "Netaccountsreceivable" in columns_list:
            result_dict['Netaccountsreceivable'] = self.get_netaccountsreceivable(time_info)
        if "Netaccountsreceivable_ttm" in columns_list:
            result_dict['Netaccountsreceivable_ttm'] = self.get_netaccountsreceivable_ttm(time_info)

        return result_dict


    def get_arturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotOpeRev = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotOpeRev")

        AccReceivable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "AccReceivable")
        BillReceivable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "BillReceivable")

        AccReceivable_LYP = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "BLC", "AccReceivable")
        BillReceivable_LYP = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "BLC", "BillReceivable")

        TotOpeRev = BasicOperations(TotOpeRev)
        AccReceivable = BasicOperations(AccReceivable)
        BillReceivable = BasicOperations(BillReceivable)
        AccReceivable_LYP = BasicOperations(AccReceivable_LYP)
        BillReceivable_LYP = BasicOperations(BillReceivable_LYP)
        const_2 = BasicOperations(2)


        result = (TotOpeRev) / ((AccReceivable + BillReceivable + AccReceivable_LYP + BillReceivable_LYP) / const_2)
        return result.val


    def get_assetsturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        TotalAssetTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "TotalAssetTRate")
        result = TotalAssetTRate
        return result

    def get_caturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        CurrentAssetsTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "CurrentAssetsTRate")
        result = CurrentAssetsTRate
        return result

    def get_invturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OpeaCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "OpeaCost")
        Inventories = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "Inventories")
        Inventories_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC", "Inventories")

        OpeaCost = BasicOperations(OpeaCost)
        Inventories = BasicOperations(Inventories)
        Inventories_LYP = BasicOperations(Inventories_LYP)
        const_2 = BasicOperations(2)

        result = OpeaCost*const_2/(Inventories+Inventories_LYP)
        return result.val

    def get_faturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        FixedAssetTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "FixedAssetTRate")
        result = FixedAssetTRate
        return result


    def get_operatingincome_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        TotOpeRev_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "TotOpeRev")

        result = TotOpeRev_TTM
        return result


    def get_operatingincome_lyr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        TotOpeRev_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "ICC", "TotOpeRev")

        result = TotOpeRev_LYR
        return result

    def get_netcashflow_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetCashFlow_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "FI", "NetCashFlow")

        result = NetCashFlow_TTM
        return result

    def get_netcashflow_lyr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetCashFlow_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "FI", "NetCashFlow")

        result = NetCashFlow_LYR
        return result

    def get_netcashfromoperating_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetOpeCFlow_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "CSC", "NetOpeCFlow")

        result = NetOpeCFlow_TTM
        return result

    def get_netcashfromoperating_lyr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetOpeCFlow_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "CSC", "NetOpeCFlow")

        result = NetOpeCFlow_LYR
        return result


    def get_netaccountsreceivable(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        AccReceivable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "AccReceivable")
        BillReceivable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "BillReceivable")

        AccReceivable = BasicOperations(AccReceivable)
        BillReceivable = BasicOperations(BillReceivable)

        result = AccReceivable + BillReceivable
        return result.val


    def get_netaccountsreceivable_lyr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        AccReceivable_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "BLC", "AccReceivable")
        BillReceivable_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "BLC", "BillReceivable")

        AccReceivable_LYR = BasicOperations(AccReceivable_LYR)
        BillReceivable_LYR = BasicOperations(BillReceivable_LYR)

        result = AccReceivable_LYR + BillReceivable_LYR
        return result.val


    def get_netaccountsreceivable_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        AccReceivable_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "BLC", "AccReceivable")
        BillReceivable_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "BLC", "BillReceivable")

        AccReceivable_TTM = BasicOperations(AccReceivable_TTM)
        BillReceivable_TTM = BasicOperations(BillReceivable_TTM)

        result = AccReceivable_TTM + BillReceivable_TTM
        return result.val

