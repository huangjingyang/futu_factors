#-*- coding: utf-8 -*-

# 现金流情况因子

from module.process.fundamental.basal_factor import *
import math


class FactorCashflowCN(BasalFactor):
    __name__ = "Cashflow_CN"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Ocftosales" in columns_list:
            result_dict['Ocftosales'] = self.get_ocftosales(time_info)
        if "Ocftooperateincome" in columns_list:
            result_dict['Ocftooperateincome'] = self.get_ocftooperateincome(time_info)
        if "Optoebt" in columns_list:
            result_dict['Optoebt'] = self.get_optoebt(time_info)
        if "Opetoebt" in columns_list:
            result_dict['Opetoebt'] = self.get_opetoebt(time_info)
        if "Ebtoebt" in columns_list:
            result_dict['Ebtoebt'] = self.get_ebtoebt(time_info)
        return result_dict

    def get_ocftosales(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetOpeCFlow = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CSC",
                                                                   "NetOpeCFlow")

        TotOpeRev = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                   "TotOpeRev")

        NetOpeCFlow = BasicOperations(NetOpeCFlow)
        TotOpeRev = BasicOperations(TotOpeRev)
        const_100 = BasicOperations(100)


        result = NetOpeCFlow / (TotOpeRev)*const_100
        return result.val

    def get_ocftooperateincome(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetOpeCFlow = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CSC",
                                                                   "NetOpeCFlow")
        OpeProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                         "OpeProfit")

        NetOpeCFlow = BasicOperations(NetOpeCFlow)
        OpeProfit = BasicOperations(OpeProfit)
        const_100 = BasicOperations(100)
        result = NetOpeCFlow / (OpeProfit)*const_100
        return result.val

    def get_optoebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OpeProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                      "OpeProfit")

        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                 "TotalProfit")

        OpeProfit = BasicOperations(OpeProfit)
        TotalProfit = BasicOperations(TotalProfit)
        const_100 = BasicOperations(100)
        result = (OpeProfit) / TotalProfit*const_100
        return result.val

    def get_opetoebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OpeProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "OpeProfit")
        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotalProfit")

        OpeProfit = BasicOperations(OpeProfit)
        TotalProfit = BasicOperations(TotalProfit)
        const_100 = BasicOperations(100)
        result = (OpeProfit) / (TotalProfit)*const_100
        return result.val

    def get_ebtoebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitExSpecialItem = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "FI", "ProfitExSpecialItem")
        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotalProfit")

        ProfitExSpecialItem = BasicOperations(ProfitExSpecialItem)
        TotalProfit = BasicOperations(TotalProfit)
        const_100 = BasicOperations(100)

        result = (ProfitExSpecialItem) / (TotalProfit)*const_100
        return result.val