#-*- coding: utf-8 -*-

# 清债能力

from module.process.fundamental.basal_factor import *

class FactorResolutionability(BasalFactor):
    __name__ = "Resolutionability"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Equitytototalcapital" in columns_list:
            result_dict['Equitytototalcapital'] = self.get_equitytototalcapital(time_info)
        if "Interestdebttototalcapital" in columns_list:
            result_dict['Interestdebttototalcapital'] = self.get_interestdebttototalcapital(time_info)
        if "Catoassets" in columns_list:
            result_dict['Catoassets'] = self.get_catoassets(time_info)
        if "Currentdebttodebt" in columns_list:
            result_dict['Currentdebttodebt'] = self.get_currentdebttodebt(time_info)
        if "Debttoassets" in columns_list:
            result_dict['Debttoassets'] = self.get_debttoassets(time_info)
        if "Assetstoequity" in columns_list:
            result_dict['Assetstoequity'] = self.get_assetstoequity(time_info)
        if "Debttoequity" in columns_list:
            result_dict['Debttoequity'] = self.get_debttoequity(time_info)
        if "Cash" in columns_list:
            result_dict['Cash'] = self.get_cash(time_info)
        return result_dict

    def get_equitytototalcapital(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                       "ShareholderEquity")
        BankLoansAndOverdraft = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                     "BankLoansAndOverdraft")

        FOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                     "FOtherLoan")

        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                        "LongtermLoan")
        NFOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                            "NFOtherLoan")

        ConBillAndBond = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                 "ConBillAndBond")
        NotesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                 "NotesPayable")
        # NFFinanceLeaseOwes = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
        #                                                            "NFFinanceLeaseOwes")
        # FFinanceLeaseOwes = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
        #                                                            "FFinanceLeaseOwes")

        ShareholderEquity = BasicOperations(ShareholderEquity)
        BankLoansAndOverdraft = BasicOperations(BankLoansAndOverdraft)
        FOtherLoan = BasicOperations(FOtherLoan)
        LongtermLoan = BasicOperations(LongtermLoan)
        NFOtherLoan = BasicOperations(NFOtherLoan)
        ConBillAndBond = BasicOperations(ConBillAndBond)
        NotesPayable = BasicOperations(NotesPayable)
        # NFFinanceLeaseOwes = BasicOperations(NFFinanceLeaseOwes)
        # FFinanceLeaseOwes = BasicOperations(FFinanceLeaseOwes)
        const_100 = BasicOperations(100)

        result = ShareholderEquity / (ShareholderEquity + NotesPayable + BankLoansAndOverdraft + FOtherLoan + LongtermLoan + NFOtherLoan + ConBillAndBond)*const_100
        return result.val

    def get_interestdebttototalcapital(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ConBillAndBond = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                  "ConBillAndBond")
        NFOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                           "NFOtherLoan")

        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "LongtermLoan")

        NotesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                     "NotesPayable")
        BankLoansAndOverdraft = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                     "BankLoansAndOverdraft")
        FOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                  "FOtherLoan")

        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "ShareholderEquity")
        const_100 = BasicOperations(100)

        ConBillAndBond = BasicOperations(ConBillAndBond)
        NFOtherLoan = BasicOperations(NFOtherLoan)
        LongtermLoan = BasicOperations(LongtermLoan)
        NotesPayable = BasicOperations(NotesPayable)
        BankLoansAndOverdraft = BasicOperations(BankLoansAndOverdraft)
        FOtherLoan = BasicOperations(FOtherLoan)
        ShareholderEquity = BasicOperations(ShareholderEquity)

        result = (ConBillAndBond + NFOtherLoan + LongtermLoan + NotesPayable + BankLoansAndOverdraft + FOtherLoan) / (ShareholderEquity + ConBillAndBond + NFOtherLoan + LongtermLoan + NotesPayable + BankLoansAndOverdraft + FOtherLoan)*const_100
        return result.val

    def get_catoassets(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        CurrentAssetsToTA = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "CurrentAssetsToTA")
        result = CurrentAssetsToTA
        return result

    def get_currentdebttodebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        CurrentLiabilityToTL = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "CurrentLiabilityToTL")
        result = CurrentLiabilityToTL
        return result


    def get_debttoassets(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        DebtAssetsRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "DebtAssetsRatio")
        result = DebtAssetsRatio
        return result

    def get_assetstoequity(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EquityMultipler = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "EquityMultipler")
        result = EquityMultipler
        return result

    def get_debttoequity(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        DebtEquityRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "DebtEquityRatio")
        result = DebtEquityRatio
        return result

    def get_cash(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        Cash = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "Cash")
        CashHoldings = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "CashHoldings")
        ShortTermDeposit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "ShortTermDeposit")
        Cash = BasicOperations(Cash)
        CashHoldings = BasicOperations(CashHoldings)
        ShortTermDeposit = BasicOperations(ShortTermDeposit)

        if Cash.val == None:
            result = Cash + CashHoldings + ShortTermDeposit
        else:
            result = Cash + CashHoldings

        return result.val