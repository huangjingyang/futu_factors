#-*- coding: utf-8 -*-

# 营运能力因子
from module.process.fundamental.basal_factor import *

class FactorOperationability(BasalFactor):
    __name__ = "Operationability"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Arturn" in columns_list:
            result_dict['Arturn'] = self.get_arturn(time_info)
        if "Netaccountsreceivable_lyr" in columns_list:
            result_dict['Netaccountsreceivable_lyr'] = self.get_netaccountsreceivable_lyr(time_info)
        if "Faturn" in columns_list:
            result_dict['Faturn'] = self.get_faturn(time_info)
        if "Assetsturn" in columns_list:
            result_dict['Assetsturn'] = self.get_assetsturn(time_info)
        if "Caturn" in columns_list:
            result_dict['Caturn'] = self.get_caturn(time_info)
        if "Invturn" in columns_list:
            result_dict['Invturn'] = self.get_invturn(time_info)
        if "Operatingincome_ttm" in columns_list:
            result_dict['Operatingincome_ttm'] = self.get_operatingincome_ttm(time_info)
        if "Operatingincome_lyr" in columns_list:
            result_dict['Operatingincome_lyr'] = self.get_operatingincome_lyr(time_info)
        if "Netcashflow_ttm" in columns_list:
            result_dict['Netcashflow_ttm'] = self.get_netcashflow_ttm(time_info)
        if "Netcashflow_lyr" in columns_list:
            result_dict['Netcashflow_lyr'] = self.get_netcashflow_lyr(time_info)
        if "Netcashfromoperating_ttm" in columns_list:
            result_dict['Netcashfromoperating_ttm'] = self.get_netcashfromoperating_ttm(time_info)
        if "Netcashfromoperating_lyr" in columns_list:
            result_dict['Netcashfromoperating_lyr'] = self.get_netcashfromoperating_lyr(time_info)
        if "Netaccountsreceivable" in columns_list:
            result_dict['Netaccountsreceivable'] = self.get_netaccountsreceivable(time_info)
        if "Netaccountsreceivable_ttm" in columns_list:
            result_dict['Netaccountsreceivable_ttm'] = self.get_netaccountsreceivable_ttm(time_info)

        return result_dict

    def get_arturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        # OperRevenues = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "OperRevenues")
        # FOperatingRevenue = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "FOperatingRevenue")

        AccountReceivable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AccountReceivable")
        BillReceivable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BillReceivable")

        AccountReceivable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AccountReceivable")
        BillReceivable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "BillReceivable")

        # OperRevenues = BasicOperations(OperRevenues)
        # FOperatingRevenue = BasicOperations(FOperatingRevenue)
        AccountReceivable = BasicOperations(AccountReceivable)
        BillReceivable = BasicOperations(BillReceivable)
        AccountReceivable_LYP = BasicOperations(AccountReceivable_LYP)
        BillReceivable_LYP = BasicOperations(BillReceivable_LYP)
        const_num = BasicOperations(2)


        OperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                      "OperatingIncome")
        OtherOperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                           "OtherOperatingIncome")

        NetInterestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                        "NetInterestIncome")
        FOtherTurnover = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "FOtherTurnover")
        CommissionIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                       "CommissionIncome")
        DividendIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "DividendIncome")
        ForeignExchangeIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "ForeignExchangeIncome")
        SecuExAndInvestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "SecuExAndInvestIncome")
        NetRentIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "NetRentIncome")

        OperatingIncome = BasicOperations(OperatingIncome)
        OtherOperatingIncome = BasicOperations(OtherOperatingIncome)

        NetInterestIncome = BasicOperations(NetInterestIncome)
        FOtherTurnover = BasicOperations(FOtherTurnover)
        CommissionIncome = BasicOperations(CommissionIncome)
        DividendIncome = BasicOperations(DividendIncome)
        ForeignExchangeIncome = BasicOperations(ForeignExchangeIncome)
        SecuExAndInvestIncome = BasicOperations(SecuExAndInvestIncome)
        NetRentIncome = BasicOperations(NetRentIncome)

        result = (OperatingIncome + OtherOperatingIncome + NetInterestIncome+ FOtherTurnover + CommissionIncome+ DividendIncome + ForeignExchangeIncome + SecuExAndInvestIncome + NetRentIncome)*const_num / (AccountReceivable + BillReceivable + AccountReceivable_LYP + BillReceivable_LYP)
        return result.val


    def get_assetsturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        TotalAssetTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "TotalAssetTRate")
        result = TotalAssetTRate
        return result

    def get_caturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        CurrentAssetsTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "CurrentAssetsTRate")
        result = CurrentAssetsTRate
        return result

    def get_invturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        InventoryTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "InventoryTRate")
        result = InventoryTRate
        return result

    def get_faturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        FixedAssetTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "FixedAssetTRate")
        result = FixedAssetTRate
        return result

    def get_operatingincome_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "OperatingIncome")

        result = OperatingIncome_TTM
        return result

    def get_operatingincome_lyr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingIncome_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "IC", "OperatingIncome")

        result = OperatingIncome_LYR
        return result

    def get_netcashflow_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "FI", "NetCashFlow")

        result = OperatingIncome_TTM
        return result

    def get_netcashflow_lyr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingIncome_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "FI", "NetCashFlow")

        result = OperatingIncome_LYR
        return result

    def get_netcashfromoperating_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetCashFromOperating_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "CS", "NetCashFromOperating")

        result = NetCashFromOperating_TTM
        return result

    def get_netcashfromoperating_lyr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetCashFromOperating_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "CS", "NetCashFromOperating")

        result = NetCashFromOperating_LYR
        return result


    def get_netaccountsreceivable(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        AccountReceivable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AccountReceivable")
        BillReceivable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BillReceivable")

        AccountReceivable = BasicOperations(AccountReceivable)
        BillReceivable = BasicOperations(BillReceivable)

        result = AccountReceivable + BillReceivable
        return result.val


    def get_netaccountsreceivable_lyr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        AccountReceivable_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "BL", "AccountReceivable")
        BillReceivable_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "BL", "BillReceivable")

        AccountReceivable_LYR = BasicOperations(AccountReceivable_LYR)
        BillReceivable_LYR = BasicOperations(BillReceivable_LYR)

        result = AccountReceivable_LYR + BillReceivable_LYR
        return result.val


    def get_netaccountsreceivable_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        AccountReceivable_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "BL", "AccountReceivable")
        BillReceivable_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "BL", "BillReceivable")

        AccountReceivable_TTM = BasicOperations(AccountReceivable_TTM)
        BillReceivable_TTM = BasicOperations(BillReceivable_TTM)

        result = AccountReceivable_TTM + BillReceivable_TTM
        return result.val