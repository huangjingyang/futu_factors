#-*- coding: utf-8 -*-

# 清债能力

from module.process.fundamental.basal_factor import *

class FactorResolutionabilityCN(BasalFactor):
    __name__ = "Resolutionability_CN"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Equitytototalcapital" in columns_list:
            result_dict['Equitytototalcapital'] = self.get_equitytototalcapital(time_info)
        if "Interestdebttototalcapital" in columns_list:
            result_dict['Interestdebttototalcapital'] = self.get_interestdebttototalcapital(time_info)
        if "Catoassets" in columns_list:
            result_dict['Catoassets'] = self.get_catoassets(time_info)
        if "Currentdebttodebt" in columns_list:
            result_dict['Currentdebttodebt'] = self.get_currentdebttodebt(time_info)
        if "Debttoassets" in columns_list:
            result_dict['Debttoassets'] = self.get_debttoassets(time_info)
        if "Assetstoequity" in columns_list:
            result_dict['Assetstoequity'] = self.get_assetstoequity(time_info)
        if "Debttoequity" in columns_list:
            result_dict['Debttoequity'] = self.get_debttoequity(time_info)
        if "Cash" in columns_list:
            result_dict['Cash'] = self.get_cash(time_info)
        return result_dict


    def get_equitytototalcapital(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NonCurLiaIn1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                     "NonCurLiaIn1Y")

        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                       "SEWithoutMI")
        ShortTermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                     "ShortTermLoan")

        NotesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                     "NotesPayable")

        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                        "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                            "BondsPayable")

        BorFromCB = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                 "BorFromCB")

        NonCurLiaIn1Y = BasicOperations(NonCurLiaIn1Y)
        SEWithoutMI = BasicOperations(SEWithoutMI)
        ShortTermLoan = BasicOperations(ShortTermLoan)
        LongtermLoan = BasicOperations(LongtermLoan)
        NotesPayable = BasicOperations(NotesPayable)
        BondsPayable = BasicOperations(BondsPayable)
        BorFromCB = BasicOperations(BorFromCB)
        const_100 = BasicOperations(100)

        result = SEWithoutMI*const_100/\
                 (SEWithoutMI+ShortTermLoan+NonCurLiaIn1Y+NotesPayable+LongtermLoan+BondsPayable+BorFromCB)
        return result.val

    def get_interestdebttototalcapital(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                  "SEWithoutMI")
        ShortTermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                           "ShortTermLoan")

        NonCurLiaIn1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "NonCurLiaIn1Y")

        NotesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                     "NotesPayable")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                     "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                  "BondsPayable")

        BorFromCB = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "BorFromCB")

        SEWithoutMI = BasicOperations(SEWithoutMI)
        ShortTermLoan = BasicOperations(ShortTermLoan)
        NonCurLiaIn1Y = BasicOperations(NonCurLiaIn1Y)
        NotesPayable = BasicOperations(NotesPayable)
        LongtermLoan = BasicOperations(LongtermLoan)
        BondsPayable = BasicOperations(BondsPayable)
        BorFromCB = BasicOperations(BorFromCB)
        const_100 = BasicOperations(100)


        result = (ShortTermLoan+NonCurLiaIn1Y+NotesPayable+LongtermLoan+BondsPayable+BorFromCB)*const_100/\
                 (SEWithoutMI+ShortTermLoan+NonCurLiaIn1Y+NotesPayable+LongtermLoan+BondsPayable+BorFromCB)
        return result.val


    def get_catoassets(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        CurrentAssetsToTA = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "CurrentAssetsToTA")
        result = CurrentAssetsToTA
        return result

    def get_currentdebttodebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        CurrentLiabilityToTL = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "CurrentLiabilityToTL")
        result = CurrentLiabilityToTL
        return result


    def get_debttoassets(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        DebtAssetsRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "DebtAssetsRatio")
        result = DebtAssetsRatio
        return result

    def get_debtequityratio (self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EquityMultipler = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "DebtEquityRatio")
        result = EquityMultipler
        return result

    def get_assetstoequity(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EquityMultipler = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "EquityMultipler")
        result = EquityMultipler
        return result

    def get_debttoequity(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        DebtEquityRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "DebtEquityRatio")
        result = DebtEquityRatio
        return result

    def get_cash(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        CashEquivalents = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "CashEquivalents")
        CashEquivalents = BasicOperations(CashEquivalents)
        # print(CashEquivalents.val)

        result = CashEquivalents
        return result.val

