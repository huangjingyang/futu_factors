#-*- coding: utf-8 -*-

# 现金流情况因子

from module.process.fundamental.basal_factor import *
import math


class FactorCashflow(BasalFactor):
    __name__ = "Cashflow"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Ocftosales" in columns_list:
            result_dict['Ocftosales'] = self.get_ocftosales(time_info)
        if "Ocftooperateincome" in columns_list:
            result_dict['Ocftooperateincome'] = self.get_ocftooperateincome(time_info)
        if "Optoebt" in columns_list:
            result_dict['Optoebt'] = self.get_optoebt(time_info)
        if "Opetoebt" in columns_list:
            result_dict['Opetoebt'] = self.get_opetoebt(time_info)
        if "Ebtoebt" in columns_list:
            result_dict['Ebtoebt'] = self.get_ebtoebt(time_info)
        return result_dict

    def get_ocftosales(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetCashFromOperating = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CS",
                                                                   "NetCashFromOperating")
        OperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                      "OperatingIncome")
        OtherOperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                           "OtherOperatingIncome")

        NetInterestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                        "NetInterestIncome")
        FOtherTurnover = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "FOtherTurnover")
        CommissionIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                       "CommissionIncome")
        DividendIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "DividendIncome")
        ForeignExchangeIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "ForeignExchangeIncome")
        SecuExAndInvestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "SecuExAndInvestIncome")
        NetRentIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "NetRentIncome")
        NetCashFromOperating = BasicOperations(NetCashFromOperating)
        OperatingIncome = BasicOperations(OperatingIncome)
        OtherOperatingIncome = BasicOperations(OtherOperatingIncome)

        NetInterestIncome = BasicOperations(NetInterestIncome)
        FOtherTurnover = BasicOperations(FOtherTurnover)
        CommissionIncome = BasicOperations(CommissionIncome)
        DividendIncome = BasicOperations(DividendIncome)
        ForeignExchangeIncome = BasicOperations(ForeignExchangeIncome)
        SecuExAndInvestIncome = BasicOperations(SecuExAndInvestIncome)
        NetRentIncome = BasicOperations(NetRentIncome)
        const_100 = BasicOperations(100)

        result = NetCashFromOperating*const_100 / (OperatingIncome + OtherOperatingIncome + NetInterestIncome+ FOtherTurnover + CommissionIncome+ DividendIncome + ForeignExchangeIncome + SecuExAndInvestIncome + NetRentIncome)
        return result.val

    def get_ocftooperateincome(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetCashFromOperating = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CS",
                                                                            "NetCashFromOperating")
        OperateProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                         "OperateProfit")

        AfPrepareOpePayoff = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "AfPrepareOpePayoff")
        NetCashFromOperating = BasicOperations(NetCashFromOperating)
        OperateProfit = BasicOperations(OperateProfit)
        AfPrepareOpePayoff = BasicOperations(AfPrepareOpePayoff)
        const_100 = BasicOperations(100)

        result = NetCashFromOperating / (OperateProfit + AfPrepareOpePayoff)*const_100
        return result.val

    def get_optoebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                      "OperateProfit")

        AfPrepareOpePayoff = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "AfPrepareOpePayoff")
        FOperatingExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "FOperatingExpense")

        EarningBeforeTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                         "EarningBeforeTax")

        OperProfit = BasicOperations(OperProfit)
        AfPrepareOpePayoff = BasicOperations(AfPrepareOpePayoff)
        FOperatingExpense = BasicOperations(FOperatingExpense)
        EarningBeforeTax = BasicOperations(EarningBeforeTax)
        const_100 = BasicOperations(100)

        result = (OperProfit + AfPrepareOpePayoff) / EarningBeforeTax*const_100
        return result.val

    def get_opetoebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperateProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                      "OperateProfit")

        AfPrepareOpePayoff = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "AfPrepareOpePayoff")
        EarningBeforeTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                         "EarningBeforeTax")

        OperateProfit = BasicOperations(OperateProfit)
        AfPrepareOpePayoff = BasicOperations(AfPrepareOpePayoff)
        EarningBeforeTax = BasicOperations(EarningBeforeTax)
        const_100 = BasicOperations(100)

        result = (OperateProfit + AfPrepareOpePayoff) / EarningBeforeTax*const_100
        return result.val

    def get_ebtoebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        EndOrSpecProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "EndOrSpecProfit")
        EarningBeforeTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                         "EarningBeforeTax")

        EndOrSpecProfit = BasicOperations(EndOrSpecProfit)
        EarningBeforeTax = BasicOperations(EarningBeforeTax)
        const_100 = BasicOperations(100)

        result = (EarningBeforeTax - EndOrSpecProfit) / EarningBeforeTax*const_100
        return result.val
