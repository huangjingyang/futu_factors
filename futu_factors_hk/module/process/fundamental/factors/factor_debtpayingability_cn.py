#-*- coding: utf-8 -*-

# 偿债能力因子
from module.process.fundamental.basal_factor import *

class FactorDebtpayingabilityCN(BasalFactor):
    __name__ = "Debtpayingability_CN"
    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Ebitdatodebt" in columns_list:
            result_dict['Ebitdatodebt'] = self.get_ebitdatodebt(time_info)
        if "Tangibleassettodebt" in columns_list:
            result_dict['Tangibleassettodebt'] = self.get_tangibleassettodebt(time_info)
        if "Current" in columns_list:
            result_dict['Current'] = self.get_current(time_info)
        if "Quick" in columns_list:
            result_dict['Quick'] = self.get_quick(time_info)
        if "Optoliqdebt" in columns_list:
            result_dict['Optoliqdebt'] = self.get_optoliqdebt(time_info)
        if "Ocftoliqdebt" in columns_list:
            result_dict['Ocftoliqdebt'] = self.get_ocftoliqdebt(time_info)
        if "Equitytodebt" in columns_list:
            result_dict['Equitytodebt'] = self.get_equitytodebt(time_info)
        if "Dtoa" in columns_list:
            result_dict['Dtoa'] = self.get_dtoa(time_info)
        if "Interestcover" in columns_list:
            result_dict['Interestcover'] = self.get_InterestCover(time_info)
        if "Opercashflowtotl" in columns_list:
            result_dict['Opercashflowtotl'] = self.get_Opercashflowtotl(time_info)
        return result_dict


    def get_ebitdatodebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotalProfit")
        FinExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "FinExpense")
        TotalLiab = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "TotalLiab")
        FAssetDep = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CSC", "FAssetDep")
        IntAAmort = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CSC", "IntAAmort")

        TotalProfit = BasicOperations(TotalProfit)
        FinExpense = BasicOperations(FinExpense)
        TotalLiab = BasicOperations(TotalLiab)
        FAssetDep = BasicOperations(FAssetDep)
        IntAAmort = BasicOperations(IntAAmort)

        result = (TotalProfit+ FinExpense+FAssetDep+IntAAmort)/TotalLiab
        return result.val

    def get_tangibleassettodebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        '''
        FixedAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                       "FixedAssets")
        ConsMaterials = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                     "ConsMaterials")

        ConsInProcess = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                     "ConsInProcess")
        FixedAssetsLiq = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                     "FixedAssetsLiq")
        BiolAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                     "BiolAssets")
        OilGasAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                     "OilGasAssets")
        TotalLiab = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "TotalLiab")
        FixedAssets = BasicOperations(FixedAssets)
        ConsMaterials = BasicOperations(ConsMaterials)
        ConsInProcess = BasicOperations(ConsInProcess)
        FixedAssetsLiq = BasicOperations(FixedAssetsLiq)
        OilGasAssets = BasicOperations(OilGasAssets)
        BiolAssets = BasicOperations(BiolAssets)



        TotalLiab = BasicOperations(TotalLiab)

        result = (FixedAssets+ConsMaterials+ConsInProcess+FixedAssetsLiq + OilGasAssets + BiolAssets)/ TotalLiab
        '''
        # 有形资产／负债合计
        # (总资产 - 无形资产)/总负债
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "TotalAssets")
        IntAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "IntAssets")
        TotalLiab = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "TotalLiab")
        TotalAssets = BasicOperations(TotalAssets)
        IntAssets = BasicOperations(IntAssets)
        TotalLiab = BasicOperations(TotalLiab)
        result = (TotalAssets - IntAssets) / TotalLiab
        return result.val

    def get_current(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        CurrentRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "CurrentRatio")
        result = CurrentRatio
        return result

    def get_quick(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        QuickRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "QuickRatio")
        result = QuickRatio
        return result


    def get_optoliqdebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperProfitToCL = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "OperProfitToCL")
        result = OperProfitToCL
        return result


    def get_ocftoliqdebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperCashFlowToCL = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "OperCashFlowToCL")
        result = OperCashFlowToCL
        return result


    def get_equitytodebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        SEWithoutMIToTL = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "SEWithoutMIToTL")
        result = SEWithoutMIToTL
        return result

    def get_dtoa(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "TotalLiab")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "TotalShEquity")

        TotalLiability = BasicOperations(TotalLiability)
        TotalAssets = BasicOperations(TotalAssets)

        result = TotalLiability / TotalAssets
        return result.val

    def get_InterestCover(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotalProfit")
        FinExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "FinExpense")

        TotalProfit = BasicOperations(TotalProfit)
        FinExpense = BasicOperations(FinExpense)

        result = (TotalProfit+ FinExpense)/FinExpense
        return result.val

    def get_Opercashflowtotl(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperCashFlowToTL = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "OperCashFlowToTL")
        result = OperCashFlowToTL
        return result

    '''
    def get_blev(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalNCurrentLiability_num = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC","TotalNonCurLia")
        TotalNCurrentLiability_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC","TotalNonCurLia")
        TotalEquity_num = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC","TotalShEquity")
        TotalEquity_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC", "TotalShEquity")
        TotalNCurrentLiability_num = BasicOperations(TotalNCurrentLiability_num)
        TotalNCurrentLiability_lyp = BasicOperations(TotalNCurrentLiability_lyp)
        TotalEquity_num = BasicOperations(TotalEquity_num)
        TotalEquity_lyp = BasicOperations(TotalEquity_lyp)

        if TotalNCurrentLiability_lyp.val == None or str(TotalNCurrentLiability_lyp.val) == 'nan':
            TotalNCurrentLiability = TotalNCurrentLiability_num
        else:
            TotalNCurrentLiability = BasicOperations((TotalNCurrentLiability_num.val + TotalNCurrentLiability_lyp.val)/2)
        if TotalEquity_lyp.val == None or str(TotalEquity_lyp.val) == 'nan':
            TotalEquity = TotalEquity_num
        else:
            TotalEquity = BasicOperations((TotalEquity_num.val + TotalEquity_lyp.val)/2)
        result = TotalNCurrentLiability / TotalEquity
        return result.val
    '''
