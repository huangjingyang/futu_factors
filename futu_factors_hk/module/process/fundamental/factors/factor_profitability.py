#-*- coding: utf-8-*-

# 盈利能力因子计算
from module.process.fundamental.basal_factor import *

class FactorProfitability(BasalFactor):
    __name__ = "Profitability"
    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass


    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Roic1" in columns_list:
            result_dict['Roic1'] = self.get_roic1(time_info)
        if "Roe_ttm3" in columns_list:
            result_dict['Roe_ttm3'] = self.get_roe_ttm3(time_info)
        if "Ebitdatosales" in columns_list:
            result_dict['Ebitdatosales'] = self.get_ebitdatosales(time_info)
        if "Roe_diluted" in columns_list:
            result_dict['Roe_diluted'] = self.get_roe_diluted(time_info)
        if "Finaexpensetogr" in columns_list:
            result_dict['Finaexpensetogr'] = self.get_finaexpensetogr(time_info)
        if "Roe_deducted" in columns_list:
            result_dict['Roe_deducted'] = self.get_roe_deducted(time_info)
        if "Roe_exdiluted" in columns_list:
            result_dict['Roe_exdiluted'] = self.get_roe_exdiluted(time_info)
        if "Roa2" in columns_list:
            result_dict['Roa2'] = self.get_roa2(time_info)
        if "Roic" in columns_list:
            result_dict['Roic'] = self.get_roic(time_info)
        if "Optogr" in columns_list:
            result_dict['Optogr'] = self.get_optogr(time_info)
        if "Dupont_ebittosales" in columns_list:
            result_dict['Dupont_ebittosales'] = self.get_dupont_ebittosales(time_info)
        if "Dupont_roe" in columns_list:
            result_dict['Dupont_roe'] = self.get_dupont_roe(time_info)
        if "Grossprofitmargin_ttm3" in columns_list:
            result_dict['Grossprofitmargin_ttm3'] = self.get_grossprofitmargin_ttm3(time_info)
        if "Netprofitmargin_ttm3" in columns_list:
            result_dict['Netprofitmargin_ttm3'] = self.get_netprofitmargin_ttm3(time_info)
        if "Roa_ttm2" in columns_list:
            result_dict['Roa_ttm2'] = self.get_roa_ttm2(time_info)
        if "Netprofittoassets2" in columns_list:
            result_dict['Netprofittoassets2'] = self.get_netprofittoassets2(time_info)
        if "Roa2_ttm3" in columns_list:
            result_dict['Roa2_ttm3'] = self.get_roa2_ttm3(time_info)
        if "Ebittoassets_ttm" in columns_list:
            result_dict['Ebittoassets_ttm'] = self.get_ebittoassets_ttm(time_info)
        if "Profittogr_ttm3" in columns_list:
            result_dict['Profittogr_ttm3'] = self.get_profittogr_ttm3(time_info)
        if "Optogr_ttm3" in columns_list:
            result_dict['Optogr_ttm3'] = self.get_optogr_ttm3(time_info)
        if "Grossprofitmargin" in columns_list:
            result_dict['Grossprofitmargin'] = self.get_grossprofitmargin(time_info)
        if "Netprofitmargin" in columns_list:
            result_dict['Netprofitmargin'] = self.get_netprofitmargin(time_info)
        if "Roe" in columns_list:
            result_dict['Roe'] = self.get_roe(time_info)
        if "Roa" in columns_list:
            result_dict['Roa'] = self.get_roa(time_info)
        if "Dupont_nptosales" in columns_list:
            result_dict['Dupont_nptosales'] = self.get_dupont_nptosales(time_info)
        if "Ebitda" in columns_list:
            result_dict['Ebitda'] = self.get_ebitda(time_info)
        if "Roic_ttm" in columns_list:
            result_dict['Roic_ttm'] = self.get_roic_ttm(time_info)
        if "Roic_ttm2" in columns_list:
            result_dict['Roic_ttm2'] = self.get_roic_ttm2(time_info)
        if "Grossincome_ttm" in columns_list:
            result_dict['Grossincome_ttm'] = self.get_grossincome_ttm(time_info)
        if "Operatingincome_ttm" in columns_list:
            result_dict['Operatingincome_ttm'] = self.get_operatingincome_ttm(time_info)
        if "Netmargin_ttm" in columns_list:
            result_dict['Netmargin_ttm'] = self.get_netmargin_ttm(time_info)
        if "Profittoshareholders_ttm" in columns_list:
            result_dict['Profittoshareholders_ttm'] = self.get_profittoshareholders_ttm(time_info)
        if "Earningbeforetax_ttm" in columns_list:
            result_dict['Earningbeforetax_ttm'] = self.get_earningbeforetax_ttm(time_info)
        if "Earningbeforetax_ttm2" in columns_list:
            result_dict['Earningbeforetax_ttm2'] = self.get_earningbeforetax_ttm2(time_info)
        if "Operatingprofit_ttm" in columns_list:
            result_dict['Operatingprofit_ttm'] = self.get_operatingprofit_ttm(time_info)
        if "Abs" in columns_list:
            result_dict['Abs'] = self.get_abs(time_info)
        if "Egro" in columns_list:
            result_dict['Egro'] = self.get_egro(time_info)
        if "Sgro" in columns_list:
            result_dict['Sgro'] = self.get_sgro(time_info)
        if "Cetoe" in columns_list:
            result_dict['Cetoe'] = self.get_cetoe(time_info)
        if "Ebit" in columns_list:
            result_dict['Ebit'] = self.get_Ebit(time_info)
        return result_dict

    def get_roic1(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "ProfitToShareholders")
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ShareholderEquity")
        BankLoansAndOverdraft = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BankLoansAndOverdraft")
        FOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","FOtherLoan")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        NFOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "NFOtherLoan")
        ConBillAndBond = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ConBillAndBond")
        ShareholderEquity_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "ShareholderEquity")
        BankLoansAndOverdraft_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "BankLoansAndOverdraft")
        FOtherLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "FOtherLoan")
        LongtermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        NFOtherLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "NFOtherLoan")
        ConBillAndBond_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","ConBillAndBond")

        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        const_1 = BasicOperations(2)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        BankLoansAndOverdraft = BasicOperations(BankLoansAndOverdraft)
        FOtherLoan = BasicOperations(FOtherLoan)
        LongtermLoan = BasicOperations(LongtermLoan)
        NFOtherLoan = BasicOperations(NFOtherLoan)
        ConBillAndBond = BasicOperations(ConBillAndBond)
        ShareholderEquity_LYP = BasicOperations(ShareholderEquity_LYP)
        BankLoansAndOverdraft_LYP = BasicOperations(BankLoansAndOverdraft_LYP)
        FOtherLoan_LYP = BasicOperations(FOtherLoan_LYP)
        LongtermLoan_LYP = BasicOperations(LongtermLoan_LYP)
        NFOtherLoan_LYP = BasicOperations(NFOtherLoan_LYP)
        ConBillAndBond_LYP = BasicOperations(ConBillAndBond_LYP)
        const_100 = BasicOperations(100)
        result = ProfitToShareholders * const_1 / (ShareholderEquity + BankLoansAndOverdraft + FOtherLoan + LongtermLoan + NFOtherLoan + ConBillAndBond \
             + ShareholderEquity_LYP + BankLoansAndOverdraft_LYP + FOtherLoan_LYP + LongtermLoan_LYP \
             + NFOtherLoan_LYP + ConBillAndBond_LYP)*const_100

        return result.val

    def get_roe_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "ProfitToShareholders")
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ShareholderEquity")

        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        const_100 = BasicOperations(100)

        result = ProfitToShareholders / ShareholderEquity*const_100
        return result.val

    def get_ebitdatosales(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningBeforeTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "EarningBeforeTax")
        DepDividerSale = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "DepDividerSale")
        FInterestExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "FInterestExpense")
        FinancingCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "FinancingCost")
        OperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                      "OperatingIncome")
        OtherOperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                           "OtherOperatingIncome")


        NetInterestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                        "NetInterestIncome")
        FOtherTurnover = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "FOtherTurnover")
        CommissionIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                       "CommissionIncome")
        DividendIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "DividendIncome")
        ForeignExchangeIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "ForeignExchangeIncome")
        SecuExAndInvestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "SecuExAndInvestIncome")
        NetRentIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "NetRentIncome")


        EarningBeforeTax = BasicOperations(EarningBeforeTax)
        DepDividerSale = BasicOperations(DepDividerSale)
        OperatingIncome = BasicOperations(OperatingIncome)
        OtherOperatingIncome = BasicOperations(OtherOperatingIncome)
        FInterestExpense = BasicOperations(FInterestExpense)
        FinancingCost = BasicOperations(FinancingCost)

        NetInterestIncome = BasicOperations(NetInterestIncome)
        FOtherTurnover = BasicOperations(FOtherTurnover)
        CommissionIncome = BasicOperations(CommissionIncome)
        DividendIncome = BasicOperations(DividendIncome)
        ForeignExchangeIncome = BasicOperations(ForeignExchangeIncome)
        SecuExAndInvestIncome = BasicOperations(SecuExAndInvestIncome)
        NetRentIncome = BasicOperations(NetRentIncome)
        const_100 = BasicOperations(100)

        result = (EarningBeforeTax + DepDividerSale+ FInterestExpense+FinancingCost) / (OperatingIncome + OtherOperatingIncome + NetInterestIncome+ FOtherTurnover + CommissionIncome+ DividendIncome + ForeignExchangeIncome + SecuExAndInvestIncome + NetRentIncome)*const_100
        return result.val

    def get_roe_diluted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "ProfitToShareholders")
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ShareholderEquity")

        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        const_100 = BasicOperations(100)

        result = ProfitToShareholders / ShareholderEquity*const_100
        return result.val

    def get_finaexpensetogr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        FinancingCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "FinancingCost")
        FOperatingExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "FInterestExpense")
        OperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                      "OperatingIncome")
        OtherOperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                           "OtherOperatingIncome")

        NetInterestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                        "NetInterestIncome")
        FOtherTurnover = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "FOtherTurnover")
        CommissionIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                       "CommissionIncome")
        DividendIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "DividendIncome")
        ForeignExchangeIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "ForeignExchangeIncome")
        SecuExAndInvestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "SecuExAndInvestIncome")
        NetRentIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "NetRentIncome")

        FinancingCost = BasicOperations(FinancingCost)
        FOperatingExpense = BasicOperations(FOperatingExpense)
        OperatingIncome = BasicOperations(OperatingIncome)
        OtherOperatingIncome = BasicOperations(OtherOperatingIncome)

        NetInterestIncome = BasicOperations(NetInterestIncome)
        FOtherTurnover = BasicOperations(FOtherTurnover)
        CommissionIncome = BasicOperations(CommissionIncome)
        DividendIncome = BasicOperations(DividendIncome)
        ForeignExchangeIncome = BasicOperations(ForeignExchangeIncome)
        SecuExAndInvestIncome = BasicOperations(SecuExAndInvestIncome)
        NetRentIncome = BasicOperations(NetRentIncome)
        const_100 = BasicOperations(100)

        result = (FinancingCost + FOperatingExpense) / (OperatingIncome + OtherOperatingIncome + NetInterestIncome+ FOtherTurnover + CommissionIncome+ DividendIncome + ForeignExchangeIncome + SecuExAndInvestIncome + NetRentIncome)*const_100
        return result.val

    def get_roe_deducted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningAfterTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "EarningAfterTax")
        EndOrSpecProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                        "EndOrSpecProfit")
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "ShareholderEquity")
        ShareholderEquity_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                        "ShareholderEquity")

        EarningAfterTax = BasicOperations(EarningAfterTax)
        EndOrSpecProfit = BasicOperations(EndOrSpecProfit)
        const_1 = BasicOperations(2)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        ShareholderEquity_LYP = BasicOperations(ShareholderEquity_LYP)
        const_100 = BasicOperations(100)

        result = (EarningAfterTax - EndOrSpecProfit)*const_1 / (ShareholderEquity + ShareholderEquity_LYP)*const_100
        return result.val

    def get_roe_exdiluted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningAfterTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "EarningAfterTax")
        EndOrSpecProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                        "EndOrSpecProfit")
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "ShareholderEquity")

        EarningAfterTax = BasicOperations(EarningAfterTax)
        EndOrSpecProfit = BasicOperations(EndOrSpecProfit)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        const_100 = BasicOperations(100)

        result = (EarningAfterTax - EndOrSpecProfit) / (ShareholderEquity)*const_100
        return result.val

    def get_roa2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningAfterTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "EarningAfterTax")
        FinancingCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "FinancingCost")
        FInterestExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "FInterestExpense")

        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "TotalAssets")
        TotalAssets_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "TotalAssets")

        EarningAfterTax = BasicOperations(EarningAfterTax)
        FinancingCost = BasicOperations(FinancingCost)
        FInterestExpense = BasicOperations(FInterestExpense)

        const_1 = BasicOperations(2)
        TotalAssets = BasicOperations(TotalAssets)
        TotalAssets_LYP = BasicOperations(TotalAssets_LYP)
        const_100 = BasicOperations(100)

        result = (EarningAfterTax + FinancingCost + FInterestExpense)*const_1 / (TotalAssets + TotalAssets_LYP)*const_100
        return result.val


    def get_roic(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningAfterTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "EarningAfterTax")
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ShareholderEquity")
        BankLoansAndOverdraft = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BankLoansAndOverdraft")
        FOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","FOtherLoan")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        NFOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "NFOtherLoan")
        ConBillAndBond = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ConBillAndBond")
        ShareholderEquity_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "ShareholderEquity")
        BankLoansAndOverdraft_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "BankLoansAndOverdraft")
        FOtherLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "FOtherLoan")
        LongtermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        NFOtherLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "NFOtherLoan")
        ConBillAndBond_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","ConBillAndBond")

        EarningAfterTax = BasicOperations(EarningAfterTax)
        const_1 = BasicOperations(2)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        BankLoansAndOverdraft = BasicOperations(BankLoansAndOverdraft)
        FOtherLoan = BasicOperations(FOtherLoan)
        LongtermLoan = BasicOperations(LongtermLoan)
        NFOtherLoan = BasicOperations(NFOtherLoan)
        ConBillAndBond = BasicOperations(ConBillAndBond)
        ShareholderEquity_LYP = BasicOperations(ShareholderEquity_LYP)
        BankLoansAndOverdraft_LYP = BasicOperations(BankLoansAndOverdraft_LYP)
        FOtherLoan_LYP = BasicOperations(FOtherLoan_LYP)
        LongtermLoan_LYP = BasicOperations(LongtermLoan_LYP)
        NFOtherLoan_LYP = BasicOperations(NFOtherLoan_LYP)
        ConBillAndBond_LYP = BasicOperations(ConBillAndBond_LYP)
        const_100 = BasicOperations(100)
        result = EarningAfterTax * const_1 / (ShareholderEquity + BankLoansAndOverdraft + FOtherLoan + LongtermLoan + NFOtherLoan + ConBillAndBond \
             + ShareholderEquity_LYP + BankLoansAndOverdraft_LYP + FOtherLoan_LYP + LongtermLoan_LYP \
             + NFOtherLoan_LYP + ConBillAndBond_LYP)*const_100

        return result.val

    def get_optogr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
 #        OperProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "OperProfit")
        OperProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "OperateProfit")
        AfPrepareOpePayoff = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "AfPrepareOpePayoff")

        OperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                      "OperatingIncome")
        OtherOperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                           "OtherOperatingIncome")

        NetInterestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                        "NetInterestIncome")
        FOtherTurnover = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "FOtherTurnover")
        CommissionIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                       "CommissionIncome")
        DividendIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "DividendIncome")
        ForeignExchangeIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "ForeignExchangeIncome")
        SecuExAndInvestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "SecuExAndInvestIncome")
        NetRentIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "NetRentIncome")

        OperProfit = BasicOperations(OperProfit)
        AfPrepareOpePayoff = BasicOperations(AfPrepareOpePayoff)
        OperatingIncome = BasicOperations(OperatingIncome)
        OtherOperatingIncome = BasicOperations(OtherOperatingIncome)

        NetInterestIncome = BasicOperations(NetInterestIncome)
        FOtherTurnover = BasicOperations(FOtherTurnover)
        CommissionIncome = BasicOperations(CommissionIncome)
        DividendIncome = BasicOperations(DividendIncome)
        ForeignExchangeIncome = BasicOperations(ForeignExchangeIncome)
        SecuExAndInvestIncome = BasicOperations(SecuExAndInvestIncome)
        NetRentIncome = BasicOperations(NetRentIncome)
        const_100 = BasicOperations(100)
        result = (OperProfit + AfPrepareOpePayoff) / (OperatingIncome + OtherOperatingIncome + NetInterestIncome+ FOtherTurnover + CommissionIncome+ DividendIncome + ForeignExchangeIncome + SecuExAndInvestIncome + NetRentIncome)*const_100
        return result.val

    def get_dupont_ebittosales(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningBeforeTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "EarningBeforeTax")
        FinancingCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "FinancingCost")
        FInterestExpense = self.common_function_obj.get_data_point_num(stock_info,data_dict , time_info , "IC", "FInterestExpense")
        OperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                      "OperatingIncome")
        OtherOperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                           "OtherOperatingIncome")

        NetInterestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                        "NetInterestIncome")
        FOtherTurnover = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "FOtherTurnover")
        CommissionIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                       "CommissionIncome")
        DividendIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "DividendIncome")
        ForeignExchangeIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "ForeignExchangeIncome")
        SecuExAndInvestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "SecuExAndInvestIncome")
        NetRentIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "NetRentIncome")

        EarningBeforeTax = BasicOperations(EarningBeforeTax)
        FinancingCost = BasicOperations(FinancingCost)
        FInterestExpense = BasicOperations(FInterestExpense)
        OperatingIncome = BasicOperations(OperatingIncome)
        OtherOperatingIncome = BasicOperations(OtherOperatingIncome)

        NetInterestIncome = BasicOperations(NetInterestIncome)
        FOtherTurnover = BasicOperations(FOtherTurnover)
        CommissionIncome = BasicOperations(CommissionIncome)
        DividendIncome = BasicOperations(DividendIncome)
        ForeignExchangeIncome = BasicOperations(ForeignExchangeIncome)
        SecuExAndInvestIncome = BasicOperations(SecuExAndInvestIncome)
        NetRentIncome = BasicOperations(NetRentIncome)
        const_100 = BasicOperations(100)

        result = (EarningBeforeTax + FinancingCost + FInterestExpense) / (OperatingIncome + OtherOperatingIncome + NetInterestIncome+ FOtherTurnover + CommissionIncome+ DividendIncome + ForeignExchangeIncome + SecuExAndInvestIncome + NetRentIncome)*const_100
        return result.val

    def get_dupont_roe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "ProfitToShareholders")
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ShareholderEquity")
        ShareholderEquity_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "ShareholderEquity")
        ShareholderEquity_PRE = self.common_function_obj.get_data_point_pre(stock_info, data_dict, time_info, "BL", "ShareholderEquity")

        # 求平均值
        # mean_ShareholderEquity = None
        check_lyp_list = pd.isnull(np.array([ShareholderEquity, ShareholderEquity_LYP], dtype=float))
        check_lyp_num = np.count_nonzero(check_lyp_list == False)
        mean_lyp_ShareholderEquity = (BasicOperations(ShareholderEquity) + BasicOperations(ShareholderEquity_LYP))/BasicOperations(check_lyp_num)



        check_pre_list = pd.isnull(np.array([ShareholderEquity, ShareholderEquity_PRE], dtype=float))
        check_pre_num = np.count_nonzero(check_pre_list == False)
        mean_pre_ShareholderEquity = (BasicOperations(ShareholderEquity) + BasicOperations(ShareholderEquity_PRE)) / BasicOperations(check_pre_num)

        # check_lyp_lyr = pd.isnull(np.array([mean_lyp_ShareholderEquity, mean_lyr_ShareholderEquity], dtype=float))

        lyp_distinct = (BasicOperations(mean_lyp_ShareholderEquity) - BasicOperations(ShareholderEquity)).abs_value()
        lyr_distinct = (BasicOperations(mean_pre_ShareholderEquity) - BasicOperations(ShareholderEquity)).abs_value()

        if (lyr_distinct-lyp_distinct).val <0:
            mean_ShareholderEquity=mean_pre_ShareholderEquity
        else:
            mean_ShareholderEquity = mean_lyp_ShareholderEquity

        # mean_ShareholderEquity = (BasicOperations(ShareholderEquity) + BasicOperations(ShareholderEquity_LYR)) / BasicOperations(check_lyr_num)

        const_100 = BasicOperations(100)
        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        result = ProfitToShareholders/ mean_ShareholderEquity * const_100
        '''
        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        const_1 = BasicOperations(2)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        ShareholderEquity_LYP = BasicOperations(ShareholderEquity_LYP)
        const_100 = BasicOperations(100)

        result = (ProfitToShareholders*const_1) / (ShareholderEquity + ShareholderEquity_LYP)*const_100
        '''
        return result.val

    def get_grossprofitmargin_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        indus = self.common_function_obj.get_hs_industry(stock_info, data_dict, time_info, 'IndustryNum')
        if indus in set([27]):
            return None
        else :
            OperExpenses_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                          "OperExpenses")
            SalesCost_TMM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                          "SalesCost")
            OperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                              "OperatingIncome")
            OtherOperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info,
                                                                                   "IC",
                                                                                   "OtherOperatingIncome")

            NetInterestIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                                "NetInterestIncome")
            FOtherTurnover_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                             "FOtherTurnover")
            CommissionIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                               "CommissionIncome")
            DividendIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                             "DividendIncome")
            ForeignExchangeIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info,
                                                                                    "IC",
                                                                                    "ForeignExchangeIncome")
            SecuExAndInvestIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info,
                                                                                    "IC",
                                                                                    "SecuExAndInvestIncome")
            NetRentIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                            "NetRentIncome")

            OperExpenses_TTM = BasicOperations(OperExpenses_TTM)
            SalesCost_TMM = BasicOperations(SalesCost_TMM)
            OperatingIncome_TTM = BasicOperations(OperatingIncome_TTM)
            OtherOperatingIncome_TTM = BasicOperations(OtherOperatingIncome_TTM)

            NetInterestIncome_TTM = BasicOperations(NetInterestIncome_TTM)
            FOtherTurnover_TTM = BasicOperations(FOtherTurnover_TTM)
            CommissionIncome_TTM = BasicOperations(CommissionIncome_TTM)
            DividendIncome_TTM = BasicOperations(DividendIncome_TTM)
            ForeignExchangeIncome_TTM = BasicOperations(ForeignExchangeIncome_TTM)
            SecuExAndInvestIncome_TTM = BasicOperations(SecuExAndInvestIncome_TTM)
            NetRentIncome_TTM = BasicOperations(NetRentIncome_TTM)
            const_100 = BasicOperations(100)
            GrossIncome = OperatingIncome_TTM + OtherOperatingIncome_TTM - OperExpenses_TTM - SalesCost_TMM
            if GrossIncome.val == (OperatingIncome_TTM + OtherOperatingIncome_TTM).val:
                return None
            else :
                result = GrossIncome / (
                        OperatingIncome_TTM + OtherOperatingIncome_TTM + NetInterestIncome_TTM + FOtherTurnover_TTM + CommissionIncome_TTM + DividendIncome_TTM + ForeignExchangeIncome_TTM + SecuExAndInvestIncome_TTM + NetRentIncome_TTM) * const_100
                return result.val



    def get_netprofitmargin_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningAfterTax_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                          "EarningAfterTax")
        OperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                          "OperatingIncome")
        OtherOperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info,
                                                                               "IC",
                                                                               "OtherOperatingIncome")

        NetInterestIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                            "NetInterestIncome")
        FOtherTurnover_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                         "FOtherTurnover")
        CommissionIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                           "CommissionIncome")
        DividendIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                         "DividendIncome")
        ForeignExchangeIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info,
                                                                                "IC",
                                                                                "ForeignExchangeIncome")
        SecuExAndInvestIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info,
                                                                                "IC",
                                                                                "SecuExAndInvestIncome")
        NetRentIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                        "NetRentIncome")

        EarningAfterTax_TTM = BasicOperations(EarningAfterTax_TTM)
        OperatingIncome_TTM = BasicOperations(OperatingIncome_TTM)
        OtherOperatingIncome_TTM = BasicOperations(OtherOperatingIncome_TTM)

        NetInterestIncome_TTM = BasicOperations(NetInterestIncome_TTM)
        FOtherTurnover_TTM = BasicOperations(FOtherTurnover_TTM)
        CommissionIncome_TTM = BasicOperations(CommissionIncome_TTM)
        DividendIncome_TTM = BasicOperations(DividendIncome_TTM)
        ForeignExchangeIncome_TTM = BasicOperations(ForeignExchangeIncome_TTM)
        SecuExAndInvestIncome_TTM = BasicOperations(SecuExAndInvestIncome_TTM)
        NetRentIncome_TTM = BasicOperations(NetRentIncome_TTM)
        const_100 = BasicOperations(100)

        result = (EarningAfterTax_TTM) / (
                OperatingIncome_TTM + OtherOperatingIncome_TTM + NetInterestIncome_TTM + FOtherTurnover_TTM + CommissionIncome_TTM + DividendIncome_TTM + ForeignExchangeIncome_TTM + SecuExAndInvestIncome_TTM + NetRentIncome_TTM) * const_100
        return result.val



    def get_roa_ttm2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningAfterTax_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "EarningAfterTax")
        InterestIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "InterestIncome")
        FinancingCost_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "FinancingCost")
        Tax_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","Tax")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")

        EarningAfterTax_TTM = BasicOperations(EarningAfterTax_TTM)
        const_1 = BasicOperations(1)
        InterestIncome_TTM= BasicOperations(InterestIncome_TTM)
        FinancingCost_TTM = BasicOperations(FinancingCost_TTM)
        Tax_TTM = BasicOperations(Tax_TTM)
        TotalAssets = BasicOperations(TotalAssets)
        const_100 = BasicOperations(100)


        result = (EarningAfterTax_TTM)/TotalAssets*const_100
        return result.val

    def get_netprofittoassets2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","ProfitToShareholders")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")

        ProfitToShareholders_TTM = BasicOperations(ProfitToShareholders_TTM)
        TotalAssets = BasicOperations(TotalAssets)
        const_100 = BasicOperations(100)

        result = ProfitToShareholders_TTM / TotalAssets*const_100
        return result.val

    def get_roa2_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningBeforeTax_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","EarningBeforeTax")
        FinancingCost_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                    "FinancingCost")
        FInterestExpense_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                    "FInterestExpense")

        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")

        EarningBeforeTax_TTM = BasicOperations(EarningBeforeTax_TTM)
        FinancingCost_TTM = BasicOperations(FinancingCost_TTM)
        TotalAssets = BasicOperations(TotalAssets)
        FInterestExpense_TTM = BasicOperations(FInterestExpense_TTM)
        const_100 = BasicOperations(100)

        result = (EarningBeforeTax_TTM + FinancingCost_TTM + FInterestExpense_TTM) / TotalAssets*const_100
        return result.val

    def get_ebittoassets_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningBeforeTax_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","EarningBeforeTax")
        FinancingCost_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                    "FinancingCost")
        FInterestExpense_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                    "FInterestExpense")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")

        EarningBeforeTax_TTM = BasicOperations(EarningBeforeTax_TTM)
        FinancingCost_TTM = BasicOperations(FinancingCost_TTM)
        TotalAssets = BasicOperations(TotalAssets)
        FInterestExpense_TTM = BasicOperations(FInterestExpense_TTM)
        const_100 = BasicOperations(100)

        result = (EarningBeforeTax_TTM + FinancingCost_TTM + FInterestExpense_TTM) / TotalAssets*const_100
        return result.val

    def get_profittogr_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningAfterTax_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "EarningAfterTax")
        OperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                          "OperatingIncome")
        OtherOperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                               "OtherOperatingIncome")

        NetInterestIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                            "NetInterestIncome")
        FOtherTurnover_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                         "FOtherTurnover")
        CommissionIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                           "CommissionIncome")
        DividendIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                         "DividendIncome")
        ForeignExchangeIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                                "ForeignExchangeIncome")
        SecuExAndInvestIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                                "SecuExAndInvestIncome")
        NetRentIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                        "NetRentIncome")

        EarningAfterTax_TTM = BasicOperations(EarningAfterTax_TTM)
        OperatingIncome_TTM = BasicOperations(OperatingIncome_TTM)
        OtherOperatingIncome_TTM = BasicOperations(OtherOperatingIncome_TTM)

        NetInterestIncome_TTM = BasicOperations(NetInterestIncome_TTM)
        FOtherTurnover_TTM = BasicOperations(FOtherTurnover_TTM)
        CommissionIncome_TTM = BasicOperations(CommissionIncome_TTM)
        DividendIncome_TTM = BasicOperations(DividendIncome_TTM)
        ForeignExchangeIncome_TTM = BasicOperations(ForeignExchangeIncome_TTM)
        SecuExAndInvestIncome_TTM = BasicOperations(SecuExAndInvestIncome_TTM)
        NetRentIncome_TTM = BasicOperations(NetRentIncome_TTM)

        const_100 = BasicOperations(100)

        result = (EarningAfterTax_TTM) / (OperatingIncome_TTM + OtherOperatingIncome_TTM + NetInterestIncome_TTM+ FOtherTurnover_TTM + CommissionIncome_TTM+ DividendIncome_TTM + ForeignExchangeIncome_TTM + SecuExAndInvestIncome_TTM + NetRentIncome_TTM)*const_100
        return result.val

    def get_optogr_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        # OperProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "OperProfit")
        OperProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                 "OperateProfit")
        AfPrepareOpePayoff_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","AfPrepareOpePayoff")

        OperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                          "OperatingIncome")
        OtherOperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                               "OtherOperatingIncome")

        NetInterestIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                            "NetInterestIncome")
        FOtherTurnover_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                         "FOtherTurnover")
        CommissionIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                           "CommissionIncome")
        DividendIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                         "DividendIncome")
        ForeignExchangeIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                                "ForeignExchangeIncome")
        SecuExAndInvestIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                                "SecuExAndInvestIncome")
        NetRentIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                        "NetRentIncome")

        OperProfit_TTM = BasicOperations(OperProfit_TTM)
        AfPrepareOpePayoff_TTM = BasicOperations(AfPrepareOpePayoff_TTM)
        OperatingIncome_TTM = BasicOperations(OperatingIncome_TTM)
        OtherOperatingIncome_TTM = BasicOperations(OtherOperatingIncome_TTM)

        NetInterestIncome_TTM = BasicOperations(NetInterestIncome_TTM)
        FOtherTurnover_TTM = BasicOperations(FOtherTurnover_TTM)
        CommissionIncome_TTM = BasicOperations(CommissionIncome_TTM)
        DividendIncome_TTM = BasicOperations(DividendIncome_TTM)
        ForeignExchangeIncome_TTM = BasicOperations(ForeignExchangeIncome_TTM)
        SecuExAndInvestIncome_TTM = BasicOperations(SecuExAndInvestIncome_TTM)
        NetRentIncome_TTM = BasicOperations(NetRentIncome_TTM)
        const_100 = BasicOperations(100)

        result = (OperProfit_TTM + AfPrepareOpePayoff_TTM) / (OperatingIncome_TTM + OtherOperatingIncome_TTM + NetInterestIncome_TTM+ FOtherTurnover_TTM + CommissionIncome_TTM+ DividendIncome_TTM + ForeignExchangeIncome_TTM + SecuExAndInvestIncome_TTM + NetRentIncome_TTM)*const_100
        return result.val

    def get_grossprofitmargin(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        indus = self.common_function_obj.get_hs_industry(stock_info, data_dict, time_info, 'IndustryNum')
        if indus in set([27]):
            return None
        else :
            GrossIncomeRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                           "GrossIncomeRatio")
            if not isinstance(GrossIncomeRatio, np.float64):
                return None
            elif np.float64(GrossIncomeRatio) == np.float64(100):
                return None
            else:
                result = GrossIncomeRatio
                return result


    def get_netprofitmargin(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfitRatio= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfitRatio")

        result = NetProfitRatio
        return result


    def get_roe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROEWeighted= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "ROEWeighted")

        result = ROEWeighted
        return result

    def get_roa(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROA= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "ROA")

        result = ROA
        return result

    def get_dupont_nptosales(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfitRatio= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfitRatio")

        result = NetProfitRatio
        return result

    def get_ebitda(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningBeforeTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC","EarningBeforeTax")
        DepDividerSale = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "DepDividerSale")
        FInterestExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "FInterestExpense")
        FinancingCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "FinancingCost")

        EarningBeforeTax = BasicOperations(EarningBeforeTax)
        DepDividerSale = BasicOperations(DepDividerSale)
        FInterestExpense = BasicOperations(FInterestExpense)
        FinancingCost = BasicOperations(FinancingCost)

        result = EarningBeforeTax + DepDividerSale + FInterestExpense + FinancingCost
        return result.val

    def get_roic_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningAfterTax_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "EarningAfterTax")
        InterestIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "InterestIncome")
        FinancingCost_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "FinancingCost")
        Tax_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","Tax")
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ShareholderEquity")
        BankLoansAndOverdraft = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BankLoansAndOverdraft")
        FOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","FOtherLoan")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        NFOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "NFOtherLoan")
        ConBillAndBond = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ConBillAndBond")


        EarningAfterTax_TTM = BasicOperations(EarningAfterTax_TTM)
        InterestIncome_TTM= BasicOperations(InterestIncome_TTM)
        FinancingCost_TTM = BasicOperations(FinancingCost_TTM)
        Tax_TTM = BasicOperations(Tax_TTM)
        const_1 = BasicOperations(1)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        BankLoansAndOverdraft = BasicOperations(BankLoansAndOverdraft)
        FOtherLoan = BasicOperations(FOtherLoan)
        LongtermLoan = BasicOperations(LongtermLoan)
        NFOtherLoan = BasicOperations(NFOtherLoan)
        ConBillAndBond = BasicOperations(ConBillAndBond)
        const_100 = BasicOperations(100)

        result = (EarningAfterTax_TTM + InterestIncome_TTM + FinancingCost_TTM)*(const_1 - Tax_TTM/EarningAfterTax_TTM)/ (ShareholderEquity + BankLoansAndOverdraft + FOtherLoan + LongtermLoan + NFOtherLoan + ConBillAndBond)
        return result.val

    def get_roic_ttm2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "ProfitToShareholders")
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ShareholderEquity")
        BankLoansAndOverdraft = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BankLoansAndOverdraft")
        FOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","FOtherLoan")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        NFOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "NFOtherLoan")
        ConBillAndBond = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ConBillAndBond")

        ProfitToShareholders_TTM = BasicOperations(ProfitToShareholders_TTM)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        BankLoansAndOverdraft = BasicOperations(BankLoansAndOverdraft)
        FOtherLoan = BasicOperations(FOtherLoan)
        LongtermLoan = BasicOperations(LongtermLoan)
        NFOtherLoan = BasicOperations(NFOtherLoan)
        ConBillAndBond = BasicOperations(ConBillAndBond)
        const_100 = BasicOperations(100)

        result = (ProfitToShareholders_TTM)/ (ShareholderEquity + BankLoansAndOverdraft + FOtherLoan + LongtermLoan + NFOtherLoan + ConBillAndBond)
        return result.val

    def get_grossincome_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        indus = self.common_function_obj.get_hs_industry(stock_info, data_dict, time_info, 'IndustryNum')
        if indus in set([27]):
            return None
        else :
            GrossIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                          "GrossIncome")
            if GrossIncome_TTM != None and GrossIncome_TTM != 0:
                result = GrossIncome_TTM
                return result
            else:
                OperExpenses_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                               "OperExpenses")
                SalesCost_TMM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                            "SalesCost")
                OperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info,
                                                                                  "IC",
                                                                                  "OperatingIncome")
                OtherOperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info,
                                                                                       "IC",
                                                                                       "OtherOperatingIncome")
                OperatingIncome_TTM = BasicOperations(OperatingIncome_TTM)
                OtherOperatingIncome_TTM = BasicOperations(OtherOperatingIncome_TTM)
                SalesCost_TMM = BasicOperations(SalesCost_TMM)
                OperExpenses_TTM = BasicOperations(OperExpenses_TTM)
                result = OperatingIncome_TTM + OtherOperatingIncome_TTM - SalesCost_TMM - OperExpenses_TTM
                if result.val == (OperatingIncome_TTM + OtherOperatingIncome_TTM).val:
                    return None
                else :
                    return result.val


    def get_operatingincome_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                          "OperatingIncome")
        OtherOperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                               "OtherOperatingIncome")

        NetInterestIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                            "NetInterestIncome")
        FOtherTurnover_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                         "FOtherTurnover")
        CommissionIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                           "CommissionIncome")
        DividendIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                         "DividendIncome")
        ForeignExchangeIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                                "ForeignExchangeIncome")
        SecuExAndInvestIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                                "SecuExAndInvestIncome")
        NetRentIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                        "NetRentIncome")

        OperatingIncome_TTM = BasicOperations(OperatingIncome_TTM)
        OtherOperatingIncome_TTM = BasicOperations(OtherOperatingIncome_TTM)

        NetInterestIncome_TTM = BasicOperations(NetInterestIncome_TTM)
        FOtherTurnover_TTM = BasicOperations(FOtherTurnover_TTM)
        CommissionIncome_TTM = BasicOperations(CommissionIncome_TTM)
        DividendIncome_TTM = BasicOperations(DividendIncome_TTM)
        ForeignExchangeIncome_TTM = BasicOperations(ForeignExchangeIncome_TTM)
        SecuExAndInvestIncome_TTM = BasicOperations(SecuExAndInvestIncome_TTM)
        NetRentIncome_TTM = BasicOperations(NetRentIncome_TTM)

        result = (OperatingIncome_TTM + OtherOperatingIncome_TTM + NetInterestIncome_TTM+ FOtherTurnover_TTM + CommissionIncome_TTM+ DividendIncome_TTM + ForeignExchangeIncome_TTM + SecuExAndInvestIncome_TTM + NetRentIncome_TTM)
        return result.val

    def get_netmargin_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningAfterTax_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "EarningAfterTax")

        result = EarningAfterTax_TTM
        return result

    def get_profittoshareholders_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "ProfitToShareholders")

        result = ProfitToShareholders_TTM
        return result

    def get_earningbeforetax_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningBeforeTax_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "EarningBeforeTax")

        result = EarningBeforeTax_TTM
        return result

    def get_earningbeforetax_ttm2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningBeforeTax_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","EarningBeforeTax")
        FinancingCost_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","FinancingCost")
        FInterestExpense_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                        "FInterestExpense")
        EarningBeforeTax_TTM = BasicOperations(EarningBeforeTax_TTM)
        FinancingCost_TTM = BasicOperations(FinancingCost_TTM)
        FInterestExpense_TTM = BasicOperations(FInterestExpense_TTM)

        result = (EarningBeforeTax_TTM + FinancingCost_TTM + FInterestExpense_TTM)
        return result.val

    def get_operatingprofit_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        # OperProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "OperProfit")
        # AfPrepareOpePayoff_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","AfPrepareOpePayoff")
        # FOperatingExpense_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","FOperatingExpense")

        OperProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                 "OperateProfit")
        AfPrepareOpePayoff_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","AfPrepareOpePayoff")



        OperProfit_TTM = BasicOperations(OperProfit_TTM)
        AfPrepareOpePayoff_TTM = BasicOperations(AfPrepareOpePayoff_TTM)

        result = (OperProfit_TTM + AfPrepareOpePayoff_TTM)
        return result.val


    def get_abs(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        TotalCurrentAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","TotalCurrentAssets")
        Cash = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "Cash")
        CashHoldings = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "CashHoldings")
        TotalCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","TotalCurrentLiability")
        NotesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "NotesPayable")
        BankLoansAndOverdraft = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BankLoansAndOverdraft")
        FOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "FOtherLoan")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")
        DepDividerSale = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "DepDividerSale")

        TotalCurrentAssets_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalCurrentAssets")
        Cash_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "Cash")
        CashHoldings_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "CashHoldings")
        TotalCurrentLiability_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",  "TotalCurrentLiability")
        NotesPayable_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","NotesPayable")
        BankLoansAndOverdraft_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","BankLoansAndOverdraft")
        FOtherLoan_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","FOtherLoan")

        TotalCurrentAssets = BasicOperations(TotalCurrentAssets)
        Cash = BasicOperations(Cash)
        CashHoldings = BasicOperations(CashHoldings)
        TotalCurrentLiability = BasicOperations(TotalCurrentLiability)
        NotesPayable = BasicOperations(NotesPayable)
        BankLoansAndOverdraft = BasicOperations(BankLoansAndOverdraft)
        FOtherLoan = BasicOperations(FOtherLoan)
        TotalAssets = BasicOperations(TotalAssets)
        DepDividerSale = BasicOperations(DepDividerSale)
        TotalCurrentAssets_lyp = BasicOperations(TotalCurrentAssets_lyp)
        Cash_lyp = BasicOperations(Cash_lyp)
        CashHoldings_lyp = BasicOperations(CashHoldings_lyp)
        TotalCurrentLiability_lyp = BasicOperations(TotalCurrentLiability_lyp)
        NotesPayable_lyp = BasicOperations(NotesPayable_lyp)
        BankLoansAndOverdraft_lyp = BasicOperations(BankLoansAndOverdraft_lyp)
        FOtherLoan_lyp = BasicOperations(FOtherLoan_lyp)

        result = ((TotalCurrentAssets - TotalCurrentAssets_lyp) - ((Cash + CashHoldings) - (Cash_lyp + CashHoldings_lyp)) - (TotalCurrentLiability - TotalCurrentLiability_lyp) + ((NotesPayable + BankLoansAndOverdraft + FOtherLoan) - (NotesPayable_lyp + BankLoansAndOverdraft_lyp + FOtherLoan_lyp)) - DepDividerSale) / TotalAssets
        return result.val

    def get_egro(self, time_info):
        import statsmodels.api as sm
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        BasicEPS_list = []
        Year = time_info['EndDate'].year
        Year_list = list(np.arange(Year, Year - 5, -1))
        for i in range(5):
            time_info_temp = time_info.copy()
            end_time = time_info_temp['EndDate']
            end_time_year_ago =  end_time - pd.DateOffset(years=i)
            time_info_temp['EndDate'] = end_time_year_ago
            BasicEPS = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info_temp, "MI", "BasicEPS", num = 1)
            BasicEPS_list.append(BasicEPS)
        BasicEPS_list_notnone = [item for item in BasicEPS_list if item != None and str(item) != 'nan']
        if not BasicEPS_list_notnone:
            return None
        else:
            x = sm.add_constant(Year_list)
            y = BasicEPS_list
            model = sm.OLS(y, x, missing='drop')
            reg = model.fit()
            beta = reg.params[1]
            return beta / np.nanmean(BasicEPS_list_notnone)

    def get_sgro(self, time_info):
        import statsmodels.api as sm
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        MainIncomePS_list = []
        Year = time_info['EndDate'].year
        Year_list = list(np.arange(Year, Year - 5, -1))
        for i in range(5):
            time_info_temp = time_info.copy()
            end_time = time_info_temp['EndDate']
            end_time_year_ago = end_time - pd.DateOffset(years=i)
            time_info_temp['EndDate'] = end_time_year_ago
            MainIncomePS = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info_temp, "MI", "MainIncomePS", num=1)
            MainIncomePS_list.append(MainIncomePS)
        MainIncomePS_list_notnone = [item for item in MainIncomePS_list if item != None and str(item) != 'nan']
        if not MainIncomePS_list_notnone:
            return None
        else:
            x = sm.add_constant(Year_list)
            y = MainIncomePS_list
            model = sm.OLS(y, x, missing='drop')
            reg = model.fit()
            beta = reg.params[1]
            return beta / np.nanmean(MainIncomePS_list_notnone)


    def get_cetoe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetCashFromOperating = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "CS", "NetCashFromOperating")
        EarningAfterTax = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "EarningAfterTax")
        NetCashFromOperating = BasicOperations(NetCashFromOperating)
        EarningAfterTax = BasicOperations(EarningAfterTax)
        result = NetCashFromOperating/EarningAfterTax
        return result.val

    def get_Ebit(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningBeforeTax = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","EarningBeforeTax")
        FInterestExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "FInterestExpense")
        FinancingCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "FinancingCost")

        EarningBeforeTax = BasicOperations(EarningBeforeTax)
        FInterestExpense = BasicOperations(FInterestExpense)
        FinancingCost = BasicOperations(FinancingCost)

        result = EarningBeforeTax + FInterestExpense + FinancingCost
        return result.val