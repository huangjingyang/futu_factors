#-*- coding: utf-8 -*-

# 市场表现因子

from module.process.fundamental.basal_factor import *
import math


class FactorMarketPerformance(BasalFactor):
    __name__ = "MarketPerformance"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Ebitps2" in columns_list:
            result_dict['Ebitps2'] = self.get_ebitps2(time_info)
        if "Eps_adjust2" in columns_list:
            result_dict['Eps_adjust2'] = self.get_eps_adjust2(time_info)
        if "Eps_diluted3" in columns_list:
            result_dict['Eps_diluted3'] = self.get_eps_diluted3(time_info)
        if "Fcffps2" in columns_list:
            result_dict['Fcffps2'] = self.get_fcffps2(time_info)
        if "Fcfeps2" in columns_list:
            result_dict['Fcfeps2'] = self.get_fcfeps2(time_info)
        if "Eps_basic" in columns_list:
            result_dict['Eps_basic'] = self.get_eps_basic(time_info)
        if "Eps_diluted" in columns_list:
            result_dict['Eps_diluted'] = self.get_eps_diluted(time_info)
        if "Bps" in columns_list:
            result_dict['Bps'] = self.get_bps(time_info)
        if "Ocfps" in columns_list:
            result_dict['Ocfps'] = self.get_ocfps(time_info)
        if "Cfps" in columns_list:
            result_dict['Cfps'] = self.get_cfps(time_info)
        if "Orps" in columns_list:
            result_dict['Orps'] = self.get_orps(time_info)
        return result_dict

    def get_ebitps2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningBeforeTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "EarningBeforeTax")
        FInterestExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "FInterestExpense")
        FinancingCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "FinancingCost")
        # ShareCapital = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "FI",
        #                                                            "ShareCapital")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        EarningBeforeTax = BasicOperations(EarningBeforeTax)
        FInterestExpense = BasicOperations(FInterestExpense)
        FinancingCost = BasicOperations(FinancingCost)
        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)

        result = (EarningBeforeTax + FInterestExpense+FinancingCost) / (ListedShares + NotHKShares)
        return result.val

    def get_eps_adjust2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "ProfitToShareholders")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)

        result = ProfitToShareholders/(ListedShares + NotHKShares)
        return result.val

    def get_eps_diluted3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "ProfitToShareholders")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)

        result = ProfitToShareholders/ (ListedShares + NotHKShares)
        return result.val

    def get_fcffps2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "ProfitToShareholders")
        DevalAndAccBadDebt = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                  "DevalAndAccBadDebt")
        FDevaAndAccruedBadDebt = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "FDevaAndAccruedBadDebt")
        DepDividerSale = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                  "DepDividerSale")
        FinancingCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "FinancingCost")
        FInterestExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                  "FInterestExpense")
        Tax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "Tax")
        EarningBeforeTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                  "EarningBeforeTax")

        FixedAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                 "FixedAssets")
        WorkshopAndEquipment = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                  "WorkshopAndEquipment")
        ConstruInProcess = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                 "ConstruInProcess")
        FixedAssets_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                  "FixedAssets")
        WorkshopAndEquipment_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                  "WorkshopAndEquipment")
        ConstruInProcess_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                  "ConstruInProcess")

        TotalCurrentAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                 "TotalCurrentAssets")
        TotalCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                  "TotalCurrentLiability")

        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        DevalAndAccBadDebt = BasicOperations(DevalAndAccBadDebt)
        FDevaAndAccruedBadDebt = BasicOperations(FDevaAndAccruedBadDebt)
        DepDividerSale = BasicOperations(DepDividerSale)
        FinancingCost = BasicOperations(FinancingCost)
        FInterestExpense = BasicOperations(FInterestExpense)
        Tax = BasicOperations(Tax)
        EarningBeforeTax = BasicOperations(EarningBeforeTax)
        FixedAssets = BasicOperations(FixedAssets)
        WorkshopAndEquipment = BasicOperations(WorkshopAndEquipment)
        ConstruInProcess = BasicOperations(ConstruInProcess)
        FixedAssets_LYP = BasicOperations(FixedAssets_LYP)
        WorkshopAndEquipment_LYP = BasicOperations(WorkshopAndEquipment_LYP)
        ConstruInProcess_LYP = BasicOperations(ConstruInProcess_LYP)
        TotalCurrentAssets = BasicOperations(TotalCurrentAssets)
        TotalCurrentLiability = BasicOperations(TotalCurrentLiability)
        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)
        const_1 = BasicOperations(1)

        result = (ProfitToShareholders + DevalAndAccBadDebt + FDevaAndAccruedBadDebt + DepDividerSale + (FinancingCost + FInterestExpense) * (const_1-Tax / EarningBeforeTax)-(FixedAssets+WorkshopAndEquipment+ConstruInProcess-FixedAssets_LYP-WorkshopAndEquipment_LYP-ConstruInProcess_LYP+DepDividerSale)-(TotalCurrentAssets-TotalCurrentLiability))/(ListedShares + NotHKShares)

        return result.val

    def get_fcfeps2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningBeforeTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "EarningBeforeTax")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        EarningBeforeTax = BasicOperations(EarningBeforeTax)
        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)

        result = EarningBeforeTax/(ListedShares + NotHKShares)
        return result.val

    def get_eps_basic(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        BasicEPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                   "EPSBasic")
        result = BasicEPS
        return result

    def get_eps_diluted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        DilutedEPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                   "EPS")
        result = DilutedEPS
        return result

    def get_bps(self, time_info):
        result = None
        '''
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetAssetPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "NetAssetPS")
        result = NetAssetPS
        '''
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "ShareholderEquity")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")
        ShareholderEquity = BasicOperations(ShareholderEquity)
        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)
        result = ShareholderEquity /(ListedShares + NotHKShares)
        return result.val


    def get_ocfps(self, time_info):
        result = None
        '''
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperCashFlowPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "OperCashFlowPS")
        result = OperCashFlowPS
        return result
        '''
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetCashFromOperating = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CS",
                                                                   "NetCashFromOperating")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")
        NetCashFromOperating = BasicOperations(NetCashFromOperating)
        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)
        result = NetCashFromOperating/(ListedShares + NotHKShares)
        return result.val



    def get_cfps(self, time_info):
        result = None
        '''
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        CashFlowPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "CashFlowPS")
        result = CashFlowPS
        return result
        '''
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetCash = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CS",
                                                                   "NetCash")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")
        NetCash = BasicOperations(NetCash)
        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)
        result = NetCash/(ListedShares + NotHKShares)
        return result.val

    def get_orps(self, time_info):
        result = None
        '''
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        MainIncomePS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "MainIncomePS")
        result = MainIncomePS
        return result
        '''
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                      "OperatingIncome")
        OtherOperatingIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                           "OtherOperatingIncome")

        NetInterestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                        "NetInterestIncome")
        FOtherTurnover = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "FOtherTurnover")
        CommissionIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                       "CommissionIncome")
        DividendIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                     "DividendIncome")
        ForeignExchangeIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "ForeignExchangeIncome")
        SecuExAndInvestIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "SecuExAndInvestIncome")
        NetRentIncome = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "NetRentIncome")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        OperatingIncome = BasicOperations(OperatingIncome)
        OtherOperatingIncome = BasicOperations(OtherOperatingIncome)
        NetInterestIncome = BasicOperations(NetInterestIncome)
        FOtherTurnover = BasicOperations(FOtherTurnover)
        CommissionIncome = BasicOperations(CommissionIncome)
        DividendIncome = BasicOperations(DividendIncome)
        ForeignExchangeIncome = BasicOperations(ForeignExchangeIncome)
        SecuExAndInvestIncome = BasicOperations(SecuExAndInvestIncome)
        NetRentIncome = BasicOperations(NetRentIncome)
        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)
        result = (OperatingIncome + OtherOperatingIncome + NetInterestIncome+ FOtherTurnover + CommissionIncome+
                  DividendIncome + ForeignExchangeIncome + SecuExAndInvestIncome + NetRentIncome)/(ListedShares + NotHKShares)
        return result.val
