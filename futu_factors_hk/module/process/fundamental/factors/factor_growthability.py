#-*- coding: utf-8 -*-

# 成长能力因子
from module.process.fundamental.basal_factor import *
import math

class FactorGrowthability(BasalFactor):
    __name__ = "Growthability"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Growth_ocf" in columns_list:
            result_dict['Growth_ocf'] = self.get_growth_ocf(time_info)
        if "Yoyocfps" in columns_list:
            result_dict['Yoyocfps'] = self.get_yoyocfps(time_info)
        if "Yoy_or" in columns_list:
            result_dict['Yoy_or'] = self.get_yoy_or(time_info)
        if "Yoynetprofit_deducted" in columns_list:
            result_dict['Yoynetprofit_deducted'] = self.get_yoynetprofit_deducted(time_info)
        if "Yoyocf" in columns_list:
            result_dict['Yoyocf'] = self.get_yoyocf(time_info)
        if "Yoyroe" in columns_list:
            result_dict['Yoyroe'] = self.get_yoyroe(time_info)
        if "Growth_roe" in columns_list:
            result_dict['Growth_roe'] = self.get_growth_roe(time_info)
        if "Yoyroic" in columns_list:
            result_dict['Yoyroic'] = self.get_yoyroic(time_info)
        if "Yoyroic_3" in columns_list:
            result_dict['Yoyroic_3'] = self.get_yoyroic_3(time_info)
        if "Yoyeps_basic" in columns_list:
            result_dict['Yoyeps_basic'] = self.get_yoyeps_basic(time_info)
        if "Yoyop2" in columns_list:
            result_dict['Yoyop2'] = self.get_yoyop2(time_info)
        if "Yoyebt" in columns_list:
            result_dict['Yoyebt'] = self.get_yoyebt(time_info)
        if "Yoynetprofit" in columns_list:
            result_dict['Yoynetprofit'] = self.get_yoynetprofit(time_info)
        if "Growth_or" in columns_list:
            result_dict['Growth_or'] = self.get_growth_or(time_info)
        if "Growth_op" in columns_list:
            result_dict['Growth_op'] = self.get_growth_op(time_info)
        if "Growth_ebt" in columns_list:
            result_dict['Growth_ebt'] = self.get_growth_ebt(time_info)
        if "Growth_netprofit" in columns_list:
            result_dict['Growth_netprofit'] = self.get_growth_netprofit(time_info)
        if "Growth_assets" in columns_list:
            result_dict['Growth_assets'] = self.get_growth_assets(time_info)
        return result_dict


    def get_yoyocfps(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperCashFlowPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                            "OperCashFlowPS")
        OperCashFlowPS_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI",
                                                                         "OperCashFlowPS")

        OperCashFlowPS = BasicOperations(OperCashFlowPS)
        OperCashFlowPS_LYP = BasicOperations(OperCashFlowPS_LYP)
        OperCashFlowPS_LYP_abs = BasicOperations(OperCashFlowPS_LYP).abs_value()
        #OperCashFlowPS_LYP_abs.val = abs(OperCashFlowPS_LYP_abs.val)
        const_100 = BasicOperations(100)

        result = (OperCashFlowPS - OperCashFlowPS_LYP) / OperCashFlowPS_LYP_abs*const_100
        return result.val

    def get_yoynetprofit_deducted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        ProfitToShareholders = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "ProfitToShareholders")
        ProfitToShareholders_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "IC",
                                                                         "ProfitToShareholders")
        EndOrSpecProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                            "EndOrSpecProfit")
        EndOrSpecProfit_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "IC",
                                                                         "EndOrSpecProfit")

        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        ProfitToShareholders_LYP = BasicOperations(ProfitToShareholders_LYP)
        EndOrSpecProfit = BasicOperations(EndOrSpecProfit)
        EndOrSpecProfit_LYP = BasicOperations(EndOrSpecProfit_LYP)
        ProfitToShareholders_LYP_abs = BasicOperations(ProfitToShareholders_LYP - EndOrSpecProfit_LYP).abs_value()
        const_100 = BasicOperations(100)
        #ProfitToShareholders_LYP_abs.val = abs(ProfitToShareholders_LYP_abs.val)

        result = (ProfitToShareholders - EndOrSpecProfit - ProfitToShareholders_LYP + EndOrSpecProfit_LYP) / ProfitToShareholders_LYP_abs*const_100
        return result.val

    def get_yoyocf(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetCashFromOperating = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CS",
                                                                            "NetCashFromOperating")
        NetCashFromOperating_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "CS",
                                                                         "NetCashFromOperating")

        NetCashFromOperating = BasicOperations(NetCashFromOperating)
        NetCashFromOperating_LYP = BasicOperations(NetCashFromOperating_LYP)
        NetCashFromOperating_LYP_abs = BasicOperations(NetCashFromOperating_LYP).abs_value()
        const_100 = BasicOperations(100)
        #NetCashFromOperating_LYP_abs.val = abs(NetCashFromOperating_LYP_abs.val)

        result = (NetCashFromOperating - NetCashFromOperating_LYP) / NetCashFromOperating_LYP_abs*const_100
        return result.val

    def get_yoyroe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        ROEWeighted = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                            "ROEWeighted")
        ROEWeighted_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI",
                                                                         "ROEWeighted")

        ROEWeighted = BasicOperations(ROEWeighted)
        ROEWeighted_LYP = BasicOperations(ROEWeighted_LYP)
        ROEWeighted_LYP_abs = BasicOperations(ROEWeighted_LYP).abs_value()
        const_100 = BasicOperations(100)
        #ROEWeighted_LYP_abs.val = abs(ROEWeighted_LYP_abs.val)
        result = (ROEWeighted - ROEWeighted_LYP) / ROEWeighted_LYP_abs*const_100
        return result.val


    def get_growth_ocf(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetCashFromOperating = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CS",
                                                                   "NetCashFromOperating")
        NetCashFromOperating_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "CS",
                                                                        "NetCashFromOperating", num=3)

        NetCashFromOperating = BasicOperations(NetCashFromOperating)
        NetCashFromOperating_LYP3 = BasicOperations(NetCashFromOperating_LYP3)
        NetCashFromOperating_LYP3_abs = BasicOperations(NetCashFromOperating_LYP3).abs_value()
        const_100 = BasicOperations(100)
        #NetCashFromOperating_LYP3_abs.val = abs(NetCashFromOperating_LYP3_abs.val)

        result = (NetCashFromOperating - NetCashFromOperating_LYP3)/ NetCashFromOperating_LYP3_abs*const_100
        return result.val

    def get_growth_roe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROEWeighted = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "ROEWeighted")
        ROEWeighted_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI",
                                                                        "ROEWeighted", num=3)

        ROEWeighted = BasicOperations(ROEWeighted)
        ROEWeighted_LYP3 = BasicOperations(ROEWeighted_LYP3)
        ROEWeighted_LYP3_abs = BasicOperations(ROEWeighted_LYP3).abs_value()
        const_100 = BasicOperations(100)
        #ROEWeighted_LYP3_abs.val = abs(ROEWeighted_LYP3_abs.val)

        result = (ROEWeighted - ROEWeighted_LYP3)/ ROEWeighted_LYP3_abs*const_100
        return result.val

    def get_yoyroic(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                   "ProfitToShareholders")
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "ShareholderEquity")
        BankLoansAndOverdraft = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "BankLoansAndOverdraft")
        FOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "FOtherLoan")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "LongtermLoan")
        NFOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "NFOtherLoan")
        ConBillAndBond = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "ConBillAndBond")
        ProfitToShareholders_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "IC",
                                                                   "ProfitToShareholders")
        ShareholderEquity_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "ShareholderEquity")
        BankLoansAndOverdraft_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "BankLoansAndOverdraft")
        FOtherLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "FOtherLoan")
        LongtermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "LongtermLoan")
        NFOtherLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "NFOtherLoan")
        ConBillAndBond_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "ConBillAndBond")
        ShareholderEquity_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "ShareholderEquity", num=2)
        BankLoansAndOverdraft_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "BankLoansAndOverdraft", num=2)
        FOtherLoan_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "FOtherLoan", num=2)
        LongtermLoan_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "LongtermLoan", num=2)
        NFOtherLoan_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "NFOtherLoan", num=2)
        ConBillAndBond_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "ConBillAndBond", num=2)

        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        const_2 = BasicOperations(2)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        BankLoansAndOverdraft = BasicOperations(BankLoansAndOverdraft)
        FOtherLoan = BasicOperations(FOtherLoan)
        LongtermLoan = BasicOperations(LongtermLoan)
        NFOtherLoan = BasicOperations(NFOtherLoan)
        ConBillAndBond = BasicOperations(ConBillAndBond)
        ProfitToShareholders_LYP = BasicOperations(ProfitToShareholders_LYP)
        ShareholderEquity_LYP = BasicOperations(ShareholderEquity_LYP)
        BankLoansAndOverdraft_LYP = BasicOperations(BankLoansAndOverdraft_LYP)
        FOtherLoan_LYP = BasicOperations(FOtherLoan_LYP)
        LongtermLoan_LYP = BasicOperations(LongtermLoan_LYP)
        NFOtherLoan_LYP = BasicOperations(NFOtherLoan_LYP)
        ConBillAndBond_LYP = BasicOperations(ConBillAndBond_LYP)
        ShareholderEquity_LYP2 = BasicOperations(ShareholderEquity_LYP2)
        BankLoansAndOverdraft_LYP2 = BasicOperations(BankLoansAndOverdraft_LYP2)
        FOtherLoan_LYP2 = BasicOperations(FOtherLoan_LYP2)
        LongtermLoan_LYP2 = BasicOperations(LongtermLoan_LYP2)
        NFOtherLoan_LYP2 = BasicOperations(NFOtherLoan_LYP2)
        ConBillAndBond_LYP2 = BasicOperations(ConBillAndBond_LYP2)
        const_100 = BasicOperations(100)

        ProfitToShareholders_abs = ProfitToShareholders_LYP*const_2/(ShareholderEquity_LYP + BankLoansAndOverdraft_LYP + FOtherLoan_LYP\
                                                                 + LongtermLoan_LYP + NFOtherLoan_LYP + ConBillAndBond_LYP + ShareholderEquity_LYP2\
                                                                 + BankLoansAndOverdraft_LYP2 + FOtherLoan_LYP2 + LongtermLoan_LYP2 + NFOtherLoan_LYP2 + ConBillAndBond_LYP2)
        #ProfitToShareholders_abs.val = abs(ProfitToShareholders_abs.val)
        ProfitToShareholders_abs = BasicOperations(ProfitToShareholders_abs).abs_value()
        ROIC = ProfitToShareholders*const_2/(ShareholderEquity + BankLoansAndOverdraft + FOtherLoan\
                                                                 + LongtermLoan + NFOtherLoan + ConBillAndBond + ShareholderEquity_LYP\
                                                                 + BankLoansAndOverdraft_LYP + FOtherLoan_LYP + LongtermLoan_LYP + NFOtherLoan_LYP + ConBillAndBond_LYP)
        ROIC_LYP = ProfitToShareholders_LYP*const_2/(ShareholderEquity_LYP + BankLoansAndOverdraft_LYP + FOtherLoan_LYP\
                                                                 + LongtermLoan_LYP + NFOtherLoan_LYP + ConBillAndBond_LYP + ShareholderEquity_LYP2\
                                                                 + BankLoansAndOverdraft_LYP2 + FOtherLoan_LYP2 + LongtermLoan_LYP2 + NFOtherLoan_LYP2 + ConBillAndBond_LYP2)
        result = (ROIC - ROIC_LYP)/ProfitToShareholders_abs*const_100
        return result.val

    def get_yoyroic_3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                   "ProfitToShareholders")
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "ShareholderEquity")
        BankLoansAndOverdraft = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "BankLoansAndOverdraft")
        FOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "FOtherLoan")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "LongtermLoan")
        NFOtherLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "NFOtherLoan")
        ConBillAndBond = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "ConBillAndBond")
        ShareholderEquity_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "ShareholderEquity")
        BankLoansAndOverdraft_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "BankLoansAndOverdraft")
        FOtherLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "FOtherLoan")
        LongtermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "LongtermLoan")
        NFOtherLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "NFOtherLoan")
        ConBillAndBond_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "ConBillAndBond")
        ProfitToShareholders_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "IC",
                                                                   "ProfitToShareholders", num=3)
        ShareholderEquity_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "ShareholderEquity", num=3)
        BankLoansAndOverdraft_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "BankLoansAndOverdraft", num=3)
        FOtherLoan_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "FOtherLoan", num=3)
        LongtermLoan_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "LongtermLoan", num=3)
        NFOtherLoan_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "NFOtherLoan", num=3)
        ConBillAndBond_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "ConBillAndBond", num=3)
        ShareholderEquity_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "ShareholderEquity", num=4)
        BankLoansAndOverdraft_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "BankLoansAndOverdraft", num=4)
        FOtherLoan_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "FOtherLoan", num=4)
        LongtermLoan_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "LongtermLoan", num=4)
        NFOtherLoan_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "NFOtherLoan", num=4)
        ConBillAndBond_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                   "ConBillAndBond", num=4)

        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        const_2 = BasicOperations(2)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        BankLoansAndOverdraft = BasicOperations(BankLoansAndOverdraft)
        FOtherLoan = BasicOperations(FOtherLoan)
        LongtermLoan = BasicOperations(LongtermLoan)
        NFOtherLoan = BasicOperations(NFOtherLoan)
        ConBillAndBond = BasicOperations(ConBillAndBond)
        ShareholderEquity_LYP = BasicOperations(ShareholderEquity_LYP)
        BankLoansAndOverdraft_LYP = BasicOperations(BankLoansAndOverdraft_LYP)
        FOtherLoan_LYP = BasicOperations(FOtherLoan_LYP)
        LongtermLoan_LYP = BasicOperations(LongtermLoan_LYP)
        NFOtherLoan_LYP = BasicOperations(NFOtherLoan_LYP)
        ConBillAndBond_LYP = BasicOperations(ConBillAndBond_LYP)
        ProfitToShareholders_LYP3 = BasicOperations(ProfitToShareholders_LYP3)
        ShareholderEquity_LYP3 = BasicOperations(ShareholderEquity_LYP3)
        BankLoansAndOverdraft_LYP3 = BasicOperations(BankLoansAndOverdraft_LYP3)
        FOtherLoan_LYP3 = BasicOperations(FOtherLoan_LYP3)
        LongtermLoan_LYP3 = BasicOperations(LongtermLoan_LYP3)
        NFOtherLoan_LYP3 = BasicOperations(NFOtherLoan_LYP3)
        ConBillAndBond_LYP3 = BasicOperations(ConBillAndBond_LYP3)
        ShareholderEquity_LYP4 = BasicOperations(ShareholderEquity_LYP4)
        BankLoansAndOverdraft_LYP4 = BasicOperations(BankLoansAndOverdraft_LYP4)
        FOtherLoan_LYP4 = BasicOperations(FOtherLoan_LYP4)
        LongtermLoan_LYP4 = BasicOperations(LongtermLoan_LYP4)
        NFOtherLoan_LYP4 = BasicOperations(NFOtherLoan_LYP4)
        ConBillAndBond_LYP4 = BasicOperations(ConBillAndBond_LYP4)
        const_100 = BasicOperations(100)

        ProfitToShareholders_abs = ProfitToShareholders_LYP3*const_2/(ShareholderEquity_LYP3 + BankLoansAndOverdraft_LYP3 + FOtherLoan_LYP3\
                                                                 + LongtermLoan_LYP3 + NFOtherLoan_LYP3 + ConBillAndBond_LYP3 + ShareholderEquity_LYP4\
                                                                 + BankLoansAndOverdraft_LYP4 + FOtherLoan_LYP4 + LongtermLoan_LYP4 + NFOtherLoan_LYP4 + ConBillAndBond_LYP4)
        ProfitToShareholders_abs = BasicOperations(ProfitToShareholders_abs).abs_value()
        ROIC = ProfitToShareholders*const_2/(ShareholderEquity + BankLoansAndOverdraft + FOtherLoan\
                                                                 + LongtermLoan + NFOtherLoan + ConBillAndBond + ShareholderEquity_LYP\
                                                                 + BankLoansAndOverdraft_LYP + FOtherLoan_LYP + LongtermLoan_LYP + NFOtherLoan_LYP + ConBillAndBond_LYP)
        ROIC_LYP3 = ProfitToShareholders_LYP3*const_2/(ShareholderEquity_LYP3 + BankLoansAndOverdraft_LYP3 + FOtherLoan_LYP3\
                                                                 + LongtermLoan_LYP3 + NFOtherLoan_LYP3 + ConBillAndBond_LYP3 + ShareholderEquity_LYP4\
                                                                 + BankLoansAndOverdraft_LYP4 + FOtherLoan_LYP4 + LongtermLoan_LYP4 + NFOtherLoan_LYP4 + ConBillAndBond_LYP4)
        result = (ROIC - ROIC_LYP3)/ProfitToShareholders_abs*const_100
        return result.val

    def get_yoyeps_basic(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        BasicEPS1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "BasicEPS1Y")
        result = BasicEPS1Y
        return result

    def get_yoy_or(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingRevenueGR1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "OperatingRevenueGR1Y")
        result = OperatingRevenueGR1Y
        return result

    def get_yoyop2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperProfitGR1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "OperProfitGR1Y")
        result = OperProfitGR1Y
        return result

    def get_yoyebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        EBTGR1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "EBTGR1Y")
        result = EBTGR1Y
        return result

    def get_yoynetprofit(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NPParentCompanyGR1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "NPParentCompanyGR1Y")
        result = NPParentCompanyGR1Y
        return result

    def get_growth_or(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingRevenueGR3Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "OperatingRevenueGR3Y")
        result = OperatingRevenueGR3Y
        return result

    def get_growth_op(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperProfitGR3Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "OperProfitGR3Y")
        result = OperProfitGR3Y
        return result

    def get_growth_ebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        EBTGR3Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "EBTGR3Y")
        result = EBTGR3Y
        return result

    def get_growth_netprofit(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetProfitGR3Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "NetProfitGR3Y")
        result = NetProfitGR3Y
        return result

    def get_growth_assets(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        TotalAssetGR3Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "TotalAssetGR3Y")
        result = TotalAssetGR3Y
        return result