#-*- coding: utf-8-*-

# 盈利能力因子计算
from module.process.fundamental.basal_factor import *

class FactorProfitabilityCN(BasalFactor):
    __name__ = "Profitability_CN"
    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Roic1" in columns_list:
            result_dict['Roic1'] = self.get_roic1(time_info)
        if "Roe_ttm3" in columns_list:
            result_dict['Roe_ttm3'] = self.get_roe_ttm3(time_info)
        if "Ebitdatosales" in columns_list:
            result_dict['Ebitdatosales'] = self.get_ebitdatosales(time_info)
        if "Roe_diluted" in columns_list:
            result_dict['Roe_diluted'] = self.get_roe_diluted(time_info)
        if "Finaexpensetogr" in columns_list:
            result_dict['Finaexpensetogr'] = self.get_finaexpensetogr(time_info)
        if "Roe_deducted" in columns_list:
            result_dict['Roe_deducted'] = self.get_roe_deducted(time_info)
        if "Roe_exdiluted" in columns_list:
            result_dict['Roe_exdiluted'] = self.get_roe_exdiluted(time_info)
        if "Roa2" in columns_list:
            result_dict['Roa2'] = self.get_roa2(time_info)
        if "Roic" in columns_list:
            result_dict['Roic'] = self.get_roic(time_info)
        if "Optogr" in columns_list:
            result_dict['Optogr'] = self.get_optogr(time_info)
        if "Dupont_ebittosales" in columns_list:
            result_dict['Dupont_ebittosales'] = self.get_dupont_ebittosales(time_info)
        if "Dupont_roe" in columns_list:
            result_dict['Dupont_roe'] = self.get_dupont_roe(time_info)
        if "Grossprofitmargin_ttm3" in columns_list:
            result_dict['Grossprofitmargin_ttm3'] = self.get_grossprofitmargin_ttm3(time_info)
        if "Netprofitmargin_ttm3" in columns_list:
            result_dict['Netprofitmargin_ttm3'] = self.get_netprofitmargin_ttm3(time_info)
        if "Roa_ttm2" in columns_list:
            result_dict['Roa_ttm2'] = self.get_roa_ttm2(time_info)
        if "Netprofittoassets2" in columns_list:
            result_dict['Netprofittoassets2'] = self.get_netprofittoassets2(time_info)
        if "Roa2_ttm3" in columns_list:
            result_dict['Roa2_ttm3'] = self.get_roa2_ttm3(time_info)
        if "Ebittoassets_ttm" in columns_list:
            result_dict['Ebittoassets_ttm'] = self.get_ebittoassets_ttm(time_info)
        if "Profittogr_ttm3" in columns_list:
            result_dict['Profittogr_ttm3'] = self.get_profittogr_ttm3(time_info)
        if "Optogr_ttm3" in columns_list:
            result_dict['Optogr_ttm3'] = self.get_optogr_ttm3(time_info)
        if "Grossprofitmargin" in columns_list:
            result_dict['Grossprofitmargin'] = self.get_grossprofitmargin(time_info)
        if "Netprofitmargin" in columns_list:
            result_dict['Netprofitmargin'] = self.get_netprofitmargin(time_info)
        if "Roe" in columns_list:
            result_dict['Roe'] = self.get_roe(time_info)
        if "Roa" in columns_list:
            result_dict['Roa'] = self.get_roa(time_info)
        if "Dupont_nptosales" in columns_list:
            result_dict['Dupont_nptosales'] = self.get_dupont_nptosales(time_info)
        if "Ebitda" in columns_list:
            result_dict['Ebitda'] = self.get_ebitda(time_info)
        if "Roic_ttm" in columns_list:
            result_dict['Roic_ttm'] = self.get_roic_ttm(time_info)
        if "Roic_ttm2" in columns_list:
            result_dict['Roic_ttm2'] = self.get_roic_ttm2(time_info)
        if "Grossincome_ttm" in columns_list:
            result_dict['Grossincome_ttm'] = self.get_grossincome_ttm(time_info)
        if "Operatingincome_ttm" in columns_list:
            result_dict['Operatingincome_ttm'] = self.get_operatingincome_ttm(time_info)
        if "Netmargin_ttm" in columns_list:
            result_dict['Netmargin_ttm'] = self.get_netmargin_ttm(time_info)
        if "Profittoshareholders_ttm" in columns_list:
            result_dict['Profittoshareholders_ttm'] = self.get_profittoshareholders_ttm(time_info)
        if "Earningbeforetax_ttm" in columns_list:
            result_dict['Earningbeforetax_ttm'] = self.get_earningbeforetax_ttm(time_info)
        if "Earningbeforetax_ttm2" in columns_list:
            result_dict['Earningbeforetax_ttm2'] = self.get_earningbeforetax_ttm2(time_info)
        if "Operatingprofit_ttm" in columns_list:
            result_dict['Operatingprofit_ttm'] = self.get_operatingprofit_ttm(time_info)
        if "Abs" in columns_list:
            result_dict['Abs'] = self.get_abs(time_info)
        if "Egro" in columns_list:
            result_dict['Egro'] = self.get_egro(time_info)
        if "Sgro" in columns_list:
            result_dict['Sgro'] = self.get_sgro(time_info)
        if "Cetoe" in columns_list:
            result_dict['Cetoe'] = self.get_cetoe(time_info)
        if "Ebit" in columns_list:
            result_dict['Ebit'] = self.get_Ebit(time_info)
        return result_dict

    def get_roic1(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPPCompOwners = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "NPPCompOwners")
        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "SEWithoutMI")
        ShortTermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "ShortTermLoan")
        NonCurLiaIn2Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC","NonCurLiaIn1Y")
        NotesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "NotesPayable")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "BondsPayable")
        BorFromCB = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "BorFromCB")

        SEWithoutMI_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC", "SEWithoutMI")
        ShortTermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC", "ShortTermLoan")
        NonCurLiaIn2Y_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC", "NonCurLiaIn1Y")
        NotesPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC", "NotesPayable")
        LongtermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC", "LongtermLoan")
        BondsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC", "BondsPayable")
        BorFromCB_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC", "BorFromCB")

        NPPCompOwners = BasicOperations(NPPCompOwners)
        const_1 = BasicOperations(2)
        SEWithoutMI = BasicOperations(SEWithoutMI)
        ShortTermLoan = BasicOperations(ShortTermLoan)
        NonCurLiaIn2Y = BasicOperations(NonCurLiaIn2Y)
        NotesPayable = BasicOperations(NotesPayable)
        LongtermLoan = BasicOperations(LongtermLoan)
        BondsPayable = BasicOperations(BondsPayable)
        BorFromCB = BasicOperations(BorFromCB)
        const_100 = BasicOperations(100)

        SEWithoutMI_LYP = BasicOperations(SEWithoutMI_LYP)
        ShortTermLoan_LYP = BasicOperations(ShortTermLoan_LYP)
        NonCurLiaIn2Y_LYP = BasicOperations(NonCurLiaIn2Y_LYP)
        NotesPayable_LYP = BasicOperations(NotesPayable_LYP)
        LongtermLoan_LYP = BasicOperations(LongtermLoan_LYP)
        BondsPayable_LYP = BasicOperations(BondsPayable_LYP)
        BorFromCB_LYP = BasicOperations(BorFromCB_LYP)
        result = NPPCompOwners * const_1 *const_100/ (SEWithoutMI + ShortTermLoan + NonCurLiaIn2Y + NotesPayable + LongtermLoan + BondsPayable + BorFromCB
                                            + SEWithoutMI_LYP + ShortTermLoan_LYP + NonCurLiaIn2Y_LYP + NotesPayable_LYP + LongtermLoan_LYP + BondsPayable_LYP
                                            + BorFromCB_LYP)
        return result.val

    def get_roe_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "FI", "ProfitToShareholders")
        TotalShareholderEquityEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "TotalShEquity")

        ProfitToShareholders_TTM = BasicOperations(ProfitToShareholders_TTM)
        TotalShareholderEquity = BasicOperations(TotalShareholderEquityEquity)
        const_100 = BasicOperations(100)

        result = ProfitToShareholders_TTM*const_100 / TotalShareholderEquity
        return result.val

    def get_ebitdatosales(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotalProfit")
        FinExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "FinExpense")
        TotOpeRev = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotOpeRev")

        TotalProfit = BasicOperations(TotalProfit)
        FinExpense = BasicOperations(FinExpense)
        TotOpeRev = BasicOperations(TotOpeRev)
        const_100 = BasicOperations(100)

        result = (TotalProfit + FinExpense)*const_100 / TotOpeRev
        return result.val

    def get_roe_diluted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPPCompOwners = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "NPPCompOwners")
        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "SEWithoutMI")

        NPPCompOwners = BasicOperations(NPPCompOwners)
        SEWithoutMI = BasicOperations(SEWithoutMI)
        const_100 = BasicOperations(100)

        result = NPPCompOwners*const_100 / SEWithoutMI
        return result.val

    def get_finaexpensetogr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        FinExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "FinExpense")
        TotOpeRev = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotOpeRev")

        FinExpense = BasicOperations(FinExpense)
        TotOpeRev = BasicOperations(TotOpeRev)
        const_100 = BasicOperations(100)

        result = FinExpense*const_100 / TotOpeRev
        return result.val

    def get_roe_deducted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OpeProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "NetProfit")
        # SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "SEWithoutMI")
        # SEWithoutMI_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
        #                                                               "SEWithoutMI")
        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "TotalShEquity")
        SEWithoutMI_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                      "TotalShEquity")
        OpeProfit = BasicOperations(OpeProfit)
        SEWithoutMI = BasicOperations(SEWithoutMI)
        SEWithoutMI_LYP = BasicOperations(SEWithoutMI_LYP)
        const_100 = BasicOperations(100)
        const_2 = BasicOperations(2)

        result = OpeProfit*const_2/ (SEWithoutMI+SEWithoutMI_LYP)*const_100
        return result.val

    def get_roe_exdiluted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OpeProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "NetProfit")
        # SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "SEWithoutMI")
        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                  "TotalShEquity")
        OpeProfit = BasicOperations(OpeProfit)
        SEWithoutMI = BasicOperations(SEWithoutMI)
        const_100 = BasicOperations(100)

        result = OpeProfit/ SEWithoutMI * const_100
        return result.val

    def get_roa2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalProfit = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "TotalProfit")
        FinExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "FinExpense")
        TotalAssets = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "BLC", "TotalAssets")
        TotalAssets_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                       "TotalAssets")

        TotalProfit = BasicOperations(TotalProfit)
        FinExpense = BasicOperations(FinExpense)
        TotalAssets = BasicOperations(TotalAssets)
        TotalAssets_LYP = BasicOperations(TotalAssets_LYP)
        const_100 = BasicOperations(100)
        const_2 = BasicOperations(2)

        result = (TotalProfit+FinExpense)*const_2*const_100 / (TotalAssets+TotalAssets_LYP)
        return result.val

    def get_optogr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OpeProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "OpeProfit")
        TotOpeRev = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotOpeRev")

        OpeProfit = BasicOperations(OpeProfit)
        TotOpeRev = BasicOperations(TotOpeRev)
        const_100 = BasicOperations(100)

        result = OpeProfit*const_100/TotOpeRev
        return result.val

    def get_dupont_ebittosales(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotalProfit")
        TotOpeRev = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotOpeRev")
        FinExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "FinExpense")

        TotalProfit = BasicOperations(TotalProfit)
        TotOpeRev = BasicOperations(TotOpeRev)
        FinExpense = BasicOperations(FinExpense)
        const_100 = BasicOperations(100)

        result = (TotalProfit+FinExpense)*const_100/TotOpeRev
        return result.val

    def get_dupont_roe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "NPPCompOwners")
        ShareholderEquity = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "SEWithoutMI")
        ShareholderEquity_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC", "SEWithoutMI")
        ShareholderEquity_PRE = self.common_function_obj.get_data_point_pre(stock_info, data_dict, time_info, "BLC", "SEWithoutMI")

        # 求平均值
        # mean_ShareholderEquity = None
        check_lyp_list = pd.isnull(np.array([ShareholderEquity, ShareholderEquity_LYP], dtype=float))
        check_lyp_num = np.count_nonzero(check_lyp_list == False)
        mean_lyp_ShareholderEquity = (BasicOperations(ShareholderEquity) + BasicOperations(
            ShareholderEquity_LYP)) / BasicOperations(check_lyp_num)

        check_pre_list = pd.isnull(np.array([ShareholderEquity, ShareholderEquity_PRE], dtype=float))
        check_pre_num = np.count_nonzero(check_pre_list == False)
        mean_pre_ShareholderEquity = (BasicOperations(ShareholderEquity) + BasicOperations(
            ShareholderEquity_PRE)) / BasicOperations(check_pre_num)

        # check_lyp_lyr = pd.isnull(np.array([mean_lyp_ShareholderEquity, mean_lyr_ShareholderEquity], dtype=float))

        lyp_distinct = (BasicOperations(mean_lyp_ShareholderEquity) - BasicOperations(ShareholderEquity)).abs_value()
        lyr_distinct = (BasicOperations(mean_pre_ShareholderEquity) - BasicOperations(ShareholderEquity)).abs_value()

        if (lyr_distinct-lyp_distinct).val <0:
            mean_ShareholderEquity = mean_pre_ShareholderEquity
        else:
            mean_ShareholderEquity = mean_lyp_ShareholderEquity

        const_100 = BasicOperations(100)
        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        result = ProfitToShareholders / mean_ShareholderEquity * const_100
        """
        ProfitToShareholders = BasicOperations(ProfitToShareholders)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        ShareholderEquity_LYP = BasicOperations(ShareholderEquity_LYP)
        const_100 = BasicOperations(100)
        const_2 = BasicOperations(2)

        result = ProfitToShareholders*const_2*const_100/(ShareholderEquity + ShareholderEquity_LYP)
        """
        return result.val

    def get_grossprofitmargin_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        indus = self.common_function_obj.get_hs_industry(stock_info, data_dict, time_info, 'IndustryNum')
        if indus in set([27]):
            return None
        else :
            OpeCost_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC",
                                                                      "OpeaCost")
            TotOpeRev_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC",
                                                                        "TotOpeRev")

            OpeCost_TTM = BasicOperations(OpeCost_TTM)
            TotOpeRev_TTM = BasicOperations(TotOpeRev_TTM)
            const_100 = BasicOperations(100)


        result = (TotOpeRev_TTM - OpeCost_TTM)*const_100/TotOpeRev_TTM
        return result.val

    def get_netprofitmargin_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "NetProfit")
        TotOpeRev_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "TotOpeRev")

        NetProfit_TTM = BasicOperations(NetProfit_TTM)
        TotOpeRev_TTM = BasicOperations(TotOpeRev_TTM)
        const_100 = BasicOperations(100)

        result = NetProfit_TTM*const_100/TotOpeRev_TTM
        return result.val

    def get_roa_ttm2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OpeProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "NetProfit")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "TotalAssets")

        OpeProfit_TTM = BasicOperations(OpeProfit_TTM)
        TotalAssets = BasicOperations(TotalAssets)
        const_100 = BasicOperations(100)

        result = OpeProfit_TTM*const_100/TotalAssets
        return result.val

    def get_netprofittoassets2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "NPPCompOwners")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "TotalAssets")

        ProfitToShareholders_TTM = BasicOperations(ProfitToShareholders_TTM)
        TotalAssets = BasicOperations(TotalAssets)
        const_100 = BasicOperations(100)

        result = ProfitToShareholders_TTM*const_100/TotalAssets
        return result.val


    def get_roa2_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "TotalProfit")
        FinExpense_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "FinExpense")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "TotalAssets")

        TotalProfit_TTM = BasicOperations(TotalProfit_TTM)
        FinExpense_TTM = BasicOperations(FinExpense_TTM)
        TotalAssets = BasicOperations(TotalAssets)
        const_100 = BasicOperations(100)

        result = (TotalProfit_TTM+FinExpense_TTM)*const_100/TotalAssets
        return result.val

    def get_ebittoassets_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "TotalProfit")
        FinExpense_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "FinExpense")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "TotalAssets")

        TotalProfit_TTM = BasicOperations(TotalProfit_TTM)
        FinExpense_TTM = BasicOperations(FinExpense_TTM)
        TotalAssets = BasicOperations(TotalAssets)
        const_100 = BasicOperations(100)

        result = (TotalProfit_TTM+FinExpense_TTM)*const_100/TotalAssets
        return result.val

    def get_profittogr_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "NetProfit")
        TotOpeRev = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotOpeRev")

        NetProfit_TTM = BasicOperations(NetProfit_TTM)
        TotOpeRev = BasicOperations(TotOpeRev)
        const_100 = BasicOperations(100)

        result = NetProfit_TTM*const_100/TotOpeRev
        return result.val

    def get_optogr_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OpeProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "OpeProfit")
        TotOpeRev = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotOpeRev")

        OpeProfit_TTM = BasicOperations(OpeProfit_TTM)
        TotOpeRev = BasicOperations(TotOpeRev)
        const_100 = BasicOperations(100)

        result = OpeProfit_TTM*const_100/TotOpeRev
        return result.val

    def get_grossprofitmargin(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        indus = self.common_function_obj.get_hs_industry(stock_info, data_dict, time_info, 'IndustryNum')
        if indus in set([27]):
            return None
        else :
            GrossIncomeRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                           "GrossIncomeRatio")

            result = GrossIncomeRatio
            return result


    def get_netprofitmargin(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfitRatio= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfitRatio")

        result = NetProfitRatio
        return result


    def get_roe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROEWeighted= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "ROEWeighted")

        result = ROEWeighted
        return result

    def get_roa(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROA= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "ROA")

        result = ROA
        return result

    def get_dupont_nptosales(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfitRatio= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfitRatio")

        result = NetProfitRatio
        return result

    def get_ebitda(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotalProfit")
        FinExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "FinExpense")

        TotalProfit = BasicOperations(TotalProfit)
        FinExpense = BasicOperations(FinExpense)

        result = TotalProfit+FinExpense
        return result.val


    def get_grossincome_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        indus = self.common_function_obj.get_hs_industry(stock_info, data_dict, time_info, 'IndustryNum')
        if indus in set([27]):
            return None
        else :
            TotOpeRev_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC",
                                                                        "TotOpeRev")
            OpeCost_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC",
                                                                      "OpeCost")

            TotOpeRev_TTM = BasicOperations(TotOpeRev_TTM)
            OpeCost_TTM = BasicOperations(OpeCost_TTM)

            result = TotOpeRev_TTM - OpeCost_TTM
            return result.val



    def get_operatingincome_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotOpeRev_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "TotOpeRev")

        TotOpeRev_TTM = BasicOperations(TotOpeRev_TTM)

        result = TotOpeRev_TTM
        return result.val


    def get_netmargin_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "NetProfit")

        NetProfit_TTM = BasicOperations(NetProfit_TTM)

        result = NetProfit_TTM
        return result.val

    def get_profittoshareholders_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "NPPCompOwners")

        result = ProfitToShareholders_TTM
        return result

    def get_earningbeforetax_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "TotalProfit")
        FinExpense_TTM= self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "FinExpense")

        TotalProfit_TTM = BasicOperations(TotalProfit_TTM)
        FinExpense_TTM = BasicOperations(FinExpense_TTM)

        result = TotalProfit_TTM+FinExpense_TTM
        return result.val

    def get_earningbeforetax_ttm2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "TotalProfit")
        FinExpense_TTM= self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "FinExpense")

        TotalProfit_TTM = BasicOperations(TotalProfit_TTM)
        FinExpense_TTM = BasicOperations(FinExpense_TTM)

        result = TotalProfit_TTM+FinExpense_TTM
        return result.val

    def get_operatingprofit_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OpeProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "OpeProfit")

        OpeProfit_TTM = BasicOperations(OpeProfit_TTM)

        result = OpeProfit_TTM
        return result.val

    def get_roic(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPPCompOwners = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                    "NPPCompOwners")
        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                  "SEWithoutMI")
        ShortTermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                    "ShortTermLoan")
        NonCurLiaIn2Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                    "NonCurLiaIn1Y")
        NotesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "NotesPayable")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "BondsPayable")
        BorFromCB = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "BorFromCB")

        SEWithoutMI_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                      "SEWithoutMI")
        ShortTermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                        "ShortTermLoan")
        NonCurLiaIn2Y_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                        "NonCurLiaIn1Y")
        NotesPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                       "NotesPayable")
        LongtermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                       "LongtermLoan")
        BondsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                       "BondsPayable")
        BorFromCB_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                    "BorFromCB")

        NPPCompOwners = BasicOperations(NPPCompOwners)
        const_1 = BasicOperations(2)
        SEWithoutMI = BasicOperations(SEWithoutMI)
        ShortTermLoan = BasicOperations(ShortTermLoan)
        NonCurLiaIn2Y = BasicOperations(NonCurLiaIn2Y)
        NotesPayable = BasicOperations(NotesPayable)
        LongtermLoan = BasicOperations(LongtermLoan)
        BondsPayable = BasicOperations(BondsPayable)
        BorFromCB = BasicOperations(BorFromCB)
        const_100 = BasicOperations(100)

        SEWithoutMI_LYP = BasicOperations(SEWithoutMI_LYP)
        ShortTermLoan_LYP = BasicOperations(ShortTermLoan_LYP)
        NonCurLiaIn2Y_LYP = BasicOperations(NonCurLiaIn2Y_LYP)
        NotesPayable_LYP = BasicOperations(NotesPayable_LYP)
        LongtermLoan_LYP = BasicOperations(LongtermLoan_LYP)
        BondsPayable_LYP = BasicOperations(BondsPayable_LYP)
        BorFromCB_LYP = BasicOperations(BorFromCB_LYP)
        result = NPPCompOwners * const_1 * const_100 / (
                    SEWithoutMI + ShortTermLoan + NonCurLiaIn2Y + NotesPayable + LongtermLoan + BondsPayable + BorFromCB
                    + SEWithoutMI_LYP + ShortTermLoan_LYP + NonCurLiaIn2Y_LYP + NotesPayable_LYP + LongtermLoan_LYP + BondsPayable_LYP
                    + BorFromCB_LYP)
        return result.val

    def get_roic_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OpeProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "OpeProfit")
        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "SEWithoutMI")
        ShortTermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "ShortTermLoan")
        NonCurLiaIn1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC","NonCurLiaIn1Y")
        NotesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "NotesPayable")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "BondsPayable")
        BorFromCB = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "BorFromCB")

        OpeProfit_TTM = BasicOperations(OpeProfit_TTM)
        SEWithoutMI = BasicOperations(SEWithoutMI)
        ShortTermLoan = BasicOperations(ShortTermLoan)
        NonCurLiaIn1Y = BasicOperations(NonCurLiaIn1Y)
        NotesPayable = BasicOperations(NotesPayable)
        LongtermLoan = BasicOperations(LongtermLoan)
        BondsPayable = BasicOperations(BondsPayable)
        BorFromCB = BasicOperations(BorFromCB)
        const_100 = BasicOperations(100)

        result = OpeProfit_TTM*const_100/(SEWithoutMI+ShortTermLoan+NonCurLiaIn1Y+NotesPayable+LongtermLoan+BondsPayable+BorFromCB)

        return result.val

    def get_roic_ttm2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPPCompOwners_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "NPPCompOwners")
        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "SEWithoutMI")
        ShortTermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "ShortTermLoan")
        NonCurLiaIn1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC","NonCurLiaIn1Y")
        NotesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "NotesPayable")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "BondsPayable")
        BorFromCB = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "BorFromCB")

        NPPCompOwners_TTM = BasicOperations(NPPCompOwners_TTM)
        SEWithoutMI = BasicOperations(SEWithoutMI)
        ShortTermLoan = BasicOperations(ShortTermLoan)
        NonCurLiaIn1Y = BasicOperations(NonCurLiaIn1Y)
        NotesPayable = BasicOperations(NotesPayable)
        LongtermLoan = BasicOperations(LongtermLoan)
        BondsPayable = BasicOperations(BondsPayable)
        BorFromCB = BasicOperations(BorFromCB)
        const_100 = BasicOperations(100)

        result = NPPCompOwners_TTM*const_100/(SEWithoutMI+ShortTermLoan+NonCurLiaIn1Y+NotesPayable+LongtermLoan+BondsPayable+BorFromCB)
        return result.val

    def get_abs(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        TotalCurAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                     "TotalCurAssets")
        CashEquivalents = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                      "CashEquivalents")
        TotalCurLia = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                  "TotalCurLia")
        ShortTermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                    "ShortTermLoan")
        STBPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC", "STBPayable")
        NotesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                   "NotesPayable")
        FAssetDep = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CSC", "FAssetDep")
        IntAAmort = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CSC", "IntAAmort")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                  "TotalAssets")

        TotalCurAssets_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                         "TotalCurAssets")
        CashEquivalents_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                          "CashEquivalents")
        TotalCurLia_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                      "TotalCurLia")
        ShortTermLoan_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                        "ShortTermLoan")
        STBPayable_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                     "STBPayable")
        NotesPayable_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                       "NotesPayable")

        TotalCurAssets = BasicOperations(TotalCurAssets)
        CashEquivalents = BasicOperations(CashEquivalents)
        TotalCurLia = BasicOperations(TotalCurLia)
        ShortTermLoan = BasicOperations(ShortTermLoan)
        STBPayable = BasicOperations(STBPayable)
        NotesPayable = BasicOperations(NotesPayable)
        FAssetDep = BasicOperations(FAssetDep)
        IntAAmort = BasicOperations(IntAAmort)
        TotalAssets = BasicOperations(TotalAssets)
        TotalCurAssets_lyp = BasicOperations(TotalCurAssets_lyp)
        CashEquivalents_lyp = BasicOperations(CashEquivalents_lyp)
        TotalCurLia_lyp = BasicOperations(TotalCurLia_lyp)
        ShortTermLoan_lyp = BasicOperations(ShortTermLoan_lyp)
        STBPayable_lyp = BasicOperations(STBPayable_lyp)
        NotesPayable_lyp = BasicOperations(NotesPayable_lyp)

        result = ((TotalCurAssets - TotalCurAssets_lyp) - (CashEquivalents - CashEquivalents_lyp) - (
                    TotalCurLia - TotalCurLia_lyp) + ((ShortTermLoan + STBPayable + NotesPayable) - (
                    ShortTermLoan_lyp + STBPayable_lyp + NotesPayable_lyp)) - (FAssetDep + IntAAmort)) / TotalAssets
        return result.val


    def get_egro(self, time_info):
        import statsmodels.api as sm
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        BasicEPS_list = []
        Year = time_info['EndDate'].year
        Year_list = list(np.arange(Year, Year - 5, -1))
        for i in range(5):
            time_info_temp = time_info.copy()
            end_time = time_info_temp['EndDate']
            end_time_year_ago = end_time - pd.DateOffset(years=i)
            time_info_temp['EndDate'] = end_time_year_ago
            BasicEPS = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info_temp, "MI", "BasicEPS", num=1)
            BasicEPS_list.append(BasicEPS)
        BasicEPS_list_notnone = [item for item in BasicEPS_list if item != None and str(item) != 'nan']
        if not BasicEPS_list_notnone:
            return None
        else:
            x = sm.add_constant(Year_list)
            y = BasicEPS_list
            model = sm.OLS(y, x, missing='drop')
            reg = model.fit()
            beta = reg.params[1]
            return beta / np.nanmean(BasicEPS_list_notnone)

    def get_sgro(self, time_info):
        import statsmodels.api as sm
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        MainIncomePS_list = []
        Year = time_info['EndDate'].year
        Year_list = list(np.arange(Year, Year - 5, -1))
        for i in range(5):
            time_info_temp = time_info.copy()
            end_time = time_info_temp['EndDate']
            end_time_year_ago = end_time - pd.DateOffset(years=i)
            time_info_temp['EndDate'] = end_time_year_ago
            MainIncomePS = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info_temp, "MI", "MainIncomePS", num=1)
            MainIncomePS_list.append(MainIncomePS)
        MainIncomePS_list_notnone = [item for item in MainIncomePS_list if item != None and str(item) != 'nan']
        if not MainIncomePS_list_notnone:
            return None
        else:
            x = sm.add_constant(Year_list)
            y = MainIncomePS_list
            model = sm.OLS(y, x, missing='drop')
            reg = model.fit()
            beta = reg.params[1]
            return beta / np.nanmean(MainIncomePS_list_notnone)


    def get_cetoe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetOpeCFlow = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "CSC", "NetOpeCFlow")
        NetProfit = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "ICC", "NetProfit")
        NetOpeCFlow = BasicOperations(NetOpeCFlow)
        NetProfit = BasicOperations(NetProfit)
        result = NetOpeCFlow / NetProfit
        return result.val

    def get_Ebit(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "TotalProfit")
        FinExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC", "FinExpense")

        TotalProfit = BasicOperations(TotalProfit)
        FinExpense = BasicOperations(FinExpense)

        result = TotalProfit+FinExpense
        return result.val