#-*- coding: utf-8 -*-

# 市场表现因子

from module.process.fundamental.basal_factor import *
import math


class FactorMarketPerformanceCN(BasalFactor):
    __name__ = "MarketPerformance_CN"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Ebitps2" in columns_list:
            result_dict['Ebitps2'] = self.get_ebitps2(time_info)
        if "Eps_adjust2" in columns_list:
            result_dict['Eps_adjust2'] = self.get_eps_adjust2(time_info)
        if "Eps_diluted3" in columns_list:
            result_dict['Eps_diluted3'] = self.get_eps_diluted3(time_info)
        if "Fcffps2" in columns_list:
            result_dict['Fcffps2'] = self.get_fcffps2(time_info)
        if "Fcfeps2" in columns_list:
            result_dict['Fcfeps2'] = self.get_fcfeps2(time_info)
        if "Eps_basic" in columns_list:
            result_dict['Eps_basic'] = self.get_eps_basic(time_info)
        if "Eps_diluted" in columns_list:
            result_dict['Eps_diluted'] = self.get_eps_diluted(time_info)
        if "Bps" in columns_list:
            result_dict['Bps'] = self.get_bps(time_info)
        if "Ocfps" in columns_list:
            result_dict['Ocfps'] = self.get_ocfps(time_info)
        if "Cfps" in columns_list:
            result_dict['Cfps'] = self.get_cfps(time_info)
        if "Orps" in columns_list:
            result_dict['Orps'] = self.get_orps(time_info)
        return result_dict


    def get_ebitps2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningBeforeTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                       "TotalProfit")
        # FInterestExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
        #                                                                "FInterestExpense")
        # FinancingCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
        #                                                             "FinancingCost")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)

        EarningBeforeTax = BasicOperations(EarningBeforeTax)
        # FInterestExpense = BasicOperations(FInterestExpense)
        # FinancingCost = BasicOperations(FinancingCost)

        result = (EarningBeforeTax) / (ListedShares + NotHKShares)
        return result.val

    def get_eps_adjust2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                           "NPPCompOwners")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)

        ProfitToShareholders = BasicOperations(ProfitToShareholders)

        result = ProfitToShareholders / (ListedShares + NotHKShares)
        return result.val

    def get_eps_diluted3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                           "NPPCompOwners")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)

        ProfitToShareholders = BasicOperations(ProfitToShareholders)


        result = ProfitToShareholders / (ListedShares + NotHKShares)
        return result.val

    def get_fcffps2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPPCompOwners = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                           "NPPCompOwners")
        AssetImpLoss = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                         "AssetImpLoss")
        FinExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                             "FinExpense")
        IncTaxCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                     "IncTaxCost")
        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                    "TotalProfit")
        # FInterestExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
        #                                                                "FInterestExpense")
        # Tax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
        #                                                   "Tax")
        # EarningBeforeTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
        #                                                                "EarningBeforeTax")

        FixedAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                  "FixedAssets")
        ConsMaterials = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                           "ConsMaterials")
        ConsInProcess = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                       "ConsInProcess")
        FixedAssets_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                      "FixedAssets")
        ConsMaterials_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                               "ConsMaterials")
        ConsInProcess_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BLC",
                                                                           "ConsInProcess")

        TotalCurAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                         "TotalCurAssets")
        TotalCurLia = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                            "TotalCurLia")

        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)
        NPPCompOwners = BasicOperations(NPPCompOwners)
        AssetImpLoss = BasicOperations(AssetImpLoss)
        FinExpense = BasicOperations(FinExpense)
        IncTaxCost = BasicOperations(IncTaxCost)
        TotalProfit = BasicOperations(TotalProfit)
        # FInterestExpense = BasicOperations(FInterestExpense)
        # Tax = BasicOperations(Tax)
        # EarningBeforeTax = BasicOperations(EarningBeforeTax)
        FixedAssets = BasicOperations(FixedAssets)
        ConsMaterials = BasicOperations(ConsMaterials)
        ConsInProcess = BasicOperations(ConsInProcess)
        FixedAssets_LYP = BasicOperations(FixedAssets_LYP)
        ConsMaterials_LYP = BasicOperations(ConsMaterials_LYP)
        ConsInProcess_LYP = BasicOperations(ConsInProcess_LYP)
        TotalCurAssets = BasicOperations(TotalCurAssets)
        TotalCurLia = BasicOperations(TotalCurLia)
        const_1 = BasicOperations(1)

        result = (NPPCompOwners + AssetImpLoss +
                  FinExpense*(const_1 - IncTaxCost/TotalProfit) -
                  (FixedAssets + ConsMaterials + ConsInProcess - FixedAssets_LYP - ConsMaterials_LYP - ConsInProcess_LYP) +
                  (TotalCurAssets - TotalCurLia))/(ListedShares + NotHKShares)

        return result.val

    def get_fcfeps2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EarningBeforeTax = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "FI",
                                                                       "EarningBeforeTax")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)
        EarningBeforeTax = BasicOperations(EarningBeforeTax)

        result = EarningBeforeTax / (ListedShares + NotHKShares)
        return result.val

    def get_eps_basic(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        BasicEPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                               "BasicEPS")
        result = BasicEPS
        return result

    def get_eps_diluted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        DilutedEPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                 "DilutedEPS")
        result = DilutedEPS
        return result

    def get_bps(self, time_info):
        result = None
        '''
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetAssetPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                 "NetAssetPS")
        result = NetAssetPS
        return result
        '''
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BLC",
                                                                       "SEWithoutMI")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)
        SEWithoutMI = BasicOperations(SEWithoutMI)

        result = SEWithoutMI / (ListedShares + NotHKShares)
        return result.val


    def get_ocfps(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        '''
        OperCashFlowPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                     "OperCashFlowPS")
        result = OperCashFlowPS
        return result
        '''
        NetOpeCFlow = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CSC",
                                                                       "NetOpeCFlow")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)
        NetOpeCFlow = BasicOperations(NetOpeCFlow)

        result = NetOpeCFlow / (ListedShares + NotHKShares)
        return result.val


    def get_cfps(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        '''
        CashFlowPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                 "CashFlowPS")
        result = CashFlowPS
        return result
        '''
        CEquInc = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CSC",
                                                                       "CEquInc")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)
        CEquInc = BasicOperations(CEquInc)

        result = CEquInc / (ListedShares + NotHKShares)
        return result.val


    def get_orps(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        '''
        MainIncomePS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "MainIncomePS")
        result = MainIncomePS
        return result
        '''
        TotOpeRev = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "ICC",
                                                                       "TotOpeRev")
        ListedShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "ListedShares")
        NotHKShares = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "NotHKShares")

        ListedShares = BasicOperations(ListedShares)
        NotHKShares = BasicOperations(NotHKShares)
        TotOpeRev = BasicOperations(TotOpeRev)

        result = TotOpeRev / (ListedShares + NotHKShares)
        return result.val