#-*- coding: utf-8 -*-

from module.process.fundamental.factors.factor_profitability import *
from module.process.fundamental.factors.factor_profitability_cn import *
from module.process.fundamental.factors.factor_debtpayingability import *
from module.process.fundamental.factors.factor_debtpayingability_cn import *
from module.process.fundamental.factors.factor_resolutionability import *
from module.process.fundamental.factors.factor_resolutionability_cn import *
from module.process.fundamental.factors.factor_operationability import *
from module.process.fundamental.factors.factor_operationability_cn import *
from module.process.fundamental.factors.factor_growthability import *
from module.process.fundamental.factors.factor_growthability_cn import *
from module.process.fundamental.factors.factor_cashflow import *
from module.process.fundamental.factors.factor_cashflow_cn import *
from module.process.fundamental.factors.factor_marketperformance import *
from module.process.fundamental.factors.factor_marketperformance_cn import *
import threading
import logging
import os

class FactorsCalculate(object):
    def __init__(self, stock_info_dict, financial_data_dict):
        self.stock_info_dict = stock_info_dict
        self.financial_data_dict = financial_data_dict
        self.account_standard = financial_data_dict['Standard']
        #self.logger = self.get_logger()
        self.func_dict = {"Profitability": 'self.calculate_profitability()',
                          "Debtpayingability": 'self.calculate_debtpayingability()',
                          "Resolutionability": 'self.calculate_resolutionability()',
                          "Operationability": 'self.calculate_operationability()',
                          "Growthability" : 'self.calculate_growthability()',
                          "Cashflow" : 'self.calculate_cashflow()',
                          "MarketPerformance" : 'self.calculate_marketperformance()'}
        self.func_list = ["Profitability", "Debtpayingability", "Resolutionability", "Operationability", "Growthability", "Cashflow", "MarketPerformance"]

        #self.func_list = ["Profitability","Debtpayingability"]

    '''
    def get_logger(self):
        logger = logging.getLogger()
        stream_handler = logging.StreamHandler()
        file_handler = logging.FileHandler(os.getcwd() + r'/log_files/' + datetime.datetime.now().strftime('%Y-%m-%d') + ".log")
        formatter = logging.Formatter(
            '%(asctime)s %(filename)s %(funcName)-12s %(levelname)-8s %(message)s')
        stream_handler.setFormatter(formatter)
        file_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)
        logger.addHandler(file_handler)
        logger.setLevel(logging.DEBUG)
        return logger
    '''

    '''
    def calculate_factors(self):
        threads = []
        for i in range(len(self.func_list)):
            p = threading.Thread(target=self.run_caculate_func, args=(self.func_list[i],))
            threads.append(p)
            p.start()
            p.join()
    '''

    def calculate_factors(self):
        for i in range(len(self.func_list)):
            self.run_caculate_func(self.func_list[i])


    def run_caculate_func(self, func_key):
        eval(self.func_dict[func_key])
        pass
    # 盈利能力
    def calculate_profitability(self):
        factor_profitability_obj = FactorProfitability(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()

        factor_profitability_obj = FactorProfitabilityCN(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data_cn()

    # 偿债能力
    def calculate_debtpayingability(self):
        factor_profitability_obj = FactorDebtpayingability(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()

        factor_profitability_obj = FactorDebtpayingabilityCN(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data_cn()


    # 清债能力
    def calculate_resolutionability(self):
        factor_profitability_obj = FactorResolutionability(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()

        factor_profitability_obj = FactorResolutionabilityCN(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data_cn()

    # 营运能力因子
    def calculate_operationability(self):
        factor_profitability_obj = FactorOperationability(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()

        factor_profitability_obj = FactorOperationabilityCN(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data_cn()

    # 成长能力因子
    def calculate_growthability(self):
        factor_profitability_obj = FactorGrowthability(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()

        factor_profitability_obj = FactorGrowthabilityCN(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data_cn()



    # 现金流情况因子
    def calculate_cashflow(self):
        factor_profitability_obj = FactorCashflow(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()

        factor_profitability_obj = FactorCashflowCN(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data_cn()

    # 市场表现因子
    def calculate_marketperformance(self):
        factor_profitability_obj = FactorMarketPerformance(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()

        factor_profitability_obj = FactorMarketPerformanceCN(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data_cn()
