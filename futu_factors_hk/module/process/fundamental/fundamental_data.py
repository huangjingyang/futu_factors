#-*- coding: utf-8 -*-BalanceSheet

# 计算基本面数据， 非TTM。对TTM的，需要另外处理
# 设置成为一个公共模块
# 采用针对一个股票的批量计算
# 股票 -> 时间段 -> 公司代码 -> 批量计算
# 每一次一定要做批量计算
# 一律采用合并未调整
# 在内存中计算


from module.process.fundamental.factors_calculate import *

class FundamentalData():
    def __init__(self, stock_info_df, start_time, end_time):
        self.stock_info_df = stock_info_df
        self.start_time = start_time
        self.end_time = end_time
        self.start_time_format = self.convert_string_to_datetime(start_time)
        self.end_time_format = self.convert_string_to_datetime(end_time)

    # 取数据,对于三大财务报表的数据，取出来。在内存中计算
    def get_financial_data_from_db(self, company_code, search_start_time, search_end_time):
        financial_data_dict = dict()
        # 由于要做同期比较，所以取的数据要尽量多一年
        search_start_time = datetime.datetime.strftime(datetime.datetime.strptime(search_start_time, '%Y-%m-%d') - datetime.timedelta(days=365 * 2), "%Y-%m-%d")
        search_end_time = search_end_time
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        engine_neo_factors_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                            db="neo_factors", charset="utf8mb4")
        balance_sheet_sql_stmt = text("""select * from HK_BalanceSheetFull where CompanyCode='{company_code}' and InfoPublDate >= Date('{search_start_time}') and
                            InfoPublDate <= Date('{search_end_time}') and Mark = 2 and (
                            (DateTypeCode=3 and DateType='3个月' and InfoSource in ('第一季报','招股章程'))
                             or (DateTypeCode=6 and DateType='半年度' and InfoSource in ('中期报告','招股章程'))
                             or (DateTypeCode=9 and DateType='9个月' and InfoSource in ('第三季报','招股章程'))
                             or (DateTypeCode=12 and DateType='年度' and InfoSource in ('年度报告','招股章程'))
                             )  order by EndDate;""".format(company_code=company_code, \
                            search_start_time=search_start_time, search_end_time=search_end_time))

        cn_balance_sheet_sql_stmt = text("""select PeriodMark as DateTypeCode, HK_BalanceSheetCN.* from HK_BalanceSheetCN where CompanyCode='{company_code}' and InfoPublDate >= Date('{search_start_time}') and
                            InfoPublDate <= Date('{search_end_time}') and Mark = 2 and Gmark = 2 and (
                            (PeriodMark=3 and InfoSource in ('第一季报','招股章程'))
                            or (PeriodMark=6 and InfoSource in ('中期报告','招股章程'))
                            or (PeriodMark=9 and InfoSource in ('第三季报','招股章程'))
                            or (PeriodMark=12 and InfoSource in ('年度报告','招股章程'))
                            ) order by EndDate;""".format(company_code=company_code, \
                            search_start_time=search_start_time, search_end_time=search_end_time))

        income_statement_sql_stmt = text("""select * from HK_IncomeStatementFull where CompanyCode='{company_code}' and InfoPublDate >= Date('{search_start_time}') and
                            InfoPublDate <= Date('{search_end_time}') and Mark = 2 and (
                            (DateTypeCode=3 and DateType='3个月' and InfoSource in ('第一季报','招股章程'))
                             or (DateTypeCode=6 and DateType='半年度' and InfoSource in ('中期报告','招股章程'))
                             or (DateTypeCode=9 and DateType='9个月' and InfoSource in ('第三季报','招股章程'))
                             or (DateTypeCode=12 and DateType='年度' and InfoSource in ('年度报告','招股章程'))
                             ) order by EndDate;""".format(company_code=company_code, \
                            search_start_time=search_start_time, search_end_time=search_end_time))

        cn_income_statement_sql_stmt = text("""select PeriodMark as DateTypeCode, HK_IncomeStatementCN.* from HK_IncomeStatementCN where CompanyCode='{company_code}' and InfoPublDate >= Date('{search_start_time}') and
                                    InfoPublDate <= Date('{search_end_time}') and Mark = 2 and Gmark = 2 and (
                            (PeriodMark=3 and InfoSource in ('第一季报','招股章程'))
                            or (PeriodMark=6 and InfoSource in ('中期报告','招股章程'))
                            or (PeriodMark=9 and InfoSource in ('第三季报','招股章程'))
                            or (PeriodMark=12 and InfoSource in ('年度报告','招股章程'))
                            ) order by EndDate;""".format(company_code=company_code, \
                                    search_start_time=search_start_time, search_end_time=search_end_time))

        cashflow_statement_sql_stmt = text("""select * from HK_CashFlowStatementFull where CompanyCode='{company_code}' and InfoPublDate >= Date('{search_start_time}') and
                            InfoPublDate <= Date('{search_end_time}') and Mark = 2 and (
                            (DateTypeCode=3 and DateType='3个月' and InfoSource in ('第一季报','招股章程'))
                             or (DateTypeCode=6 and DateType='半年度' and InfoSource in ('中期报告','招股章程'))
                             or (DateTypeCode=9 and DateType='9个月' and InfoSource in ('第三季报','招股章程'))
                             or (DateTypeCode=12 and DateType='年度' and InfoSource in ('年度报告','招股章程'))
                             ) order by EndDate;""".format(company_code=company_code, \
                            search_start_time=search_start_time, search_end_time=search_end_time))

        cn_cashflow_statement_sql_stmt = text("""select PeriodMark as DateTypeCode, HK_CashFlowStatementCN.* from HK_CashFlowStatementCN where CompanyCode='{company_code}' and InfoPublDate >= Date('{search_start_time}') and
                                    InfoPublDate <= Date('{search_end_time}') and Mark = 2 and Gmark = 2 and (
                            (PeriodMark=3 and InfoSource in ('第一季报','招股章程'))
                            or (PeriodMark=6 and InfoSource in ('中期报告','招股章程'))
                            or (PeriodMark=9 and InfoSource in ('第三季报','招股章程'))
                            or (PeriodMark=12 and InfoSource in ('年度报告','招股章程'))
                            ) order by EndDate;""".format(company_code=company_code, \
                                    search_start_time=search_start_time, search_end_time=search_end_time))

        hk_mian_index_stmt = text("""select PeriodMark as DateTypeCode, HK_MainIndex.* from HK_MainIndex where CompanyCode='{company_code}' and BeginDate >= Date('{search_start_time}') and
                            EndDate <= Date('{search_end_time}') and (
                            PeriodMark=12 or PeriodMark=3 or PeriodMark=6 or PeriodMark=9) order by EndDate;""".format(company_code=company_code, \
                                                                                     search_start_time=search_start_time, search_end_time=search_end_time))
        hk_financial_index_stmt = text("""select PeriodMark as DateTypeCode, HK_FinancialIndex.* from HK_FinancialIndex where CompanyCode='{company_code}' and EndDate >= Date('{search_start_time}') and
                            EndDate <= Date('{search_end_time}') and IfAdjusted=2 and (
                            PeriodMark=12 or PeriodMark=3 or PeriodMark=6 or PeriodMark=9) order by EndDate;""".format(company_code=company_code, \
                                                                                     search_start_time=search_start_time, search_end_time=search_end_time))
        hk_sharestru_stmt = text("""select * from  HK_ShareStru where CompanyCode='{company_code}' and EndDate >= Date('{search_start_time}') and
                            EndDate <= Date('{search_end_time}') order by EndDate;""".format(company_code=company_code, \
                                                                                     search_start_time=search_start_time, search_end_time=search_end_time))

        hs_industry_stmt = text("""select * from  hs_industry where CompanyCode='{company_code}' and TradingDay >= Date('{search_start_time}') and
                            TradingDay <= Date('{search_end_time}') order by TradingDay;""".format(company_code=company_code, \
                                                                                     search_start_time=search_start_time, search_end_time=search_end_time))
        engine = create_engine(engine_str)
        engine_neo_factors = create_engine(engine_neo_factors_str)
        balance_sheet_result = pd.read_sql(balance_sheet_sql_stmt, engine)
        cn_balance_sheet_result = pd.read_sql(cn_balance_sheet_sql_stmt, engine)
        income_statement_result = pd.read_sql(income_statement_sql_stmt, engine)
        cn_income_statement_result = pd.read_sql(cn_income_statement_sql_stmt, engine)
        cashflow_statement_result = pd.read_sql(cashflow_statement_sql_stmt, engine)
        cn_cashflow_statement_result = pd.read_sql(cn_cashflow_statement_sql_stmt, engine)
        hk_mian_index_result = pd.read_sql(hk_mian_index_stmt, engine)
        hk_financial_index_result = pd.read_sql(hk_financial_index_stmt, engine)
        hk_sharestru_result = pd.read_sql(hk_sharestru_stmt, engine)
        hs_industry_result = pd.read_sql(hs_industry_stmt, engine_neo_factors)
        # 数据库会计标准：非中国或者中国
        accounting_standard_db = "FULL"
        if balance_sheet_result.shape[0] ==0:
            # 非中国会计标准
            accounting_standard_db = "CN"
            financial_data_dict['Standard'] = accounting_standard_db
        else:
            financial_data_dict['Standard'] = accounting_standard_db

        financial_data_dict['BalanceSheet'] = balance_sheet_result
        financial_data_dict['IncomeStatement'] = income_statement_result
        financial_data_dict['CashFlowStatement'] = cashflow_statement_result
        financial_data_dict['CNBalanceSheet'] = cn_balance_sheet_result
        financial_data_dict['CNIncomeStatement'] = cn_income_statement_result
        financial_data_dict['CNCashFlowStatement'] = cn_cashflow_statement_result
        financial_data_dict['MainIndex'] = hk_mian_index_result
        financial_data_dict['FinancialIndex'] = hk_financial_index_result
        financial_data_dict['ShareStru'] = hk_sharestru_result
        financial_data_dict['HSIndustry'] = hs_industry_result
        return financial_data_dict


    # 分公司处理基本面计算问题
    def traverse_stock_info_df(self):
        stock_info_df = self.stock_info_df
        for i in range(stock_info_df.shape[0]):
            inner_code = stock_info_df['InnerCode'][i]
            company_code = stock_info_df['CompanyCode'][i]
            secu_code = stock_info_df['SecuCode'][i]
            #futu_secu_code = stock_info_df['Futu_SecuCode'][i]
            # 上市往前推三年
            listed_date = stock_info_df['ListedDate'][i] - datetime.timedelta(days=365 * 3)
            delisting_date = stock_info_df['DelistingDate'][i]
            search_end_time = self.end_time_format
            if str(delisting_date) not in ['NaT' , 'nan', 'None']:
                #delisting_date = datetime.datetime.strptime(delisting_date_str, '%Y-%m-%d')
                search_end_time = self.end_time_format if self.start_time_format <= delisting_date else delisting_date
            #listed_date = datetime.datetime.strptime(listed_date_str, '%Y-%m-%d')
            search_start_time = self.start_time_format if self.start_time_format >= listed_date else listed_date
            search_start_time_str = search_start_time.strftime('%Y-%m-%d')
            search_end_time_str = search_end_time.strftime('%Y-%m-%d')
            #print(search_start_time_str, search_end_time_str)
            stock_info_dict = dict()
            stock_info_dict['InnerCode'] = inner_code
            stock_info_dict['CompanyCode'] = company_code
            stock_info_dict['SecuCode'] = secu_code
            #stock_info_dict['Futu_SecuCode'] = futu_secu_code
            stock_info_dict['Start_Time'] = search_start_time_str
            stock_info_dict['End_Time'] = search_end_time_str
            stock_info_dict['Currency_Data'] = dict()
            stock_info_dict['Currency_Temp'] = dict()
            #print(stock_info_dict)
            self.calculate_factors(stock_info_dict)
        pass


    def save_error_code(self, code, error):
        day_str = datetime.datetime.now().strftime('%Y-%m-%d')
        file_name = os.getcwd() + r"/log_files/error_log/" + day_str +"_" +code +"_fd_" + ".log"
        hs = open(file_name, "a+")
        hs.write("\n"+ str(code)+ "\terror\t" + error)
        hs.close()

    # 在内存中做因子计算
    # 因子分类,假设是6大类
    # 按类做多进程计算
    def calculate_factors(self, stock_info_dict):
        try:
            inner_code = stock_info_dict['InnerCode']
            company_code = stock_info_dict['CompanyCode']
            secu_code = stock_info_dict['SecuCode']
            #futu_secu_code = stock_info_dict['Futu_SecuCode']
            search_start_time = stock_info_dict['Start_Time']
            search_end_time = stock_info_dict['End_Time']
            financial_data_dict = self.get_financial_data_from_db(company_code, search_start_time, search_end_time)

            #以资产负债表为基准, 得到EndDate
            income_statement_result = financial_data_dict['IncomeStatement']
            #print(balance_sheet_result)
            #balance_sheet_result.to_csv("../../test_files/balance_sheet.csv", index=False)
            temp_income_statement_result = income_statement_result[income_statement_result['InfoPublDate']>= self.start_time_format]
            #print(temp_balance_sheet_result)
            end_date_list = temp_income_statement_result['EndDate'].tolist()
            end_date_list_str = [self.convert_datetime_to_string(end_time) for end_time in end_date_list]


            # 执行
            factor_calculate_obj = FactorsCalculate(stock_info_dict, financial_data_dict)
            factor_calculate_obj.calculate_factors()
        except Exception as err:
            self.save_error_code(secu_code, "Fundamental: " + str(err))






    #日期转化为字符
    def convert_datetime_to_string(self, datetime_foramt):
        datetime_str = ""
        datetime_str = datetime_foramt.strftime('%Y-%m-%d')
        return datetime_str

    # 字符转化为日期
    def convert_string_to_datetime(self, datetime_str):
        datetime_foramt = datetime.datetime.min
        datetime_foramt = datetime.datetime.strptime(datetime_str, '%Y-%m-%d')
        return datetime_foramt





if __name__=="__main__":
    stock_info_df = pd.read_csv("../../test_files/stock_info_tencent.csv", engine='python')
    #print(stock_info_df)
    start_time = "2006-01-01"
    end_time = "2018-01-01"
    obj = FundamentalData(stock_info_df, start_time, end_time)
    #company_code = '1000546'
    #stock_info_dict = {'InnerCode': 1000546, 'CompanyCode': 1000546, 'SecuCode': 700, 'Futu_SecuCode': 700, 'Start_Time': '2008-01-01', 'End_Time': '2010-01-01'}
    #print(obj.get_finnacial_statement_from_db(company_code, start_time, end_time))
    #obj.calculate_factors(stock_info_dict)
    obj.traverse_stock_info_df()
    '''
    cfg = ConfigParser()
    cfg.read('config/config.ini')
    #test = cfg.get('sql', 'hk_financial_index_sql')
    print(cfg.sections())
    '''











