#!/usr/bin/env python
# encoding: utf-8
# copyright reserved by futu5.com

"""
@version: 2.0
@author: alienz
@license: Copywright Reserved by Futu
@contact: limingxia07@gmail.com
@site: http://www.futu5.com
@software: PyCharm
@file: adjfactors.py
@time: 2018/6/7 10:36
"""
from sqlalchemy import text
from .db_info import get_engine
from datetime import datetime,timedelta
import pandas as pd
import math
import configparser

def chunks(l, sub_lenth=None, chunk_num=None):
    """Yield successive n-sized chunks from l."""
    if chunk_num is None:
        for i in range(0, len(l), sub_lenth):
            yield l[i:i + sub_lenth]
    else:
        sub_lenth = int(math.floor(len(l)/chunk_num))
        for i in range(0, len(l), sub_lenth):
            yield l[i:i + sub_lenth]


def convert_secucode(code):
    """jy secu code to futu secu code"""
    if len(code) > 5:
        return 0
    else:
        return int(code)


def get_trading_days(market, start_date=None,end_date=None,offset=0):
    """
    trading day between
    :param market:
    :param start_date:
    :param end_date:
    :param offset:
    :return:
    """
    # TODO: after filling in HK daily quote
    # TODO: us and cn
    if start_date is None:
        start_date = datetime(year=2005, month=1, day=1).date()
    else:
        start_date = datetime.strptime(str(start_date), "%Y-%m-%d").date()
    if end_date is None or len(end_date) != 0:
        end_date = datetime.today().date()
    else:
        end_date = datetime.strptime(str(end_date), "%Y-%m-%d").date()
    if offset > 0:
        end_date = end_date + timedelta(days=offset)
    elif offset < 0:
        start_date = start_date + timedelta(days=offset)
    else:
        pass
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        table = "QT_HKDailyQuote"
        config = configparser.ConfigParser()
        config.read("config.ini")
        stmt = config["sqls"]["get_trading_days"]
        stmt = text(stmt.format(table=table))
        stmt = stmt.bindparams(start_date=start_date,end_date=end_date)
        df = pd.read_sql(stmt,engine)
        if df.empty:
            return list()
        else:
            return df["TradingDay"].tolist()
    else:
        raise ValueError(market)


def get_code_list(market, sp_date, field="all", start_date=None, drop_duplicate=True):
    """
    inner code list
    :param market:
    :param sp_date:
    :param field:
    :param start_date
    :param drop_duplicate
    :return:
    """
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        table = "HK_SecuMain"
        if start_date is None:
            stmt = text("SELECT InnerCode,CompanyCode,SecuCode,SecuCode FROM HK_SecuMain WHERE "
                        "SecuCategory in (3,51,52,53) AND ListedDate IS NOT NULL AND "
                        "ListedDate<=:sp_date AND (DelistingDate is NULL "
                        "OR DelistingDate > :sp_date) ORDER BY ListedDate".format(table=table))
            stmt = stmt.bindparams(sp_date=sp_date)
        else:
            stmt = text("SELECT InnerCode,CompanyCode,SecuCode,SecuCode FROM HK_SecuMain WHERE "
                        "SecuCategory in (3,51,52,53) AND ListedDate IS NOT NULL AND "
                        "ListedDate<=:sp_date AND (DelistingDate is NULL "
                        "OR DelistingDate > :start_date) ORDER BY ListedDate".format(table=table))
            stmt = stmt.bindparams(sp_date=sp_date,start_date=start_date)

        df_ret = pd.read_sql(stmt,engine)
        if drop_duplicate:
            df_ret.drop_duplicates(subset=["CompanyCode"],keep="last",inplace=True)
    else:
        # TODO: other market
        raise ValueError(market)
    if field == "all":
        return df_ret
    else:
        return df_ret[field].tolist()


def get_transfer_plate_stock(market):
    """
    transfer plate stock
    :param market:
    :return:
    """
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        stmt = text("SELECT InnerCode, CompanyCode, SecuCode, ChiName, ListedDate"
                    ",DelistingDate, ListedSector,ListedState "
                    "FROM HK_SecuMain WHERE CompanyCode in (SELECT CompanyCode FROM HK_SecuMain "
                    "WHERE SecuCategory in (3,51,52,53) AND ListedDate IS NOT NULL AND (ListedDate < DelistingDate "
                    "OR DelistingDate IS NULL) GROUP BY CompanyCode HAVING count(InnerCode)>1 "
                    "AND sum(ListedSector)=7) AND SecuCategory in (3,51,52,53) ORDER BY CompanyCode,ListedDate")

        df_ret = pd.read_sql(stmt, engine)
        return df_ret
    else:
        raise ValueError(market)


def get_industry_list(market, sp_date, level=0):
    """
    industry list
    :param market:
    :param level:
    :param sp_date:
    :return:
    """
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        config = configparser.ConfigParser()
        config.read("config.ini")
        stmt = config["sqls"]["get_industry_list"]
        stmt = text(stmt)
        stmt = stmt.bindparams(sp_date=sp_date)
        df = pd.read_sql(stmt,engine)
        if level != 0:
            df = df[df["level"] == level]
        return df
    else:
        raise ValueError(market)


def get_industry_component(market, sp_date, industry_list=None, level=None):
    """
    industry component for sp date
    :param market:
    :param sp_date:
    :param industry_list:
    :param level:
    :return:
    """
    if industry_list is None and level is None:
        raise ValueError(industry_list, level)
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        if industry_list is None:
            df_industry = get_industry_list(market, sp_date, level)
            industry_list = df_industry["IndustryNum"].tolist()

        config = configparser.ConfigParser()
        config.read("config.ini")
        stmt = config["sqls"]["get_industry_component"]
        stmt = text(stmt)
        stmt = stmt.bindparams(industry_list=industry_list, sp_date=sp_date)
        df_info = pd.read_sql(stmt, engine)
        return df_info


def get_gics_industry(market, sp_date, industry_list=None, level=None):
    """GICS industry from jy"""
    df_ret = pd.DataFrame()
    if industry_list is None and level is None:
        raise ValueError(industry_list, level)
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        if industry_list is None:
            if level != 1:
                raise ValueError(level)
            config = configparser.ConfigParser()
            config.read("config.ini")
            stmt = text(config["sqls"]["get_gics_industry1"]).bindparams(sp_date=sp_date)
            df_industry = pd.read_sql(stmt,engine)
            if df_industry.empty:
                return df_ret
            stmt = text(config["sqls"]["get_gics_industry2"]).bindparams(sp_date=sp_date)
            df_industry_name = pd.read_sql(stmt,engine)
            df_industry = pd.merge(df_industry,df_industry_name,how="inner",on=["IndustryCode"])
            stmt = text(config["sqls"]["get_gics_industry3"]).bindparams(
                sp_date=sp_date, industry_num_list=df_industry["IndustryNum"].tolist())
            df_industry_components = pd.read_sql(stmt,engine)
            df_ret = pd.merge(df_industry_components,df_industry,how="inner", on=["IndustryNum"])
            df_ret.loc[:,"IndustryLevel"] = 1
            df_ret.loc[:,"TradingDay"] = pd.to_datetime(sp_date)
            df_ret.drop(columns=["IndustryCode"],inplace=True)
            return df_ret


def convert_code(market, in_code, input_key="InnerCode", sp_date=None):
    """
    convert code in different format for sp date
    :param market:
    :param in_code:
    :param input_key:
    :param sp_date:
    :return:
    """
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        if type(in_code) is not list:
            in_code = [in_code]
        regexp = "|".join(in_code)
        stmt = text("SELECT InnerCode,CompanyCode,SecuCode,ListedDate,DelistingDate FROM HK_SecuMain WHERE SecuCode "
                    "REGEXP :regexp AND ListedDate <=:sp_date AND (DelistingDate IS NULL OR "
                    "DelistingDate >:sp_date) AND SecuCategory in (3,51,52,53)".format(input_key=input_key))
        stmt = stmt.bindparams(sp_date=sp_date,regexp=regexp)
        df = pd.read_sql(stmt,engine)
    else:
        raise ValueError(market)
    return df


def get_index_info(market):
    """
    indexes from hk market
    :param market:
    :return:
    """
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        stmt = text("SELECT DISTINCT(IndexInnerCode), b.SecuCode, b.ChiName FROM HK_IndexComponent a, HK_SecuMain b "
                    "WHERE a.IndexInnerCode=b.InnerCode")
        df = pd.read_sql(stmt, engine)
        return df
    else:
        raise ValueError(market)


def get_index_component(market,sp_date,index_value,kw="IndexInnerCode"):
    """
    return dataframe with component secuinnercode
    :param market:
    :param sp_date:
    :param index_value:
    :param kw:
    :return:
    """
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        stmt = text("SELECT SecuInnerCode, InDate, OutDate, Flag, SecuCode FROM (SELECT a.IndexInnerCode, "
                    "a.SecuInnerCode, a.InDate, a.OutDate, a.Flag, a.SecuCode, b.SecuCode, b.ChiName "
                    "FROM HK_IndexComponent a, HK_SecuMain b WHERE a.IndexInnerCode=b.InnerCode AND Indate <= :sp_date"
                    " AND (OutDate >:sp_date OR OutDate IS NULL)) c WHERE c.{field}=:index_value".format(field=kw))
        stmt = stmt.bindparams(sp_date=sp_date,index_value=index_value)
        df = pd.read_sql(stmt, engine)
        return df
    else:
        raise ValueError(market)


def get_index_weight(market, sp_date, index_value,kw="IndexInnerCode"):
    """
    TODO: fill up weight for date before "2015-02-02"
    :param market:
    :param sp_date:
    :param index_value:
    :param kw:
    :return:
    """
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        stmt = text("SELECT c.InnerCode, Weight, SecuCode FROM (SELECT a.InnerCode, Weight FROM HK_IndexCPsWeight "
                    "a, HK_SecuMain b WHERE {field}=:index_value AND EndDate =:sp_date AND a.IndexCode=b.innercode) c,"
                    " HK_SecuMain d WHERE c.InnerCode=d.InnerCode".format(field=kw))
        stmt = stmt.bindparams(sp_date=sp_date,index_value=index_value)
        df = pd.read_sql(stmt,engine)
        return df
    else:
        raise ValueError(market)


def componet_change_alert(market):
    """
    to change list
    :param market:
    :return:
    """
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        watchlist = [1001098]
        stmt = text("(SELECT SecuInnerCode,SecuCode,InDate,OutDate,UpdateTime, 1 as type FROM HK_IndexComponent "
                    "WHERE InDate > now() AND IndexInnerCode in :watchlist) UNION (SELECT SecuInnerCode,SecuCode,"
                    "InDate,OutDate,UpdateTime, 2 as type FROM HK_IndexComponent WHERE Flag=1 AND OutDate is NOT NULL "
                    "AND IndexInnerCode in :watchlist)")
        stmt = stmt.bindparams(watchlist=watchlist)
        df = pd.read_sql(stmt, engine)
        return df
    else:
        raise ValueError(market)


def get_adjfactor_data(market, sp_date, code_list=None, start_date=None):
    """
    TODO: add tranfer plate info
    :param market:
    :param sp_date:
    :param code_list:
    :param start_date:
    :return:
    """
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        if start_date is None:
            start_date = "2005-01-01"
        if code_list is None:
            stmt = text("""SELECT a.*, CompanyCode,SecuCode,SecuCode FROM
    (SELECT a.InnerCode, a.EffectDate,CombinationX, CombinationY, SplitX, SplitY,CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY,OfferRatioX,OfferRatioY,IssuePrice,LastDay,b.ClosePrice as LastClose FROM
    (SELECT a.InnerCode, a.EffectDate,CombinationX, CombinationY, SplitX, SplitY,CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY,OfferRatioX,OfferRatioY,IssuePrice, max(TradingDay) as LastDay FROM
    (SELECT a.InnerCode,a.EffectDate,CombinationX, CombinationY, SplitX, SplitY,CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY,OfferRatioX,OfferRatioY,IssuePrice FROM
    (SELECT DISTINCT InnerCode, EffectDate FROM HK_SplitCombinationShare WHERE Process=3131 AND EffectDate BETWEEN :start_date AND :sp_date
    UNION DISTINCT
    SELECT DISTINCT InnerCode, ExDate as EffectDate FROM HK_Dividend WHERE Process=1004 AND ExDate IS NOT NULL AND ExDate BETWEEN :start_date AND :sp_date AND (DividendBase IS NULL OR DividendBase=10)
    UNION DISTINCT
    SELECT DISTINCT InnerCode, ExDiviDate as EffectDate FROM HK_ShareIPO WHERE IssueType=3 AND Process in (3131, 1004) AND ExDiviDate BETWEEN :start_date AND :sp_date AND OfferRatioX IS NOT NULL AND OfferRatioY IS NOT NULL) a
    LEFT OUTER JOIN 
    (SELECT InnerCode, EffectDate, CombinationX, CombinationY, SplitX, SplitY FROM HK_SplitCombinationShare WHERE EffectDate IS NOT NULL AND Process=3131 AND EffectDate BETWEEN :start_date AND :sp_date) b
    ON a.InnerCode=b.InnerCode AND a.EffectDate=b.EffectDate
    LEFT OUTER JOIN
    (SELECT InnerCode, ExDate as EffectDate, CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY FROM HK_Dividend WHERE Process=1004 AND IfDividend=1 AND ExDate IS NOT NULL AND ExDate BETWEEN :start_date AND :sp_date AND (dividendbase IS NULL OR DividendBase=10)) c 
    ON a.InnerCode=c.InnerCode AND a.EffectDate=c.EffectDate
    LEFT OUTER JOIN
    (SELECT InnerCode,ExDiviDate as EffectDate,OfferRatioX,OfferRatioY,IssuePrice FROM HK_ShareIPO WHERE IssueType=3 AND Process in (3131,1004) AND ExDiviDate BETWEEN :start_date AND :sp_date AND OfferRatioX IS NOT NULL AND OfferRatioY IS NOT NULL) d 
    ON a.InnerCode=d.InnerCode AND a.EffectDate=d.EffectDate) a,
    QT_HKDailyQuote b WHERE a.InnerCode=b.InnerCode AND TradingDay < a.EffectDate AND ClosePrice IS NOT NULL AND ClosePrice>0 GROUP BY a.InnerCode, a.EffectDate) a, QT_HKDailyQuote b WHERE a.InnerCode=b.InnerCode AND a.LastDay=b.TradingDay) a,
HK_SecuMain b WHERE a.InnerCode=b.InnerCode AND b.SecuCategory in (3,51,52,53) AND (b.DelistingDate IS NULL OR b.DelistingDate>:sp_date) AND b.ListedDate<=:sp_date""")
            stmt = stmt.bindparams(start_date=start_date,sp_date=sp_date)
        else:
            stmt = text("""SELECT a.*, CompanyCode,SecuCode,SecuCode FROM
                (SELECT a.InnerCode, a.EffectDate,CombinationX, CombinationY, SplitX, SplitY,CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY,OfferRatioX,OfferRatioY,IssuePrice,LastDay,b.ClosePrice as LastClose FROM
                (SELECT a.InnerCode, a.EffectDate,CombinationX, CombinationY, SplitX, SplitY,CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY,OfferRatioX,OfferRatioY,IssuePrice, max(TradingDay) as LastDay FROM
                (SELECT a.InnerCode,a.EffectDate,CombinationX, CombinationY, SplitX, SplitY,CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY,OfferRatioX,OfferRatioY,IssuePrice FROM
                (SELECT DISTINCT InnerCode, EffectDate FROM HK_SplitCombinationShare WHERE Process=3131 AND EffectDate BETWEEN :start_date AND :sp_date
                UNION DISTINCT
                SELECT DISTINCT InnerCode, ExDate as EffectDate FROM HK_Dividend WHERE Process=1004 AND ExDate IS NOT NULL AND ExDate BETWEEN :start_date AND :sp_date AND (DividendBase IS NULL OR DividendBase=10)
                UNION DISTINCT
                SELECT DISTINCT InnerCode, ExDiviDate as EffectDate FROM HK_ShareIPO WHERE IssueType=3 AND Process in (3131, 1004) AND ExDiviDate BETWEEN :start_date AND :sp_date AND OfferRatioX IS NOT NULL AND OfferRatioY IS NOT NULL) a
                LEFT OUTER JOIN 
                (SELECT InnerCode, EffectDate, CombinationX, CombinationY, SplitX, SplitY FROM HK_SplitCombinationShare WHERE EffectDate IS NOT NULL AND Process=3131 AND EffectDate BETWEEN :start_date AND :sp_date AND InnerCode in :code_list) b
                ON a.InnerCode=b.InnerCode AND a.EffectDate=b.EffectDate
                LEFT OUTER JOIN
                (SELECT InnerCode, ExDate as EffectDate, CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY FROM HK_Dividend WHERE Process=1004 AND IfDividend=1 AND ExDate IS NOT NULL AND ExDate BETWEEN :start_date AND :sp_date AND (dividendbase IS NULL OR DividendBase=10) AND InnerCode in :code_list) c 
                ON a.InnerCode=c.InnerCode AND a.EffectDate=c.EffectDate
                LEFT OUTER JOIN
                (SELECT InnerCode,ExDiviDate as EffectDate,OfferRatioX,OfferRatioY,IssuePrice FROM HK_ShareIPO WHERE IssueType=3 AND Process in (3131,1004) AND ExDiviDate BETWEEN :start_date AND :sp_date AND OfferRatioX IS NOT NULL AND OfferRatioY IS NOT NULL AND InnerCode in :code_list) d 
                ON a.InnerCode=d.InnerCode AND a.EffectDate=d.EffectDate) a,
                QT_HKDailyQuote b WHERE a.InnerCode=b.InnerCode AND TradingDay < a.EffectDate AND ClosePrice IS NOT NULL AND ClosePrice>0  GROUP BY a.InnerCode, a.EffectDate) a, QT_HKDailyQuote b WHERE a.InnerCode=b.InnerCode AND a.LastDay=b.TradingDay) a,
            HK_SecuMain b WHERE a.InnerCode=b.InnerCode AND b.SecuCategory in (3,51,52,53) AND (b.DelistingDate IS NULL OR b.DelistingDate>:sp_date) AND b.ListedDate<=:sp_date AND a.InnerCode in :code_list""")
            stmt = stmt.bindparams(start_date=start_date, sp_date=sp_date, code_list=code_list)
        df = pd.read_sql(stmt,engine)
        # add transfer plate
        df_transfer_plate = get_transfer_plate_stock(market)
        inner_code_list = df["InnerCode"].unique()
        df_add = pd.DataFrame()
        for code in df_transfer_plate["InnerCode"]:
            if code in inner_code_list:
                company_code = df_transfer_plate[df_transfer_plate["InnerCode"] == code]["CompanyCode"].values[0]
                df_codes = df_transfer_plate[df_transfer_plate["CompanyCode"] == company_code]
                another_code_list = list()
                for another_code in df_codes["InnerCode"]:
                    if another_code != code:
                        another_code_list.append(another_code)
                if len(another_code_list) == 0:
                    raise ValueError(code)
                else:
                    stmt = text("""SELECT a.*, CompanyCode,SecuCode,SecuCode FROM
                                    (SELECT a.InnerCode, a.EffectDate,CombinationX, CombinationY, SplitX, SplitY,CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY,OfferRatioX,OfferRatioY,IssuePrice,LastDay,b.ClosePrice as LastClose FROM
                                    (SELECT a.InnerCode, a.EffectDate,CombinationX, CombinationY, SplitX, SplitY,CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY,OfferRatioX,OfferRatioY,IssuePrice, max(TradingDay) as LastDay FROM
                                    (SELECT a.InnerCode,a.EffectDate,CombinationX, CombinationY, SplitX, SplitY,CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY,OfferRatioX,OfferRatioY,IssuePrice FROM
                                    (SELECT DISTINCT InnerCode, EffectDate FROM HK_SplitCombinationShare WHERE Process=3131 AND EffectDate BETWEEN :start_date AND :sp_date
                                    UNION DISTINCT
                                    SELECT DISTINCT InnerCode, ExDate as EffectDate FROM HK_Dividend WHERE Process=1004 AND ExDate IS NOT NULL AND ExDate BETWEEN :start_date AND :sp_date AND (DividendBase IS NULL OR DividendBase=10)
                                    UNION DISTINCT
                                    SELECT DISTINCT InnerCode, ExDiviDate as EffectDate FROM HK_ShareIPO WHERE IssueType=3 AND Process in (3131, 1004) AND ExDiviDate BETWEEN :start_date AND :sp_date AND OfferRatioX IS NOT NULL AND OfferRatioY IS NOT NULL) a
                                    LEFT OUTER JOIN 
                                    (SELECT InnerCode, EffectDate, CombinationX, CombinationY, SplitX, SplitY FROM HK_SplitCombinationShare WHERE EffectDate IS NOT NULL AND Process=3131 AND EffectDate BETWEEN :start_date AND :sp_date) b
                                    ON a.InnerCode=b.InnerCode AND a.EffectDate=b.EffectDate
                                    LEFT OUTER JOIN
                                    (SELECT InnerCode, ExDate as EffectDate, CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY FROM HK_Dividend WHERE Process=1004 AND IfDividend=1 AND ExDate IS NOT NULL AND ExDate BETWEEN :start_date AND :sp_date AND (dividendbase IS NULL OR DividendBase=10)) c 
                                    ON a.InnerCode=c.InnerCode AND a.EffectDate=c.EffectDate
                                    LEFT OUTER JOIN
                                    (SELECT InnerCode,ExDiviDate as EffectDate,OfferRatioX,OfferRatioY,IssuePrice FROM HK_ShareIPO WHERE IssueType=3 AND Process in (3131,1004) AND ExDiviDate BETWEEN :start_date AND :sp_date AND OfferRatioX IS NOT NULL AND OfferRatioY IS NOT NULL) d 
                                    ON a.InnerCode=d.InnerCode AND a.EffectDate=d.EffectDate) a,
                                    QT_HKDailyQuote b WHERE a.InnerCode=b.InnerCode AND TradingDay < a.EffectDate AND ClosePrice IS NOT NULL AND ClosePrice>0  GROUP BY a.InnerCode, a.EffectDate) a, QT_HKDailyQuote b WHERE a.InnerCode=b.InnerCode AND a.LastDay=b.TradingDay) a,
                                HK_SecuMain b WHERE a.InnerCode=b.InnerCode AND b.SecuCategory in (3,51,52,53) AND (b.DelistingDate IS NULL OR b.DelistingDate>:sp_date) AND b.ListedDate<=:sp_date AND a.InnerCode in :code_list""")
                    stmt = stmt.bindparams(start_date=start_date, sp_date=sp_date, code_list=another_code_list)
                    df_tp = pd.read_sql(stmt,engine)
                    df_tp["InnerCode"] = code
                    df_add = df_add.append(df_tp,ignore_index=True)
        df = df.append(df_add,ignore_index=True)
        df.sort_values(by=["EffectDate"],inplace=True)
        return df
    else:
        raise ValueError(market)


def get_adjfactor(market, sp_date, code_list=None, start_date=None):
    """
    adjfactor
    :param market:
    :param sp_date:
    :param code_list:
    :param start_date:
    :return:
    """
    if market == "HK":
        if start_date is None:
            start_date = "2005-01-01"
        df_info = get_adjfactor_data(market,sp_date,code_list, start_date)
        if df_info.empty:
            df_ret = pd.DataFrame(columns=["InnerCode","EffectDate","adj_factor"])
            return df_ret
        # fill in data
        # 1. combination and spilt
        values = {"CombinationX":1,"CombinationY":1,"SplitX":1,"SplitY":1,"CashDividendPSHKD":0,"SpecialDividendPSHKD":0
                  ,"ShareDividendRateX":1,"ShareDividendRateY":0,"TransferRatX":1,"TransferRatY":0,"OfferRatioX":1,
                  "OfferRatioY":0,"IssuePrice":0}
        df_info.fillna(value=values,inplace=True)
        if code_list is not None:
            df_info = df_info[df_info["InnerCode"].isin(code_list)]
        else:
            pass
        df_info.loc[:,"adj"] = df_info["CombinationY"] * df_info["SplitY"] * \
                               (df_info["LastClose"] + df_info["CashDividendPSHKD"] + df_info["SpecialDividendPSHKD"])\
                               * (df_info["ShareDividendRateX"] + df_info["ShareDividendRateY"]) * \
                               (df_info["TransferRatX"] + df_info["TransferRatY"]) * \
                               (df_info["OfferRatioX"] + df_info["OfferRatioY"]) / \
                               (df_info["CombinationX"] * df_info["SplitX"] * df_info["ShareDividendRateX"] *
                                df_info["TransferRatX"] * (df_info["OfferRatioX"] * df_info["LastClose"] +
                                                           (df_info["OfferRatioY"] * df_info["IssuePrice"])))
        df_adj = pd.DataFrame()
        for code, df in df_info.groupby(["InnerCode"]):
            df_adj_add = df[["InnerCode","EffectDate","adj"]]
            df_adj_add = df_adj_add.sort_values(by=["EffectDate"])
            df_adj_add = df_adj_add.assign(**{"adj_factor": df_adj_add["adj"].cumprod()})
            # TODO: fix bug for closeprice=0 AND fill in the correct price
            # df_has_inf = df_adj_add[np.isinf(df_adj_add["adj_factor"])]
            # df_has_inf_info = pd.read_pickle("df_has_inf.pickle")
            # df_has_inf_info = df_has_inf_info.append(df_has_inf, ignore_index=True)
            # df_has_inf_info.drop_duplicates(subset=["InnerCode", "EffectDate"], keep="last", inplace=True)
            # df_has_inf_info.to_pickle("df_has_inf.pickle")
            # df_adj_add.replace([np.inf, -np.inf], np.nan, inplace=True)
            # df_adj_add.fillna(method="ffill", inplace=True)
            df_adj = df_adj.append(df_adj_add,ignore_index=True)
        df_adj = df_adj.loc[:,["InnerCode","EffectDate","adj_factor"]]
        return df_adj
    else:
        raise ValueError(market)


def get_temp_par_trade_data(market, code_list=None, start_date=None,end_date=None,offset=0):
    """

    :param market:
    :param code_list:
    :param start_date:
    :param end_date:
    :param offset:
    :return:
    """
    if start_date is None or len(str(start_date)) != 10:
        start_date = datetime(year=2005, month=1, day=1).date()
    else:
        start_date = datetime.strptime(str(start_date), "%Y-%m-%d").date()
    if end_date is None:
        end_date = datetime.today().date()
    else:
        end_date = datetime.strptime(str(end_date), "%Y-%m-%d").date()
    if offset > 0:
        end_date = end_date + timedelta(days=offset)
    elif offset < 0:
        start_date = start_date + timedelta(days=offset)
    else:
        pass
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        if code_list is None:
            stmt = text("SELECT a.InnerCode, CompanyCode, SecuCode,TradingDay, OpenPrice,ClosePrice,HighPrice,"
                        "LowPrice,TurnoverValue,TurnoverVolume,Lot, Mark FROM (SELECT b.InnerCode,TradingDay, "
                        "ClosePrice,HighPrice,LowPrice,OpenPrice,TurnoverValue,TurnoverVolume,Lot, Mark FROM "
                        "QT_HKDailyQuote a, (SELECT InnerCode, TempInnerCode, TempTradeBeginDate, SimulTradeEndDate, "
                        "EventType, SimulTradeBeginDate FROM HK_TempParTrade WHERE EventType <> 1) b WHERE "
                        "a.InnerCode=b.TempInnerCode AND TradingDay BETWEEN TempTradeBeginDate AND SimulTradeEndDate "
                        "AND TradingDay BETWEEN :start_date AND :end_date ORDER BY TradingDay) a, HK_SecuMain b "
                        "WHERE a.InnerCode=b.InnerCode")
            stmt = stmt.bindparams(start_date=start_date,end_date=end_date)
        else:
            stmt = text("SELECT a.InnerCode, CompanyCode, SecuCode,TradingDay, OpenPrice,ClosePrice,HighPrice,"
                        "LowPrice,TurnoverValue,TurnoverVolume,Lot, Mark FROM (SELECT b.InnerCode,TradingDay, "
                        "ClosePrice,HighPrice,LowPrice,OpenPrice,TurnoverValue,TurnoverVolume,Lot, Mark FROM "
                        "QT_HKDailyQuote a, (SELECT InnerCode, TempInnerCode, TempTradeBeginDate, SimulTradeEndDate, "
                        "EventType, SimulTradeBeginDate FROM HK_TempParTrade WHERE EventType <> 1) b WHERE "
                        "a.InnerCode=b.TempInnerCode AND TradingDay BETWEEN TempTradeBeginDate AND SimulTradeEndDate "
                        "AND TradingDay BETWEEN :start_date AND :end_date AND b.InnerCode in :code_list"
                        " ORDER BY TradingDay) a, HK_SecuMain b "
                        "WHERE a.InnerCode=b.InnerCode")
            stmt = stmt.bindparams(start_date=start_date,end_date=end_date,code_list=code_list)
        df_ret = pd.read_sql(stmt,engine)
    else:
        raise ValueError(market)
    return df_ret


def kline_combiner(df_quote, ktype="Day"):
    """
    combline kline for weekly monthly annually
    :param df_quote:
    :param ktype:
    :return:
    """
    if ktype == "Day":
        pass
    else:
        def take_first(array_like):
            try:
                return array_like[0]
            except Exception as e:
                return 0

        def take_last(array_like):
            try:
                return array_like[-1]
            except Exception as e:
                return 0

        if df_quote.index.name != "TradingDay":
            df_quote.loc[:,"TradingDay"] = pd.to_datetime(df_quote["TradingDay"])
            df_quote.set_index("TradingDay", inplace=True)
        df_ret_combined = pd.DataFrame()

        if ktype == "Week":
            for code, df in df_quote.groupby(["InnerCode"]):
                df_to_modify = df[["OpenPrice", "HighPrice", "LowPrice", "ClosePrice", "TurnoverValue"]]
                output = df_to_modify.resample('W',how={'OpenPrice': take_first,'HighPrice': 'max','LowPrice': 'min',
                                                        'ClosePrice': take_last,'TurnoverValue': 'sum'},
                                               loffset=pd.offsets.timedelta(days=-6))  # to put the labels to Monday

                output = output[["OpenPrice", "HighPrice", "LowPrice", "ClosePrice", "TurnoverValue"]]
                output.loc[:,"InnerCode"] = code
                output.loc[:,"CompanyCode"] = df["CompanyCode"].values[0]
                if "SecuCode" in df.columns.values:
                    output.loc[:,"SecuCode"] = df["SecuCode"].values[0]
                if "Futu_SecuCode" in df.columns.values:
                    output.loc[:,"Futu_SecuCode"] = df["Futu_SecuCode"].values[0]
                df_ret_combined = df_ret_combined.append(output)
        elif ktype in ["Month","Year"]:
            freq = ktype[0] + "S"
            for code, df in df_quote.groupby(["InnerCode"]):
                df_to_modify = df[["OpenPrice", "HighPrice", "LowPrice", "ClosePrice", "TurnoverValue"]]
                output = df_to_modify.resample(freq,how={'OpenPrice': take_first,'HighPrice': 'max','LowPrice': 'min',
                                                         'ClosePrice': take_last,'TurnoverValue': 'sum'})

                output = output[["OpenPrice", "HighPrice", "LowPrice", "ClosePrice", "TurnoverValue"]]
                output.loc[:,"InnerCode"] = code
                output.loc[:,"CompanyCode"] = df["CompanyCode"].values[0]
                if "SecuCode" in df.columns.values:
                    output.loc[:,"SecuCode"] = df["SecuCode"].values[0]
                if "Futu_SecuCode" in df.columns.values:
                    output.loc[:,"Futu_SecuCode"] = df["Futu_SecuCode"].values[0]
                df_ret_combined = df_ret_combined.append(output)
        else:
            raise ValueError(ktype)
        df_ret_combined.reset_index(inplace=True)
        df_ret_combined.rename(columns={"index": "TradingDay"}, inplace=True)
        df_quote = df_ret_combined
        df_quote = df_quote[df_quote["ClosePrice"] > 0]
    df_quote.dropna(subset=["HighPrice"], inplace=True)
    if df_quote.index.name == "TradingDay":
        df_quote.reset_index(inplace=True)
    return df_quote


def get_stock_kline(market, code_list, start_date=None,end_date=None,offset=0,ktype="Day",autype=None):
    """
    stock kline from jy db
    :param market:
    :param code_list:
    :param start_date:
    :param end_date:
    :param offset:
    :param ktype:
    :param autype:
    :return:
    """
    if start_date is None:
        start_date = datetime(year=2005, month=1, day=1).date()
    else:
        start_date = datetime.strptime(str(start_date), "%Y-%m-%d").date()
    if end_date is None:
        end_date = datetime.today().date()
    else:
        end_date = datetime.strptime(str(end_date), "%Y-%m-%d").date()
    if offset > 0:
        end_date = end_date + timedelta(days=offset)
    elif offset < 0:
        start_date = start_date + timedelta(days=offset)
    else:
        pass
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        # TODO: add transfer plate code and temp code before combine and split
        if code_list is None:
            if start_date == end_date and offset == 0:
                # fetch kline daily
                stmt = text("SELECT TradingDay,a.InnerCode,CompanyCode,SecuCode,OpenPrice,ClosePrice,HighPrice,"
                            "LowPrice,TurnoverValue,TurnoverVolume,Lot,Mark FROM QT_HKDailyQuote a, "
                            "(SELECT InnerCode, CompanyCode, SecuCode, SecuCode FROM HK_SecuMain "
                            "WHERE SecuCategory in (3,51,52,53) AND ListedDate IS NOT NULL AND ListedDate<=:sp_date"
                            " AND (DelistingDate is NULL OR DelistingDate>:sp_date)) b WHERE a.InnerCode=b.InnerCode"
                            " AND TradingDay BETWEEN :s_date AND :sp_date AND Mark <> 2")
                stmt = stmt.bindparams(sp_date=end_date,s_date=start_date)
            else:
                stmt = text("SELECT TradingDay,a.InnerCode,CompanyCode,SecuCode,OpenPrice,ClosePrice,HighPrice,"
                            "LowPrice,TurnoverValue,TurnoverVolume,Lot,Mark FROM QT_HKDailyQuote a, "
                            "(SELECT InnerCode, CompanyCode, SecuCode, SecuCode FROM HK_SecuMain "
                            "WHERE SecuCategory in (3,51,52,53) AND ListedDate IS NOT NULL AND ListedDate<=:e_date"
                            " AND (DelistingDate is NULL OR DelistingDate>:s_date)) b WHERE a.InnerCode=b.InnerCode"
                            " AND TradingDay BETWEEN :s_date AND :e_date AND Mark <> 2")
                stmt = stmt.bindparams(s_date=start_date,e_date=end_date)
        else:
            # check code list format
            stmt = text("SELECT TradingDay,a.InnerCode,CompanyCode,SecuCode,OpenPrice,ClosePrice,HighPrice,"
                        "LowPrice,TurnoverValue,TurnoverVolume,Lot,Mark FROM QT_HKDailyQuote a, "
                        "(SELECT InnerCode, CompanyCode, SecuCode FROM HK_SecuMain "
                        "WHERE SecuCategory in (3,51,52,53) AND ListedDate IS NOT NULL AND ListedDate<=:e_date"
                        " AND (DelistingDate is NULL OR DelistingDate>:s_date)) b WHERE a.InnerCode=b.InnerCode"
                        " AND TradingDay BETWEEN :s_date AND :e_date AND a.InnerCode in :code_list AND Mark <>2 ")
            stmt = stmt.bindparams(s_date=start_date, e_date=end_date, code_list=code_list)

        df_quote = pd.read_sql(stmt,engine)
        if df_quote.empty:
            df_ret = pd.DataFrame(columns=["InnerCode","TradingDay","CompanyCode","SecuCode","HighPrice",
                                           "LowPrice","ClosePrice","TurnoverValue","TurnoverVolume","Lot",
                                           "adj_factor","Mark"])
            return df_ret
        # transfer plate
        df_transfer_plate = get_transfer_plate_stock(market)
        inner_code_list = df_quote["InnerCode"].unique()
        df_quote_add = pd.DataFrame()
        for code in df_transfer_plate["InnerCode"]:
            if code in inner_code_list:
                company_code = df_transfer_plate[df_transfer_plate["InnerCode"] == code]["CompanyCode"].values[0]
                df_codes = df_transfer_plate[df_transfer_plate["CompanyCode"] == company_code]
                another_code_list = list()
                for another_code in df_codes["InnerCode"]:
                    if another_code != code:
                        another_code_list.append(another_code)
                if len(another_code_list) == 0:
                    raise ValueError(code)
                else:
                    stmt = text(
                        "SELECT TradingDay,:num as InnerCode,CompanyCode,SecuCode,OpenPrice,ClosePrice,HighPrice,"
                        "LowPrice,TurnoverValue,TurnoverVolume,Lot,Mark FROM QT_HKDailyQuote a, "
                        "(SELECT InnerCode, CompanyCode, SecuCode FROM HK_SecuMain "
                        "WHERE SecuCategory in (3,51,52,53) AND ListedDate IS NOT NULL AND ListedDate<=:e_date"
                        " AND (DelistingDate is NULL OR DelistingDate>:s_date)) b WHERE a.InnerCode=b.InnerCode"
                        " AND TradingDay BETWEEN :s_date AND :e_date AND a.InnerCode in :code_list AND Mark <> 2")
                    stmt = stmt.bindparams(s_date=start_date,e_date=end_date,code_list=another_code_list,
                                           num=code)
                    df_quote_tp = pd.read_sql(stmt,engine)
                    df_quote_add = df_quote_add.append(df_quote_tp,ignore_index=True)
        df_quote = df_quote.append(df_quote_add,ignore_index=True)
        df_quote.loc[:,"InnerCode"] = df_quote["InnerCode"].apply(int)
        # temp code
        df_temp_code = get_temp_par_trade_data(market,code_list,start_date,end_date)
        df_merge = pd.merge(df_quote, df_temp_code, how="outer",on=["InnerCode","TradingDay"])
        df_merge["TurnoverValue_x"].fillna(0,inplace=True)
        df_merge["TurnoverValue_y"].fillna(0, inplace=True)
        df_merge["TurnoverVolume_x"].fillna(0, inplace=True)
        df_merge["TurnoverVolume_y"].fillna(0, inplace=True)
        df_merge.loc[:,"TurnoverValue_x"] = df_merge["TurnoverValue_x"].add(df_merge["TurnoverValue_y"],fill_value=0)
        df_merge.loc[:, "TurnoverVolume_x"] = df_merge["TurnoverVolume_x"].add(df_merge["TurnoverVolume_y"],
                                                                               fill_value=0)
        df_merge_left_nan = df_merge[pd.isnull(df_merge["CompanyCode_x"])]
        if not df_merge_left_nan.empty:
            df_merge_left_nan.loc[:, "CompanyCode_x":"Mark_x"] = df_merge_left_nan.loc[:,"CompanyCode_y":"Mark_y"]
            df_merge_left_nan.drop(df_merge_left_nan.columns[2:12],axis=1,inplace=True)
            df_merge_left_nan.rename(columns=lambda x:x.replace("_y",""),inplace=True)
        else:
            df_merge_left_nan = pd.DataFrame()
        df_merge_inner = df_merge[pd.notnull(df_merge["CompanyCode_x"])]
        if not df_merge_inner.empty:
            df_merge_inner.drop(df_merge_inner.columns[12:],axis=1,inplace=True)
            df_merge_inner.rename(columns=lambda x:x.replace("_x",""),inplace=True)
        else:
            df_merge_inner = pd.DataFrame()
        df_quote = df_merge_inner.append(df_merge_left_nan,ignore_index=True)
        df_quote.dropna(subset=["HighPrice"],inplace=True)
        df_quote = df_quote.sort_values(by=["TradingDay"])
        df_quote.drop_duplicates(subset=["InnerCode","TradingDay"],keep="first",inplace=True)
    else:
        raise ValueError(market)

    df_ret = pd.DataFrame()

    if autype is not None:
        # fetch wind adj factor
        if autype == "qfq":
            df_adj = get_adjfactor(market,end_date,code_list,start_date)
            # TODO: fix empty
            if df_adj.empty:
                df_quote.loc[:,"adj_factor"] = 1
                df_ret = df_quote
            else:
                for code, df in df_quote.groupby(["InnerCode"]):
                    if code not in df_adj["InnerCode"].tolist():
                        df_adjprice_add = df
                        df_adjprice_add = df_adjprice_add.assign(**{"adj_factor":[1]*len(df)})
                    else:
                        df_adj_slice = df_adj[df_adj["InnerCode"] == code]
                        df_adj_add = df_adj_slice.rename(columns={"EffectDate":"TradingDay"})
                        df_adjprice_add = pd.merge(df,df_adj_add,how="outer",on=["InnerCode","TradingDay"])
                        df_adjprice_add.sort_values(by=["TradingDay"], inplace=True)
                        df_adjprice_add["adj_factor"].fillna(method="ffill",inplace=True)
                        df_adjprice_add["adj_factor"].fillna(1,inplace=True)
                        df_adjprice_add.dropna(subset=["ClosePrice"],inplace=True)
                    # adjust price
                    # TODO: check cash dividend adjust
                    # TODO: adjust TurnoverVolume according to price
                    adjust_series = df_adjprice_add["adj_factor"] / df_adjprice_add["adj_factor"].values[-1]
                    df_adjprice_add.update(df_adjprice_add.loc[:,["OpenPrice","ClosePrice","HighPrice","LowPrice"]].multiply(
                        adjust_series,axis="index"), overwrite=True)
                    df_ret = df_ret.append(df_adjprice_add,ignore_index=True)
            pass
        elif autype == "hfq":
            # TODO: fixed with similar method
            pass
        else:
            raise ValueError(autype)
    else:
        df_ret = df_quote

    # TODO: filter nan and inf
    # combine kline
    if df_ret.index.name == "TradingDay":
        df_ret.reset_index(inplace=True)
    df_ret = kline_combiner(df_ret,ktype)
    return df_ret


def get_index_kline(market, index_code=None, start_date=None,end_date=None,offset=0,kw="InnerCode",ktype="Day"):
    """
    index kline
    :param market:
    :param index_code:
    :param start_date:
    :param end_date:
    :param offset:
    :param kw:
    :param ktype:
    :return:
    """
    if start_date is None:
        start_date = datetime(year=2005, month=1, day=1).date()
    else:
        start_date = datetime.strptime(str(start_date), "%Y-%m-%d").date()
    if end_date is None or len(end_date) != 0:
        end_date = datetime.today().date()
    else:
        end_date = datetime.strptime(str(end_date), "%Y-%m-%d").date()
    if offset > 0:
        end_date = end_date + timedelta(days=offset)
    elif offset < 0:
        start_date = start_date + timedelta(days=offset)
    else:
        pass
    if market == "HK":
        engine = get_engine("jy_factor_hk")
        if index_code is None:
            # HangSang Index
            index_code, kw = 1001098, "InnerCode"
        target_table = "QT_OSIndexQuote"
        stmt = text("SELECT TradingDay,IndexCode as InnerCode,CompanyCode,SecuCode,OpenPrice,ClosePrice,HighPrice,"
                    "LowPrice,TurnoverValue,TurnoverVolume FROM {target_table} a, "
                    "(SELECT InnerCode, CompanyCode, SecuCode, SecuCode FROM HK_SecuMain "
                    "WHERE {kw}=:index_code) b WHERE IndexCode=b.InnerCode"
                    " AND TradingDay BETWEEN :s_date AND :sp_date "
                    "ORDER BY TradingDay".format(target_table=target_table,kw=kw))
        stmt = stmt.bindparams(sp_date=end_date, s_date=start_date, index_code=index_code)
        df_ret = pd.read_sql(stmt,engine)
    elif market == "US":
        engine = get_engine("jy_factor_hk")
        if index_code is None:
            # S&P 500
            index_code, kw = 3210, "InnerCode"
        target_table = "QT_OSIndexQuote"
        stmt = text("SELECT TradingDay,IndexCode as InnerCode,CompanyCode,SecuCode,OpenPrice,ClosePrice,HighPrice,"
                    "LowPrice,TurnoverValue,TurnoverVolume FROM {target_table} a, "
                    "(SELECT InnerCode, CompanyCode, SecuCode, SecuCode FROM HK_SecuMain "
                    "WHERE {kw}=:index_code) b WHERE IndexCode=b.InnerCode"
                    " AND TradingDay BETWEEN :s_date AND :sp_date "
                    "ORDER BY TradingDay".format(target_table=target_table,kw=kw))
        stmt = stmt.bindparams(sp_date=end_date, s_date=start_date, index_code=index_code)
        df_ret = pd.read_sql(stmt,engine)
    else:
        raise ValueError(market)
    df_ret.reset_index(inplace=True)
    df_ret = kline_combiner(df_ret,ktype)
    return df_ret


def get_stock_structure():
    pass


def get_status():
    # suspension and backdoor
    pass


def get_performance_express():
    pass


if __name__ == '__main__':
    # engine = get_engine(db="fsi_exright")
    # df = get_latest_exright_info(engine,market="US",offset=15)
    # engine = get_engine("neo_factors")
    # dataframe_upsert_auto(df,engine,"exright")
    market = "HK"
    start_date = "2005-01-01"
    end_date = "2018-09-03"
    df = get_index_kline(market,start_date=start_date,end_date=end_date)
    trading_days = get_trading_days(market,start_date,end_date)

    # df = get_gics_industry(market,end_date,level=1)
    # code_list = get_code_list(market,end_date,start_date=start_date)["InnerCode"].tolist()
    # for sub_code_list in list(chunks(code_list,chunk_num=500)):
    #     df = get_stock_kline(market,sub_code_list,start_date,end_date,autype="qfq")
    #     print(df)
    # code_list = [700, 3988, 1666, 922, 2882]
    # code_list = [1000457]
    # innercode_list = convert_secucode(market, code_list, "f2c", end_date)["InnerCode"].tolist()
    # innercode_list = [1000457]
    # df = get_adjfactor(market,end_date,innercode_list)
    # df = get_stock_kline(market, innercode_list, start_date, end_date, ktype="Day")
    # df_index = get_index_kline(market,"恒生指数",start_date,end_date,kw="ChiName",ktype="Month")
    # df = get_temp_par_trade_data(market,innercode_list,start_date,end_date)

    # sp_date = "2017-01-01"
    # get_adjfactor_data(market, end_date)
    # get_adjfactor(market, end_date)
    # trading_days = get_trading_days(market,start_date,end_date)
    # df = get_code_list(market,sp_date)
    # df = get_transfer_plate_stock(market)
    # df = get_industry_list(market,level=1)
    # df = get_industry_component(market,sp_date,level=1)
    # df = componet_change_alert(market)
    print(df)
    # df = get_daily_factor(market,start_date,end_date)
    # df = get_bara_factor("HK")
    # print(df)
