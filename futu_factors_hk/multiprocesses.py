#-*- coding:utf-8 -*-

from module.basic.stock_basic_info_search import *
from module.basic.stock_basic_info import *
from module.process.fundamental.fundamental_data import *
from module.process.volume_price.volume_price_data import *
#from module.basic.currency_convert import *
import logging
import multiprocessing
import threading
import numba

class Multiprocess():
    lock = multiprocessing.RLock()
    def __init__(self,start_time, end_time):
        self.start_time = start_time
        self.end_time = end_time
        pass


    def get_stock_info_secu_code(self, secu_code):
        market_code = "HK"
        obj = StockBasicInfo(market_code, self.start_time, self.end_time, secu_code)
        stock_info_df = obj.get_stock_basic_info()
        return stock_info_df

    def calculate_factors_stock(self, secu_code):
        run_start = datetime.datetime.now()
        print("Caculate stock", secu_code, "Run Start: ", run_start)
        Process_jobs = []
        try:
            stock_info_df = self.get_stock_info_secu_code(secu_code)
            if stock_info_df.empty:
                return
            Process_jobs.append(threading.Thread(target=self.caculate_fundamental_data, args=(stock_info_df,)))
            Process_jobs.append(threading.Thread(target=self.caculate_volumeprice_date, args=(stock_info_df,)))

            for job in Process_jobs:
                job.start()

            for job in Process_jobs:
                job.join()

        except Exception as err:
            Multiprocess.lock.acquire()
            self.save_error_code(secu_code, str(err))
            Multiprocess.lock.release()
        run_end = datetime.datetime.now()
        run_time = (run_end - run_start).seconds
        print("Caculate stock", secu_code, "Run End", run_end, "cost time", run_time)


    def save_error_code(self, code, error):
        day_str = datetime.datetime.now().strftime('%Y-%m-%d')
        file_name = os.getcwd() + r"/log_files/error_log/" + day_str + ".log"
        hs = open(file_name, "a+")
        hs.write("\n"+ str(code)+ "\terror\t" + error)
        hs.close()

    @numba.jit
    def caculate_fundamental_data(self, stock_info_df):
        obj = FundamentalData(stock_info_df, self.start_time, self.end_time)
        obj.traverse_stock_info_df()

    @numba.jit
    def caculate_volumeprice_date(self, stock_info_df):
        obj = VolumePriceData(stock_info_df, self.start_time, self.end_time)
        obj.calculate_factors()

    def calcualte_factors_stock_list(self, secu_code_list):
        run_start = datetime.datetime.now()
        num = len(secu_code_list)
        pool = multiprocessing.Pool(processes=num)
        pool.map(self.calculate_factors_stock, secu_code_list)
        pool.close()
        pool.join()
        run_end = datetime.datetime.now()
        run_time = (run_end - run_start).seconds
        print("Multithreading", run_end, "cost time", run_time)

    def calculate_batch_test(self):
        run_start = datetime.datetime.now()
        num = 2
        stock_list = ['03992']
        pool = multiprocessing.Pool(processes=num)
        pool.map(self.calculate_factors_stock, stock_list)
        pool.close()
        pool.join()
        run_end = datetime.datetime.now()
        run_time = (run_end - run_start).seconds
        print("Multithreading", run_end, "cost time", run_time)


if __name__=="__main__":
    start_time = "2018-12-10"
    end_time = "2019-1-2"
    obj = Multiprocess(start_time, end_time)
    obj.calculate_batch_test()




