#-*- coding: utf-8 -*-

# 跑批

#from main import *
from multiprocesses import *
import configparser
import os
import datetime
import math


class Batch():
    def __init__(self):
        pass

    def batch_run(self):
        run_start = datetime.datetime.now()
        config = configparser.ConfigParser()
        config.read("config/config.ini")
        start_time = str(config['global']['start_date'])
        end_time = str(config['global']['end_date'])
        #start_time = "2005-01-01"
        #end_time = "2018-09-28"
        num = int(config['global']['threads'])
        stock_list = self.get_stock_list()
        run_len = math.ceil(len(stock_list) / num)
        # run_len = len(stock_list)
        for i in range(run_len):
            calculate_list = stock_list[i * num: (i + 1) * num]
            # calculate_list = [stock_list[i]]HK_Growthability
            obj_main = Multiprocess(start_time, end_time)
            obj_main.calcualte_factors_stock_list(calculate_list)

        run_end = datetime.datetime.now()
        run_time = (run_end - run_start).seconds
        print("All Batch Run End", run_end, "cost time", run_time)

    def get_stock_list(self):
        config = configparser.ConfigParser()
        config.read("config/config.ini")
        input_file = str(config['global']['input_file'])
        stock_info = pd.read_csv('./input/files/' + input_file, dtype="str")
        stock_list = stock_info['SecuCode'].tolist()
        return stock_list

if __name__ == "__main__":
    obj = Batch()
    obj.batch_run()