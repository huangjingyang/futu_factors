-- 用来记录那些年跑过得SQL


-- HK Market
-- 读取聚源港股复权数据（不包括转板股票）
SELECT a.*, CompanyCode,SecuCode,Futu_SecuCode FROM
(SELECT a.InnerCode, a.EffectDate,CombinationX, CombinationY, SplitX, SplitY,CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY,OfferRatioX,OfferRatioY,IssuePrice,LastDay,b.ClosePrice as LastClose FROM
(SELECT a.InnerCode, a.EffectDate,CombinationX, CombinationY, SplitX, SplitY,CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY,OfferRatioX,OfferRatioY,IssuePrice, max(TradingDay) as LastDay FROM
(SELECT a.InnerCode,a.EffectDate,CombinationX, CombinationY, SplitX, SplitY,CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY,OfferRatioX,OfferRatioY,IssuePrice FROM
(SELECT DISTINCT InnerCode, EffectDate FROM HK_SplitCombinationShare WHERE Process=3131 AND EffectDate BETWEEN "2005-01-01" AND "2018-08-10"
UNION DISTINCT
SELECT DISTINCT InnerCode, ExDate as EffectDate FROM HK_Dividend WHERE Process=1004 AND ExDate IS NOT NULL AND ExDate BETWEEN "2005-01-01" AND "2018-08-10" AND (DividendBase IS NULL OR DividendBase=10)
UNION DISTINCT
SELECT DISTINCT InnerCode, ExDiviDate as EffectDate FROM HK_ShareIPO WHERE IssueType=3 AND Process in (3131, 1004) AND ExDiviDate BETWEEN "2005-01-01" AND "2018-08-10" AND OfferRatioX IS NOT NULL AND OfferRatioY IS NOT NULL) a
LEFT OUTER JOIN
(SELECT InnerCode, EffectDate, CombinationX, CombinationY, SplitX, SplitY FROM HK_SplitCombinationShare WHERE EffectDate IS NOT NULL AND Process=3131 AND EffectDate BETWEEN "2005-01-01" AND "2018-08-10") b
ON a.InnerCode=b.InnerCode AND a.EffectDate=b.EffectDate
LEFT OUTER JOIN
(SELECT InnerCode, ExDate as EffectDate, CashDividendPSHKD, SpecialDividendPSHKD, ShareDividendRateX,ShareDividendRateY,WarrantDividendRateX,WarrantDividendRateY,PhysicalDividendRateX,PhysicalDividendRateY,TransferRatX,TransferRatY FROM HK_Dividend WHERE Process=1004 AND IfDividend=1 AND ExDate IS NOT NULL AND ExDate BETWEEN "2005-01-01" AND "2018-08-10" AND (dividendbase IS NULL OR DividendBase=10)) c
ON a.InnerCode=c.InnerCode AND a.EffectDate=c.EffectDate
LEFT OUTER JOIN
(SELECT InnerCode,ExDiviDate as EffectDate,OfferRatioX,OfferRatioY,IssuePrice FROM HK_ShareIPO WHERE IssueType=3 AND Process in (3131,1004) AND ExDiviDate BETWEEN "2005-01-01" AND "2018-08-10" AND OfferRatioX IS NOT NULL AND OfferRatioY IS NOT NULL) d
ON a.InnerCode=d.InnerCode AND a.EffectDate=d.EffectDate) a,
QT_HKDailyQuote b WHERE a.InnerCode=b.InnerCode AND TradingDay < a.EffectDate GROUP BY a.InnerCode, a.EffectDate) a, QT_HKDailyQuote b WHERE a.InnerCode=b.InnerCode AND a.LastDay=b.TradingDay) a,
HK_SecuMain b WHERE a.InnerCode=b.InnerCode AND b.SecuCategory in (3,51,52,53) AND (b.DelistingDate IS NULL OR b.DelistingDate>"2018-08-10") AND b.ListedDate<="2018-08-10"

-- Trading Day
SELECT distinct(TradingDay) as TradingDay FROM QT_HKDailyQuoteIndex WHERE TradingDay BETWEEN :start_date AND :end_date ORDER BY TradingDay Desc
-- Code list
SELECT InnerCode as code,CompanyCode,SecuCode, Futu_SecuCode FROM HK_SecuMain WHERE SecuCategory in (3,51,52,53) AND ListedDate IS NOT NULL AND ListedDate<=:sp_date AND (DelistingDate is NULL OR DelistingDate > :sp_date)
-- Transfer plate
SELECT InnerCode, CompanyCode, SecuCode, ChiName, ListedDate,DelistingDate, ListedSector,ListedState FROM HK_SecuMain WHERE CompanyCode in (SELECT CompanyCode FROM HK_SecuMain WHERE SecuCategory in (3,51,52,53) AND ListedDate IS NOT NULL AND (ListedDate < DelistingDate OR DelistingDate IS NULL) GROUP BY CompanyCode HAVING count(InnerCode)>1 AND sum(ListedSector)=7) AND SecuCategory in (3,51,52,53) ORDER BY CompanyCode,ListedDate
-- Industry list
(SELECT distinct(IndustryNum),IndustryName,IndustryCode,FatherIndustryCode, 1 as level FROM HK_IndustryCategory WHERE Standard=100 AND FatherIndustryCode IS NULL) UNION (SELECT distinct(IndustryNum), IndustryName, IndustryCode,FatherIndustryCode, 2 as level FROM HK_IndustryCategory WHERE FatherIndustryCode in (SELECT distinct(IndustryCode) FROM HK_IndustryCategory WHERE Standard=100 AND FatherIndustryCode IS NULL)) UNION (SELECT DISTINCT(IndustryNum), IndustryName, IndustryCode, FatherIndustryCode, 3 as level FROM HK_IndustryCategory WHERE FatherIndustryCode IN (SELECT distinct(IndustryCode) FROM HK_IndustryCategory WHERE FatherIndustryCode in (SELECT distinct(IndustryCode) FROM HK_IndustryCategory WHERE Standard=100 AND FatherIndustryCode IS NULL)))
-- Industry component
SELECT SecuInnerCode, InDate, OutDate, Flag, Futu_SecuCode FROM (SELECT a.IndexInnerCode, a.SecuInnerCode, a.InDate, a.OutDate, a.Flag, a.Futu_SecuCode, b.SecuCode, b.ChiName FROM HK_IndexComponent a, HK_SecuMain b WHERE a.IndexInnerCode=b.InnerCode AND Indate <= :sp_date AND (OutDate >:sp_date OR OutDate IS NULL)) c WHERE c.{field}=:index_value
-- Industry weight
SELECT c.InnerCode, Weight, Futu_SecuCode FROM (SELECT a.InnerCode, Weight FROM HK_IndexCPsWeight a, HK_SecuMain b WHERE {field}=:index_value AND EndDate =:sp_date AND a.IndexCode=b.innercode) c, HK_SecuMain d WHERE c.InnerCode=d.InnerCode
-- Component change
(SELECT SecuInnerCode,Futu_SecuCode,InDate,OutDate,UpdateTime, 1 as type FROM HK_IndexComponent WHERE InDate > now() AND IndexInnerCode in :watchlist) UNION (SELECT SecuInnerCode,Futu_SecuCode,InDate,OutDate,UpdateTime, 2 as type FROM HK_IndexComponent WHERE Flag=1 AND OutDate is NOT NULL AND IndexInnerCode in :watchlist)
-- TempParTrade
SELECT a.InnerCode, CompanyCode, Futu_SecuCode,TradingDay, OpenPrice,ClosePrice,HighPrice,LowPrice,TurnoverValue,TurnoverVolume,Lot, Mark FROM (SELECT b.InnerCode,TradingDay, ClosePrice,HighPrice,LowPrice,OpenPrice,TurnoverValue,TurnoverVolume,Lot, Mark FROM QT_HKDailyQuote a, (SELECT InnerCode, TempInnerCode, TempTradeBeginDate, SimulTradeEndDate, EventType, SimulTradeBeginDate FROM HK_TempParTrade WHERE EventType <> 1) b WHERE a.InnerCode=b.TempInnerCode AND TradingDay BETWEEN TempTradeBeginDate AND SimulTradeEndDate AND TradingDay BETWEEN :start_date AND :end_date ORDER BY TradingDay) a, HK_SecuMain b WHERE a.InnerCode=b.InnerCode
-- Stock Kline
SELECT TradingDay,a.InnerCode,CompanyCode,Futu_SecuCode,OpenPrice,ClosePrice,HighPrice,LowPrice,TurnoverValue,TurnoverVolume,Lot,Mark FROM QT_HKDailyQuote a, (SELECT InnerCode, CompanyCode, SecuCode, Futu_SecuCode FROM HK_SecuMain WHERE SecuCategory in (3,51,52,53) AND ListedDate IS NOT NULL AND ListedDate<=:sp_date AND (DelistingDate is NULL OR DelistingDate>:sp_date)) b WHERE a.InnerCode=b.InnerCode AND TradingDay BETWEEN :s_date AND :sp_date

SELECT TradingDay,:num as InnerCode,CompanyCode,Futu_SecuCode,OpenPrice,ClosePrice,HighPrice,LowPrice,TurnoverValue,TurnoverVolume,Lot,Mark FROM QT_HKDailyQuote a, (SELECT InnerCode, CompanyCode, SecuCode, Futu_SecuCode FROM HK_SecuMain WHERE SecuCategory in (3,51,52,53) AND ListedDate IS NOT NULL AND ListedDate<=:e_date AND (DelistingDate is NULL OR DelistingDate>:s_date)) b WHERE a.InnerCode=b.InnerCode AND TradingDay BETWEEN :s_date AND :e_date AND a.InnerCode in :code_list
