 DROP TABLE IF EXISTS HK_Resolutionability;
CREATE TABLE `HK_Resolutionability` (
	`InnerCode` INT(11) NOT NULL,
	`CompanyCode` INT(11) NULL DEFAULT NULL,
	`SecuCode` VARCHAR(10) NULL DEFAULT NULL,
	`Futu_SecuCode` INT(11) NULL DEFAULT '0',
	`EndDate` DATE NOT NULL,
	`DateTypeCode` INT(11) NOT NULL,
	`InfoPublDate` DATETIME NULL DEFAULT NULL,
	`Equitytototalcapital` DECIMAL(20,6) NULL DEFAULT NULL,
	`Interestdebttototalcapital` DECIMAL(20,6) NULL DEFAULT NULL,
	`Catoassets` DECIMAL(20,6) NULL DEFAULT NULL,
	`Currentdebttodebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Debttoassets` DECIMAL(20,6) NULL DEFAULT NULL,
	`Assetstoequity` DECIMAL(20,6) NULL DEFAULT NULL,
	`Debttoequity` DECIMAL(20,6) NULL DEFAULT NULL,
	`Cash` DECIMAL(20,6) NULL DEFAULT NULL,
  `Create_Time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`InnerCode`, `EndDate`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

ALTER TABLE `HK_Resolutionability` ADD INDEX `InnerCode` (`InnerCode`);
ALTER TABLE `HK_Resolutionability` ADD INDEX `CompanyCode` (`CompanyCode`);
ALTER TABLE `HK_Resolutionability` ADD INDEX `SecuCode` (`SecuCode`);
ALTER TABLE `HK_Resolutionability` ADD INDEX `EndDate` (`EndDate`);
ALTER TABLE `HK_Resolutionability` ADD INDEX `DateTypeCode` (`DateTypeCode`);