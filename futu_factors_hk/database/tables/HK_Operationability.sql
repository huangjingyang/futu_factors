 DROP TABLE IF EXISTS HK_Operationability;
CREATE TABLE `HK_Operationability` (
	`InnerCode` INT(11) NOT NULL,
	`CompanyCode` INT(11) NULL DEFAULT NULL,
	`SecuCode` VARCHAR(10) NULL DEFAULT NULL,
	`Futu_SecuCode` INT(11) NULL DEFAULT '0',
	`EndDate` DATE NOT NULL,
	`DateTypeCode` INT(11) NOT NULL,
	`InfoPublDate` DATETIME NULL DEFAULT NULL,
	`Arturn` DECIMAL(20,6) NULL DEFAULT NULL,
	`Netaccountsreceivable_lyr` DECIMAL(20,6) NULL DEFAULT NULL,
	`Faturn` DECIMAL(20,6) NULL DEFAULT NULL,
	`Assetsturn` DECIMAL(20,6) NULL DEFAULT NULL,
	`Caturn` DECIMAL(20,6) NULL DEFAULT NULL,
	`Invturn` DECIMAL(20,6) NULL DEFAULT NULL,
	`Operatingincome_ttm` DECIMAL(20,6) NULL DEFAULT NULL,
	`Operatingincome_lyr` DECIMAL(20,6) NULL DEFAULT NULL,
	`Netcashflow_ttm` DECIMAL(20,6) NULL DEFAULT NULL,
	`Netcashflow_lyr` DECIMAL(20,6) NULL DEFAULT NULL,
	`Netcashfromoperating_ttm` DECIMAL(20,6) NULL DEFAULT NULL,
	`Netcashfromoperating_lyr` DECIMAL(20,6) NULL DEFAULT NULL,
	`Netaccountsreceivable` DECIMAL(20,6) NULL DEFAULT NULL,
	`Netaccountsreceivable_ttm` DECIMAL(20,6) NULL DEFAULT NULL,
  `Create_Time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`InnerCode`, `EndDate`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

ALTER TABLE `HK_Operationability` ADD INDEX `InnerCode` (`InnerCode`);
ALTER TABLE `HK_Operationability` ADD INDEX `CompanyCode` (`CompanyCode`);
ALTER TABLE `HK_Operationability` ADD INDEX `SecuCode` (`SecuCode`);
ALTER TABLE `HK_Operationability` ADD INDEX `EndDate` (`EndDate`);
ALTER TABLE `HK_Operationability` ADD INDEX `DateTypeCode` (`DateTypeCode`);