 DROP TABLE IF EXISTS HK_Debtpayingability;
CREATE TABLE `HK_Debtpayingability` (
	`InnerCode` INT(11) NOT NULL,
	`CompanyCode` INT(11) NULL DEFAULT NULL,
	`SecuCode` VARCHAR(10) NULL DEFAULT NULL,
	`Futu_SecuCode` INT(11) NULL DEFAULT '0',
	`EndDate` DATE NOT NULL,
	`DateTypeCode` INT(11) NOT NULL,
	`InfoPublDate` DATETIME NULL DEFAULT NULL,
	`Ebitdatodebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Tangibleassettodebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Current` DECIMAL(20,6) NULL DEFAULT NULL,
	`Quick` DECIMAL(20,6) NULL DEFAULT NULL,
	`Optoliqdebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Ocftoliqdebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Equitytodebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Dtoa` DECIMAL(20,6) NULL DEFAULT NULL,
  `Create_Time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`InnerCode`, `EndDate`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

ALTER TABLE HK_Debtpayingability ADD Interestcover DECIMAL(20,6) NULL DEFAULT NULL;
ALTER TABLE HK_Debtpayingability ADD Opercashflowtotl DECIMAL(20,6) NULL DEFAULT NULL;

ALTER TABLE `HK_Debtpayingability` ADD INDEX `InnerCode` (`InnerCode`);
ALTER TABLE `HK_Debtpayingability` ADD INDEX `CompanyCode` (`CompanyCode`);
ALTER TABLE `HK_Debtpayingability` ADD INDEX `SecuCode` (`SecuCode`);
ALTER TABLE `HK_Debtpayingability` ADD INDEX `EndDate` (`EndDate`);
ALTER TABLE `HK_Debtpayingability` ADD INDEX `DateTypeCode` (`DateTypeCode`);