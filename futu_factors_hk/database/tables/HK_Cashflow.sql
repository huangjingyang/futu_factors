 DROP TABLE IF EXISTS HK_Cashflow;
CREATE TABLE `HK_Cashflow` (
	`InnerCode` INT(11) NOT NULL,
	`CompanyCode` INT(11) NULL DEFAULT NULL,
	`SecuCode` VARCHAR(10) NULL DEFAULT NULL,
	`Futu_SecuCode` INT(11) NULL DEFAULT '0',
	`EndDate` DATE NOT NULL,
	`DateTypeCode` INT(11) NOT NULL,
	`InfoPublDate` DATETIME NULL DEFAULT NULL,
	`Ocftosales` DECIMAL(20,6) NULL DEFAULT NULL,
	`Ocftooperateincome` DECIMAL(20,6) NULL DEFAULT NULL,
	`Optoebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Opetoebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Ebtoebt` DECIMAL(20,6) NULL DEFAULT NULL,
  `Create_Time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`InnerCode`, `EndDate`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

ALTER TABLE `HK_Cashflow` ADD INDEX `InnerCode` (`InnerCode`);
ALTER TABLE `HK_Cashflow` ADD INDEX `CompanyCode` (`CompanyCode`);
ALTER TABLE `HK_Cashflow` ADD INDEX `SecuCode` (`SecuCode`);
ALTER TABLE `HK_Cashflow` ADD INDEX `EndDate` (`EndDate`);
ALTER TABLE `HK_Cashflow` ADD INDEX `DateTypeCode` (`DateTypeCode`);