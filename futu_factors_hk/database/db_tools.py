#!/usr/bin/env python
# encoding: utf-8
# copyright reserved by futu5.com

"""
@version: 2.0
@author: alienz
@license: Copyright Reserved by Futu
@contact: limingxia07@gmail.com
@site: http://www.futu5.com
@software: PyCharm
@file: db_tools.py
@time: 2018/6/5 18:58
"""
import pandas as pd
import time
import logging
import inspect
import smtplib
from datetime import datetime
from sqlalchemy import text
from sqlalchemy.exc import DisconnectionError, OperationalError, SQLAlchemyError

logging.basicConfig(filename='daily_task.log', format='%(asctime)s %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG)


class UnexpectedInput(Exception):
    """
    This exception was raised when unexpected input was detected
    """

    def __init__(self, x):
        Exception.__init__(self, x)
        self.x = x


class DataUnavailable(Exception):
    """
    Raised When error code not 0 when fetching data
    """

    def __init__(self, error_code, error_data=None, error_func=None, error_args=None, error_kwargs=None):
        Exception.__init__(self, error_code, error_data, error_func, error_args, error_kwargs)
        self.error_code = error_code
        self.error_data = error_data
        self.error_func = error_func
        self.error_args = error_args
        self.error_kwargs = error_kwargs
        super(DataUnavailable, self).__init__(error_code)


class DataUnexpectedReturn(Exception):
    """
    Raised When returned data not as much as expected when fetching data
    """

    def __init__(self, x):
        Exception.__init__(self, x)
        self.x = x


def notifymail(receiver, subject, msg, sender='13087920164@163.com', sender_pwd="fttest177"):
    """
    This function send notify mail from sender to receiver for given subject and msg
    :param sender: sender mail, default was set as 163 mailbox
    :param receiver: mail address to receive the mail
    :param subject: mail subject
    :param msg: mail message
    :param sender_pwd: password to login
    """
    host = "smtp.163.com"
    port = "25"
    mail_subject = subject
    mail_txt = "App daily_task @" + datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    mail_body = "\r\n".join((
        "From: %s" % sender,
        "To: %s" % receiver,
        "Subject: %s" % mail_subject,
        "",
        mail_txt, msg
    ))
    server = smtplib.SMTP()
    server.connect(host, port)
    server.login(sender, sender_pwd)
    server.sendmail(sender, receiver, mail_body)
    server.quit()


def try_data_inserting(func):
    """
    This decorator wraps try except statement
    :param func: the func needs to call wind api
    :return: decorated func
    """
    start_t = time.clock()

    def dec(*args, **kwargs):
        """
        functions needs to read data from db
        :param args: generic input
        :param kwargs: generic keywords
        :return:
        """
        try:
            print("DB Insertion called by ", func.__name__)
            result = func(*args, **kwargs)
            return result
        except (DisconnectionError, OperationalError) as e:
            print(e)
            raise DataUnavailable(-1, e, func.__name__, args, kwargs)
        except SQLAlchemyError as e:
            print(e)
            raise DataUnavailable(-2, e, func.__name__, args, kwargs)
        except DataUnavailable as e:
            raise DataUnavailable(e.error_code, e.error_data, func.__name__, args, kwargs)
        except Exception as e:
            print(e)
            raise DataUnavailable(-3, e, func.__name__, args, kwargs)

    time_consumed = time.clock() - start_t
    if time_consumed > 10:
        print("Time consumed for func: ", func.__name__, " is ", time.clock() - start_t)
    return dec


@try_data_inserting
def dataframe_upsert_auto(data, engine, name, update_key=(), pri_key=(), all_fields=(), reserved="zz_"):
    """
    first insert into temp database when increasing the seq by one, then use insert or replace to transfer data to des
    :param data:
    :param engine:
    :param name:
    :param update_key:
    :param pri_key:
    :param all_fields:
    :param reserved:
    :return:
    """
    # Insert into temp table
    if not isinstance(data, pd.DataFrame) or (len(data) == 0):
        raise UnexpectedInput(data)
    temp_name = "{reserved}{name}".format(name=name,reserved=reserved)
    retry = 0
    while retry < 3:
        try:
            # if all_fields empty fill in; if update_key empty fill in;
            # remove create_time col and others
            if len(all_fields) == 0 or len(pri_key) == 0:
                stmt = text("SHOW COLUMNS FROM {}".format(name))
                all_fields = pd.read_sql(stmt, engine)
                pri_key = all_fields[all_fields["Key"] == "PRI"]["Field"].tolist()
                if len(update_key) == 0:
                    update_key = all_fields[all_fields["Key"] != "PRI"]["Field"].tolist()
                    if "create_time" in update_key:
                        update_key.remove("create_time")
                    if "mtime" in update_key:
                        update_key.remove("mtime")
                all_fields = all_fields["Field"].tolist()
                if "create_time" in all_fields:
                    all_fields.remove("create_time")
                    data.drop(["create_time"], axis=1, inplace=True, errors="ignore")
                if "mtime" in all_fields:
                    all_fields.remove("mtime")
                    data.drop(["mtime"], axis=1, inplace=True, errors="ignore")

            data.drop_duplicates(subset=pri_key,keep="last", inplace=True)
            # lock tables
            conn = engine.connect()
            # do stuff with conn
            try:
                conn.execute("LOCK TABLES {tb} WRITE".format(tb=temp_name))
                cursor = conn.execute("SELECT max({seq}) FROM {tb}".format(tb=temp_name,seq=reserved+"seq"))
                max_seq = cursor.fetchone()
                # max_seq = pd.read_sql(stmt, engine).iloc[0, 0]
                if len(max_seq) > 0 and max_seq[0] is None:
                    max_seq = 0
                else:
                    max_seq = max_seq[0]
                conn.execute("UNLOCK TABLES")
                data[reserved+"seq"] = max_seq + 1
                data.to_sql(temp_name, engine, if_exists="append", index=False)
            except Exception as e:
                print(e)
                logging.error("Cannot to_sql when calling func: ", inspect.stack()[1][3])
                try:
                    data.to_pickle("error_data\error_to_sql_data_" + str(time.time()) + ".pkl")
                except Exception as e:
                    print(e)
                return False
            # generate update string
            update_list = [x + "=VALUES(" + x + ")" for x in update_key]
            if len(all_fields) > len(pri_key):
                # Insert and update
                stmt = text("INSERT INTO {target} ({all_fields}) SELECT {all_fields} FROM {source} "
                            "WHERE {seq}={max_seq} "
                            "ON DUPLICATE KEY UPDATE {update_str}".format(target=name, source=temp_name,
                                                                          all_fields=",".join(all_fields),
                                                                          max_seq=str(max_seq + 1),
                                                                          seq=reserved+"seq",
                                                                          update_str=",".join(update_list)))
            else:
                # deal with all pri keys
                stmt = text("REPLACE INTO {target} ({fields}) SELECT {fields}"
                            " FROM {source} WHERE {seq}={max_seq}".format(target=name, source=temp_name,
                                                                          fields=",".join(all_fields),
                                                                          seq=reserved + "seq",
                                                                          max_seq=str(max_seq + 1)))
            # conn = engine.connect()
            trans = conn.begin()
            try:
                conn.execute(stmt)
                trans.commit()
            except SQLAlchemyError as e:
                # roll back
                print(e)
                logging.error("Cannot upsert database when calling func: ", inspect.stack()[1][3])
                try:
                    data.to_pickle("error_data\error_upsert_data_" + str(time.time()) + ".pkl")
                except Exception as e:
                    print(e)
                trans.rollback()
            finally:
                trans.close()
                conn.close()
            return True
        except (OperationalError, DisconnectionError) as e:
            print("Exception caught as ", e)
            print("Cannot connect to database, will retry in 60 seconds")
            time.sleep(60)
            retry += 1
            continue
    print("Cannot access to database, sending email now... ")
    try:
        notifymail("alienz@futunn.com", "db_access", "Cannot access to database")
    except Exception as e:
        print(e)
    finally:
        # record error message
        logging.error("Cannot access to database when calling func: ", inspect.stack()[1][3])
    return False


# THIS IS THE DELETE MODE
@try_data_inserting
def dataframe_delete_auto(data, engine, name, pri_key=(), all_fields=()):
    """
    delete input data in database
    :param data:
    :param engine:
    :param name:
    :param pri_key:
    :param all_fields:
    :return:
    """
    if not isinstance(data, pd.DataFrame) or (len(data) == 0):
        raise UnexpectedInput(data)
    retry = 0
    while retry < 3:
        try:
            if len(all_fields) == 0 or len(pri_key) == 0:
                stmt = text("SHOW COLUMNS FROM {}".format(name))
                all_fields = pd.read_sql(stmt, engine)
                pri_key = all_fields[all_fields["Key"] == "PRI"]["Field"].tolist()
                all_fields = all_fields["Field"].tolist()
                if "create_time" in all_fields:
                    all_fields.remove("create_time")
            stmt_list = list()
            for index, row in data.iterrows():
                condition_str = [key + "=:" + key for key in pri_key]
                para_dict = dict(zip(row.index, row.values))
                para_dict = dict((key, value) for key, value in para_dict.items() if key in pri_key)
                for key in para_dict.keys():
                    if key not in pri_key:
                        del para_dict[key]
                stmt = text("DELETE FROM {table} WHERE "
                            "{condition_str}".format(table=name,
                                                     condition_str=" AND ".join(condition_str)))
                stmt = stmt.bindparams(**para_dict)
                stmt_list.append(stmt)
            conn = engine.connect()
            trans = conn.begin()
            try:
                for stmt in stmt_list:
                    conn.execute(stmt)
                trans.commit()
            except Exception:
                # roll back
                logging.error("Cannot upsert database when calling func: ", inspect.stack()[1][3])
                try:
                    data.to_pickle("error_data\error_upsert_data_" + str(time.time()) + ".pkl")
                except Exception as e:
                    print(e)
                trans.rollback()
            finally:
                trans.close()
                conn.close()
            return True
        except (OperationalError, DisconnectionError) as e:
            print("Exception caught as ", e)
            print("Cannot connect to database, will retry in 60 seconds")
            time.sleep(60)
            retry += 1
            continue
    print("Cannot access to database, sending email now... ")
    try:
        notifymail("alienz@futunn.com", "db_access", "Cannot access to database")
    except Exception as e:
        print(e)
    finally:
        # record error message
        logging.error("Cannot access to database when calling func: ", inspect.stack()[1][3])
    return False


@try_data_inserting
def update_derivative_tables(engine, s_sch="factors", reserved="zz_"):
    """
    add seq,create_time,mtime for temp db and bu_db, create new table when the origin schema table created
    :param engine:
    :param fields:
    :param s_sch:
    :param reserved:
    :return:
    """
    # TODO: alter no primary key issue
    # default format
    columns_info = {
        "create_time": "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP",
        "mtime": "TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",
        "{reserved}seq".format(reserved=reserved): "BIGINT(20) NOT NULL DEFAULT 0",
    }
    add_pri_key_template = "DROP PRIMARY KEY,ADD PRIMARY KEY ({pri})"
    add_pri_key_np_template = ",ADD PRIMARY KEY ({pri})"
    # source table
    stmt = text("SELECT TABLE_NAME,COLUMN_NAME,COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=:schema")
    stmt = stmt.bindparams(schema=s_sch)
    df_source = pd.read_sql(stmt, engine)
    stmt = text("SELECT TABLE_NAME,COLUMN_NAME AS PRIMARY_KEY FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE "
                "WHERE TABLE_SCHEMA=:schema AND constraint_name='PRIMARY'")

    stmt = stmt.bindparams(schema=s_sch)
    df_pri = pd.read_sql(stmt, engine)
    origin_list = list()
    derivative_list = list()
    for item in df_source["TABLE_NAME"].unique():
        if len(item) < 4:
            origin_list.append(item)
        elif item[:len(reserved)] == reserved:
            derivative_list.append(item)
        else:
            origin_list.append(item)

    table_to_create = list()
    table_to_remove = list()
    table_to_alter = list()

    for item in origin_list:
        if reserved + item in derivative_list:
            table_to_alter.append(item)
        else:
            table_to_create.append(item)

    for item in derivative_list:
        if item[len(reserved):] not in origin_list:
            table_to_remove.append(item)

    # Create table
    stmt_create_lt = list()
    if len(table_to_create):
        for tb in table_to_create:
            stmt = text("CREATE TABLE `{s_sch}`.`{target}` LIKE `{source}`".format(target=reserved + tb,
                                                                                   source=tb, s_sch=s_sch))
            stmt_create_lt.append(stmt)

    stmt_alter_lt = list()

    # delete table
    if len(table_to_remove):
        table_str = ",".join(table_to_remove)
        stmt = text("DROP TABLE IF EXISTS {table_str}".format(table_str=table_str))
        stmt_alter_lt.append(stmt)

    # add fields and primary key
    for tb in table_to_create:
        columns = df_source[df_source["TABLE_NAME"] == tb]["COLUMN_NAME"]
        fields_to_add = []
        for col in columns_info.keys():
            if col not in columns.values:
                fields_to_add.append("ADD COLUMN `{col}` {type}".format(col=col, type=columns_info[col]))
        if len(fields_to_add) == 0:
            raise ValueError
        else:
            alter_str = ",".join(fields_to_add)
        # add primary key
        pri_key_s = df_pri[df_pri["TABLE_NAME"] == tb]["PRIMARY_KEY"].append(
            pd.Series(["{reserved}seq".format(reserved=reserved)]))
        pri_key = pri_key_s.apply(lambda x: "`" + x + "`")
        if len(pri_key) == 0:
            add_pri_key = add_pri_key_np_template.format(pri=",".join(pri_key))
        else:
            add_pri_key = add_pri_key_template.format(pri=",".join(pri_key))
        alter_str += "," + add_pri_key

        stmt = text("ALTER TABLE `{s_sch}`.`{re}{name}` {alter_str}".format(s_sch=s_sch, name=tb, re=reserved,
                                                                            alter_str=alter_str))
        stmt_alter_lt.append(stmt)

    # modify fields or type; new field, new primary key, new type
    for table_name, df in df_source.groupby(["TABLE_NAME"]):
        if table_name not in table_to_alter:
            continue
        origin_pri = df_pri[df_pri["TABLE_NAME"] == table_name]["PRIMARY_KEY"]
        derivative_pri = df_pri[df_pri["TABLE_NAME"] == reserved + table_name]["PRIMARY_KEY"]

        # check pri key
        add_pri_key = []
        if (set(origin_pri).issubset(derivative_pri)
                and "{reserved}seq".format(reserved=reserved) in derivative_pri.values
                and (len(origin_pri) + 1 == len(derivative_pri))):
            pass
        else:
            origin_pri = origin_pri.append(pd.Series([reserved + "seq"]))
            pri_key = origin_pri.apply(lambda x: "`" + x + "`")
            if len(pri_key) == 0:
                add_pri_key.append(add_pri_key_np_template.format(pri=",".join(pri_key)))
            else:
                add_pri_key.append(add_pri_key_template.format(pri=",".join(pri_key)))
        # drop columns and add new columns
        origin_col = df_source[df_source["TABLE_NAME"] == table_name]
        derivative_col = df_source[df_source["TABLE_NAME"] == reserved + table_name]

        col_to_check = set(origin_col["COLUMN_NAME"]) & set(derivative_col["COLUMN_NAME"])
        col_to_add = set(origin_col["COLUMN_NAME"]) - col_to_check | (set(columns_info.keys()) -
                                                                      set(derivative_col["COLUMN_NAME"]))
        col_to_delete = set(derivative_col["COLUMN_NAME"]) - col_to_check - set(columns_info.keys())

        add_key_info = origin_col[origin_col["COLUMN_NAME"].isin(col_to_add)]
        add_key_info_dict = dict(zip(add_key_info["COLUMN_NAME"], add_key_info["COLUMN_TYPE"]))

        col_to_add_str = []
        for col in col_to_add:
            if col not in add_key_info_dict.keys():
                if col == reserved + "seq":
                    col_to_add_str.append("ADD COLUMN `{col}` {type}".format(col=col, type=columns_info[col]))
            else:
                col_to_add_str.append("ADD COLUMN `{col}` {type}".format(col=col, type=add_key_info_dict[col]))

        col_to_delete_str = []
        for col in col_to_delete:
            col_to_delete_str.append("DROP COLUMN `{col}`".format(col=col))

        # check type
        ori_check_key_info = origin_col[origin_col["COLUMN_NAME"].isin(col_to_check)]
        ori_check_key_info_dict = dict(zip(ori_check_key_info["COLUMN_NAME"], ori_check_key_info["COLUMN_TYPE"]))
        der_check_key_info = derivative_col[derivative_col["COLUMN_NAME"].isin(col_to_check)]
        der_check_key_info_dict = dict(zip(der_check_key_info["COLUMN_NAME"], der_check_key_info["COLUMN_TYPE"]))

        type_to_change_str = []
        for col, col_type in ori_check_key_info_dict.items():
            if col_type != der_check_key_info_dict[col]:
                type_to_change_str.append("CHANGE COLUMN `{col}` `{col}` {type}".format(col=col,type=col_type))

        alter_stmt = col_to_add_str + col_to_delete_str + type_to_change_str + add_pri_key
        alter_stmt_str = ",".join(alter_stmt)
        if len(alter_stmt_str) == 0:
            continue
        stmt = text("ALTER TABLE `{s_sch}`.`{re}{name}` {alter_str}".format(s_sch=s_sch, name=table_name, re=reserved,
                                                                            alter_str=alter_stmt_str
                                                                            ))
        stmt_alter_lt.append(stmt)

    # execute stmt
    conn = engine.connect()
    trans = conn.begin()
    stmt_lt = stmt_create_lt + stmt_alter_lt
    try:
        for stmt in stmt_lt:
            conn.execute(stmt)
        trans.commit()
    except Exception as e:
        # roll back
        print(e)
        # print(stmt)
        logging.error("Cannot alter database when calling func: ", inspect.stack()[1][3])
        trans.rollback()
        return False
    finally:
        trans.close()
        conn.close()
    return True


def transfer_data(engine, source_tb, des_tb, func=None, stmt=None):
    """
    transfer source_tb to des_tb, if stmt is none, transfer all
    :param engine:
    :param source_tb:
    :param des_tb:
    :param func:
    :param stmt:
    """
    if stmt is None:
        stmt = text("SELECT * FROM {tb}".format(tb=source_tb))
    try:
        df = pd.read_sql(stmt, engine)
    except Exception as e:
        print(e)
        raise DataUnexpectedReturn(e)
    if func is None:
        pass
    else:
        df = func(df)
    dataframe_upsert_auto(df, engine, des_tb)


if __name__ == '__main__':
    # from workflow import init_config
    from db_info import get_engine

    engine = get_engine("neo_factors")
    df = pd.read_sql("SELECT * FROM `stock_list`",engine)
    dataframe_upsert_auto(df,engine,"stock_list")
    print(df)
    # update_derivative_tables(engine, s_sch="neo_factors")
    # from datetime import datetime
    #
    # market = "US"
    # start_date = "2016-11-15"
    # end_date = "2018-01-10"
    # config = init_config(market, end_date)
    #
    #
    # def reformat_data(df):
    #     df = df[df["Date"] < datetime(year=2017, month=9, day=30).date()]
    #     df.rename(columns={"Date": "report_date"}, inplace=True)
    #     df["Date"] = datetime.today().date()
    #     return df
    #
    #
    # transfer_data(config["db_engine"], "w_report_date", "w_reports_date", func=reformat_data)
    # d = pd.read_pickle("data/us_price_180110.pkl")
    # dataframe_upsert_auto(d,config["db_engine"],"wq_us_stock_daily")
    # data = pd.read_pickle("test_price.pkl")
    # data = pd.read_csv("test_trading_day.csv")
    # data = data.append({"Date":"2018-01-07","Market":"HK"},ignore_index=True)
    # dataframe_delete_auto(data,config["db_engine"],"test_trading_day")
    # dataframe_upsert_auto(data, config["db_engine"], "test_trading_day", config["trading_day_temp"])
    # t_data(config)
    # update_derivative_tables(config["db_engine"])
    print("Done")
