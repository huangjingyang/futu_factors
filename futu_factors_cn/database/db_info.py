#!/usr/bin/env python
# encoding: utf-8
# copyright reserved by futu5.com

"""
@version: 2.0
@author: alienz
@license: Copywright Reserved by Futu 
@contact: limingxia07@gmail.com
@site: http://www.futu5.com
@software: PyCharm
@file: db_info.py
@time: 2018/6/7 10:58
"""

from sqlalchemy import create_engine

def get_engine(db="factors"):
    """
    return engine
    :param db:
    :return:
    """
    db_dict = {
        "factors":{
            "host": "172.28.249.6",
            "port": "13357",
            "usr": "stock_writer",
            "pwd": "stock_writer@",
            "charset": "utf8mb4",
            "db": "factors"
        },
        "neo_factors": {
            "host": "172.28.249.6",
            "port": "13357",
            "usr": "stock_writer",
            "pwd": "stock_writer@",
            "charset": "utf8mb4",
            "db": "neo_factors"
        },
        "fsi_exright":{
            "host": "172.28.1.33",
            "port": "13357",
            "usr": "fsiread",
            "pwd": "fsi@123",
            "charset": "utf8mb4",
            "db": "fsi_exright"
        },
        "jy_factor_cn":{
            "host": "172.28.249.24",
            "port": "13358",
            "usr": "fsi_check",
            "pwd": "fsi_check@",
            "charset": "utf8mb4",
            "db": "cn_finance"
        },
        "jy_factor_hk": {
            "host": "172.28.249.24",
            "port": "13358",
            "usr": "fsi_check",
            "pwd": "fsi_check@",
            "charset": "utf8mb4",
            "db": "hk_finance"
        },
        "jy_factor_hk_formal": {
            "host": "172.28.1.161",
            "port": "13357",
            "usr": "fsiread",
            "pwd": "fsiread@123",
            "charset": "utf8mb4",
            "db": "jydb"
        },

    }
    engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
    engine_str = engine_template.format(**db_dict[db])
    engine = create_engine(engine_str)
    return engine


def get_target_table(key,market="HK",source="wind"):
    """
    get table name for specified key
    :param key:
    :param market:
    :param source:
    :return:
    """
    target = dict()
    if source == "wind":
        target.update({
            "daily_log": ("daily_log", "neo_factors"),
            "trading_day": ("market_day","neo_factors"),
            "industry": ("industry", "neo_factors"),
            "exright": ("exright", "neo_factors"),
            "status": ("status", "neo_factors"),
            "stock_index": ("index_day_kline", "neo_factors"),
            "day_kline": ("day_kline", "neo_factors"),
            "wind_adjfactor": ("wind_adjfactor", "neo_factors"),
            "report_date": ("report_date", "neo_factors"),
            "ipo_info": ("ipo_info", "neo_factors"),
            "daily_value": ("daily_value","neo_factors"),
            "daily_quality": ("daily_quality", "neo_factors"),
            "daily_growth": ("daily_growth", "neo_factors"),
            "daily_market": ("daily_market", "neo_factors"),
            "daily_pershare": ("daily_pershare", "neo_factors"),
            "daily_predict": ("daily_predict", "neo_factors"),
            "quarterly_value": ("quarterly_value", "neo_factors"),
            "quarterly_quality": ("quarterly_quality", "neo_factors"),
            "quarterly_growth": ("quarterly_growth", "neo_factors"),
            "quarterly_market": ("quarterly_market", "neo_factors"),
            "quarterly_pershare": ("quarterly_pershare", "neo_factors"),
            "quarterly_predict": ("quarterly_predict", "neo_factors"),
        })
    elif source == "jy":
        if market == "HK":
            target.update({
                "TotalLiability":("HK_FinancialIndex","jy_factor_hk","Quarterly","总负债(元)"),
                "TotalShareholderEquity": ("HK_FinancialIndex", "jy_factor_hk", "Quarterly", "股东权益/资产净值(元)"),
                "EarningAfterTax": ("HK_FinancialIndex", "jy_factor_hk", "Quarterly", "除税后溢利(元)"),
                "OperatingIncome": ("HK_FinancialIndex", "jy_factor_hk", "Quarterly", "营业收入(元)"),
                "TotalAssets": ("HK_FinancialIndex", "jy_factor_hk", "Quarterly", "总资产(元)"),
                "CurrentAssets": ("HK_FinancialIndex", "jy_factor_hk", "Quarterly", "流动资产(元)"),
                "CashEquivalentEndPer": ("HK_FinancialIndex", "jy_factor_hk", "Quarterly", "期末现金及现金等价物(元)"),
                "CashEquivalentBeginPer": ("HK_FinancialIndex", "jy_factor_hk", "Quarterly", "期初现金及现金等价物(元)"),
                "CurrentLiability": ("HK_FinancialIndex", "jy_factor_hk", "Quarterly", "流动负债(元)"),
                "NonurrentLiability": ("HK_FinancialIndex", "jy_factor_hk", "Quarterly", "非流动负债(元)"),
                "Dividend": ("HK_FinancialIndex", "jy_factor_hk", "Quarterly", "股息(元)"),
            })
    else:
        raise ValueError(source)
    return target[key]


def get_daily_fields(key,market="HK"):
    """
    get field info for specified key
    :param key:
    :param market:
    :return:
    """
    target = dict()
    industry_dict = {
        "HK": ["H","HSI","GGT","H_HS_NYY","H_HS_YCLY","H_HS_GY","H_HS_XFPZZY","H_HS_XFZFWY","H_HS_DXY",
               "H_HS_GYSY","H_HS_JRY","H_HS_DCJZY","H_HS_ZXKJY","H_HS_ZHQY","H_HS_QT"],
        "CN": ["A"],
        "US": ["US"]
    }

    target.update({
        "industry": industry_dict[market],
    })
    return target[key]


if __name__ == '__main__':
    pass