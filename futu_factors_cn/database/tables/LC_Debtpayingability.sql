 DROP TABLE IF EXISTS LC_Debtpayingability;
CREATE TABLE `LC_Debtpayingability` (
	`InnerCode` INT(11) NOT NULL,
	`CompanyCode` INT(11) NULL DEFAULT NULL,
	`SecuCode` VARCHAR(10) NULL DEFAULT NULL,
	`Futu_SecuCode` INT(11) NULL DEFAULT '0',
	`EndDate` DATE NOT NULL,
	`InfoPublDate` DATETIME NULL DEFAULT NULL,
	`Ebitdatodebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Tangibleassettodebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Current` DECIMAL(20,6) NULL DEFAULT NULL,
	`Quick` DECIMAL(20,6) NULL DEFAULT NULL,
	`Optoliqdebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Ocftoliqdebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Equitytodebt` DECIMAL(20,6) NULL DEFAULT NULL,
	`Dtoa` DECIMAL(20,6) NULL DEFAULT NULL,
  `Create_Time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`InnerCode`, `EndDate`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;