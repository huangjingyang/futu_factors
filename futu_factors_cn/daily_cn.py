#-*- coding: utf-8 -*-

# 每天跑8天的数据
import datetime
from main import *
from apscheduler.schedulers.blocking import BlockingScheduler
import smtplib
import configparser
import math
from email.mime.text import MIMEText
from email.header import Header

class Daily():
    def __init__(self):
        pass

    def daily_run(self):
        now = datetime.datetime.now()
        now_date = now.strftime("%Y-%m-%d")
        start_time = now - datetime.timedelta(days=15)
        start_date = start_time.strftime("%Y-%m-%d")
        #print(now_date, start_date)
        config = configparser.ConfigParser()
        config.read("config/config.ini")
        num = int(config['global']['threads'])
        all_stock_basic_info = self.get_stock_basic_info(now_date)
        stock_list = all_stock_basic_info['SecuCode'].tolist()
        run_len = math.ceil(len(stock_list) / num)
        # run_len = len(stock_list)
        for i in range(run_len):
            calculate_list = stock_list[i * num: (i + 1) * num]
            # calculate_list = [stock_list[i]]HK_Growthability
            obj_main = Main(start_date, now_date)
            obj_main.calcualte_factors_stock_list(calculate_list)

        run_end = datetime.datetime.now()
        run_time = (run_end - now).seconds
        print("All Daily", "Run End", run_end, "cost time", run_time)

    # 获取A股所有股票
    def get_stock_basic_info(self, now_date):
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_stmt = text(
            "SELECT InnerCode, CompanyCode, SecuCode, ListedState, ListedDate  FROM SecuMain " \
            "where SecuCategory = 1 and ListedSector in (1,2,6) and ListedDate <= Date('{end_time}');".format(end_time=now_date))
        engine = create_engine(engine_str)
        df_result = pd.read_sql(sql_stmt, engine)
        return df_result

    # 发送邮箱
    def send_email_daily_info(self):
        # 因子日期
        config = configparser.ConfigParser()
        config.read("config/config.ini")
        save_db = str(config["global"]["save_db"])

        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str_factor = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6",
                                                   port="13357",
                                                   db=save_db, charset="utf8mb4")
        engine_str_data = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6",
                                                 port="13357",
                                                 db="neo_factors", charset="utf8mb4")
        engine_neo_factor = create_engine(engine_str_data)
        engine_factor = create_engine(engine_str_factor)
        # 因子数据
        sql_stmt_factor = text("select max(Trading_Day) as Max_Day from LC_Volumn_Price_Common;")
        df_result = pd.read_sql(sql_stmt_factor, engine_factor)
        Max_Day_factor = str(df_result["Max_Day"][0])

        # 明夏相关数据
        # 1、指数数据
        sql_stmt_index = text("select max(TradingDay) as Max_Day from index_daily;")
        df_result = pd.read_sql(sql_stmt_index, engine_neo_factor)
        Max_Day_Index = str(df_result["Max_Day"][0])

        # 2、申万行业数据
        sql_stmt_sw = text("select max(TradingDay) as Max_Day from cn_sw_industry; ")
        df_result = pd.read_sql(sql_stmt_sw, engine_neo_factor)
        Max_Day_SW = str(df_result["Max_Day"][0])


        # 4、日前复权数据
        sql_stmt_Qhk = text("select max(TradingDay) as Max_Day from qcn_stock_daily;")
        df_result = pd.read_sql(sql_stmt_Qhk, engine_neo_factor)
        Max_Day_QHK = str(df_result["Max_Day"][0])

        # 5、月前复权数据
        sql_stmt_MQhk = text("select max(TradingDay) as Max_Day from qcn_stock_monthly;")
        df_result = pd.read_sql(sql_stmt_MQhk, engine_neo_factor)
        Max_Day_MQHK = str(df_result["Max_Day"][0])

        receiver_email_list = ["yadongcao@futunn.com", "lucas@futunn.com", "alienz@futunn.com",
                           "elainehuang@futunn.com",
                           "fortduan@futunn.com", "zoehuang@futunn.com", "cadenxie@futunn.com",
                           "haitaozhang@futunn.com"]

        now = datetime.datetime.now()
        now_date = now.strftime("%Y-%m-%d")
        mail_title = "A股因子_" + now_date
        mail_content = "<p>A股因子计算结束时间： <b>" + str(datetime.datetime.now()) + "</b></p>"
        mail_content += "<p>A股因子最近日期 <b style='color:red'>" + Max_Day_factor + "</b></p>"
        mail_content += "<p>指数数据最近日期(index_daily) <b style='color:red'>" + Max_Day_Index + "</b></p>"
        mail_content += "<p>申万行业数据最近日期(cn_sw_industry) <b style='color:red'>" + Max_Day_SW + "</b></p>"
        mail_content += "<p>日前复权数据最近日期(qcn_stock_daily) <b style='color:red'>" + Max_Day_QHK + "</b></p>"
        mail_content += "<p>月前复权数据最近日期(qcn_stock_monthly) <b style='color:red'>" + Max_Day_MQHK + "</b></p>"

        sender_mail = "futu5quanter@gmail.com"
        s = smtplib.SMTP('smtp.gmail.com', 587)
        s.starttls()
        s.login("futu5quanter@gmail.com", "futu5quanter@@")
        msg = MIMEText(mail_content, "html", 'utf-8')
        msg["Subject"] = Header(mail_title, 'utf-8')
        msg["From"] = sender_mail
        # msg["To"] = receiver_email_list
        # receiver_email_list_temp = [x.decode('utf-8') for x in receiver_email_list]
        for i in range(len(receiver_email_list)):
            receiver_email = receiver_email_list[i]
            msg["To"] = receiver_email
            s.sendmail(sender_mail, receiver_email, msg.as_string())
        s.quit()

def job():
    try:
        obj = Daily()
        obj.daily_run()
        obj.send_email_daily_info()
    except Exception as err:
        save_error_code(str(err))

def save_error_code(error):
    day_str = datetime.datetime.now().strftime('%Y-%m-%d')
    file_name = os.getcwd() + r"/log_files/error_log/" + day_str + ".log"
    hs = open(file_name, "a+")
    hs.write("\n"+ "Daily"+ "\terror\t" + error)
    hs.close()

def timed_task():
    scheduler = BlockingScheduler()
    scheduler.add_job(job, 'cron', hour=21, minute=0)
    scheduler.start()

if __name__=="__main__":
    job()
    timed_task()