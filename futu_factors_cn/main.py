#-*- coding: utf-8 -*-

#入口函数

#第一步，获得要计算的因子以及时间范围
#计算
#导入到数据库

#需要补充的内容

from module.basic.stock_basic_info_search import *
from module.basic.stock_basic_info import *
from module.process.fundamental.fundamental_data import *
from module.process.volume_price.volume_price_data import *
import multiprocessing
import threading
import numba


class Main():
    lock = multiprocessing.RLock()
    def __init__(self, start_time, end_time):
        self.start_time = start_time
        self.end_time = end_time
        pass


    #获取所需要计算的股票
    def get_stock_info_secu_code(self, secu_code):
        market_code = "CN"
        obj = StockBasicInfo(market_code, self.start_time, self.end_time, secu_code)
        stock_info_df = obj.get_stock_basic_info()
        return stock_info_df

    # 开始基本面计算
    @numba.jit
    def caculate_fundamental_data(self, stock_info_df):
        obj = FundamentalData(stock_info_df, self.start_time, self.end_time)
        obj.traverse_stock_info_df()

    # 量价计算
    @numba.jit
    def caculate_volumeprice_date(self, stock_info_df):
        obj = VolumePriceData(stock_info_df, self.start_time, self.end_time)
        obj.calculate_factors()

    # 保存错误文件
    # F: 基本面
    # V: 量价
    def save_error_code(self, code, error):
        day_str = datetime.datetime.now().strftime('%Y-%m-%d')
        file_name = os.getcwd() + r"/log_files/error_log/" + day_str + ".log"
        hs = open(file_name, "a+")
        hs.write("\n" + str(code) + "\terror\t" + error)
        hs.close()


    # 一次跑5个股票
    def calcualte_factors_stock_list(self, secu_code_list):
        run_start = datetime.datetime.now()
        num = len(secu_code_list)
        pool = multiprocessing.Pool(processes=num)
        pool.map(self.calculate_factors_stock, secu_code_list)
        pool.close()
        pool.join()
        run_end = datetime.datetime.now()
        run_time = (run_end - run_start).seconds
        print("Multithreading", run_end, "cost time", run_time)

    # 一个进程中多个线程
    def calculate_factors_stock(self, secu_code):
        run_start = datetime.datetime.now()
        print("Caculate stock", secu_code, "Run Start: ", run_start)
        Process_jobs = []

        try:
            stock_info_df = self.get_stock_info_secu_code(secu_code)
            if stock_info_df.empty:
                return
            Process_jobs.append(threading.Thread(target=self.caculate_fundamental_data, args=(stock_info_df,)))
            Process_jobs.append(threading.Thread(target=self.caculate_volumeprice_date, args=(stock_info_df,)))

            for job in Process_jobs:
                job.start()

            for job in Process_jobs:
                job.join()

        except Exception as err:
            Main.lock.acquire()
            self.save_error_code(secu_code, str(err))
            Main.lock.release()
        run_end = datetime.datetime.now()
        run_time = (run_end - run_start).seconds
        print("Caculate stock", secu_code, "Run End", run_end, "cost time", run_time)

    # 测试用的
    def calculate_batch_test(self):
        #stock_list =  ['00995','01138','00107', '00914','02068','01186','00902','01071','01330','02611','03328','02601','00386','01171','01088','00670','00753','00525','02196','00874','02238','00358','00338','00323','00553','06869']
        run_start = datetime.datetime.now()
        stock_list = ["000002", "000001", "600000"]
        num = len(stock_list)
        pool = multiprocessing.Pool(processes=num)
        pool.map(self.calculate_factors_stock, stock_list)
        pool.close()
        pool.join()
        run_end = datetime.datetime.now()
        run_time = (run_end - run_start).seconds
        print("Multithreading", run_end, "cost time", run_time)



if __name__=="__main__":
    run_start = datetime.datetime.now()
    start_time = "2005-01-01"
    end_time = "2019-03-26"
    obj = Main(start_time, end_time)
    #obj.caluclate_batch_test()
    obj.calculate_batch_test()
    run_end = datetime.datetime.now()
    run_time = (run_end - run_start).seconds
    print("All cost time", run_time, "seconds")