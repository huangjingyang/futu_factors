#-*- coding: utf-8 -*-

# 基础因子
from module.process.fundamental.common_function import *
from module.basic.basic_operations import *
import configparser
from module.basic.handle_db import *



class BasalFactor():
    __name__="Basal"
    def __init__(self, stock_info_dict, data_dict):
        self.stock_info_dict = stock_info_dict
        self.data_dict = data_dict
        self.start_time = stock_info_dict['Start_Time']
        self.end_time = stock_info_dict['End_Time']
        self.common_function_obj = CommonFunction()
        self.start_time_format = self.common_function_obj.convert_string_to_datetime(self.start_time)
        self.end_time_format = self.common_function_obj.convert_string_to_datetime(self.end_time)

        pass


    def get_columns_list(self):
        '''
        columns_list = ["InnerCode", "CompanyCode", "Futu_SecuCode", "EndDate", "DateTypeCode", "AccountingStandards",
                        "AccountingStandardsName", "CurrencyUnit", "InfoPublDate"]
        '''
        columns_list = ["InnerCode", "CompanyCode", "SecuCode", "EndDate", "InfoPublDate"]
        with open("config/formula.json") as f:
            config_data = json.load(f)
        map_dict = config_data['fundamental'][self.__name__]
        # 判断siwth, 0:不需要跑；1：需要跑
        switch = map_dict['swith']
        data_points_dict = map_dict['data_points']
        switch_flag = False
        if switch == "1":
            switch_flag=True
        for key, value in data_points_dict.items():
            if value == "1":
                columns_list.append(key)
        return switch_flag, columns_list

    # 处理数据
    def get_handle_data(self):
        switch_flag, columns_list= self.get_columns_list()
        if switch_flag == False:
            return
        #with open("../../../config/formula.json") as f:
        result_df = pd.DataFrame(columns=columns_list)
        income_statement_result = self.data_dict['IncomeStatement']
        temp_income_statement_result = income_statement_result[income_statement_result['InfoPublDate'] >= self.start_time_format]
        temp_income_statement_result = temp_income_statement_result.reset_index(drop=True)
        for i in range(temp_income_statement_result.shape[0]):
            end_date = temp_income_statement_result['EndDate'][i]
            date_type_code = end_date.month



            #currency_unit = temp_income_statement_result['CurrencyUnit'][i]
            #currency_unit_code = temp_income_statement_result['CurrencyUnitCode'][i]
            info_publ_date = temp_income_statement_result['InfoPublDate'][i]
            time_info = dict()
            time_info['EndDate'] = end_date
            time_info['DateTypeCode'] = date_type_code
            print(self.__name__, end_date)


            #time_info['CurrencyUnit'] = currency_unit
            #time_info['CurrencyUnitCode'] = currency_unit_code

            time_info['InfoPublDate'] = info_publ_date
            #kk = self.map_config_and_method(time_info)
            #print(kk)
            date_points_dict = self.get_need_data_points(time_info, columns_list)
            result_dict = dict()
            for i in range(len(columns_list)):
                key = columns_list[i]
                value = None
                if self.stock_info_dict.get(key) != None:
                    value = self.stock_info_dict.get(key)
                elif date_points_dict.get(key) != None:
                    value = date_points_dict.get(key)
                elif time_info.get(key) != None:
                    value = time_info.get(key)
                result_dict[key] = value
            result_df = result_df.append(result_dict, ignore_index=True)
        #print(result_df)
        '''
        result_df.to_csv("test/" + self.__name__ + "." + str(self.stock_info_dict['SecuCode']) + ".csv", index=False)
            # 存储,先不考虑SQL存储
        '''
        # 开始DB存储
        self.save_data(result_df)

    def save_data(self, data_df):
        config = configparser.ConfigParser()
        config.read("config/config.ini")
        savedb = str(config["global"]["savedb"])
        if savedb == "1":
            self.save_data_to_db(data_df)
        else:
            data_df.to_csv("test/" + self.__name__ + "." + str(self.stock_info_dict['SecuCode']) + ".csv", index=False)

    def save_data_to_db(self, data_df):
        obj_handledb = HandleDB()
        if self.__name__ == "Cashflow":
            obj_handledb.save_cashflow_data(data_df)
        elif self.__name__ == "Debtpayingability":
            obj_handledb.save_debtpayingability_data(data_df)
        elif self.__name__ == "Growthability":
            obj_handledb.save_growthability_data(data_df)
        elif self.__name__ == "MarketPerformance":
            obj_handledb.save_marketperformance_data(data_df)
        elif self.__name__ == "Operationability":
            obj_handledb.save_operationability_data(data_df)
        elif self.__name__ == "Profitability":
            obj_handledb.save_profitability_data(data_df)
        elif self.__name__ =="Resolutionability":
            obj_handledb.save_resolutionability_data(data_df)

    # 处理数据
    '''
    def get_handle_data_cn(self):
        switch_flag, columns_list = self.get_columns_list()
        if switch_flag == False:
            return
        # with open("../../../config/formula.json") as f:
        result_df = pd.DataFrame(columns=columns_list)
        income_statement_result = self.data_dict['CNIncomeStatement']
        temp_income_statement_result = income_statement_result[
            income_statement_result['InfoPublDate'] >= self.start_time_format]
        temp_income_statement_result = temp_income_statement_result.reset_index(drop=True)
        for i in range(temp_income_statement_result.shape[0]):
            end_date = temp_income_statement_result['EndDate'][i]
            date_type_code = temp_income_statement_result['PeriodMark'][i]
            accounting_standards = u'中国会计标准'
            accounting_standards_name = u'中国会计标准'
            currency_unit = u"人民币"
            currency_unit_code = temp_income_statement_result['CurrencyUnit'][i]
            info_publ_date = temp_income_statement_result['InfoPublDate'][i]
            time_info = dict()
            time_info['EndDate'] = end_date
            print(self.__name__, end_date)
            time_info['DateTypeCode'] = date_type_code
            #time_info['AccountingStandards'] = accounting_standards
            time_info['CurrencyUnit'] = currency_unit
            time_info['CurrencyUnitCode'] = currency_unit_code
            time_info['AccountingStandardsName'] = accounting_standards_name
            time_info['InfoPublDate'] = info_publ_date
            # kk = self.map_config_and_method(time_info)
            # print(kk)
            date_points_dict = self.get_need_data_points(time_info, columns_list)
            result_dict = dict()
            for i in range(len(columns_list)):
                key = columns_list[i]
                value = None
                if self.stock_info_dict.get(key) != None:
                    value = self.stock_info_dict.get(key)
                elif date_points_dict.get(key) != None:
                    value = date_points_dict.get(key)
                elif time_info.get(key) != None:
                    value = time_info.get(key)
                result_dict[key] = value
            result_df = result_df.append(result_dict, ignore_index=True)
        # print(result_df)

        #result_df.to_csv(
        #    "test/" + self.__name__ + "." + str(self.stock_info_dict['SecuCode']) + ".csv",
        #    index=False)
        
        # 开始DB存储
        self.save_data(result_df)
    '''

    # 建立配置与代码之间的关系
    # 该功能暂时没有实现
    '''
    def map_config_and_method(self, time_info):
        result = dict()
        config_data = object()
        with open("../../../config/formula.json") as f:
            config_data = json.load(f)
        map_dict = config_data['fundamental']['basal_factor']
        for key, value in map_dict.items():
            print(key, value)
            time_info = time_info
            #temp_value = getattr(time_info, value)
            #temp_value = globals()[value](time_info)
            temp_value = getattr(time_info,value)
            result[key] =temp_value
        return result
    '''

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Wgsd_grossprofitmargin" in columns_list:
            wgsd_grossprofitmargin = self.get_grossprofitmargin(time_info)
            result_dict['Wgsd_grossprofitmargin'] = wgsd_grossprofitmargin
        if "Wgsd_netprofitmargin" in columns_list:
            wgsd_netprofitmargin = self.get_netprofitmargin(time_info)
            result_dict['Wgsd_netprofitmargin'] = wgsd_netprofitmargin
        return result_dict


    # 根据配置写写相关代码
    def get_grossprofitmargin(self, time_info):
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        table_name ="MI"
        data_name="GrossIncomeRatio"
        result = self.common_function_obj.get_data_point(stock_info, data_dict, time_info, table_name, data_name)
        return result

    def get_netprofitmargin(self, time_info):
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        table_name = "MI"
        data_name = "NetProfitRatio"
        result = self.common_function_obj.get_data_point(stock_info, data_dict, time_info, table_name, data_name)
        return result




if __name__=="__main__":
    stock_info_dict = {'InnerCode': 1000546, 'CompanyCode': 1000546, 'SecuCode': 700, 'Futu_SecuCode': 700, 'Start_Time': '2006-01-01', 'End_Time': '2018-01-01'}
    pkl_file = open("../../test_files/test.pkl", "rb")
    data_dict = pickle.load(pkl_file)
    pkl_file.close()
    obj = BasalFactor(stock_info_dict, data_dict)
    obj.get_handle_data()