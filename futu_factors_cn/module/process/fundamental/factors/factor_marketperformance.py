#-*- coding: utf-8 -*-

# 市场表现因子

from module.process.fundamental.basal_factor import *
import math


class FactorMarketPerformance(BasalFactor):
    __name__ = "MarketPerformance"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Ebitps2" in columns_list:
            result_dict['Ebitps2'] = self.get_ebitps2(time_info)
        if "Eps_adjust2" in columns_list:
            result_dict['Eps_adjust2'] = self.get_eps_adjust2(time_info)
        if "Eps_diluted3" in columns_list:
            result_dict['Eps_diluted3'] = self.get_eps_diluted3(time_info)
        if "Fcffps2" in columns_list:
            result_dict['Fcffps2'] = self.get_fcffps2(time_info)
        if "Fcfeps2" in columns_list:
            result_dict['Fcfeps2'] = self.get_fcfeps2(time_info)
        if "Eps_basic" in columns_list:
            result_dict['Eps_basic'] = self.get_eps_basic(time_info)
        if "Eps_diluted" in columns_list:
            result_dict['Eps_diluted'] = self.get_eps_diluted(time_info)
        if "Bps" in columns_list:
            result_dict['Bps'] = self.get_bps(time_info)
        if "Ocfps" in columns_list:
            result_dict['Ocfps'] = self.get_ocfps(time_info)
        if "Cfps" in columns_list:
            result_dict['Cfps'] = self.get_cfps(time_info)
        if "Orps" in columns_list:
            result_dict['Orps'] = self.get_orps(time_info)
        return result_dict

    def get_ebitps2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EBIT = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "EBIT")
        ShareCapital = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "TotalShares")

        EBIT = BasicOperations(EBIT)
        ShareCapital = BasicOperations(ShareCapital)


        result = EBIT / ShareCapital
        return result.val

    def get_eps_adjust2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPParentCompanyOwners = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "NPParentCompanyOwners")
        ShareCapital = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "TotalShares")

        NPParentCompanyOwners = BasicOperations(NPParentCompanyOwners)
        ShareCapital = BasicOperations(ShareCapital)

        result = NPParentCompanyOwners/ShareCapital
        return result.val

    def get_eps_diluted3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPParentCompanyOwners = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                 "NPParentCompanyOwners")
        ShareCapital_LYR = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "TotalShares")

        NPParentCompanyOwners = BasicOperations(NPParentCompanyOwners)
        ShareCapital_LYR = BasicOperations(ShareCapital_LYR)

        result = NPParentCompanyOwners/ ShareCapital_LYR
        return result.val

    def get_fcffps2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        FreeCashFlow = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                 "FreeCashFlow")


        ShareCapital = self.common_function_obj.get_data_point_lastest_date(stock_info, data_dict, time_info, "SS",
                                                                   "TotalShares")

        FreeCashFlow = BasicOperations(FreeCashFlow)
        ShareCapital = BasicOperations(ShareCapital)

        result = FreeCashFlow/ShareCapital

        return result.val

    def get_fcfeps2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ShareHolderFCFPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                 "ShareHolderFCFPS")

        result = ShareHolderFCFPS
        return result

    def get_eps_basic(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        BasicEPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                   "BasicEPS")
        result = BasicEPS
        return result

    def get_eps_diluted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        DilutedEPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                   "DilutedEPS")
        result = DilutedEPS
        return result

    def get_bps(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetAssetPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "NetAssetPS")
        result = NetAssetPS
        return result


    def get_ocfps(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperCashFlowPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "OperCashFlowPS")
        result = OperCashFlowPS
        return result

    def get_cfps(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        CashFlowPS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "CashFlowPS")
        result = CashFlowPS
        return result

    def get_orps(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalOperatingRevenuePS = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "TotalOperatingRevenuePS")
        result = TotalOperatingRevenuePS
        return result