#-*- coding: utf-8-*-

# 盈利能力因子计算
from module.process.fundamental.basal_factor import *

class FactorProfitability(BasalFactor):
    __name__ = "Profitability"
    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    '''
    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Testfactor" in columns_list:
            result_dict['Testfactor'] = self.get_testfactor(time_info)
        return result_dict

    def get_testfactor(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalShares = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "SS", "TotalShares")
        #TotalShares = BasicOperations(TotalShares)
        #result = TotalShares
        result = TotalShares
        return result


    '''
    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Roic1" in columns_list:
            result_dict['Roic1'] = self.get_roic1(time_info)
        if "Roe_ttm3" in columns_list:
            result_dict['Roe_ttm3'] = self.get_roe_ttm3(time_info)
        if "Ebitdatosales" in columns_list:
            result_dict['Ebitdatosales'] = self.get_ebitdatosales(time_info)
        if "Roe_diluted" in columns_list:
            result_dict['Roe_diluted'] = self.get_roe_diluted(time_info)
        if "Finaexpensetogr" in columns_list:
            result_dict['Finaexpensetogr'] = self.get_finaexpensetogr(time_info)
        if "Roe_deducted" in columns_list:
            result_dict['Roe_deducted'] = self.get_roe_deducted(time_info)
        if "Roe_exdiluted" in columns_list:
            result_dict['Roe_exdiluted'] = self.get_roe_exdiluted(time_info)
        if "Roa2" in columns_list:
            result_dict['Roa2'] = self.get_roa2(time_info)
        if "Roic" in columns_list:
            result_dict['Roic'] = self.get_roic(time_info)
        if "Optogr" in columns_list:
            result_dict['Optogr'] = self.get_optogr(time_info)
        if "Dupont_ebittosales" in columns_list:
            result_dict['Dupont_ebittosales'] = self.get_dupont_ebittosales(time_info)
        if "Dupont_roe" in columns_list:
            result_dict['Dupont_roe'] = self.get_dupont_roe(time_info)
        if "Grossprofitmargin_ttm3" in columns_list:
            result_dict['Grossprofitmargin_ttm3'] = self.get_grossprofitmargin_ttm3(time_info)
        if "Netprofitmargin_ttm3" in columns_list:
            result_dict['Netprofitmargin_ttm3'] = self.get_netprofitmargin_ttm3(time_info)
        if "Roa_ttm2" in columns_list:
            result_dict['Roa_ttm2'] = self.get_roa_ttm2(time_info)
        if "Netprofittoassets2" in columns_list:
            result_dict['Netprofittoassets2'] = self.get_netprofittoassets2(time_info)
        if "Roa2_ttm3" in columns_list:
            result_dict['Roa2_ttm3'] = self.get_roa2_ttm3(time_info)
        if "Ebittoassets_ttm" in columns_list:
            result_dict['Ebittoassets_ttm'] = self.get_ebittoassets_ttm(time_info)
        if "Profittogr_ttm3" in columns_list:
            result_dict['Profittogr_ttm3'] = self.get_profittogr_ttm3(time_info)
        if "Optogr_ttm3" in columns_list:
            result_dict['Optogr_ttm3'] = self.get_optogr_ttm3(time_info)
        if "Grossprofitmargin" in columns_list:
            result_dict['Grossprofitmargin'] = self.get_grossprofitmargin(time_info)
        if "Netprofitmargin" in columns_list:
            result_dict['Netprofitmargin'] = self.get_netprofitmargin(time_info)
        if "Roe" in columns_list:
            result_dict['Roe'] = self.get_roe(time_info)
        if "Roa" in columns_list:
            result_dict['Roa'] = self.get_roa(time_info)
        if "Dupont_nptosales" in columns_list:
            result_dict['Dupont_nptosales'] = self.get_dupont_nptosales(time_info)
        if "Ebitda" in columns_list:
            result_dict['Ebitda'] = self.get_ebitda(time_info)
        if "Roic_ttm" in columns_list:
            result_dict['Roic_ttm'] = self.get_roic_ttm(time_info)
        if "Roic_ttm2" in columns_list:
            result_dict['Roic_ttm2'] = self.get_roic_ttm2(time_info)
        if "Grossincome_ttm" in columns_list:
            result_dict['Grossincome_ttm'] = self.get_grossincome_ttm(time_info)
        if "Operatingincome_ttm" in columns_list:
            result_dict['Operatingincome_ttm'] = self.get_operatingincome_ttm(time_info)
        if "Netmargin_ttm" in columns_list:
            result_dict['Netmargin_ttm'] = self.get_netmargin_ttm(time_info)
        if "Profittoshareholders_ttm" in columns_list:
            result_dict['Profittoshareholders_ttm'] = self.get_profittoshareholders_ttm(time_info)
        if "Earningbeforetax_ttm" in columns_list:
            result_dict['Earningbeforetax_ttm'] = self.get_earningbeforetax_ttm(time_info)
        if "Earningbeforetax_ttm2" in columns_list:
            result_dict['Earningbeforetax_ttm2'] = self.get_earningbeforetax_ttm2(time_info)
        if "Operatingprofit_ttm" in columns_list:
            result_dict['Operatingprofit_ttm'] = self.get_operatingprofit_ttm(time_info)
        # if "Abs" in columns_list:
        #     result_dict['Abs'] = self.get_abs(time_info)
        if "Egro" in columns_list:
            result_dict['Egro'] = self.get_egro(time_info)
        if "Sgro" in columns_list:
            result_dict['Sgro'] = self.get_sgro(time_info)
        # if "Cetoe" in columns_list:
        #     result_dict['Cetoe'] = self.get_cetoe(time_info)
        if "IntangibleAssetRatio" in columns_list:
            result_dict['IntangibleAssetRatio'] = self.get_IntangibleAssetRatio(time_info)
        if "OperatingExpenseRate" in columns_list:
            result_dict['OperatingExpenseRate'] = self.get_OperatingExpenseRate(time_info)
        if "SalesCostRatio" in columns_list:
            result_dict['SalesCostRatio'] = self.get_SalesCostRatio(time_info)
        if "SaleServiceCashToOR" in columns_list:
            result_dict['SaleServiceCashToOR'] = self.get_SaleServiceCashToOR(time_info)
        if "TaxRatio" in columns_list:
            result_dict['TaxRatio'] = self.get_TaxRatio(time_info)
        if "TotalProfitCostRatio" in columns_list:
            result_dict['TotalProfitCostRatio'] = self.get_TotalProfitCostRatio(time_info)
        return result_dict

    def get_roic1(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPParentCompanyOwners = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "NPParentCompanyOwners")
        NetProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfit")
        TotalLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalLiability")
        AccountsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","AccountsPayable")
        NotAccountsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "NotAccountsPayable")
        AdvanceReceipts = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AdvanceReceipts")
        SalariesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "SalariesPayable")
        TaxsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TaxsPayable")
        OtherPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "OtherPayable")
        AccruedExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AccruedExpense")
        DeferredProceeds = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "DeferredProceeds")
        OtherCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability")
        TotalNonCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BondsPayable")

        NetProfit_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI", "NetProfit")
        TotalLiability_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalLiability")
        AccountsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","AccountsPayable")
        NotAccountsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "NotAccountsPayable")
        AdvanceReceipts_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AdvanceReceipts")
        SalariesPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "SalariesPayable")
        TaxsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TaxsPayable")
        OtherPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherPayable")
        AccruedExpense_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AccruedExpense")
        DeferredProceeds_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "DeferredProceeds")
        OtherCurrentLiability_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability")
        TotalNonCurrentLiability_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability")
        LongtermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        BondsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "BondsPayable")



        NPParentCompanyOwners =  BasicOperations(NPParentCompanyOwners)
        NetProfit =  BasicOperations(NetProfit)
        TotalLiability =  BasicOperations(TotalLiability)
        AccountsPayable =  BasicOperations(AccountsPayable)
        NotAccountsPayable =  BasicOperations(NotAccountsPayable)
        AdvanceReceipts =  BasicOperations(AdvanceReceipts)
        SalariesPayable =  BasicOperations(SalariesPayable)
        TaxsPayable =  BasicOperations(TaxsPayable)
        OtherPayable =  BasicOperations(OtherPayable)
        AccruedExpense =  BasicOperations(AccruedExpense)
        DeferredProceeds =  BasicOperations(DeferredProceeds)
        OtherCurrentLiability =  BasicOperations(OtherCurrentLiability)
        TotalNonCurrentLiability =  BasicOperations(TotalNonCurrentLiability)
        LongtermLoan =  BasicOperations(LongtermLoan)
        BondsPayable =  BasicOperations(BondsPayable)
        NetProfit_LYP =  BasicOperations(NetProfit_LYP)
        TotalLiability_LYP =  BasicOperations(TotalLiability_LYP)
        AccountsPayable_LYP =  BasicOperations(AccountsPayable_LYP)
        NotAccountsPayable_LYP =  BasicOperations(NotAccountsPayable_LYP)
        AdvanceReceipts_LYP =  BasicOperations(AdvanceReceipts_LYP)
        SalariesPayable_LYP =  BasicOperations(SalariesPayable_LYP)
        TaxsPayable_LYP =  BasicOperations(TaxsPayable_LYP)
        OtherPayable_LYP =  BasicOperations(OtherPayable_LYP)
        AccruedExpense_LYP =  BasicOperations(AccruedExpense_LYP)
        DeferredProceeds_LYP =  BasicOperations(DeferredProceeds_LYP)
        OtherCurrentLiability_LYP =  BasicOperations(OtherCurrentLiability_LYP)
        TotalNonCurrentLiability_LYP =  BasicOperations(TotalNonCurrentLiability_LYP)
        LongtermLoan_LYP =  BasicOperations(LongtermLoan_LYP)
        BondsPayable_LYP =  BasicOperations(BondsPayable_LYP)
        const_100 = BasicOperations(100)
        const_2 = BasicOperations(2)
        result = NPParentCompanyOwners*const_2 * const_100/(NetProfit + TotalLiability - AccountsPayable - NotAccountsPayable -
                                          AdvanceReceipts - SalariesPayable - TaxsPayable - OtherPayable -
                                          AccruedExpense - DeferredProceeds - OtherCurrentLiability-
                                          TotalNonCurrentLiability + LongtermLoan + BondsPayable +
                                          NetProfit_LYP + TotalLiability_LYP - AccountsPayable_LYP -
                                          NotAccountsPayable_LYP - AdvanceReceipts_LYP - SalariesPayable_LYP -
                                          TaxsPayable_LYP - OtherPayable_LYP - AccruedExpense_LYP -
                                          DeferredProceeds_LYP - OtherCurrentLiability_LYP-
                                          TotalNonCurrentLiability_LYP + LongtermLoan_LYP + BondsPayable_LYP)

        return result.val

    def get_roe_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROETTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "MI", "ROETTM")

        result = ROETTM
        return result

    def get_ebitdatosales(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EBITDA = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "EBITDA")

        TotalOperatingRevenue = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                        "TotalOperatingRevenue")

        EBITDA = BasicOperations(EBITDA)
        TotalOperatingRevenue = BasicOperations(TotalOperatingRevenue)

        const_100 = BasicOperations(100)

        result = EBITDA / TotalOperatingRevenue*const_100
        return result.val

    def get_roe_diluted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROE = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "ROE")

        result = ROE
        return result

    def get_finaexpensetogr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        FinancialExpenseRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "FinancialExpenseRate")

        result = FinancialExpenseRate
        return result

    def get_roe_deducted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROECutWeighted = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "ROECutWeighted")

        result = ROECutWeighted
        return result

    def get_roe_exdiluted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperSustNetP = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                    "OperSustNetP")

        SEWithoutMI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "SEWithoutMI")

        OperSustNetP = BasicOperations(OperSustNetP)
        SEWithoutMI = BasicOperations(SEWithoutMI)
        const_100 = BasicOperations(100)

        result = OperSustNetP / (SEWithoutMI)*const_100
        return result.val

    def get_roa2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROA_EBIT = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                    "ROA_EBIT")


        result = ROA_EBIT
        return result


    def get_roic(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPParentCompanyOwners = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "NetProfit")
        NetProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfit")
        TotalLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalLiability")
        AccountsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","AccountsPayable")
        NotAccountsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "NotAccountsPayable")
        AdvanceReceipts = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AdvanceReceipts")
        SalariesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "SalariesPayable")
        TaxsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TaxsPayable")
        OtherPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "OtherPayable")
        AccruedExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AccruedExpense")
        DeferredProceeds = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "DeferredProceeds")
        OtherCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability")
        TotalNonCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BondsPayable")

        NetProfit_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI", "NetProfit")
        TotalLiability_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalLiability")
        AccountsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","AccountsPayable")
        NotAccountsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "NotAccountsPayable")
        AdvanceReceipts_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AdvanceReceipts")
        SalariesPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "SalariesPayable")
        TaxsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TaxsPayable")
        OtherPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherPayable")
        AccruedExpense_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AccruedExpense")
        DeferredProceeds_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "DeferredProceeds")
        OtherCurrentLiability_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability")
        TotalNonCurrentLiability_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability")
        LongtermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        BondsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "BondsPayable")



        NPParentCompanyOwners =  BasicOperations(NPParentCompanyOwners)
        NetProfit =  BasicOperations(NetProfit)
        TotalLiability =  BasicOperations(TotalLiability)
        AccountsPayable =  BasicOperations(AccountsPayable)
        NotAccountsPayable =  BasicOperations(NotAccountsPayable)
        AdvanceReceipts =  BasicOperations(AdvanceReceipts)
        SalariesPayable =  BasicOperations(SalariesPayable)
        TaxsPayable =  BasicOperations(TaxsPayable)
        OtherPayable =  BasicOperations(OtherPayable)
        AccruedExpense =  BasicOperations(AccruedExpense)
        DeferredProceeds =  BasicOperations(DeferredProceeds)
        OtherCurrentLiability =  BasicOperations(OtherCurrentLiability)
        TotalNonCurrentLiability =  BasicOperations(TotalNonCurrentLiability)
        LongtermLoan =  BasicOperations(LongtermLoan)
        BondsPayable =  BasicOperations(BondsPayable)
        NetProfit_LYP =  BasicOperations(NetProfit_LYP)
        TotalLiability_LYP =  BasicOperations(TotalLiability_LYP)
        AccountsPayable_LYP =  BasicOperations(AccountsPayable_LYP)
        NotAccountsPayable_LYP =  BasicOperations(NotAccountsPayable_LYP)
        AdvanceReceipts_LYP =  BasicOperations(AdvanceReceipts_LYP)
        SalariesPayable_LYP =  BasicOperations(SalariesPayable_LYP)
        TaxsPayable_LYP =  BasicOperations(TaxsPayable_LYP)
        OtherPayable_LYP =  BasicOperations(OtherPayable_LYP)
        AccruedExpense_LYP =  BasicOperations(AccruedExpense_LYP)
        DeferredProceeds_LYP =  BasicOperations(DeferredProceeds_LYP)
        OtherCurrentLiability_LYP =  BasicOperations(OtherCurrentLiability_LYP)
        TotalNonCurrentLiability_LYP =  BasicOperations(TotalNonCurrentLiability_LYP)
        LongtermLoan_LYP =  BasicOperations(LongtermLoan_LYP)
        BondsPayable_LYP =  BasicOperations(BondsPayable_LYP)
        const_100 = BasicOperations(100)
        const_2 = BasicOperations(2)
        result = NPParentCompanyOwners*const_2 * const_100/(NetProfit + TotalLiability - AccountsPayable - NotAccountsPayable -
                                          AdvanceReceipts - SalariesPayable - TaxsPayable - OtherPayable -
                                          AccruedExpense - DeferredProceeds - OtherCurrentLiability-
                                          TotalNonCurrentLiability + LongtermLoan + BondsPayable +
                                          NetProfit_LYP + TotalLiability_LYP - AccountsPayable_LYP -
                                          NotAccountsPayable_LYP - AdvanceReceipts_LYP - SalariesPayable_LYP -
                                          TaxsPayable_LYP - OtherPayable_LYP - AccruedExpense_LYP -
                                          DeferredProceeds_LYP - OtherCurrentLiability_LYP-
                                          TotalNonCurrentLiability_LYP + LongtermLoan_LYP + BondsPayable_LYP)

        return result.val


    def get_optogr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperatingProfitToTOR= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "OperatingProfitToTOR")

        result = OperatingProfitToTOR
        return result

    def get_dupont_ebittosales(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EBIT = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "EBIT")

        TotalOperatingRevenue = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                        "TotalOperatingRevenue")

        EBIT = BasicOperations(EBIT)
        TotalOperatingRevenue = BasicOperations(TotalOperatingRevenue)

        const_100 = BasicOperations(100)

        result = EBIT / TotalOperatingRevenue*const_100
        return result.val

    def get_dupont_roe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROEAvg= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "ROEAvg")

        result = ROEAvg
        return result

    def get_grossprofitmargin_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        GrossIncomeRatioTTM= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "GrossIncomeRatioTTM")

        result = GrossIncomeRatioTTM
        return result

    def get_netprofitmargin_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfitRatioTTM= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfitRatioTTM")

        result = NetProfitRatioTTM
        return result

    def get_roa_ttm2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROATTM= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "ROATTM")

        result = ROATTM
        return result

    def get_netprofittoassets2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","NetProfit")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")

        ProfitToShareholders_TTM = BasicOperations(ProfitToShareholders_TTM)
        TotalAssets = BasicOperations(TotalAssets)
        const_100 = BasicOperations(100)

        result = ProfitToShareholders_TTM / TotalAssets*const_100
        return result.val

    def get_roa2_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROA_EBITTTM= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "ROA_EBITTTM")

        result = ROA_EBITTTM
        return result

    def get_ebittoassets_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EBIT_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "MI","EBIT")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")

        EBIT_TTM = BasicOperations(EBIT_TTM)
        TotalAssets = BasicOperations(TotalAssets)
        const_100 = BasicOperations(100)

        result = EBIT_TTM / TotalAssets*const_100
        return result.val

    def get_profittogr_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPToTORTTM= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NPToTORTTM")

        result = NPToTORTTM
        return result


    def get_optogr_ttm3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperatingProfitToTORTTM = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "OperatingProfitToTORTTM")

        result = OperatingProfitToTORTTM
        return result

    def get_grossprofitmargin(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        GrossIncomeRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "GrossIncomeRatio")

        result = GrossIncomeRatio
        return result

    def get_netprofitmargin(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfitRatio= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfitRatio")

        result = NetProfitRatio
        return result


    def get_roe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPPCToNP_DuPont= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NPPCToNP_DuPont")
        NetProfitRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfitRatio")
        TotalAssetTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "TotalAssetTRate")
        EquityMultipler_DuPont = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "EquityMultipler_DuPont")

        NPPCToNP_DuPont = BasicOperations(NPPCToNP_DuPont)
        NetProfitRatio = BasicOperations(NetProfitRatio)
        TotalAssetTRate = BasicOperations(TotalAssetTRate)
        EquityMultipler_DuPont = BasicOperations(EquityMultipler_DuPont)

        result = NPPCToNP_DuPont*NetProfitRatio*TotalAssetTRate*EquityMultipler_DuPont
        return result.val

    def get_roa(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROA= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "ROA")

        result = ROA
        return result

    def get_dupont_nptosales(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfitRatio= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NPToTOR")

        result = NetProfitRatio
        return result

    def get_ebitda(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EBITDA= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "EBITDA")

        result = EBITDA
        return result

    def get_roic_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EBIT = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "MI", "EBIT")
        IncomeTaxCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "IncomeTaxCost")
        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "TotalProfit")

        NetProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfit")
        TotalLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalLiability")
        AccountsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","AccountsPayable")
        NotAccountsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "NotAccountsPayable")
        AdvanceReceipts = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AdvanceReceipts")
        SalariesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "SalariesPayable")
        TaxsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TaxsPayable")
        OtherPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "OtherPayable")
        AccruedExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AccruedExpense")
        DeferredProceeds = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "DeferredProceeds")
        OtherCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability")
        TotalNonCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BondsPayable")

        EBIT =  BasicOperations(EBIT)
        IncomeTaxCost =  BasicOperations(IncomeTaxCost)
        TotalProfit =  BasicOperations(TotalProfit)
        NetProfit =  BasicOperations(NetProfit)
        TotalLiability =  BasicOperations(TotalLiability)
        AccountsPayable =  BasicOperations(AccountsPayable)
        NotAccountsPayable =  BasicOperations(NotAccountsPayable)
        AdvanceReceipts =  BasicOperations(AdvanceReceipts)
        SalariesPayable =  BasicOperations(SalariesPayable)
        TaxsPayable =  BasicOperations(TaxsPayable)
        OtherPayable =  BasicOperations(OtherPayable)
        AccruedExpense =  BasicOperations(AccruedExpense)
        DeferredProceeds =  BasicOperations(DeferredProceeds)
        OtherCurrentLiability =  BasicOperations(OtherCurrentLiability)
        TotalNonCurrentLiability =  BasicOperations(TotalNonCurrentLiability)
        LongtermLoan =  BasicOperations(LongtermLoan)
        BondsPayable =  BasicOperations(BondsPayable)
        const_100 = BasicOperations(100)
        const_1 = BasicOperations(1)

        result = EBIT*(const_1 -IncomeTaxCost/TotalProfit) * const_100/(NetProfit + TotalLiability - AccountsPayable -
                                                                        NotAccountsPayable -
                                                                        AdvanceReceipts - SalariesPayable - TaxsPayable - OtherPayable -
                                          AccruedExpense - DeferredProceeds - OtherCurrentLiability-
                                          TotalNonCurrentLiability + LongtermLoan + BondsPayable )
        return result.val

    def get_roic_ttm2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPParentCompanyOwners = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "NPParentCompanyOwners")
        IncomeTaxCost = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "IncomeTaxCost")
        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "TotalProfit")

        NetProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfit")
        TotalLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalLiability")
        AccountsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","AccountsPayable")
        NotAccountsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "NotAccountsPayable")
        AdvanceReceipts = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AdvanceReceipts")
        SalariesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "SalariesPayable")
        TaxsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TaxsPayable")
        OtherPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "OtherPayable")
        AccruedExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AccruedExpense")
        DeferredProceeds = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "DeferredProceeds")
        OtherCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability")
        TotalNonCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BondsPayable")

        NPParentCompanyOwners =  BasicOperations(NPParentCompanyOwners)
        IncomeTaxCost =  BasicOperations(IncomeTaxCost)
        TotalProfit =  BasicOperations(TotalProfit)
        NetProfit =  BasicOperations(NetProfit)
        TotalLiability =  BasicOperations(TotalLiability)
        AccountsPayable =  BasicOperations(AccountsPayable)
        NotAccountsPayable =  BasicOperations(NotAccountsPayable)
        AdvanceReceipts =  BasicOperations(AdvanceReceipts)
        SalariesPayable =  BasicOperations(SalariesPayable)
        TaxsPayable =  BasicOperations(TaxsPayable)
        OtherPayable =  BasicOperations(OtherPayable)
        AccruedExpense =  BasicOperations(AccruedExpense)
        DeferredProceeds =  BasicOperations(DeferredProceeds)
        OtherCurrentLiability =  BasicOperations(OtherCurrentLiability)
        TotalNonCurrentLiability =  BasicOperations(TotalNonCurrentLiability)
        LongtermLoan =  BasicOperations(LongtermLoan)
        BondsPayable =  BasicOperations(BondsPayable)
        const_100 = BasicOperations(100)
        const_1 = BasicOperations(1)

        result = NPParentCompanyOwners*(const_1 -IncomeTaxCost/TotalProfit) * const_100/(NetProfit + TotalLiability - AccountsPayable -
                                                                        NotAccountsPayable -
                                                                        AdvanceReceipts - SalariesPayable - TaxsPayable - OtherPayable -
                                          AccruedExpense - DeferredProceeds - OtherCurrentLiability-
                                          TotalNonCurrentLiability + LongtermLoan + BondsPayable )
        return result.val

    def get_grossincome_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        GrossIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "MI", "GrossIncomeRatioTTM")

        result = GrossIncome_TTM
        return result

    def get_operatingincome_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperatingRevenue_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                          "OperatingRevenue")

        result = OperatingRevenue_TTM
        return result

    def get_netmargin_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfitRatioTTM_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "MI", "NetProfitRatioTTM")

        result = NetProfitRatioTTM_TTM
        return result

    def get_profittoshareholders_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ProfitToShareholders_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "NPParentCompanyOwners")

        result = ProfitToShareholders_TTM
        return result

    def get_earningbeforetax_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EBIT_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "MI", "EBIT")

        result = EBIT_TTM
        return result

    def get_earningbeforetax_ttm2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EBIT_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "MI", "EBIT")

        result = EBIT_TTM
        return result

    def get_operatingprofit_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        # OperProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "OperProfit")
        # AfPrepareOpePayoff_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","AfPrepareOpePayoff")
        # FOperatingExpense_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","FOperatingExpense")

        OperatingProfit_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC",
                                                                 "OperatingProfit")

        result = OperatingProfit_TTM
        return result


    def get_egro(self, time_info):
        import statsmodels.api as sm
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        BasicEPS_list = []
        Year = time_info['EndDate'].year
        Year_list = list(np.arange(Year, Year - 5, -1))
        for i in range(5):
            time_info_temp = time_info.copy()
            end_time = time_info_temp['EndDate']
            end_time_year_ago =  end_time - pd.DateOffset(years=i)
            time_info_temp['EndDate'] = end_time_year_ago
            BasicEPS = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info_temp, "MI", "BasicEPS", num = 1)
            BasicEPS_list.append(BasicEPS)
        BasicEPS_list_notnone = [item for item in BasicEPS_list if item != None and str(item) != 'nan']
        if not BasicEPS_list_notnone:
            return None
        else:
            x = sm.add_constant(Year_list)
            y = BasicEPS_list
            model = sm.OLS(y, x, missing='drop')
            reg = model.fit()
            beta = reg.params[1]
            return beta / np.nanmean(BasicEPS_list_notnone)

    def get_sgro(self, time_info):
        import statsmodels.api as sm
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        MainIncomePS_list = []
        Year = time_info['EndDate'].year
        Year_list = list(np.arange(Year, Year - 5, -1))
        for i in range(5):
            time_info_temp = time_info.copy()
            end_time = time_info_temp['EndDate']
            end_time_year_ago = end_time - pd.DateOffset(years=i)
            time_info_temp['EndDate'] = end_time_year_ago
            MainIncomePS = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info_temp, "MI", "MainIncomePS", num=1)
            MainIncomePS_list.append(MainIncomePS)
        MainIncomePS_list_notnone = [item for item in MainIncomePS_list if item != None and str(item) != 'nan']
        if not MainIncomePS_list_notnone:
            return None
        else:
            x = sm.add_constant(Year_list)
            y = MainIncomePS_list
            model = sm.OLS(y, x, missing='drop')
            reg = model.fit()
            beta = reg.params[1]
            return beta / np.nanmean(MainIncomePS_list_notnone)

    #
    # def get_cetoe(self, time_info):
    #     result = None
    #     stock_info = self.stock_info_dict
    #     data_dict = self.data_dict
    #     NetCashFromOperating = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "CS", "NetCashFromOperating")
    #     EarningAfterTax = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "EarningAfterTax")
    #     NetCashFromOperating = BasicOperations(NetCashFromOperating)
    #     EarningAfterTax = BasicOperations(EarningAfterTax)
    #     result = NetCashFromOperating/EarningAfterTax
    #     return result.val

    def get_IntangibleAssetRatio(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        IntangibleAssetRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "IntangibleAssetRatio")

        result = IntangibleAssetRatio
        return result

    def get_OperatingExpenseRate(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperatingExpenseRateTTM = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "OperatingExpenseRateTTM")

        result = OperatingExpenseRateTTM
        return result

    def get_SalesCostRatio(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        SalesCostRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "SalesCostRatio")

        result = SalesCostRatio
        return result

    def get_SaleServiceCashToOR(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        SaleServiceCashToORTTM = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "SaleServiceCashToORTTM")

        result = SaleServiceCashToORTTM
        return result

    def get_TaxRatio(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperatingTaxSurcharges_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC","OperatingTaxSurcharges")
        OperatingRevenue_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "OperatingRevenue")

        OperatingTaxSurcharges_TTM = BasicOperations(OperatingTaxSurcharges_TTM)
        OperatingRevenue_TTM = BasicOperations(OperatingRevenue_TTM)
        const_100 = BasicOperations(100)

        result = OperatingTaxSurcharges_TTM / OperatingRevenue_TTM*const_100
        return result.val


    def get_TotalProfitCostRatio(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalProfitCostRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "TotalProfitCostRatio")

        result = TotalProfitCostRatio
        return result