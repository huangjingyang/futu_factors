#-*- coding: utf-8 -*-

# 清债能力

from module.process.fundamental.basal_factor import *

class FactorResolutionability(BasalFactor):
    __name__ = "Resolutionability"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Equitytototalcapital" in columns_list:
            result_dict['Equitytototalcapital'] = self.get_equitytototalcapital(time_info)
        if "Interestdebttototalcapital" in columns_list:
            result_dict['Interestdebttototalcapital'] = self.get_interestdebttototalcapital(time_info)
        if "Catoassets" in columns_list:
            result_dict['Catoassets'] = self.get_catoassets(time_info)
        if "Currentdebttodebt" in columns_list:
            result_dict['Currentdebttodebt'] = self.get_currentdebttodebt(time_info)
        if "Debttoassets" in columns_list:
            result_dict['Debttoassets'] = self.get_debttoassets(time_info)
        if "Assetstoequity" in columns_list:
            result_dict['Assetstoequity'] = self.get_assetstoequity(time_info)
        if "Debttoequity" in columns_list:
            result_dict['Debttoequity'] = self.get_debttoequity(time_info)
        if "Cash" in columns_list:
            result_dict['Cash'] = self.get_cash(time_info)
        if "BondsPayableToAsset" in columns_list:
            result_dict['BondsPayableToAsset'] = self.get_BondsPayableToAsset(time_info)
        if "EquityFixedAssetRatio" in columns_list:
            result_dict['EquityFixedAssetRatio'] = self.get_EquityFixedAssetRatio(time_info)
        if "LongDebtToAsset" in columns_list:
            result_dict['LongDebtToAsset'] = self.get_LongDebtToAsset(time_info)
        if "LongDebtToWorkingCapital" in columns_list:
            result_dict['LongDebtToWorkingCapital'] = self.get_LongDebtToWorkingCapital(time_info)
        if "LongTermDebtToAsset" in columns_list:
            result_dict['LongTermDebtToAsset'] = self.get_LongTermDebtToAsset(time_info)
        if "NonCurrentAssetsRatio" in columns_list:
            result_dict['NonCurrentAssetsRatio'] = self.get_NonCurrentAssetsRatio(time_info)
        if "OperCashInToCurrentLiability" in columns_list:
            result_dict['OperCashInToCurrentLiability'] = self.get_OperCashInToCurrentLiability(time_info)
        return result_dict

    def get_equitytototalcapital(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        SEWithoutMIToTotalCapital = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                       "SEWithoutMIToTotalCapital")

        result = SEWithoutMIToTotalCapital
        return result

    def get_interestdebttototalcapital(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        InteBearDebtToTotalCapital = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                       "InteBearDebtToTotalCapital")

        result = InteBearDebtToTotalCapital
        return result

    def get_catoassets(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        CurrentAssetsToTA = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "CurrentAssetsToTA")
        result = CurrentAssetsToTA
        return result

    def get_currentdebttodebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        CurrentLiabilityToTL = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "CurrentLiabilityToTL")
        result = CurrentLiabilityToTL
        return result


    def get_debttoassets(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        DebtAssetsRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "DebtAssetsRatio")
        result = DebtAssetsRatio
        return result

    def get_assetstoequity(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EquityMultipler = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "EquityMultipler")
        result = EquityMultipler
        return result

    def get_debttoequity(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        DebtEquityRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "DebtEquityRatio")
        result = DebtEquityRatio
        return result

    def get_cash(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        Cash = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "CashEquivalents")
        # CashHoldings = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
        #                                                            "CashHoldings")
        # ShortTermDeposit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
        #                                                            "ShortTermDeposit")
        # Cash = BasicOperations(Cash)
        # CashHoldings = BasicOperations(CashHoldings)
        # ShortTermDeposit = BasicOperations(ShortTermDeposit)
        #
        # if Cash.val == None:
        #     result = Cash + CashHoldings + ShortTermDeposit
        # else:
        #     result = Cash + CashHoldings
        result = Cash
        return result

    ### add on

    def get_BondsPayableToAsset(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        BondsPayable= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BondsPayable")
        STBondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "STBondsPayable")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")

        BondsPayable = BasicOperations(BondsPayable)
        STBondsPayable = BasicOperations(STBondsPayable)
        TotalAssets = BasicOperations(TotalAssets)

        result = (BondsPayable + STBondsPayable)/TotalAssets
        return result.val

    def get_EquityFixedAssetRatio(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalShareholderEquity= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalShareholderEquity")
        ConstructionMaterials = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ConstructionMaterials")
        ConstruInProcess = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "ConstruInProcess")
        FixedAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                       "FixedAssets")

        TotalShareholderEquity = BasicOperations(TotalShareholderEquity)
        ConstructionMaterials = BasicOperations(ConstructionMaterials)
        ConstruInProcess = BasicOperations(ConstruInProcess)
        FixedAssets = BasicOperations(FixedAssets)

        result = TotalShareholderEquity/(ConstructionMaterials + ConstruInProcess + FixedAssets)
        return result.val

    def get_LongDebtToAsset(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        LongtermLoan= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")

        LongtermLoan = BasicOperations(LongtermLoan)
        TotalAssets = BasicOperations(TotalAssets)

        result = (LongtermLoan)/TotalAssets
        return result.val

    def get_LongDebtToWorkingCapital(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalNonCurrentLiability= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability")
        TotalCurrentAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalCurrentAssets")
        TotalCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalCurrentLiability")

        TotalNonCurrentLiability = BasicOperations(TotalNonCurrentLiability)
        TotalCurrentAssets = BasicOperations(TotalCurrentAssets)
        TotalCurrentLiability = BasicOperations(TotalCurrentLiability)

        result = (TotalNonCurrentLiability)/(TotalCurrentAssets - TotalCurrentLiability)
        return result.val

    def get_LongTermDebtToAsset(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalNonCurrentLiability= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")

        TotalNonCurrentLiability = BasicOperations(TotalNonCurrentLiability)
        TotalAssets = BasicOperations(TotalAssets)

        result = (TotalNonCurrentLiability)/TotalAssets
        return result.val

    def get_NonCurrentAssetsRatio(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalNonCurrentAssets= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalNonCurrentAssets")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")

        TotalNonCurrentAssets = BasicOperations(TotalNonCurrentAssets)
        TotalAssets = BasicOperations(TotalAssets)

        result = TotalNonCurrentAssets/TotalAssets
        return result.val

    def get_OperCashInToCurrentLiability(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperCashInToCurrentDebt= self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "OperCashInToCurrentDebt")

        result = OperCashInToCurrentDebt
        return result

