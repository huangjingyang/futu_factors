#-*- coding: utf-8 -*-

# 成长能力因子
from module.process.fundamental.basal_factor import *
import math

class FactorGrowthability(BasalFactor):
    __name__ = "Growthability"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Growth_ocf" in columns_list:
            result_dict['Growth_ocf'] = self.get_growth_ocf(time_info)
        if "Yoyocfps" in columns_list:
            result_dict['Yoyocfps'] = self.get_yoyocfps(time_info)
        if "Yoy_or" in columns_list:
            result_dict['Yoy_or'] = self.get_yoy_or(time_info)
        if "Yoynetprofit_deducted" in columns_list:
            result_dict['Yoynetprofit_deducted'] = self.get_yoynetprofit_deducted(time_info)
        if "Yoyocf" in columns_list:
            result_dict['Yoyocf'] = self.get_yoyocf(time_info)
        if "Yoyroe" in columns_list:
            result_dict['Yoyroe'] = self.get_yoyroe(time_info)
        if "Growth_roe" in columns_list:
            result_dict['Growth_roe'] = self.get_growth_roe(time_info)
        if "Yoyroic" in columns_list:
            result_dict['Yoyroic'] = self.get_yoyroic(time_info)
        if "Yoyroic_3" in columns_list:
            result_dict['Yoyroic_3'] = self.get_yoyroic_3(time_info)
        if "Yoyeps_basic" in columns_list:
            result_dict['Yoyeps_basic'] = self.get_yoyeps_basic(time_info)
        if "Yoyop2" in columns_list:
            result_dict['Yoyop2'] = self.get_yoyop2(time_info)
        if "Yoyebt" in columns_list:
            result_dict['Yoyebt'] = self.get_yoyebt(time_info)
        if "Yoynetprofit" in columns_list:
            result_dict['Yoynetprofit'] = self.get_yoynetprofit(time_info)
        if "Growth_or" in columns_list:
            result_dict['Growth_or'] = self.get_growth_or(time_info)
        if "Growth_op" in columns_list:
            result_dict['Growth_op'] = self.get_growth_op(time_info)
        if "Growth_ebt" in columns_list:
            result_dict['Growth_ebt'] = self.get_growth_ebt(time_info)
        if "Growth_netprofit" in columns_list:
            result_dict['Growth_netprofit'] = self.get_growth_netprofit(time_info)
        if "Growth_assets" in columns_list:
            result_dict['Growth_assets'] = self.get_growth_assets(time_info)
        if "FinancingCashGrowRate" in columns_list:
            result_dict['FinancingCashGrowRate'] = self.get_FinancingCashGrowRate(time_info)
        if "InvestCashGrowRate" in columns_list:
            result_dict['InvestCashGrowRate'] = self.get_InvestCashGrowRate(time_info)
        return result_dict

    def get_yoyocfps(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperCashPSGrowRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                            "OperCashPSGrowRate")
        # OperCashFlowPS_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI",
        #                                                                  "OperCashFlowPS")
        #
        # OperCashFlowPS = BasicOperations(OperCashFlowPS)
        # OperCashFlowPS_LYP = BasicOperations(OperCashFlowPS_LYP)
        # OperCashFlowPS_LYP_abs = BasicOperations(OperCashFlowPS_LYP).abs_value()
        # #OperCashFlowPS_LYP_abs.val = abs(OperCashFlowPS_LYP_abs.val)
        # const_100 = BasicOperations(100)

        result = OperCashPSGrowRate
        return result

    def get_yoynetprofit_deducted(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NPParentCompanyCutYOY = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                            "NPParentCompanyCutYOY")
        # ProfitToShareholders_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "IC",
        #                                                                  "ProfitToShareholders")
        # EndOrSpecProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
        #                                                                     "EndOrSpecProfit")
        # EndOrSpecProfit_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "IC",
        #                                                                  "EndOrSpecProfit")
        #
        # ProfitToShareholders = BasicOperations(ProfitToShareholders)
        # ProfitToShareholders_LYP = BasicOperations(ProfitToShareholders_LYP)
        # EndOrSpecProfit = BasicOperations(EndOrSpecProfit)
        # EndOrSpecProfit_LYP = BasicOperations(EndOrSpecProfit_LYP)
        # ProfitToShareholders_LYP_abs = BasicOperations(ProfitToShareholders_LYP - EndOrSpecProfit_LYP).abs_value()
        # const_100 = BasicOperations(100)
        #ProfitToShareholders_LYP_abs.val = abs(ProfitToShareholders_LYP_abs.val)

        result = NPParentCompanyCutYOY
        return result

    def get_yoyocf(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetOperateCashFlowYOY = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                            "NetOperateCashFlowYOY")
        # NetCashFromOperating_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "CS",
        #                                                                  "NetCashFromOperating")
        #
        # NetCashFromOperating = BasicOperations(NetCashFromOperating)
        # NetCashFromOperating_LYP = BasicOperations(NetCashFromOperating_LYP)
        # NetCashFromOperating_LYP_abs = BasicOperations(NetCashFromOperating_LYP).abs_value()
        # const_100 = BasicOperations(100)
        # #NetCashFromOperating_LYP_abs.val = abs(NetCashFromOperating_LYP_abs.val)

        result = NetOperateCashFlowYOY
        return result

    def get_yoyroe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        ROE = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                            "ROE")
        ROE_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI",
                                                                         "ROE")

        ROE = BasicOperations(ROE)
        ROE_LYP = BasicOperations(ROE_LYP)
        ROE_LYP_abs = BasicOperations(ROE_LYP).abs_value()
        const_100 = BasicOperations(100)
        #ROEWeighted_LYP_abs.val = abs(ROEWeighted_LYP_abs.val)
        result = (ROE - ROE_LYP) / ROE_LYP_abs*const_100
        return result.val


    def get_growth_ocf(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetCashFromOperating = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "CS",
                                                                   "NetOperateCashFlow")
        NetCashFromOperating_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "CS",
                                                                        "NetOperateCashFlow", num=3)

        NetCashFromOperating = BasicOperations(NetCashFromOperating)
        NetCashFromOperating_LYP3 = BasicOperations(NetCashFromOperating_LYP3)
        NetCashFromOperating_LYP3_abs = BasicOperations(NetCashFromOperating_LYP3).abs_value()
        const_100 = BasicOperations(100)
        #NetCashFromOperating_LYP3_abs.val = abs(NetCashFromOperating_LYP3_abs.val)

        result = (NetCashFromOperating - NetCashFromOperating_LYP3)/ NetCashFromOperating_LYP3_abs*const_100
        return result.val

    def get_growth_roe(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        ROEWeighted = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "ROE")
        ROEWeighted_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI",
                                                                        "ROE", num=3)

        ROEWeighted = BasicOperations(ROEWeighted)
        ROEWeighted_LYP3 = BasicOperations(ROEWeighted_LYP3)
        ROEWeighted_LYP3_abs = BasicOperations(ROEWeighted_LYP3).abs_value()
        const_100 = BasicOperations(100)
        #ROEWeighted_LYP3_abs.val = abs(ROEWeighted_LYP3_abs.val)

        result = (ROEWeighted - ROEWeighted_LYP3)/ ROEWeighted_LYP3_abs*const_100
        return result.val

    def get_yoyroic(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPParentCompanyOwners = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "NetProfit")
        NetProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfit")
        TotalLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalLiability")
        AccountsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","AccountsPayable")
        NotAccountsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "NotAccountsPayable")
        AdvanceReceipts = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AdvanceReceipts")
        SalariesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "SalariesPayable")
        TaxsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TaxsPayable")
        OtherPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "OtherPayable")
        AccruedExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AccruedExpense")
        DeferredProceeds = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "DeferredProceeds")
        OtherCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability")
        TotalNonCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BondsPayable")

        NetProfit_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI", "NetProfit")
        TotalLiability_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalLiability")
        AccountsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","AccountsPayable")
        NotAccountsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "NotAccountsPayable")
        AdvanceReceipts_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AdvanceReceipts")
        SalariesPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "SalariesPayable")
        TaxsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TaxsPayable")
        OtherPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherPayable")
        AccruedExpense_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AccruedExpense")
        DeferredProceeds_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "DeferredProceeds")
        OtherCurrentLiability_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability")
        TotalNonCurrentLiability_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability")
        LongtermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        BondsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "BondsPayable")



        NPParentCompanyOwners =  BasicOperations(NPParentCompanyOwners)
        NetProfit =  BasicOperations(NetProfit)
        TotalLiability =  BasicOperations(TotalLiability)
        AccountsPayable =  BasicOperations(AccountsPayable)
        NotAccountsPayable =  BasicOperations(NotAccountsPayable)
        AdvanceReceipts =  BasicOperations(AdvanceReceipts)
        SalariesPayable =  BasicOperations(SalariesPayable)
        TaxsPayable =  BasicOperations(TaxsPayable)
        OtherPayable =  BasicOperations(OtherPayable)
        AccruedExpense =  BasicOperations(AccruedExpense)
        DeferredProceeds =  BasicOperations(DeferredProceeds)
        OtherCurrentLiability =  BasicOperations(OtherCurrentLiability)
        TotalNonCurrentLiability =  BasicOperations(TotalNonCurrentLiability)
        LongtermLoan =  BasicOperations(LongtermLoan)
        BondsPayable =  BasicOperations(BondsPayable)
        NetProfit_LYP =  BasicOperations(NetProfit_LYP)
        TotalLiability_LYP =  BasicOperations(TotalLiability_LYP)
        AccountsPayable_LYP =  BasicOperations(AccountsPayable_LYP)
        NotAccountsPayable_LYP =  BasicOperations(NotAccountsPayable_LYP)
        AdvanceReceipts_LYP =  BasicOperations(AdvanceReceipts_LYP)
        SalariesPayable_LYP =  BasicOperations(SalariesPayable_LYP)
        TaxsPayable_LYP =  BasicOperations(TaxsPayable_LYP)
        OtherPayable_LYP =  BasicOperations(OtherPayable_LYP)
        AccruedExpense_LYP =  BasicOperations(AccruedExpense_LYP)
        DeferredProceeds_LYP =  BasicOperations(DeferredProceeds_LYP)
        OtherCurrentLiability_LYP =  BasicOperations(OtherCurrentLiability_LYP)
        TotalNonCurrentLiability_LYP =  BasicOperations(TotalNonCurrentLiability_LYP)
        LongtermLoan_LYP =  BasicOperations(LongtermLoan_LYP)
        BondsPayable_LYP =  BasicOperations(BondsPayable_LYP)
        const_100 = BasicOperations(100)
        const_2 = BasicOperations(2)
        ROIC = NPParentCompanyOwners*const_2 * const_100/(NetProfit + TotalLiability - AccountsPayable - NotAccountsPayable -
                                          AdvanceReceipts - SalariesPayable - TaxsPayable - OtherPayable -
                                          AccruedExpense - DeferredProceeds - OtherCurrentLiability-
                                          TotalNonCurrentLiability + LongtermLoan + BondsPayable +
                                          NetProfit_LYP + TotalLiability_LYP - AccountsPayable_LYP -
                                          NotAccountsPayable_LYP - AdvanceReceipts_LYP - SalariesPayable_LYP -
                                          TaxsPayable_LYP - OtherPayable_LYP - AccruedExpense_LYP -
                                          DeferredProceeds_LYP - OtherCurrentLiability_LYP-
                                          TotalNonCurrentLiability_LYP + LongtermLoan_LYP + BondsPayable_LYP)

        NPParentCompanyOwners_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "IC", "NetProfit")
        NetProfit_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI", "NetProfit",num=2)
        TotalLiability_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalLiability",num=2)
        AccountsPayable_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","AccountsPayable",num=2)
        NotAccountsPayable_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "NotAccountsPayable",num=2)
        AdvanceReceipts_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AdvanceReceipts",num=2)
        SalariesPayable_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "SalariesPayable",num=2)
        TaxsPayable_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TaxsPayable",num=2)
        OtherPayable_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherPayable",num=2)
        AccruedExpense_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AccruedExpense",num=2)
        DeferredProceeds_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "DeferredProceeds",num=2)
        OtherCurrentLiability_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability",num=2)
        TotalNonCurrentLiability_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability",num=2)
        LongtermLoan_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "LongtermLoan",num=2)
        BondsPayable_LYP2 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "BondsPayable",num=2)

        NPParentCompanyOwners_LYP =  BasicOperations(NPParentCompanyOwners_LYP)
        NetProfit_LYP2 =  BasicOperations(NetProfit_LYP2)
        TotalLiability_LYP2 =  BasicOperations(TotalLiability_LYP2)
        AccountsPayable_LYP2 =  BasicOperations(AccountsPayable_LYP2)
        NotAccountsPayable_LYP2 =  BasicOperations(NotAccountsPayable_LYP2)
        AdvanceReceipts_LYP2 =  BasicOperations(AdvanceReceipts_LYP2)
        SalariesPayable_LYP2 =  BasicOperations(SalariesPayable_LYP2)
        TaxsPayable_LYP2 =  BasicOperations(TaxsPayable_LYP2)
        OtherPayable_LYP2 =  BasicOperations(OtherPayable_LYP2)
        AccruedExpense_LYP2 =  BasicOperations(AccruedExpense_LYP2)
        DeferredProceeds_LYP2 =  BasicOperations(DeferredProceeds_LYP2)
        OtherCurrentLiability_LYP2 =  BasicOperations(OtherCurrentLiability_LYP2)
        TotalNonCurrentLiability_LYP2 =  BasicOperations(TotalNonCurrentLiability_LYP2)
        LongtermLoan_LYP2 =  BasicOperations(LongtermLoan_LYP2)
        BondsPayable_LYP2 =  BasicOperations(BondsPayable_LYP2)

        ROIC_LYP = NPParentCompanyOwners_LYP*const_2 * const_100/(NetProfit_LYP2 + TotalLiability_LYP2 - AccountsPayable_LYP2 -
                                                                  NotAccountsPayable_LYP2 -
                                          AdvanceReceipts_LYP2 - SalariesPayable_LYP2 - TaxsPayable_LYP2 - OtherPayable_LYP2 -
                                          AccruedExpense_LYP2 - DeferredProceeds_LYP2 - OtherCurrentLiability_LYP2-
                                          TotalNonCurrentLiability_LYP2 + LongtermLoan_LYP2 + BondsPayable_LYP2 +
                                          NetProfit_LYP + TotalLiability_LYP - AccountsPayable_LYP -
                                          NotAccountsPayable_LYP - AdvanceReceipts_LYP - SalariesPayable_LYP -
                                          TaxsPayable_LYP - OtherPayable_LYP - AccruedExpense_LYP -
                                          DeferredProceeds_LYP - OtherCurrentLiability_LYP-
                                          TotalNonCurrentLiability_LYP + LongtermLoan_LYP + BondsPayable_LYP)

        ROIC_LYP_abs = BasicOperations(ROIC_LYP).abs_value()

        result = (ROIC - ROIC_LYP)/ROIC_LYP_abs*const_100
        return result.val

    def get_yoyroic_3(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NPParentCompanyOwners = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC", "NetProfit")
        NetProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NetProfit")
        TotalLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalLiability")
        AccountsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","AccountsPayable")
        NotAccountsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "NotAccountsPayable")
        AdvanceReceipts = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AdvanceReceipts")
        SalariesPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "SalariesPayable")
        TaxsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TaxsPayable")
        OtherPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "OtherPayable")
        AccruedExpense = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AccruedExpense")
        DeferredProceeds = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "DeferredProceeds")
        OtherCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability")
        TotalNonCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability")
        LongtermLoan = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        BondsPayable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BondsPayable")

        NetProfit_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI", "NetProfit")
        TotalLiability_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalLiability")
        AccountsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","AccountsPayable")
        NotAccountsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "NotAccountsPayable")
        AdvanceReceipts_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AdvanceReceipts")
        SalariesPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "SalariesPayable")
        TaxsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TaxsPayable")
        OtherPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherPayable")
        AccruedExpense_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AccruedExpense")
        DeferredProceeds_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "DeferredProceeds")
        OtherCurrentLiability_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability")
        TotalNonCurrentLiability_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability")
        LongtermLoan_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "LongtermLoan")
        BondsPayable_LYP = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "BondsPayable")

        NPParentCompanyOwners_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "IC", "NetProfit", num=3)
        NetProfit_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI", "NetProfit",num=3)
        TotalLiability_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalLiability",num=3)
        AccountsPayable_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","AccountsPayable",num=3)
        NotAccountsPayable_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "NotAccountsPayable",num=3)
        AdvanceReceipts_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AdvanceReceipts",num=3)
        SalariesPayable_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "SalariesPayable",num=3)
        TaxsPayable_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TaxsPayable",num=3)
        OtherPayable_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherPayable",num=3)
        AccruedExpense_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AccruedExpense",num=3)
        DeferredProceeds_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "DeferredProceeds",num=3)
        OtherCurrentLiability_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability",num=3)
        TotalNonCurrentLiability_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability",num=3)
        LongtermLoan_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "LongtermLoan",num=3)
        BondsPayable_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "BondsPayable",num=3)

        NetProfit_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "MI", "NetProfit",num=4)
        TotalLiability_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalLiability",num=4)
        AccountsPayable_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","AccountsPayable",num=4)
        NotAccountsPayable_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "NotAccountsPayable",num=4)
        AdvanceReceipts_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AdvanceReceipts",num=4)
        SalariesPayable_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "SalariesPayable",num=4)
        TaxsPayable_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TaxsPayable",num=4)
        OtherPayable_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherPayable",num=4)
        AccruedExpense_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "AccruedExpense",num=4)
        DeferredProceeds_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "DeferredProceeds",num=4)
        OtherCurrentLiability_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "OtherCurrentLiability",num=4)
        TotalNonCurrentLiability_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "TotalNonCurrentLiability",num=4)
        LongtermLoan_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "LongtermLoan",num=4)
        BondsPayable_LYP4 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL", "BondsPayable",num=4)

        NPParentCompanyOwners =  BasicOperations(NPParentCompanyOwners)
        NetProfit =  BasicOperations(NetProfit)
        TotalLiability =  BasicOperations(TotalLiability)
        AccountsPayable =  BasicOperations(AccountsPayable)
        NotAccountsPayable =  BasicOperations(NotAccountsPayable)
        AdvanceReceipts =  BasicOperations(AdvanceReceipts)
        SalariesPayable =  BasicOperations(SalariesPayable)
        TaxsPayable =  BasicOperations(TaxsPayable)
        OtherPayable =  BasicOperations(OtherPayable)
        AccruedExpense =  BasicOperations(AccruedExpense)
        DeferredProceeds =  BasicOperations(DeferredProceeds)
        OtherCurrentLiability =  BasicOperations(OtherCurrentLiability)
        TotalNonCurrentLiability =  BasicOperations(TotalNonCurrentLiability)
        LongtermLoan =  BasicOperations(LongtermLoan)
        BondsPayable =  BasicOperations(BondsPayable)
        NetProfit_LYP =  BasicOperations(NetProfit_LYP)
        TotalLiability_LYP =  BasicOperations(TotalLiability_LYP)
        AccountsPayable_LYP =  BasicOperations(AccountsPayable_LYP)
        NotAccountsPayable_LYP =  BasicOperations(NotAccountsPayable_LYP)
        AdvanceReceipts_LYP =  BasicOperations(AdvanceReceipts_LYP)
        SalariesPayable_LYP =  BasicOperations(SalariesPayable_LYP)
        TaxsPayable_LYP =  BasicOperations(TaxsPayable_LYP)
        OtherPayable_LYP =  BasicOperations(OtherPayable_LYP)
        AccruedExpense_LYP =  BasicOperations(AccruedExpense_LYP)
        DeferredProceeds_LYP =  BasicOperations(DeferredProceeds_LYP)
        OtherCurrentLiability_LYP =  BasicOperations(OtherCurrentLiability_LYP)
        TotalNonCurrentLiability_LYP =  BasicOperations(TotalNonCurrentLiability_LYP)
        LongtermLoan_LYP =  BasicOperations(LongtermLoan_LYP)
        BondsPayable_LYP =  BasicOperations(BondsPayable_LYP)
        const_100 = BasicOperations(100)
        const_2 = BasicOperations(2)

        NPParentCompanyOwners_LYP3 =  BasicOperations(NPParentCompanyOwners_LYP3)
        NetProfit_LYP3 =  BasicOperations(NetProfit_LYP3)
        TotalLiability_LYP3 =  BasicOperations(TotalLiability_LYP3)
        AccountsPayable_LYP3 =  BasicOperations(AccountsPayable_LYP3)
        NotAccountsPayable_LYP3 =  BasicOperations(NotAccountsPayable_LYP3)
        AdvanceReceipts_LYP3 =  BasicOperations(AdvanceReceipts_LYP3)
        SalariesPayable_LYP3 =  BasicOperations(SalariesPayable_LYP3)
        TaxsPayable_LYP3 =  BasicOperations(TaxsPayable_LYP3)
        OtherPayable_LYP3 =  BasicOperations(OtherPayable_LYP3)
        AccruedExpense_LYP3 =  BasicOperations(AccruedExpense_LYP3)
        DeferredProceeds_LYP3 =  BasicOperations(DeferredProceeds_LYP3)
        OtherCurrentLiability_LYP3 =  BasicOperations(OtherCurrentLiability_LYP3)
        TotalNonCurrentLiability_LYP3 =  BasicOperations(TotalNonCurrentLiability_LYP3)
        LongtermLoan_LYP3 =  BasicOperations(LongtermLoan_LYP3)
        BondsPayable_LYP3 =  BasicOperations(BondsPayable_LYP3)

        NetProfit_LYP4 =  BasicOperations(NetProfit_LYP4)
        TotalLiability_LYP4 =  BasicOperations(TotalLiability_LYP4)
        AccountsPayable_LYP4 =  BasicOperations(AccountsPayable_LYP4)
        NotAccountsPayable_LYP4 =  BasicOperations(NotAccountsPayable_LYP4)
        AdvanceReceipts_LYP4 =  BasicOperations(AdvanceReceipts_LYP4)
        SalariesPayable_LYP4 =  BasicOperations(SalariesPayable_LYP4)
        TaxsPayable_LYP4 =  BasicOperations(TaxsPayable_LYP4)
        OtherPayable_LYP4 =  BasicOperations(OtherPayable_LYP4)
        AccruedExpense_LYP4 =  BasicOperations(AccruedExpense_LYP4)
        DeferredProceeds_LYP4 =  BasicOperations(DeferredProceeds_LYP4)
        OtherCurrentLiability_LYP4 =  BasicOperations(OtherCurrentLiability_LYP4)
        TotalNonCurrentLiability_LYP4 =  BasicOperations(TotalNonCurrentLiability_LYP4)
        LongtermLoan_LYP4 =  BasicOperations(LongtermLoan_LYP4)
        BondsPayable_LYP4 =  BasicOperations(BondsPayable_LYP4)



        ROIC = NPParentCompanyOwners*const_2 * const_100/(NetProfit + TotalLiability - AccountsPayable - NotAccountsPayable -
                                          AdvanceReceipts - SalariesPayable - TaxsPayable - OtherPayable -
                                          AccruedExpense - DeferredProceeds - OtherCurrentLiability-
                                          TotalNonCurrentLiability + LongtermLoan + BondsPayable +
                                          NetProfit_LYP + TotalLiability_LYP - AccountsPayable_LYP -
                                          NotAccountsPayable_LYP - AdvanceReceipts_LYP - SalariesPayable_LYP -
                                          TaxsPayable_LYP - OtherPayable_LYP - AccruedExpense_LYP -
                                          DeferredProceeds_LYP - OtherCurrentLiability_LYP-
                                          TotalNonCurrentLiability_LYP + LongtermLoan_LYP + BondsPayable_LYP)





        ROIC_LYP3 = NPParentCompanyOwners_LYP3*const_2 * const_100/(NetProfit_LYP3 + TotalLiability_LYP3 - AccountsPayable_LYP3 -
                                                                  NotAccountsPayable_LYP3 -
                                          AdvanceReceipts_LYP3 - SalariesPayable_LYP3 - TaxsPayable_LYP3 - OtherPayable_LYP3 -
                                          AccruedExpense_LYP3 - DeferredProceeds_LYP3 - OtherCurrentLiability_LYP3-
                                          TotalNonCurrentLiability_LYP3 + LongtermLoan_LYP3 + BondsPayable_LYP3 +
                                          NetProfit_LYP4 + TotalLiability_LYP4 - AccountsPayable_LYP4 -
                                          NotAccountsPayable_LYP4 - AdvanceReceipts_LYP4 - SalariesPayable_LYP4 -
                                          TaxsPayable_LYP4 - OtherPayable_LYP4 - AccruedExpense_LYP4 -
                                          DeferredProceeds_LYP4 - OtherCurrentLiability_LYP4-
                                          TotalNonCurrentLiability_LYP4 + LongtermLoan_LYP4 + BondsPayable_LYP4)

        ROIC_LYP3_abs = BasicOperations(ROIC_LYP3).abs_value()

        result = (ROIC - ROIC_LYP3)/ROIC_LYP3_abs*const_100
        return result.val

    def get_yoyeps_basic(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        BasicEPS1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "BasicEPSYOY")
        result = BasicEPS1Y
        return result

    def get_yoy_or(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingRevenueGR1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "OperatingRevenueGrowRate")
        result = OperatingRevenueGR1Y
        return result

    def get_yoyop2(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperProfitGR1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "OperProfitGrowRate")
        result = OperProfitGR1Y
        return result

    def get_yoyebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        EBTGR1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "TotalProfeiGrowRate")
        result = EBTGR1Y
        return result

    def get_yoynetprofit(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NPParentCompanyGR1Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "NPParentCompanyYOY")
        result = NPParentCompanyGR1Y
        return result

    def get_growth_or(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingRevenueGR3Y = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                      "ORComGrowRate3Y")
        result = OperatingRevenueGR3Y
        return result

    def get_growth_op(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperatingProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                   "OperatingProfit")
        OperatingProfit_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "IC",
                                                                        "OperatingProfit", num=3)

        OperatingProfit = BasicOperations(OperatingProfit)
        OperatingProfit_LYP3 = BasicOperations(OperatingProfit_LYP3)
        OperatingProfit_LYP3_abs = BasicOperations(OperatingProfit_LYP3).abs_value()
        const_100 = BasicOperations(100)
        #ROEWeighted_LYP3_abs.val = abs(ROEWeighted_LYP3_abs.val)

        result = (OperatingProfit - OperatingProfit_LYP3)/ OperatingProfit_LYP3_abs*const_100
        return result.val

    def get_growth_ebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                   "NetProfit")
        NetProfit_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "IC",
                                                                        "NetProfit", num=3)

        NetProfit = BasicOperations(NetProfit)
        NetProfit_LYP3 = BasicOperations(NetProfit_LYP3)
        NetProfit_LYP3_abs = BasicOperations(NetProfit_LYP3).abs_value()
        const_100 = BasicOperations(100)
        #ROEWeighted_LYP3_abs.val = abs(ROEWeighted_LYP3_abs.val)

        result = (NetProfit - NetProfit_LYP3)/NetProfit_LYP3_abs*const_100
        return result.val

    def get_growth_netprofit(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NetProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                   "NetProfit")
        NetProfit_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "IC",
                                                                        "NetProfit", num=3)

        NetProfit = BasicOperations(NetProfit)
        NetProfit_LYP3 = BasicOperations(NetProfit_LYP3)
        NetProfit_LYP3_abs = BasicOperations(NetProfit_LYP3).abs_value()
        const_100 = BasicOperations(100)
        #ROEWeighted_LYP3_abs.val = abs(ROEWeighted_LYP3_abs.val)

        result = (NetProfit - NetProfit_LYP3)/NetProfit_LYP3_abs*const_100
        return result.val

    def get_growth_assets(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                   "TotalAssets")
        TotalAssets_LYP3 = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                        "TotalAssets", num=3)

        TotalAssets = BasicOperations(TotalAssets)
        TotalAssets_LYP3 = BasicOperations(TotalAssets_LYP3)
        TotalAssets_LYP3_abs = BasicOperations(TotalAssets_LYP3).abs_value()
        const_100 = BasicOperations(100)
        #ROEWeighted_LYP3_abs.val = abs(ROEWeighted_LYP3_abs.val)

        result = (TotalAssets - TotalAssets_LYP3)/TotalAssets_LYP3_abs*const_100
        return result.val

    def get_FinancingCashGrowRate(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        time_info_temp = time_info.copy()
        end_time = time_info_temp['EndDate']
        end_time_year_ago = end_time - pd.DateOffset(years=1)
        time_info_temp['EndDate'] = end_time_year_ago
        NetFinanceCashFlow_LYPTTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info_temp,
                                                                         "CS", "NetFinanceCashFlow", num=1)
        # print(NetFinanceCashFlow_LYPTTM, time_info_temp)
        NetFinanceCashFlow_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "CS",
                                                                   "NetFinanceCashFlow")

        NetFinanceCashFlow_TTM = BasicOperations(NetFinanceCashFlow_TTM)
        NetFinanceCashFlow_LYPTTM = BasicOperations(NetFinanceCashFlow_LYPTTM)
        NetFinanceCashFlow_LYPTTM_abs = BasicOperations(NetFinanceCashFlow_LYPTTM).abs_value()
        const_100 = BasicOperations(100)

        result = (NetFinanceCashFlow_TTM - NetFinanceCashFlow_LYPTTM)/NetFinanceCashFlow_LYPTTM_abs * const_100
        return result.val

    def get_InvestCashGrowRate(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        time_info_temp = time_info.copy()
        end_time = time_info_temp['EndDate']
        end_time_year_ago = end_time - pd.DateOffset(years=1)
        time_info_temp['EndDate'] = end_time_year_ago
        NetInvestCashFlow_LYPTTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info_temp,
                                                                         "CS", "NetInvestCashFlow", num=1)
        # print(NetFinanceCashFlow_LYPTTM, time_info_temp)
        NetInvestCashFlow_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "CS",
                                                                   "NetInvestCashFlow")

        NetInvestCashFlow_TTM = BasicOperations(NetInvestCashFlow_TTM)
        NetInvestCashFlow_LYPTTM = BasicOperations(NetInvestCashFlow_LYPTTM)
        NetInvestCashFlow_LYPTTM_abs = BasicOperations(NetInvestCashFlow_LYPTTM).abs_value()
        const_100 = BasicOperations(100)

        result = (NetInvestCashFlow_TTM - NetInvestCashFlow_LYPTTM)/NetInvestCashFlow_LYPTTM_abs * const_100
        return result.val
