#-*- coding: utf-8 -*-

# 偿债能力因子
from module.process.fundamental.basal_factor import *

class FactorDebtpayingability(BasalFactor):
    __name__ = "Debtpayingability"
    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Ebitdatodebt" in columns_list:
            result_dict['Ebitdatodebt'] = self.get_ebitdatodebt(time_info)
        if "Tangibleassettodebt" in columns_list:
            result_dict['Tangibleassettodebt'] = self.get_tangibleassettodebt(time_info)
        if "Current" in columns_list:
            result_dict['Current'] = self.get_current(time_info)
        if "Quick" in columns_list:
            result_dict['Quick'] = self.get_quick(time_info)
        if "Optoliqdebt" in columns_list:
            result_dict['Optoliqdebt'] = self.get_optoliqdebt(time_info)
        if "Ocftoliqdebt" in columns_list:
            result_dict['Ocftoliqdebt'] = self.get_ocftoliqdebt(time_info)
        if "Equitytodebt" in columns_list:
            result_dict['Equitytodebt'] = self.get_equitytodebt(time_info)
        if "Dtoa" in columns_list:
            result_dict['Dtoa'] = self.get_dtoa(time_info)
        return result_dict

    def get_ebitdatodebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        EBITDAToTLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "EBITDAToTLiability")

        result = EBITDAToTLiability
        return result

    def get_tangibleassettodebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        DebtTangibleEquityRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                       "DebtTangibleEquityRatio")

        DebtTangibleEquityRatio = BasicOperations(DebtTangibleEquityRatio)
        const_1 = BasicOperations(1)


        result = const_1/ DebtTangibleEquityRatio
        return result.val

    def get_current(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        CurrentRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "CurrentRatio")
        result = CurrentRatio
        return result

    def get_quick(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        QuickRatio = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "QuickRatio")
        result = QuickRatio
        return result


    def get_optoliqdebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperatingProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                      "OperatingProfit")
        TotalCurrentLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL",
                                                                      "TotalCurrentLiability")
        OperatingProfit = BasicOperations(OperatingProfit)
        TotalCurrentLiability = BasicOperations(TotalCurrentLiability)
        result = OperatingProfit/TotalCurrentLiability
        return result.val


    def get_ocftoliqdebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperCashFlowToCL = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "NOCFToCurrentLiability")
        result = OperCashFlowToCL
        return result


    def get_equitytodebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        SEWithoutMIToTL = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "SEWithoutMIToTL")
        result = SEWithoutMIToTL
        return result

    def get_dtoa(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalLiability = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalLiability")
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")

        TotalLiability = BasicOperations(TotalLiability)
        TotalAssets = BasicOperations(TotalAssets)

        result = TotalLiability / TotalAssets
        return result.val

    '''
    def get_blev(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalNCurrentLiability_num = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","TotalNCurrentLiability")
        TotalNCurrentLiability_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL","TotalNCurrentLiability")

        TotalEquity_num = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL","TotalInterests")
        TotalEquity_lyp = self.common_function_obj.get_data_point_lyp(stock_info, data_dict, time_info, "BL",
                                                                  "TotalInterests")
        TotalNCurrentLiability_num = BasicOperations(TotalNCurrentLiability_num)
        TotalNCurrentLiability_lyp = BasicOperations(TotalNCurrentLiability_lyp)
        TotalEquity_num = BasicOperations(TotalEquity_num)
        TotalEquity_lyp = BasicOperations(TotalEquity_lyp)
        if TotalNCurrentLiability_lyp.val == None or str(TotalNCurrentLiability_lyp.val) == 'nan':
            TotalNCurrentLiability = TotalNCurrentLiability_num
        else:
            TotalNCurrentLiability = BasicOperations((TotalNCurrentLiability_num.val + TotalNCurrentLiability_lyp.val)/2)
        if TotalEquity_lyp.val == None or str(TotalEquity_lyp.val) == 'nan':
            TotalEquity = TotalEquity_num
        else:
            TotalEquity = BasicOperations((TotalEquity_num.val + TotalEquity_lyp.val)/2)
        result = TotalNCurrentLiability / TotalEquity
        return result.val
    '''

