#-*- coding: utf-8 -*-

# 营运能力因子
from module.process.fundamental.basal_factor import *

class FactorOperationability(BasalFactor):
    __name__ = "Operationability"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Arturn" in columns_list:
            result_dict['Arturn'] = self.get_arturn(time_info)
        if "Netaccountsreceivable_lyr" in columns_list:
            result_dict['Netaccountsreceivable_lyr'] = self.get_netaccountsreceivable_lyr(time_info)
        if "Faturn" in columns_list:
            result_dict['Faturn'] = self.get_faturn(time_info)
        if "Assetsturn" in columns_list:
            result_dict['Assetsturn'] = self.get_assetsturn(time_info)
        if "Caturn" in columns_list:
            result_dict['Caturn'] = self.get_caturn(time_info)
        if "Invturn" in columns_list:
            result_dict['Invturn'] = self.get_invturn(time_info)
        if "Operatingincome_ttm" in columns_list:
            result_dict['Operatingincome_ttm'] = self.get_operatingincome_ttm(time_info)
        if "Operatingincome_lyr" in columns_list:
            result_dict['Operatingincome_lyr'] = self.get_operatingincome_lyr(time_info)
        if "Netcashflow_ttm" in columns_list:
            result_dict['Netcashflow_ttm'] = self.get_netcashflow_ttm(time_info)
        if "Netcashflow_lyr" in columns_list:
            result_dict['Netcashflow_lyr'] = self.get_netcashflow_lyr(time_info)
        if "Netcashfromoperating_ttm" in columns_list:
            result_dict['Netcashfromoperating_ttm'] = self.get_netcashfromoperating_ttm(time_info)
        if "Netcashfromoperating_lyr" in columns_list:
            result_dict['Netcashfromoperating_lyr'] = self.get_netcashfromoperating_lyr(time_info)
        if "Netaccountsreceivable" in columns_list:
            result_dict['Netaccountsreceivable'] = self.get_netaccountsreceivable(time_info)
        if "Netaccountsreceivable_ttm" in columns_list:
            result_dict['Netaccountsreceivable_ttm'] = self.get_netaccountsreceivable_ttm(time_info)
        if "AccountsPayablesTDays" in columns_list:
            result_dict['AccountsPayablesTDays'] = self.get_AccountsPayablesTDays(time_info)
        if "AdminiExpenseRate" in columns_list:
            result_dict['AdminiExpenseRate'] = self.get_AdminiExpenseRateTTM(time_info)
        if "AccountsPayablesTRate" in columns_list:
            result_dict['AccountsPayablesTRate'] = self.get_AccountsPayablesTRate(time_info)
        if "ASSI" in columns_list:
            result_dict['ASSI'] = self.get_ASSI(time_info)
        if "InventoryTDays" in columns_list:
            result_dict['InventoryTDays'] = self.get_InventoryTDays(time_info)
        return result_dict

    def get_arturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        ARTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "ARTRate")

        result = ARTRate
        return result


    def get_assetsturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        TotalAssetTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "TotalAssetTRate")
        result = TotalAssetTRate
        return result

    def get_caturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        CurrentAssetsTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "CurrentAssetsTRate")
        result = CurrentAssetsTRate
        return result

    def get_invturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        InventoryTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "InventoryTRate")
        result = InventoryTRate
        return result

    def get_faturn(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        FixedAssetTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "FixedAssetTRate")
        result = FixedAssetTRate
        return result

    def get_operatingincome_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingRevenue_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "IC", "OperatingRevenue")

        result = OperatingRevenue_TTM
        return result

    def get_operatingincome_lyr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingRevenue_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "IC", "OperatingRevenue")

        result = OperatingRevenue_LYR
        return result

    def get_netcashflow_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingIncome_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "CS", "NetOperateCashFlow")

        result = OperatingIncome_TTM
        return result

    def get_netcashflow_lyr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        OperatingIncome_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "CS", "NetOperateCashFlow")

        result = OperatingIncome_LYR
        return result

    def get_netcashfromoperating_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetCashFromOperating_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "CS", "NetOperateCashFlow")

        result = NetCashFromOperating_TTM
        return result

    def get_netcashfromoperating_lyr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetCashFromOperating_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "CS", "NetOperateCashFlow")

        result = NetCashFromOperating_LYR
        return result


    def get_netaccountsreceivable(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        AccountReceivable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "AccountReceivable")
        # BillReceivable = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "BillReceivable")

        # AccountReceivable = BasicOperations(AccountReceivable)
        # BillReceivable = BasicOperations(BillReceivable)

        # result = AccountReceivable + BillReceivable
        result = AccountReceivable
        return result


    def get_netaccountsreceivable_lyr(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        AccountReceivable_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "BL", "AccountReceivable")
        # BillReceivable_LYR = self.common_function_obj.get_data_point_lyr(stock_info, data_dict, time_info, "BL", "BillReceivable")
        #
        # AccountReceivable_LYR = BasicOperations(AccountReceivable_LYR)
        # BillReceivable_LYR = BasicOperations(BillReceivable_LYR)

        # result = AccountReceivable_LYR + BillReceivable_LYR
        result = AccountReceivable_LYR
        return result


    def get_netaccountsreceivable_ttm(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        AccountReceivable_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "BL", "AccountReceivable")
        # BillReceivable_TTM = self.common_function_obj.get_data_point_ttm(stock_info, data_dict, time_info, "BL", "BillReceivable")
        #
        # AccountReceivable_TTM = BasicOperations(AccountReceivable_TTM)
        # BillReceivable_TTM = BasicOperations(BillReceivable_TTM)

        # result = AccountReceivable_TTM + BillReceivable_TTM
        result = AccountReceivable_TTM
        return result

    def get_AccountsPayablesTDays(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        AccountsPayablesTDays = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "AccountsPayablesTDays")
        result = AccountsPayablesTDays
        return result


    def get_AccountsPayablesTRate(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        AccountsPayablesTRate = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "AccountsPayablesTRate")
        result = AccountsPayablesTRate
        return result

    def get_AdminiExpenseRateTTM(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        AdminiExpenseRateTTM = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "AdminiExpenseRateTTM")
        result = AdminiExpenseRateTTM
        return result

    def get_InventoryTDays(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        InventoryTDays = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI", "InventoryTDays")
        result = InventoryTDays
        return result

    def get_ASSI(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        TotalAssets = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "BL", "TotalAssets")
        if TotalAssets == None or str(TotalAssets) == 'nan':
            return None
        result = np.log(TotalAssets)
        return result