#-*- coding: utf-8 -*-

# 现金流情况因子

from module.process.fundamental.basal_factor import *
import math


class FactorCashflow(BasalFactor):
    __name__ = "Cashflow"

    def __init__(self, stock_info_dict, data_dict):
        BasalFactor.__init__(self, stock_info_dict, data_dict)
        pass

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Ocftosales" in columns_list:
            result_dict['Ocftosales'] = self.get_ocftosales(time_info)
        if "Ocftooperateincome" in columns_list:
            result_dict['Ocftooperateincome'] = self.get_ocftooperateincome(time_info)
        if "Optoebt" in columns_list:
            result_dict['Optoebt'] = self.get_optoebt(time_info)
        if "Opetoebt" in columns_list:
            result_dict['Opetoebt'] = self.get_opetoebt(time_info)
        if "Ebtoebt" in columns_list:
            result_dict['Ebtoebt'] = self.get_ebtoebt(time_info)
        if "CashToCurrentLiability" in columns_list:
            result_dict['CashToCurrentLiability'] = self.get_CashToCurrentLiability(time_info)
        if "NOCFToOperatingNI" in columns_list:
            result_dict['NOCFToOperatingNI'] = self.get_NOCFToOperatingNI(time_info)
        return result_dict

    def get_ocftosales(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        CashRateOfSales = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "CashRateOfSales")
        result = CashRateOfSales
        return result

    def get_ocftooperateincome(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NOCFToOperatingNI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "NOCFToOperatingNI")
        result = NOCFToOperatingNI
        return result

    def get_optoebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperatingNIToTP = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "OperatingNIToTP")
        result = OperatingNIToTP
        return result

    def get_opetoebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperatingNIToTP = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "OperatingNIToTP")
        result = OperatingNIToTP
        return result

    def get_ebtoebt(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict

        NetProfitCut = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                 "NetProfitCut")
        TotalProfit = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "IC",
                                                                         "TotalProfit")

        NetProfitCut = BasicOperations(NetProfitCut)
        TotalProfit = BasicOperations(TotalProfit)
        const_100 = BasicOperations(100)

        result = NetProfitCut / TotalProfit*const_100
        return result.val

    def get_CashToCurrentLiability(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        OperCashInToCurrentDebt = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "OperCashInToCurrentDebt")
        result = OperCashInToCurrentDebt
        return result

    def get_NOCFToOperatingNI(self, time_info):
        result = None
        stock_info = self.stock_info_dict
        data_dict = self.data_dict
        NOCFToOperatingNI = self.common_function_obj.get_data_point_num(stock_info, data_dict, time_info, "MI",
                                                                   "NOCFToOperatingNI")
        result = NOCFToOperatingNI
        return result