#-*- coding: utf-8 -*-

from module.process.fundamental.factors.factor_profitability import *
from module.process.fundamental.factors.factor_debtpayingability import *
from module.process.fundamental.factors.factor_resolutionability import *
from module.process.fundamental.factors.factor_operationability import *
from module.process.fundamental.factors.factor_growthability import *
from module.process.fundamental.factors.factor_cashflow import *
from module.process.fundamental.factors.factor_marketperformance import *

class FactorsCalculate(object):
    def __init__(self, stock_info_dict, financial_data_dict):
        self.stock_info_dict = stock_info_dict
        self.financial_data_dict = financial_data_dict
        #self.account_standard = financial_data_dict['Standard']
        #self.logger = self.get_logger()
        self.func_dict = {"Profitability": 'self.calculate_profitability()',
                          "Debtpayingability": 'self.calculate_debtpayingability()',
                          "Resolutionability": 'self.calculate_resolutionability()',
                          "Operationability": 'self.calculate_operationability()',
                          "Growthability" : 'self.calculate_growthability()',
                          "Cashflow" : 'self.calculate_cashflow()',
                          "MarketPerformance" : 'self.calculate_marketperformance()'}
        self.func_list = ["Profitability", "Debtpayingability", "Resolutionability", "Operationability", "Growthability", "Cashflow", "MarketPerformance"]

        #self.func_list = ["Profitability","Debtpayingability"]


    def calculate_factors(self):
        for i in range(len(self.func_list)):
            self.run_caculate_func(self.func_list[i])


    def run_caculate_func(self, func_key):
        eval(self.func_dict[func_key])
        pass
    # 盈利能力
    def calculate_profitability(self):
        factor_profitability_obj = FactorProfitability(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()

    # 偿债能力
    def calculate_debtpayingability(self):
        factor_profitability_obj = FactorDebtpayingability(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()


    # 清债能力
    def calculate_resolutionability(self):
        factor_profitability_obj = FactorResolutionability(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()

    # 营运能力因子
    def calculate_operationability(self):
        factor_profitability_obj = FactorOperationability(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()

    # 成长能力因子
    def calculate_growthability(self):
        factor_profitability_obj = FactorGrowthability(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()


    # 现金流情况因子
    def calculate_cashflow(self):
        factor_profitability_obj = FactorCashflow(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()

    # 市场表现因子
    def calculate_marketperformance(self):
        factor_profitability_obj = FactorMarketPerformance(self.stock_info_dict, self.financial_data_dict)
        factor_profitability_obj.get_handle_data()