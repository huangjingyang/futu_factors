#-*- coding: utf-8 -*-

# 基类计算
# 主要为了解决计算的问题

class CalculateBasic():
    def __init__(self, source_df, start_time, end_time):
        self.source_df = source_df
        self.start_time = start_time
        self.end_time = end_time
        pass

    # 获取上一年的年初数据：12'
    def get_last_year_datapoint(self):
        pass

    # 获取上一年的同期数据，month'
    def get_last_year_corresponding_period(self):
        pass