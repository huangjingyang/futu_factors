#-*- coding: utf-8 -*-

import datetime
import pickle
import json
from sqlalchemy import create_engine, text
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.exc import DisconnectionError, OperationalError, SQLAlchemyError
import pandas as pd
import numpy as np
import math
from module.basic.currency_convert import *


# 静态类
class CommonFunction():
    def __init__(self):
        pass

    # 日期转化为字符
    @classmethod
    def convert_datetime_to_string(self, datetime_foramt):
        datetime_str = ""
        datetime_str = datetime_foramt.strftime('%Y-%m-%d')
        return datetime_str

    # 字符转化为日期
    @classmethod
    def convert_string_to_datetime(self, datetime_str):
        datetime_foramt = datetime.datetime.min
        datetime_foramt = datetime.datetime.strptime(datetime_str, '%Y-%m-%d')
        return datetime_foramt

    # 取相对值
    @classmethod
    def get_data_point_lastest_date(self, stock_info, data_dict, time_info, table_name, data_name):
        inner_code = stock_info['InnerCode']
        data_point_value = np.nan
        end_time = self.convert_datetime_to_string(time_info['EndDate'])
        with open("config/formula.json") as f:
            config_data = json.load(f)
        # print(config_data)
        data_dict_key = config_data['fundamental']['table_names'][table_name]
        # print(data_dict_key)
        table_data = data_dict[data_dict_key]
        EndDate = self.convert_string_to_datetime(end_time)
        temp = table_data[table_data['EndDate'] <= EndDate]
        temp_series = temp[data_name].copy()
        temp_series = temp_series.reset_index(drop=True)
        # temp_value = temp_series[data_name].get(0)
        data_point_value = temp_series.get(0)
        return data_point_value

    # 输入，名称，时间点，输出具体的值
    @classmethod
    def get_data_point(self, stock_info, data_dict, time_info, table_name, data_name):
        inner_code = stock_info['InnerCode']
        data_point_value = np.nan
        end_time = self.convert_datetime_to_string(time_info['EndDate'])
        with open("config/formula.json") as f:
            config_data = json.load(f)
        #print(config_data)
        data_dict_key = config_data['fundamental']['table_names'][table_name]
        #print(data_dict_key)
        table_data = data_dict[data_dict_key]
        EndDate = self.convert_string_to_datetime(end_time)
        temp = table_data[table_data['EndDate'] == EndDate]
        temp_series = temp[data_name].copy()
        temp_series  = temp_series.reset_index(drop=True)
        #temp_value = temp_series[data_name].get(0)
        data_point_value = temp_series.get(0)
        return data_point_value


    '''
    # 取行情数据
    @classmethod
    def get_daily_quote(self, inner_code, data_name, date_time_str):
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        engine = create_engine(engine_str)
        quote_stmt = text("""select * from QT_HKDailyQuoteIndex where InnerCode ='{inner_code}' and TradingDay = Date('{date_time_str}');""".format(inner_code=inner_code, date_time_str=date_time_str))
        quote_df = pd.read_sql(quote_stmt, engine)
        temp_series = quote_df[data_name].copy()
        temp_series = temp_series.reset_index(drop=True)
        # temp_value = temp_series[data_name].get(0)
        data_point_value = temp_series.get(0)
        return data_point_value
    '''

    # 取上一年年末数据
    @classmethod
    def get_data_point_lyr(self, stock_info, data_dict, time_info, table_name, data_name, num=1):
        time_info_temp = time_info.copy()
        end_time = time_info_temp['EndDate']
        end_time_year_ago = end_time.replace(year=end_time.year - num, month=12, day=31)
        time_info_temp['EndDate'] = end_time_year_ago
        data_point_value = self.get_data_point_num(stock_info, data_dict, time_info_temp, table_name, data_name)
        return data_point_value

    # 去上一年的同期
    @classmethod
    def get_data_point_lyp(self, stock_info, data_dict, time_info, table_name, data_name, num=1):
        time_info_temp = time_info.copy()
        end_time = time_info_temp['EndDate']
        #end_time_last_year_period = end_time.replace(year=end_time.year - num)
        # - pd.DateOffset(years=years_num)
        end_time_last_year_period = end_time - pd.DateOffset(years=num)
        time_info_temp['EndDate'] = end_time_last_year_period
        data_point_value = self.get_data_point_num(stock_info, data_dict, time_info_temp, table_name, data_name)
        return data_point_value

    # 取TTM数据
    # 计算逻辑
    # 年报：直接取
    # 其他： 原始数据 + 上一年年报数据 - 上一期数据
    @classmethod
    def get_data_point_ttm(self, stock_info, data_dict, time_info, table_name, data_name, num=1):
        result_value = None
        current_value = self.get_data_point_num(stock_info, data_dict, time_info, table_name, data_name)
        year_ago_value = self.get_data_point_lyr(stock_info, data_dict, time_info, table_name, data_name, num)
        year_ago_period_value = self.get_data_point_lyp(stock_info, data_dict, time_info, table_name, data_name, num)
        try:
            result_value = current_value + year_ago_value - year_ago_period_value
        except:
            result_value = None
        return result_value

    '''
    # 获取当时当时时间点的汇率
    @classmethod
    def get_currency_data(self, stock_info, data_dict, time_info, table_name):
        currency_unit_code = time_info['CurrencyUnitCode']
        convert_time = time_info['EndDate']
        currency_hkd_obj = CurrencyConvertHKD()
        currency_value = currency_hkd_obj.get_currency_info(currency_unit_code, convert_time)
        return currency_value
    '''

    # 货币转换取值
    @classmethod
    def get_data_point_num(self, stock_info, data_dict, time_info, table_name, data_name):
        result = None
        data_point = self.get_data_point(stock_info, data_dict, time_info, table_name, data_name)
        if data_name == None:
            return result
        else:
            result = data_point
        return result
        #if table_name in ["BL", "IC", "CS", "MI", "SS"]:
        #    currency_value = self.get_currency_data(stock_info, data_dict, time_info, table_name)
        #    try:
        #        result = data_point * currency_value
        #    except:
        #        result = None
        #else:
        #    result = data_point
        #return result
