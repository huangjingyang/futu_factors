#-*- coding: utf-8 -*-

# 基础量价因子
from module.process.volume_price.basic_factor import *

class VPCommonFactors(BasicFactor):
    __name__ = "VPCommonFactors"

    def __init__(self, stock_info_dict, time_info_dict, data_source_dict):
        BasicFactor.__init__(self, stock_info_dict, time_info_dict, data_source_dict)

    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Ps" in columns_list:
            result_dict['Ps'] = self.get_ps(time_info)
        if "Ps_ttm" in columns_list:
            result_dict['Ps_ttm'] = self.get_ps_ttm(time_info)
        if "Ps_lyr" in columns_list:
            result_dict['Ps_lyr'] = self.get_ps_lyr(time_info)
        if "Pcf_nfl" in columns_list:
            result_dict['Pcf_nfl'] = self.get_pcf_nfl(time_info)
        if "Pcf_nfl_ttm" in columns_list:
            result_dict['Pcf_nfl_ttm'] = self.get_pcf_nfl_ttm(time_info)
        if "Pcf_nfl_lyr" in columns_list:
            result_dict['Pcf_nfl_lyr'] = self.get_pcf_nfl_lyr(time_info)
        if "Pcf_ocf" in columns_list:
            result_dict['Pcf_ocf'] = self.get_pcf_ocf(time_info)
        if "Pcf_ocf_ttm" in columns_list:
            result_dict['Pcf_ocf_ttm'] = self.get_pcf_ocf_ttm(time_info)
        if "Pcf_ocf_lyr" in columns_list:
            result_dict['Pcf_ocf_lyr'] = self.get_pcf_ocf_lyr(time_info)
        if "Btop" in columns_list:
            result_dict['Btop'] = self.get_btop(time_info)
        if "Lncap" in columns_list:
            result_dict['Lncap'] = self.get_lncap(time_info)
        if "Midcap" in columns_list:
            result_dict['Midcap'] = self.get_midcap(time_info)
        if "Etop" in columns_list:
            result_dict['Etop'] = self.get_etop(time_info)
        if "Cetop" in columns_list:
            result_dict['Cetop'] = self.get_cetop(time_info)
        if "Mlev" in columns_list:
            result_dict['Mlev'] = self.get_mlev(time_info)
        if "Blev" in columns_list:
            result_dict['Blev'] = self.get_blev(time_info)
        if "SW_Industry" in columns_list:
            result_dict['SW_Industry'] = self.get_sw_industry(time_info)
        if "Hbeta" in columns_list:
            hbeta = self.get_hbeta(time_info)
            result_dict['Hbeta'] = hbeta
        if "Halpha" in columns_list:
            halpha = self.get_alpha(time_info)
            result_dict['Halpha'] = halpha
        if "Hsigma" in columns_list:
            hsigma = self.get_hsigma(time_info)
            result_dict['Hsigma'] = hsigma
        if "Cmra" in columns_list:
            cmra = self.get_cmra(time_info)
            result_dict['Cmra'] = cmra
        if "Stom" in columns_list:
            stom = self.get_stom(time_info)
            result_dict['Stom'] = stom
        if "Stoq" in columns_list:
            stoq = self.get_stoq(time_info)
            result_dict['Stoq'] = stoq
        if "Stoa" in columns_list:
            stoa = self.get_stoa(time_info)
            result_dict['Stoa'] = stoa
        if "Dastd" in columns_list:
            dastd = self.get_dastd(time_info)
            result_dict['Dastd'] = dastd
        if "Rstr" in columns_list:
            rstr = self.get_rstr(time_info)
            result_dict['Rstr'] = rstr
        if "SW_Industry_name" in columns_list:
            result_dict['SW_Industry_name'] = self.get_sw_industry_name(time_info)
        if "Turnover" in columns_list:
            result_dict['Turnover'] = self.get_turnover(time_info)
        if "Shares_float" in columns_list:
            result_dict['Shares_float'] = self.get_shares_float(time_info)
        if "Shares_total" in columns_list:
            result_dict['Shares_total'] = self.get_shares_total(time_info)
        if "MV_float" in columns_list:
            result_dict['MV_float'] = self.get_mv_float(time_info)
        if "MV_total" in columns_list:
            result_dict['MV_total'] = self.get_mv_total(time_info)
        if "PB" in columns_list:
            result_dict['PB'] = self.get_PB(time_info)
        if "PE_ttm" in columns_list:
            result_dict['PE_ttm'] = self.get_PE_ttm(time_info)
        return result_dict


    def get_ps(self, time_info):
        # 取市值
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue')
        # 取销售收入
        OperatingIncome = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"], trady_day_stamp, "IC", "OperatingRevenue")
        market_value = BasicOperations(market_value)
        OperatingIncome = BasicOperations(OperatingIncome)
        result = market_value / OperatingIncome
        return result.val

    def get_ps_ttm(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        OperatingIncome_ttm = GetVPGlobalVariable().get_finance_data_point_ttm(self.data_source_dict["FIN"], trady_day_stamp, "IC",
                                                                               "OperatingRevenue")

        market_value = BasicOperations(market_value)
        OperatingIncome_ttm = BasicOperations(OperatingIncome_ttm)
        result = market_value / OperatingIncome_ttm
        return result.val

    def get_ps_lyr(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        OperatingIncome_lyr = GetVPGlobalVariable().get_finance_data_point_lyr(self.data_source_dict["FIN"], trady_day_stamp, "IC",
                                                                               "OperatingRevenue")
        market_value = BasicOperations(market_value)
        OperatingIncome_lyr = BasicOperations(OperatingIncome_lyr)
        result = market_value / OperatingIncome_lyr
        return result.val

    def get_pcf_nfl(self, time_info):
        # 取市值
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue')
        # 取销售收入
        NetCashFlow = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"], trady_day_stamp, "CS", "NetOperateCashFlow")
        market_value = BasicOperations(market_value)
        NetCashFlow = BasicOperations(NetCashFlow)
        result = market_value / NetCashFlow
        # print('NetCashFlow', NetCashFlow)
        return result.val

    def get_pcf_nfl_ttm(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        NetCashFlow_ttm = GetVPGlobalVariable().get_finance_data_point_ttm(self.data_source_dict["FIN"], trady_day_stamp, "CS", "NetOperateCashFlow")


        market_value = BasicOperations(market_value)
        NetCashFlow_ttm = BasicOperations(NetCashFlow_ttm)
        result = market_value / NetCashFlow_ttm

        # print('NetCashFlow_ttm', NetCashFlow_ttm)
        return result.val

    def get_pcf_nfl_lyr(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        NetCashFlow_lyr = GetVPGlobalVariable().get_finance_data_point_lyr(self.data_source_dict["FIN"], trady_day_stamp, "CS",
                                                                               "NetOperateCashFlow")

        market_value = BasicOperations(market_value)
        NetCashFlow_lyr = BasicOperations(NetCashFlow_lyr)
        result = market_value / NetCashFlow_lyr
        # print('NetCashFlow_lyr', NetCashFlow_lyr)
        return result.val

    def get_pcf_ocf(self, time_info):
        # 取市值
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue')
        # 取销售收入
        NetCashFromOperating = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"], trady_day_stamp, "CS", "NetOperateCashFlow")

        market_value = BasicOperations(market_value)
        NetCashFromOperating = BasicOperations(NetCashFromOperating)

        result = market_value / NetCashFromOperating
        return result.val

    def get_pcf_ocf_ttm(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        NetCashFromOperating_ttm = GetVPGlobalVariable().get_finance_data_point_ttm(self.data_source_dict["FIN"], trady_day_stamp, "CS",
                                                                               "NetOperateCashFlow")

        market_value = BasicOperations(market_value)
        NetCashFromOperating_ttm = BasicOperations(NetCashFromOperating_ttm)
        result = market_value / NetCashFromOperating_ttm
        return result.val

    def get_pcf_ocf_lyr(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        NetCashFromOperating_lyr = GetVPGlobalVariable().get_finance_data_point_lyr(self.data_source_dict["FIN"], trady_day_stamp, "CS",
                                                                               "NetOperateCashFlow")

        market_value = BasicOperations(market_value)
        NetCashFromOperating_lyr = BasicOperations(NetCashFromOperating_lyr)
        result = market_value / NetCashFromOperating_lyr
        return result.val

    def get_btop(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue_total')
        ShareholderEquity = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"], trady_day_stamp, "BL", "SEWithoutMI")
        if ShareholderEquity == None or str(ShareholderEquity) == 'nan':
            ShareholderEquity = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"],
                                                                             trady_day_stamp, "BL", "TotalShareholderEquity")
            if ShareholderEquity == None or str(ShareholderEquity) == 'nan':
                return None
        market_value = BasicOperations(market_value)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        btop = ShareholderEquity/market_value
        return btop.val

    def get_PB(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue_total')
        ShareholderEquity = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"], trady_day_stamp, "BL", "SEWithoutMI")
        if ShareholderEquity == None or str(ShareholderEquity) == 'nan':
            ShareholderEquity = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"],
                                                                             trady_day_stamp, "BL", "TotalShareholderEquity")
            if ShareholderEquity == None or str(ShareholderEquity) == 'nan':
                return None
        market_value = BasicOperations(market_value)
        ShareholderEquity = BasicOperations(ShareholderEquity)
        btop = market_value/ShareholderEquity
        return btop.val

    def get_lncap(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue_total')
        lncap = np.log(market_value)
        return lncap

    def get_midcap(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue_total')
        midcap = np.log(market_value)**3
        return midcap

    def get_etop(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue_total')
        NetProfit = GetVPGlobalVariable().get_finance_data_point_ttm(self.data_source_dict["FIN"], trady_day_stamp, "IC", "NPParentCompanyOwners")
        if NetProfit == None or str(NetProfit) == 'nan':
            NetProfit = GetVPGlobalVariable().get_finance_data_point_ttm(self.data_source_dict["FIN"], trady_day_stamp,
                                                                         "IC", "NetProfit")
            if NetProfit == None or str(NetProfit) == 'nan':
                return None
        NetProfit = BasicOperations(NetProfit)
        market_value = BasicOperations(market_value)
        etop = NetProfit/market_value
        return etop.val

    def get_PE_ttm(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                trady_day_stamp, 'MarketValue_total')
        NetProfit = GetVPGlobalVariable().get_finance_data_point_ttm(self.data_source_dict["FIN"], trady_day_stamp,
                                                                     "IC", "NPParentCompanyOwners")
        if NetProfit == None or str(NetProfit) == 'nan':
            NetProfit = GetVPGlobalVariable().get_finance_data_point_ttm(self.data_source_dict["FIN"], trady_day_stamp,
                                                                         "IC", "NetProfit")
            if NetProfit == None or str(NetProfit) == 'nan':
                return None
        NetProfit = BasicOperations(NetProfit)
        market_value = BasicOperations(market_value)
        etop = market_value/NetProfit
        return etop.val

    def get_cetop(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue_total')
        NetCashFromOperating = GetVPGlobalVariable().get_finance_data_point_ttm(self.data_source_dict["FIN"], trady_day_stamp, "CS", "NetOperateCashFlow")
        if NetCashFromOperating == None or str(NetCashFromOperating) == 'nan':
            return None
        NetCashFromOperating = BasicOperations(NetCashFromOperating)
        market_value = BasicOperations(market_value)
        cetop = NetCashFromOperating / market_value
        return cetop.val


    def get_mlev(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'MarketValue_total')
        TotalNCurrentLiability_num = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"], trady_day_stamp, "BL", "TotalNonCurrentLiability")
        TotalNCurrentLiability_lyp = GetVPGlobalVariable().get_finance_data_point_lyp(self.data_source_dict["FIN"], trady_day_stamp, "BL", "TotalNonCurrentLiability")
        SW_Industry = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                 trady_day_stamp, 'SWIndustry_name')
        # print('mlev', SW_Industry)
        if SW_Industry in set(["金融服务", "银行", "非银金融"]):
            return 0.0
        if TotalNCurrentLiability_num == None or str(TotalNCurrentLiability_num) == 'nan':
            return None
        elif TotalNCurrentLiability_lyp == None or str(TotalNCurrentLiability_lyp) == 'nan':
            TotalNCurrentLiability = TotalNCurrentLiability_num
        else:
            TotalNCurrentLiability = (TotalNCurrentLiability_num+TotalNCurrentLiability_lyp)/2
        mlev = TotalNCurrentLiability/(TotalNCurrentLiability + market_value)
        return mlev


    def get_blev(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        TotalNCurrentLiability_num = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"],
                                                                                  trady_day_stamp, "BL",
                                                                                  "TotalNonCurrentLiability")
        TotalNCurrentLiability_lyp = GetVPGlobalVariable().get_finance_data_point_lyp(self.data_source_dict["FIN"],
                                                                                      trady_day_stamp, "BL",
                                                                                      "TotalNonCurrentLiability")

        TotalEquity_num = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"],
                                                                                  trady_day_stamp, "BL",
                                                                                  "TotalShareholderEquity")
        TotalEquity_lyp = GetVPGlobalVariable().get_finance_data_point_lyp(self.data_source_dict["FIN"],
                                                                                      trady_day_stamp, "BL",
                                                                                      "TotalShareholderEquity")
        SW_Industry = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'],
                                                                                 trady_day_stamp, 'SWIndustry_name')
        # print('blev', SW_Industry)
        if SW_Industry in set(["金融服务", "银行", "非银金融"]):
            return 0.0
        TotalNCurrentLiability_num = BasicOperations(TotalNCurrentLiability_num)
        TotalNCurrentLiability_lyp = BasicOperations(TotalNCurrentLiability_lyp)
        TotalEquity_num = BasicOperations(TotalEquity_num)
        TotalEquity_lyp = BasicOperations(TotalEquity_lyp)

        if TotalNCurrentLiability_lyp.val == None or str(TotalNCurrentLiability_lyp.val) == 'nan':
            TotalNCurrentLiability = TotalNCurrentLiability_num
        else:
            TotalNCurrentLiability = BasicOperations((TotalNCurrentLiability_num.val + TotalNCurrentLiability_lyp.val) / 2)
        if TotalEquity_lyp.val == None or str(TotalEquity_lyp.val) == 'nan':
            TotalEquity = TotalEquity_num
        else:
            TotalEquity = BasicOperations((TotalEquity_num.val + TotalEquity_lyp.val) / 2)
        result = TotalNCurrentLiability / TotalEquity
        # print(result.val)
        return result.val


    def get_sw_industry(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        sw_industry = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'SWIndustry')
        return sw_industry

    def get_sw_industry_name(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        sw_industry = GetVPGlobalVariable().get_volume_price_market_data_point(self.data_source_dict['Market'], trady_day_stamp, 'SWIndustry_name')
        return sw_industry

    def get_hbeta(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        hbeta = GetVPGlobalVariable().get_volume_price_data_point(self.data_source_dict["WLS"], trady_day_stamp, 'Beta')
        return hbeta

    def get_alpha(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        before_halpha_df = GetVPGlobalVariable().get_stock_dataframe_data(self.data_source_dict["WLS"], trady_day_stamp,
                                                                          20)
        before_halpha_list = before_halpha_df['Alpha'].values[:11].tolist()
        if not before_halpha_list or before_halpha_list == len(before_halpha_list) * [None]:
            return None
        else:
            halpha = np.mean(before_halpha_list)
            return halpha


    def get_hsigma(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        residual = GetVPGlobalVariable().get_volume_price_data_point(self.data_source_dict["WLS"], trady_day_stamp,
                                                                     'Residual')
        if not isinstance(residual, np.ndarray):
            return None
        hsigma = np.std(residual)
        return hsigma


    def get_cmra(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        risk_free_rate = .03
        # monthly_risk_free_rate = [risk_free_rate/k for k in range(12, 0 , -1)]
        monthly_risk_free_rate = [(1 + risk_free_rate) ** (k / 12) - 1 for k in range(1, 13)]  # 对数收益率
        monthly_yield_rate_list = []  # 应该为对数收益率
        for i in range(1, 13):
            monthly_yield_rate_list.append(
                GetVPGlobalVariable().get_num_monthly_yield_rate(self.data_source_dict['WLS'], trady_day_stamp, i))
        monthly_excess_return = pd.Series(monthly_yield_rate_list) - pd.Series(monthly_risk_free_rate)
        monthly_excess_return = monthly_excess_return.dropna()
        if len(monthly_excess_return) <= 1:
            return None
        elif 1 < len(monthly_excess_return) <= 5:
            monthly_excess_return_max = monthly_excess_return[-2:].max()
            monthly_excess_return_min = monthly_excess_return[-2:].min()
        elif 5 < len(monthly_excess_return) <= 11:
            monthly_excess_return_max = monthly_excess_return[-6:].max()
            monthly_excess_return_min = monthly_excess_return[-6:].min()
        else:
            monthly_excess_return_max = monthly_excess_return.max()
            monthly_excess_return_min = monthly_excess_return.min()
        cmra = np.log(1 + monthly_excess_return_max) - np.log(1+ monthly_excess_return_min)
        return cmra

    def get_stom(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        monthly_turnover_rate, num_days = GetVPGlobalVariable().get_num_monthly_turnover_rate(
            self.data_source_dict['Market'], trady_day_stamp, 1)
        if num_days == 0:
            return None
        stom = np.log(monthly_turnover_rate / (num_days / 21))
        return stom

    def get_stoq(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        quarterly_turnover_rate, num_days = GetVPGlobalVariable().get_num_monthly_turnover_rate(
            self.data_source_dict['Market'], trady_day_stamp, 3)
        if num_days == 0:
            return None
        stoq = np.log(quarterly_turnover_rate / (num_days / 21))
        return stoq

    def get_stoa(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        yearly_turnover_rate, num_days = GetVPGlobalVariable().get_num_monthly_turnover_rate(
            self.data_source_dict['Market'], trady_day_stamp, 12)
        if num_days == 0:
            return None
        stoa = np.log(yearly_turnover_rate / (num_days / 21))
        return stoa

    def get_dastd(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        risk_free_rate = .03
        stock_df = GetVPGlobalVariable().get_stock_dataframe_data(self.data_source_dict["WLS"], trady_day_stamp, 252)
        if stock_df.empty:
            return None
        stock_yield_rate_array = stock_df['Yield_Rate'].values
        # stock_yield_rate_array_length = len(stock_yield_rate_array)
        # index_df = GetVPGlobalVariable().get_index_dataframe_data(self.data_source_dict["HS300"], trady_day_stamp,
        #                                                           stock_yield_rate_array_length)
        # index_yield_rate_array = index_df['yield_rate'].values
        # excess_return_array = stock_yield_rate_array - index_yield_rate_array
        excess_return_array = stock_yield_rate_array - risk_free_rate
        weights = GetVPGlobalVariable().half_life_weight_list(42, len(excess_return_array))
        excess_return_weightes_array = excess_return_array * np.array(weights)
        dastd = np.std(excess_return_weightes_array)
        return dastd



    def get_rstr(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        risk_free_rate = .03
        before_stock_df_21 = GetVPGlobalVariable().get_stock_dataframe_data(self.data_source_dict["WLS"],
                                                                            trady_day_stamp, 21)
        trady_day_previous11_previous21 = before_stock_df_21['Date'][-21 + 1:(-11 + 1) + 1].tolist()
        rstr_list = []
        for date in trady_day_previous11_previous21:
            before_stock_df = GetVPGlobalVariable().get_stock_dataframe_data(self.data_source_dict["WLS"], date, 504)
            if before_stock_df.empty:
                return None
            stock_yield_rate_array = before_stock_df['Ln_Yield_Rate'].values
            stock_yield_rate_array_length = len(stock_yield_rate_array)
            # index_yield_rate_df = GetVPGlobalVariable().get_index_dataframe_data(self.data_source_dict["HS300"], date,
            #                                                                      stock_yield_rate_array_length)
            # index_yield_rate_array = index_yield_rate_df['ln_yield_rate'].values
            # excess_return_array = stock_yield_rate_array - index_yield_rate_array
            excess_return_array = stock_yield_rate_array - risk_free_rate
            weights = GetVPGlobalVariable().half_life_weight_list(126, stock_yield_rate_array_length)
            excess_return = np.sum(weights * excess_return_array)
            rstr_list.append(excess_return)
        rstr = np.mean(rstr_list)
        return rstr

    def get_turnover(self, time_info):
        # 换手率
        trady_day_stamp = time_info['Trading_Day']
        turnover_rate = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['Market'], trady_day_stamp, 'TurnoverRate')
        return turnover_rate

    def get_shares_float(self, time_info):
        # 流通股数
        trady_day_stamp = time_info['Trading_Day']
        ListedShares = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['Market'], trady_day_stamp, 'ListedShares')
        return ListedShares

    def get_shares_total(self, time_info):
        # 总股数
        trady_day_stamp = time_info['Trading_Day']
        TotalShares = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['Market'], trady_day_stamp, 'TotalShares')
        return TotalShares

    def get_mv_float(self, time_info):
        # 流通市值
        trady_day_stamp = time_info['Trading_Day']
        market_value = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['Market'], trady_day_stamp, 'MarketValue')
        return market_value

    def get_mv_total(self, time_info):
        # 总市值
        trady_day_stamp = time_info['Trading_Day']
        market_value_total = GetVPGlobalVariable().get_volume_price_market_data_point(
            self.data_source_dict['Market'], trady_day_stamp, 'MarketValue_total')
        return market_value_total

