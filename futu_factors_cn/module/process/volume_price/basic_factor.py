#-*- coding: utf-8 -*-

# 计算基础因子
# from module.process.volume_price.vp_global_variable import *
from module.process.volume_price.data_function import *
from module.basic.basic_operations import *
import configparser
from module.basic.handle_db import *

class BasicFactor():
    __name__="BasicVolmePrice"
    def __init__(self, stock_info_dict, time_info_dict, data_source_dict):
        self.stock_info_dict = stock_info_dict
        self.start_time = time_info_dict['Cal_Start_Time']
        self.end_time = time_info_dict['Cal_End_Time']
        self.data_source_dict = data_source_dict

    # 取基本因子
    def get_columns_list(self):
        columns_list = ["Trading_Day", "InnerCode", "CompanyCode", "SecuCode"]
        with open("config/formula.json") as f:
            config_data = json.load(f)
        map_dict = config_data['fundamental'][self.__name__]
        # 判断siwth, 0:不需要跑；1：需要跑
        switch = map_dict['swith']
        data_points_dict = map_dict['data_points']
        switch_flag = False
        if switch == "1":
            switch_flag = True
        for key, value in data_points_dict.items():
            if value == "1":
                columns_list.append(key)
        return switch_flag, columns_list


    # 按HS300来取交易日期
    def calculate_factors_result(self):
        switch_flag, columns_list = self.get_columns_list()
        if switch_flag == False:
            return
        result_df = pd.DataFrame(columns=columns_list)
        date_format_str = "date"
        hs300_data_info_df = self.data_source_dict["HS300"]
        start_time_timestamp = pd.Timestamp(self.start_time)
        end_time_timestamp = pd.Timestamp(self.end_time)

        # temp = table_data[(table_data['EndDate'] <= trade_day_timestamp) & (table_data['EndDate'] > min_trade_day_timestamp)]
        target_hs300_date_info_df = hs300_data_info_df[
            (hs300_data_info_df[date_format_str] >= start_time_timestamp) & (hs300_data_info_df[date_format_str] <= end_time_timestamp)]
        target_hs300_date_info_df = target_hs300_date_info_df.reset_index(drop=True)


        for i in range(target_hs300_date_info_df.shape[0]):
            calculate_date = target_hs300_date_info_df['date'][i]
            print(self.__name__, calculate_date)
            time_info= dict()
            time_info["Trading_Day"] = calculate_date
            date_points_dict = self.get_need_data_points(time_info, columns_list)
            result_dict = dict()
            result_dict = dict()
            for i in range(len(columns_list)):
                key = columns_list[i]
                value = None
                if self.stock_info_dict.get(key) != None:
                    value = self.stock_info_dict.get(key)
                elif date_points_dict.get(key) != None:
                    value = date_points_dict.get(key)
                elif time_info.get(key) != None:
                    value = time_info.get(key)
                result_dict[key] = value
            result_df = result_df.append(result_dict, ignore_index=True)
        '''
        result_df.to_csv(
            "module/test_files/basic/" + self.__name__ + "." + str(self.stock_info_dict['SecuCode']) + ".csv",
            index=False)
        '''
        '''
        result_df.to_csv("test/" + self.__name__ + "." + str(self.stock_info_dict['SecuCode']) + ".csv", index=False)
            # trade_date = date.to_pydatetime()
            # print(date, trade_date, type(trade_date), type(date))
            # print(calculate_date, type(calculate_date))
            # 有日期了，开始计算。
        '''
        self.save_data(result_df)

    
    def save_data(self, data_df):
        config = configparser.ConfigParser()
        config.read("config/config.ini")
        savedb = str(config["global"]["savedb"])
        if savedb == "1":
            self.save_data_to_db(data_df)
        else:
            data_df.to_csv("test/" + self.__name__ + "." + str(self.stock_info_dict['SecuCode']) + ".csv", index=False, encoding='utf-8-sig')

    def save_data_to_db(self, data_df):
        obj_handledb = HandleDB()
        if self.__name__ == "VPCommonFactors":
            obj_handledb.save_volumn_price_common(data_df)

    '''
    def get_need_data_points(self, time_info, columns_list):
        result_dict = dict()
        if "Ps" in columns_list:
            ps = self.get_ps(time_info)
            result_dict['Ps'] = ps
        if "Ps_ttm" in columns_list:
            ps_ttm = self.get_ps_ttm(time_info)
            result_dict['Ps_ttm'] = ps_ttm
        if "Hbeta" in columns_list:
            hbeta = self.get_hbeta(time_info)
            result_dict['Hbeta'] = hbeta
        return result_dict


    def get_ps(self, time_info):
        # 取市值
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_data_point(self.data_source_dict["WLS"], trady_day_stamp, 'MarketValue')
        # 取销售收入
        OperatingIncome = GetVPGlobalVariable().get_finance_data_point(self.data_source_dict["FIN"], trady_day_stamp, "IC", "OperatingIncome")
        result = market_value / OperatingIncome
        return result

    def get_ps_ttm(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        # print(VPGlobalVariable.stock_data_info_df.dtypes)
        market_value = GetVPGlobalVariable().get_volume_price_data_point(self.data_source_dict["WLS"], trady_day_stamp, 'MarketValue')
        # 取销售收入TTM
        OperatingIncome_ttm = GetVPGlobalVariable().get_finance_data_point_ttm(self.data_source_dict["FIN"], trady_day_stamp, "IC",
                                                                               "OperatingIncome")
        result = market_value / OperatingIncome_ttm
        return result

    def get_hbeta(self, time_info):
        trady_day_stamp = time_info['Trading_Day']
        beta = GetVPGlobalVariable().get_volume_price_data_point(self.data_source_dict["WLS"], trady_day_stamp, 'Beta')
        return beta
    '''

if __name__=="__main__":
    stock_info_dict = {'InnerCode': 1000546, 'CompanyCode': 1000546, 'SecuCode': 700, 'Futu_SecuCode': 700,
                       'Start_Time': '2006-01-01', 'End_Time': '2018-01-01'}
    start_time = "2006-01-01"
    end_time = "2018-01-01"
    obj = BasicFactor(stock_info_dict, start_time, end_time)
    kk = obj.get_columns_list()
    print(kk)
