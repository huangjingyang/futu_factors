#-*- coding: utf-8 -*-


import pandas as pd
from sqlalchemy import create_engine, text
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.exc import DisconnectionError, OperationalError, SQLAlchemyError
from futuquant import *
import configparser
import datetime
import numpy as np
import statsmodels.api as sm
from dateutil.relativedelta import relativedelta
from module.basic.currency_convert import *
import math

# 获得数据的办法
class GetVPGlobalVariable():
    def __init__(self):
        pass

    # 取财务数据
    def get_stock_financial_data(self, company_code,inner_code, search_start_time, search_end_time):
        financial_data_dict = dict()
        #search_start_time = self.convert_datetime_to_string(search_start_time)
        #search_end_time = search_end_time
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")

        balance_sheet_sql_stmt = text("""select * from LC_BalanceSheetAll where CompanyCode='{company_code}' and InfoPublDate >= Date('{search_start_time}') and InfoPublDate <= Date('{search_end_time}') and IfMerged = 1 and IfAdjusted = 2 order by EndDate;""".format(company_code=company_code, search_start_time=search_start_time, search_end_time=search_end_time))

        income_statement_sql_stmt = text("""select * from LC_IncomeStatementAll where CompanyCode='{company_code}' and InfoPublDate >= Date('{search_start_time}') and InfoPublDate <= Date('{search_end_time}') and IfMerged = 1 and IfAdjusted = 2 order by EndDate;""".format(company_code=company_code, search_start_time=search_start_time, search_end_time=search_end_time))

        cashflow_statement_sql_stmt = text("""select * from LC_CashFlowStatementAll where CompanyCode='{company_code}' and InfoPublDate >= Date('{search_start_time}') and InfoPublDate <= Date('{search_end_time}') and IfMerged = 1 and IfAdjusted = 2 order by EndDate;""".format(company_code=company_code, search_start_time=search_start_time, search_end_time=search_end_time))

        cn_main_index_stmt = text("""select * from LC_MainIndexNew where CompanyCode='{company_code}' and EndDate >= Date('{search_start_time}') and EndDate <= Date('{search_end_time}') order by EndDate;""".format(company_code=company_code, search_start_time=search_start_time, search_end_time=search_end_time))

        cn_sharestru_stmt = text("""select * from LC_ShareStru where CompanyCode='{company_code}' and EndDate >= Date('{search_start_time}') and EndDate <= Date('{search_end_time}') order by EndDate;""".format(company_code=company_code, search_start_time=search_start_time, search_end_time=search_end_time))

        engine = create_engine(engine_str)
        balance_sheet_result = pd.read_sql(balance_sheet_sql_stmt, engine)
        income_statement_result = pd.read_sql(income_statement_sql_stmt, engine)
        cashflow_statement_result = pd.read_sql(cashflow_statement_sql_stmt, engine)
        hk_main_index_result = pd.read_sql(cn_main_index_stmt, engine)
        cn_sharestru_result = pd.read_sql(cn_sharestru_stmt, engine)
        financial_data_dict['BalanceSheet'] = balance_sheet_result
        financial_data_dict['IncomeStatement'] = income_statement_result
        financial_data_dict['CashFlowStatement'] = cashflow_statement_result
        financial_data_dict['MainIndex'] = hk_main_index_result
        financial_data_dict['ShareStru'] = cn_sharestru_result
        return financial_data_dict

    # 取石油数据
    def load_all_oil_market_data(self):
        path = r"config/OilData.xlsx"
        source_df = pd.read_excel(path, sheet_name="CL1", names=['Date', "Price"])
        return source_df

    # 取HS300数据
    def get_hs300_index_data(self, search_start_time, search_end_time):
        hs300_data_info_df = pd.DataFrame()
        #search_start_ptime = self.convert_string_to_datetime(search_start_time)
        #search_start_ptime = search_start_ptime - relativedelta(years=2)
        #search_start_time = self.convert_datetime_to_string(search_start_ptime)
        #search_end_time = search_end_time

        # 获取HS300数据
        #hs300_security = "SH.000300"
        hs300_secucode = "000300"
        #hs300_dataframe = self.get_futuquant_data(hs300_security, search_start_time, search_end_time)
        # 切换数据源
        hs300_dataframe = self.get_index_data_from_neo_factors(hs300_secucode, search_start_time, search_end_time)
        hs300_date_array = hs300_dataframe['time_key'][1:].values
        hs300_open_array = hs300_dataframe['open'][1:].values
        hs300_close_array = hs300_dataframe['close'][1:].values
        hs300_high_array = hs300_dataframe['high'][1:].values
        hs300_yield_rate_array = (hs300_dataframe['close'][1:].values - hs300_dataframe['close'][:-1].values)/hs300_dataframe['close'][:-1].values
        hs300_ln_yield_rate_array = np.log(hs300_yield_rate_array + 1)

        hs300_data_info_df["date"]=hs300_date_array
        hs300_data_info_df["open"] = hs300_open_array
        hs300_data_info_df["close"] = hs300_close_array
        hs300_data_info_df["high"] = hs300_high_array
        hs300_data_info_df["yield_rate"] = hs300_yield_rate_array
        hs300_data_info_df["ln_yield_rate"] = hs300_ln_yield_rate_array
        hs300_data_info_df = hs300_data_info_df.reset_index(drop=True)
        return hs300_data_info_df

    '''
    # 取标普数据
    def get_sp_index_data(self, search_start_time, search_end_time):
        sp_date_info_df = pd.DataFrame()
        #search_start_ptime = self.convert_string_to_datetime(search_start_time)
        #search_start_ptime = search_start_ptime - relativedelta(years=2)
        #search_start_time = self.convert_datetime_to_string(search_start_ptime)
        #search_end_time = search_end_time
        # 获取标普500
        sp_security = "US..INX"
        sp_index_code = '3210'
        #sp_dataframe = self.get_futuquant_data(sp_security, search_start_time, search_end_time)
        # 切换聚源数据
        sp_dataframe = self.get_index_data_from_gildata(sp_index_code, search_start_time, search_end_time)
        sp_date_array = sp_dataframe['time_key'][1:].values
        sp_open_array = sp_dataframe['open'][1:].values
        sp_close_array = sp_dataframe['close'][1:].values
        sp_high_array = sp_dataframe['high'][1:].values
        sp_yield_rate_array = (sp_dataframe['close'][1:].values - sp_dataframe['close'][:-1].values) / sp_dataframe['close'][:-1].values
        sp_ln_yield_rate_array = np.log(sp_yield_rate_array + 1)

        sp_date_info_df["date"] = sp_date_array
        sp_date_info_df["open"] = sp_open_array
        sp_date_info_df["close"] = sp_close_array
        sp_date_info_df["high"] = sp_high_array
        sp_date_info_df["yield_rate"] = sp_yield_rate_array
        sp_date_info_df["ln_yield_rate"] = sp_ln_yield_rate_array
        sp_date_info_df = sp_date_info_df.reset_index(drop=True)
        return sp_date_info_df

    '''
    '''
    # 从聚源取指数数据
    def get_index_data_from_gildata(self, indexcode, start, end):
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        index_sql_stmt = text(
            """select TradingDay as time_key, OpenPrice as open, ClosePrice as close,HighPrice as high from QT_OSIndexQuote where IndexCode='{indexcode}' and TradingDay >= DATE('{start}') and TradingDay <= DATE('{end}') order by TradingDay""".format(indexcode=indexcode, start=start, end=end))
        engine = create_engine(engine_str)
        result_df = pd.read_sql(index_sql_stmt, engine)
        return result_df
    '''

    # 从本地数据库取指数数据
    def get_index_data_from_neo_factors(self, secucode, start, end):
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357",
                                            db="neo_factors", charset="utf8mb4")
        index_sql_stmt = text(
            """select TradingDay as `time_key`, OpenPrice as `open`, ClosePrice as `close`, HighPrice as `high`, LowPrice as `low` from index_daily where SecuCode = '{secucode}' and TradingDay >= Date('{start}') and TradingDay <= Date('{end}') order by TradingDay;""".format(secucode=secucode, start=start, end=end))
        engine = create_engine(engine_str)
        result_df = pd.read_sql(index_sql_stmt, engine)
        return result_df



    # 获取futuquant api的数据,主要用于恒生，标普。为Barra模型服务的
    def get_futuquant_data(self, security, start, end, ktype = KLType.K_DAY, autype = AuType.NONE):
        result = 1
        config = configparser.ConfigParser()
        # TBD: 在main执行的时候，需要修改
        #config.read("../../../config/config.ini")
        config.read("config/config.ini")
        host = config["futuquant"]["host"]
        port = int(config["futuquant"]["port"])
        quote_ctx = OpenQuoteContext(host=host, port=port)
        while result != 0:
            result, result_df = quote_ctx.get_history_kline(security, start=start, end=end, ktype = ktype, autype = autype)
        quote_ctx.close()
        result_df['time_key'] = pd.to_datetime(result_df['time_key'])
        return result_df

    # 获取汇率值
    def get_currency_data(self, time_info):
        currency_unit_code = time_info['CurrencyUnitCode']
        convert_time = time_info['EndDate']
        currency_hkd_obj = CurrencyConvertHKD()
        currency_value = currency_hkd_obj.get_currency_info(currency_unit_code, convert_time)
        return currency_value

    # 财务数据取出前置操作
    # 取出数据值, DataTypeCode
    def get_finance_data_point_end_date(self, stock_financial_data_dict, trade_day_timestamp, table_name, data_name, date_type_code=0):
        data_point_value = None
        result = None
        with open("config/formula.json") as f:
            config_data = json.load(f)

        # print(config_data)
        data_dict_key = config_data['fundamental']['table_names'][table_name]
        # print(data_dict_key)
        table_data = stock_financial_data_dict[data_dict_key]
        table_data['DateTypeCode'] = table_data['EndDate'].dt.month
        # trade_day = self.convert_string_to_datetime(trade_day)
        # trade_day_timestamp = pd.Timestamp(trade_day.year, trade_day.month, trade_day.day)
        min_trade_day_timestamp = trade_day_timestamp - pd.DateOffset(years=2)
        # min_trade_day_timestamp = pd.Timestamp(min_trade_day.year, min_trade_day.month, min_trade_day.day)
        temp = pd.DataFrame()
        temp = table_data[(table_data['EndDate'] <= trade_day_timestamp) & (table_data['EndDate'] > min_trade_day_timestamp)].tail(1)
        if temp.empty or temp.shape[0] == 0:
            return None, 0, result
        temp = temp.reset_index(drop=True)
        end_data = temp['EndDate'][0]
        data_point_value = temp[data_name][0]
        data_point_type = temp['DateTypeCode'][0]
        result = data_point_value
        return end_data, data_point_type, result

    # 财务数据取出前置操作
    # 取出数据值, DataTypeCode
    def get_finance_data_point_before(self, stock_financial_data_dict, trade_day_timestamp, table_name, data_name, date_type_code=0):
        data_point_value = None
        result = None
        with open("config/formula.json") as f:
            config_data = json.load(f)

        # print(config_data)
        data_dict_key = config_data['fundamental']['table_names'][table_name]
        # print(data_dict_key)
        table_data = stock_financial_data_dict[data_dict_key]
        if table_data.empty:
            table_data['DateTypeCode'] = np.nan
        else:
            table_data['DateTypeCode'] = table_data['EndDate'].dt.month

        # trade_day = self.convert_string_to_datetime(trade_day)
        # trade_day_timestamp = pd.Timestamp(trade_day.year, trade_day.month, trade_day.day)
        min_trade_day_timestamp = trade_day_timestamp - pd.DateOffset(years=2)
        # min_trade_day_timestamp = pd.Timestamp(min_trade_day.year, min_trade_day.month, min_trade_day.day)
        temp = pd.DataFrame()
        if table_name in ["BL","IC","CS"]:
            if date_type_code == 0:
                temp = table_data[(table_data['InfoPublDate'] <= trade_day_timestamp) & (table_data['InfoPublDate'] > min_trade_day_timestamp)].tail(1)
            else:
                temp = table_data[(table_data['InfoPublDate'] <= trade_day_timestamp) & (table_data['InfoPublDate'] > min_trade_day_timestamp) & (table_data['DateTypeCode'] == date_type_code)].tail(1)
        elif table_name in ["MI"]:
            if date_type_code == 0:
                temp = table_data[(table_data['EndDate'] <= trade_day_timestamp) & (table_data['EndDate'] > min_trade_day_timestamp)].tail(1)
            else:
                temp = table_data[(table_data['EndDate'] <= trade_day_timestamp) & (table_data['EndDate'] > min_trade_day_timestamp) & (table_data['PeriodMark'] == date_type_code)].tail(1)
        else:
            return None, 0, result
        if temp.empty or temp.shape[0] == 0:
            return None, 0, result
        temp = temp.reset_index(drop=True)
        end_data = temp['EndDate'][0]
        data_point_value = temp[data_name][0]
        data_point_type = temp['DateTypeCode'][0]
        result = data_point_value
        return end_data, data_point_type, result

    # 根据日期，取得股票的财务数据, 最近数据点
    def get_finance_data_point(self, stock_financial_data_dict, trade_day_timestamp, table_name, data_name):
        _, _, result = self.get_finance_data_point_before(stock_financial_data_dict, trade_day_timestamp, table_name, data_name)
        return result

    # 根据日期，取得股票的财务数据, 最近数据点, 上一年年末数据
    def get_finance_data_point_lyr(self, stock_financial_data_dict, trade_day, table_name, data_name, num=0):
        #trade_day = self.convert_string_to_datetime(trade_day)
        trade_day_year_ago = trade_day - pd.DateOffset(years=num)
        #trade_day_year_ago = trade_day.replace(year=trade_day.year - num)
        _, _, data_point_value = self.get_finance_data_point_before(stock_financial_data_dict, trade_day_year_ago, table_name, data_name, 12)
        return data_point_value

    # 取上一年同期
    def get_finance_data_point_lyp(self, stock_financial_data_dict, trade_day, table_name, data_name, num=1):
        #trade_day = self.convert_string_to_datetime(trade_day)
        trade_day_datetime = trade_day.to_pydatetime()
        trade_end_date, data_point_type, trade_day_result = self.get_finance_data_point_before(stock_financial_data_dict, trade_day_datetime, table_name, data_name)
        if data_point_type == 0:
            return None
        trade_end_date_period = trade_end_date - pd.DateOffset(years=num)
        trade_day_period_timestamp = pd.Timestamp(trade_end_date_period)
        #trade_day_last_year_period = trade_day.replace(year=trade_day.year - num)
        _, _, data_point_value = self.get_finance_data_point_end_date(stock_financial_data_dict, trade_day_period_timestamp, table_name, data_name, data_point_type)
        return data_point_value

    # 取TTM
    def get_finance_data_point_ttm(self, stock_financial_data_dict, trade_day, table_name, data_name, num=1):
        result_value = None
        _, current_data_point_type, current_value = self.get_finance_data_point_before(stock_financial_data_dict, trade_day, table_name, data_name)
        if current_data_point_type == 12:
            return current_value
        year_ago_value = self.get_finance_data_point_lyr(stock_financial_data_dict, trade_day, table_name, data_name, num-1)
        year_ago_period_value = self.get_finance_data_point_lyp(stock_financial_data_dict, trade_day, table_name, data_name, num)
        try:
            result_value = current_value + year_ago_value - year_ago_period_value
        except:
            result_value = None
        return result_value



    # 取申万宏源一级行业数据
    def get_sw_industry_data(self, company_code, search_start_time, search_end_time):
        search_start_ptime = self.convert_string_to_datetime(search_start_time)
        search_start_ptime = search_start_ptime - relativedelta(years=2)
        search_start_time = self.convert_datetime_to_string(search_start_ptime)
        search_end_time = search_end_time

        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357", db="neo_factors", charset="utf8mb4")
        sw_industry_sql_stmt = text(
            """select * from cn_sw_industry where CompanyCode = '{company_code}' and TradingDay >= Date('{search_start_time}') and TradingDay <= Date('{search_end_time}') order by TradingDay""".format(company_code=company_code, search_start_time=search_start_time, search_end_time=search_end_time))
        engine = create_engine(engine_str)
        sw_industry_result = pd.read_sql(sw_industry_sql_stmt, engine)
        return sw_industry_result



    # 取股份数
    def get_stock_sharestru_data(self, company_code, search_start_time, search_end_time):
        search_start_ptime = self.convert_string_to_datetime(search_start_time)
        search_start_ptime = search_start_ptime - relativedelta(years=2)
        search_start_time = self.convert_datetime_to_string(search_start_ptime)
        search_end_time = search_end_time

        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sharestru_sql_stmt = text("""select * from LC_ShareStru where CompanyCode='{company_code}' and  EndDate >= Date("{search_start_time}") and EndDate <= Date('{search_end_time}') order by EndDate;""".format(company_code=company_code, search_start_time=search_start_time, search_end_time=search_end_time))
        engine = create_engine(engine_str)
        sharestru_result = pd.read_sql(sharestru_sql_stmt, engine)
        return sharestru_result


    # 获取A股交易数据（不复权）
    # 取市场股票信息:收盘价，总股份数，市值，成交量， 换手率
    def get_stock_market_data_info(self, inner_code, company_code, search_start_time, search_end_time):
        #search_start_ptime = self.convert_string_to_datetime(search_start_time)
        #search_start_ptime = search_start_ptime - relativedelta(years=2)
        #search_start_time = self.convert_datetime_to_string(search_start_ptime)
        #search_end_time = search_end_time

        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357", db="neo_factors", charset="utf8mb4")

        market_stock_daily_sql_stmt = text(
            """select TradingDay as `time_key`, CompanyCode as `code`, ClosePrice as `close`, HighPrice as `high`, LowPrice as `low`, OpenPrice as  `open`, TurnoverVolume as volume, TurnoverValue from cn_stock_daily where CompanyCode = {company_code} and TradingDay >= Date('{search_start_time}') and TradingDay <= Date('{search_end_time}') order by TradingDay;""".format(
                company_code=company_code, \
                search_start_time=search_start_time, search_end_time=search_end_time))

        engine = create_engine(engine_str)
        market_stock_daily_result_temp = pd.read_sql(market_stock_daily_sql_stmt, engine)
        '''
        inner_code_int = int(inner_code)#secucode  --  volume_price_data
        if inner_code_int < 400000:
            inner_code_string = "SZ."
        else:
            inner_code_string = "SH."
        stock_security = inner_code_string + inner_code
        market_stock_daily_result_temp = self.get_futuquant_data(stock_security, search_start_time, search_end_time)
        '''

        market_stock_daily_result = pd.DataFrame()
        market_stock_daily_result['TradingDay'] = market_stock_daily_result_temp['time_key'].values
        market_stock_daily_result['OpenPrice'] = market_stock_daily_result_temp['open'].values
        market_stock_daily_result['ClosePrice'] = market_stock_daily_result_temp['close'].values
        market_stock_daily_result['HighPrice'] = market_stock_daily_result_temp['high'].values
        market_stock_daily_result['LowPrice'] = market_stock_daily_result_temp['low'].values
        market_stock_daily_result['CompanyCode'] = market_stock_daily_result_temp['code'].values
        market_stock_daily_result['TurnoverVolume'] = market_stock_daily_result_temp['volume'].values
        market_stock_daily_result['TurnoverValue'] = market_stock_daily_result_temp['TurnoverValue'].values
        market_stock_daily_result = market_stock_daily_result.reset_index(drop=True)

        index_kline_data_df = self.get_hs300_index_data(search_start_time, search_end_time)
        market_stock_daily_result = self.adjust_stock_market_kline_data(market_stock_daily_result, index_kline_data_df)
        #market_stock_daily_result.to_csv(r'D:\workspace\futu_factors\test\market1.csv')
        #market_stock_daily_result = market_stock_daily_result.fillna(method='ffill')
        #market_stock_daily_result.to_csv(r'D:\workspace\futu_factors\test\market2.csv')
        sharestru_result = self.get_stock_sharestru_data(company_code, search_start_time, search_end_time)

        # SW 行业
        sw_industry_result = self.get_sw_industry_data(company_code, search_start_time, search_end_time)

        # 计算-》 总股份数, 市值, 换手率
        #date_list = []
        open_price_list = []
        close_price_list = []
        high_price_list = []
        low_price_list = []
        listed_shares_list = []
        total_shares_list = []
        market_value_list = []
        market_value_total_list = []
        turnover_rate_list = []
        turnover_value_list = []
        volume_list = []
        sw_industry_list = []
        sw_industry_name_list = []

        for i in range(market_stock_daily_result.shape[0]):
            daily_date = market_stock_daily_result['TradingDay'][i]
            #daily_date_timestamp = pd.Timestamp(daily_date.year, daily_date.month, daily_date.day)
            daily_open = market_stock_daily_result['OpenPrice'][i]
            daily_close = market_stock_daily_result['ClosePrice'][i]
            daily_high = market_stock_daily_result['HighPrice'][i]
            daily_low = market_stock_daily_result['LowPrice'][i]
            daily_volume = market_stock_daily_result['TurnoverVolume'][i]
            daily_turnover_value = market_stock_daily_result['TurnoverValue'][i]
            '''
            sharestru_result_temp = sharestru_result[sharestru_result['EndDate'] <= daily_date]
            if sharestru_result_temp.empty:
                sharestru_result_temp = sharestru_result.head(1)
            temp_series = sharestru_result_temp['ListedShares'].copy()
            temp_series = temp_series.reset_index(drop=True)
            # temp_value = temp_series[data_name].get(0)
            listed_shares = temp_series.values[-1]
            '''
            listed_shares = self.get_datapoint_value_from_df(sharestru_result, "EndDate", daily_date, "AFloats")
            total_shares = self.get_datapoint_value_from_df(sharestru_result, "EndDate", daily_date, "Ashares")
            market_value = None
            turnover_rate = None
            market_value_total = None
            if listed_shares != None:
                market_value = listed_shares * daily_close
                try:
                    turnover_rate = daily_volume / listed_shares
                except:
                    turnover_rate = None
            if total_shares != None:
                market_value_total = total_shares * daily_close

            # 行业
            sw_industry = self.get_datapoint_value_from_df(sw_industry_result, "TradingDay", daily_date, "IndustryNum")
            sw_industry_name = self.get_datapoint_value_from_df(sw_industry_result, "TradingDay", daily_date,"IndustryName")

            #date_list.append(daily_date_timestamp)
            listed_shares_list.append(listed_shares)
            total_shares_list.append(total_shares)
            market_value_list.append(market_value)
            market_value_total_list.append(market_value_total)
            turnover_rate_list.append(turnover_rate)
            close_price_list.append(daily_close)
            high_price_list.append(daily_high)
            low_price_list.append(daily_low)
            open_price_list.append(daily_open)
            volume_list.append(daily_volume)
            turnover_value_list.append(daily_turnover_value)
            sw_industry_list.append(sw_industry)
            sw_industry_name_list.append(sw_industry_name)

        #market_stock_daily_result['TradingDay'] = date_list
        market_stock_daily_result['ClosePrice'] = close_price_list
        market_stock_daily_result['HighPrice'] = high_price_list
        market_stock_daily_result['LowPrice'] = low_price_list
        market_stock_daily_result['OpenPrice'] = open_price_list
        market_stock_daily_result['TurnoverValue'] = turnover_value_list
        market_stock_daily_result['TurnoverVolume'] = volume_list
        market_stock_daily_result['ListedShares'] = listed_shares_list
        market_stock_daily_result['TotalShares'] = total_shares_list
        market_stock_daily_result['MarketValue'] = market_value_list
        market_stock_daily_result['MarketValue_total'] = market_value_total_list
        market_stock_daily_result['TurnoverRate'] = turnover_rate_list
        market_stock_daily_result['SWIndustry'] = sw_industry_list
        market_stock_daily_result['SWIndustry_name'] = sw_industry_name_list
        return market_stock_daily_result

    def get_datapoint_value_from_df(self, source_df, date_field, date_value, data_point_filed):
        source_df_temp = source_df[source_df[date_field] <= date_value]
        if source_df_temp.empty and source_df.shape[0] >0:
            source_df_temp = source_df.head(1)
        elif source_df_temp.empty and (source_df.empty or source_df.shape[0] ==0):
            return None
        temp_series = source_df_temp[data_point_filed].copy()
        temp_series = temp_series.reset_index(drop=True)
        # temp_value = temp_series[data_name].get(0)
        result_value = temp_series.values[-1]
        return result_value


    # 获取A股交易数据（复权）
    # 取前复权股票信息:收盘价, 收益率
    def get_stock_wq_data_info(self, company_code, search_start_time, search_end_time):
        #search_start_ptime = self.convert_string_to_datetime(search_start_time)
        #search_start_ptime = search_start_ptime - relativedelta(years=2)
        #search_start_time = self.convert_datetime_to_string(search_start_ptime)
        #search_end_time = search_end_time
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="stock_writer", pwd="stock_writer@", host="172.28.249.6", port="13357", db="neo_factors", charset="utf8mb4")
        wq_stock_daily_sql_stmt = text(
            """select TradingDay as `Date`, CompanyCode as `Code`, SecuCode, HighPrice as `High`, LowPrice as `Low`, ClosePrice as `Close`, OpenPrice as `Open`, TurnoverValue, TurnoverVolume from qcn_stock_daily where CompanyCode = {company_code} and TradingDay >= Date('{search_start_time}') and TradingDay <= Date('{search_end_time}') order by TradingDay;""".format(
                company_code=company_code, \
                search_start_time=search_start_time, search_end_time=search_end_time))

        engine = create_engine(engine_str)
        wq_stock_daily_result = pd.read_sql(wq_stock_daily_sql_stmt, engine)

        # 计算收益率
        yield_rate_array = (wq_stock_daily_result['Close'][1:].values - wq_stock_daily_result['Close'][:-1].values) / wq_stock_daily_result['Close'][:-1].values
        wq_stock_daily_result = wq_stock_daily_result[1:]
        ln_yield_rate_array = np.log(yield_rate_array + 1)
        wq_stock_daily_result['Yield_Rate'] =yield_rate_array
        wq_stock_daily_result["Ln_Yield_Rate"] = ln_yield_rate_array
        wq_stock_daily_result = wq_stock_daily_result.reset_index(drop=True)

        wq_stock_daily_result['Date'] = pd.to_datetime(wq_stock_daily_result['Date'])
        return wq_stock_daily_result

    # 根据股票的量价数据，每天计算它的WLS中的alpha, beta, residual
    def get_stock_data_info_add_wls(self, stock_data_info_df, index_info_df, start_date):
        stock_data_info_df = self.adjust_stock_wq_kline_data(stock_data_info_df, index_info_df)
        # 为了算halpha 方便
        #start_date_stamp = pd.Timestamp(start_date) - pd.Timedelta(days=30)
        start_date_stamp = pd.Timestamp(start_date)
        rows_num = stock_data_info_df.shape[0]
        alpha_list = []
        beta_list = []
        residual_list = []
        for i in range(rows_num):
            daily_date = stock_data_info_df['Date'][i]
            if daily_date < start_date_stamp - pd.DateOffset(days = 100):
                alpha_list.append(None)
                beta_list.append(None)
                residual_list.append(None)
            else:
                # 目标个股过去252个交易日的收益率时间序列
                stock_date_temp_df = self.get_stock_dataframe_data(stock_data_info_df, daily_date, 252)
                if stock_date_temp_df.empty:
                    alpha = None
                    beta = None
                    residual = None
                else:
                    stock_yield_rate_array = stock_date_temp_df['Ln_Yield_Rate'].values
                    rows_T = stock_date_temp_df.shape[0]
                    # 恒指过去252个交易日的收益率时间序列
                    index_temp_df = self.get_index_dataframe_data(index_info_df, daily_date, rows_T)
                    index_yield_rate_array = index_temp_df['ln_yield_rate'].values
                    # 63天半衰期
                    weights = self.half_life_weight_list(63, rows_T)
                    endog = stock_yield_rate_array - 0.03/252
                    #print(daily_date, index_yield_rate_array)
                    exog = sm.add_constant(index_yield_rate_array - 0.03/252)
                    wls_model = sm.WLS(endog, exog, weights=weights)
                    wls_results = wls_model.fit()
                    alpha = wls_results.params[0]
                    beta = wls_results.params[1]
                    residual = wls_results.resid
                alpha_list.append(alpha)
                beta_list.append(beta)
                residual_list.append(residual)

        stock_data_info_df['Alpha']= alpha_list
        stock_data_info_df['Beta'] = beta_list
        stock_data_info_df['Residual'] = residual_list
        return stock_data_info_df


    # 日期转化为字符
    def convert_datetime_to_string(self, datetime_foramt):
        datetime_str = ""
        datetime_str = datetime_foramt.strftime('%Y-%m-%d')
        return datetime_str

    # 字符转化为日期
    def convert_string_to_datetime(self, datetime_str):
        datetime_foramt = datetime.datetime.min
        datetime_foramt = datetime.datetime.strptime(datetime_str, '%Y-%m-%d')
        return datetime_foramt

    # 分割Dataframe按时间
    # 过去少于60个数据：return nan
    # 过去有60~120个数据：用过去60个数据计算
    # 过去有120~252个数据：用过去120个数据计算
    def get_stock_dataframe_data(self, target_df, trade_date_timestamp, days_num):
        #hs_data_info_df = VPGlobalVariable.hs_data_info_df
        # temp = table_data[(table_data['EndDate'] <= trade_day_timestamp) & (table_data['EndDate'] > min_trade_day_timestamp)]
        result__df = target_df[target_df['Date'] < trade_date_timestamp][-days_num:]
        result__df = result__df[~result__df[["Close", "High", "Low"]].isna().all(axis=1)]
        rows_df = result__df.shape[0]
        if days_num == 504:
            if rows_df >= 504:
                pass
            elif rows_df < 504 and rows_df >= 380:
                result__df = result__df[-380:]
            elif rows_df < 380 and rows_df >= 252:
                result__df = result__df[-252:]
            elif rows_df < 252 and rows_df >= 120:
                result__df = result__df[-120:]
            elif rows_df < 120 and rows_df >= 60:
                result__df = result__df[-60:]
            elif rows_df < 60:
                result__df = pd.DataFrame(columns=target_df.columns)
        elif days_num == 252:
            if rows_df >= 252:
                pass
            elif rows_df < 252 and rows_df >= 120:
                result__df = result__df[-120:]
            elif rows_df < 120 and rows_df >= 60:
                result__df = result__df[-60:]
            elif rows_df < 60:
                result__df = pd.DataFrame(columns=target_df.columns)
        elif days_num == 63:
            if rows_df >= 63:
                pass
            elif rows_df < 63 and rows_df >= 20:
                result__df = result__df[-20:]
            elif rows_df < 20:
                result__df = pd.DataFrame(columns=target_df.columns)
        result__df = result__df.reset_index(drop=True)
        return result__df

    def get_index_dataframe_data(self, target_df, trade_date_timestamp, days_num):
        # temp = table_data[(table_data['EndDate'] <= trade_day_timestamp) & (table_data['EndDate'] > min_trade_day_timestamp)]
        result__df = target_df[target_df['date'] < trade_date_timestamp][-days_num:]
        result__df = result__df.reset_index(drop=True)
        return result__df

    # 取半衰期指数权重序列
    def half_life_weight_list(self, half_life, T):
        delta = 0.5 ** (1 / half_life)
        weight_list = [delta ** (T - k) for k in range(1, T + 1)]
        weight_list2 = list(weight_list/np.sum(weight_list))
        return weight_list2


    # 恒生指数累加收益率为负的收益率序列以及对应个股收益率序列
    # 要用的时候，计算之后一直保存
    # 输入股票相应收益序列，恒生指数的收益序列
    def stock_yield_rate_compare_index_tow_day_negative(self, stock_df, index_df):
        # 恒生两天收益率为负数Dataframe
        index_df_copy = index_df.copy()
        stock_df_copy = stock_df.copy()
        temp_index_df_copy_shit = index_df_copy.shift(1)
        index_df_copy['yield_rate'] = index_df_copy['yield_rate'] + temp_index_df_copy_shit['yield_rate']
        index_df_copy = index_df_copy.loc[index_df_copy['yield_rate'] < 0]
        index_df_copy = index_df_copy.reset_index(drop=True)
        #temp_list = index_df_copy['date'].tolist()
        #print(stock_df_copy['Date'].tolist())
        stock_df_copy = stock_df_copy.loc[stock_df_copy['Date'].isin(index_df_copy['date'].tolist())]
        stock_df_copy = stock_df_copy.reset_index(drop=True)
        return stock_df_copy, index_df_copy

    # 输入收益率
    def get_num_monthly_yield_rate(self, stock_df, trade_day, num):
        start_time = trade_day
        result__df = stock_df.loc[stock_df['Date'] < start_time][-21*num:]
        result__df = result__df.reset_index(drop=True)
        rows_df = result__df.shape[0]
        # 计算 收益率
        if rows_df < 21*num:
            return np.nan
        else:
            start_close = result__df['Close'].iloc[0]
            end_close = result__df['Close'].iloc[-1]
            monthly_yield_rate = np.log(end_close/start_close)
            return monthly_yield_rate

    # 计算过去n个月的换手率
    # 输入换手率
    # 月度有算21
    def get_num_monthly_turnover_rate(self, stock_df, trade_day, num):
        if str(stock_df.loc[stock_df['TradingDay'] == trade_day]['TurnoverRate'].iloc[0]) == 'nan':
            return np.nan, 0
        else:
            temp_stock_df = stock_df.loc[stock_df['TradingDay'] < trade_day]['TurnoverRate']
            temp_stock_df = temp_stock_df.dropna()
            temp_stock_df = temp_stock_df.reset_index(drop=True)
            temp_stock_df = temp_stock_df.tail(21 * num)
            # 计算换手率
            monthly_turnover_rate_array = temp_stock_df.values
            monthly_turnover_rate = monthly_turnover_rate_array.sum()
            monthly_turnover_rate_array_length = len(monthly_turnover_rate_array)
            return monthly_turnover_rate, monthly_turnover_rate_array_length


    # 石油价格交易日收益序列
    def oil_market_data(self, oil_market_data_df, start_date_timestamp, days_num):
        # 读取石油交易数据
        #path = r"config/OilData.xlsx"
        #source_df = pd.read_excel(path, sheet_name="CL1", names=['Date', "Price"])
        source_df = oil_market_data_df.copy()
        temp_source_df = source_df.shift(1)
        yield_rate_array = (temp_source_df['Price'] - source_df['Price'])/source_df['Price']
        source_df['yield_rate'] = yield_rate_array
        source_df = source_df.loc[source_df["Date"] < start_date_timestamp][-days_num:]
        source_df = source_df.reset_index(drop=True)
        return source_df




    def get_volume_price_market_data_point(self, stock_data_info, trady_day_stamp, data_point):
        temp_df = stock_data_info.loc[stock_data_info['TradingDay'] == trady_day_stamp]
        data_point_value = temp_df[data_point].values[0]
        return data_point_value

    def get_volume_price_data_point(self, stock_data_info, trady_day_stamp, data_point):
        temp_df = stock_data_info.loc[stock_data_info['Date'] == trady_day_stamp]
        data_point_value = temp_df[data_point].values[0]
        return data_point_value

    # 调整复权股票数据，对于缺失的数据统一采用nan来处理
    def adjust_stock_wq_kline_data(self, stock_kline_data_df, index_kline_data_df):
        #stock_kline_data_df = pd.read_csv(r'D:\workspace\futu_factors\test\stock.csv')
        #index_kline_data_df = pd.read_csv(r'D:\workspace\futu_factors\test\index.csv')
        #print(stock_kline_data_df, index_kline_data_df)
        code = stock_kline_data_df['Code'][0]
        stock_kline_data_columns = stock_kline_data_df.columns
        stock_date = stock_kline_data_df['Date'].values
        index_date = index_kline_data_df['date'].values
        lack_date_list = list(set(index_date) - set(stock_date))
        lack_date_list.sort()
        for i in range(len(lack_date_list)):
            lack_date = lack_date_list[i]
            temp_data_dict = dict()
            for i in range(len(stock_kline_data_columns)):
                temp_column = stock_kline_data_columns[i]
                if temp_column == "Code":
                    temp_data_dict[temp_column] = code
                elif temp_column == "Date":
                    temp_data_dict[temp_column] = lack_date
                elif temp_column =="Yield_Rate":
                    temp_data_dict[temp_column] = 0
                elif temp_column =="Ln_Yield_Rate":
                    temp_data_dict[temp_column] = 0
                else:
                    temp_data_dict[temp_column] = None
            #temp_df = pd.DataFrame([temp_data_dict], columns=stock_kline_data_columns)
            #stock_kline_data_df.append(temp_df, ignore_index=True)
            stock_kline_data_df = stock_kline_data_df.append(temp_data_dict, ignore_index=True)
        stock_kline_data_df = stock_kline_data_df.sort_values(by=["Date"])
        stock_kline_data_df = stock_kline_data_df.reset_index(drop=True)
        return stock_kline_data_df

    # 调整市场股票数据，对于缺失的数据统一采用nan来处理
    def adjust_stock_market_kline_data(self, stock_kline_data_df, index_kline_data_df):
        # stock_kline_data_df = pd.read_csv(r'D:\workspace\futu_factors\test\stock.csv')
        # index_kline_data_df = pd.read_csv(r'D:\workspace\futu_factors\test\index.csv')
        # print(stock_kline_data_df, index_kline_data_df)
        company_code = stock_kline_data_df['CompanyCode'][0]
        stock_kline_data_columns = stock_kline_data_df.columns
        stock_date = stock_kline_data_df['TradingDay'].values
        index_date = index_kline_data_df['date'].values
        lack_date_list = list(set(index_date) - set(stock_date))
        lack_date_list.sort()
        for i in range(len(lack_date_list)):
            lack_date = lack_date_list[i]
            temp_data_dict = dict()
            for i in range(len(stock_kline_data_columns)):
                temp_column = stock_kline_data_columns[i]
                if temp_column == "CompanyCode":
                    temp_data_dict[temp_column] = company_code
                elif temp_column == "TradingDay":
                    temp_data_dict[temp_column] = lack_date
                elif temp_column == "TurnoverValue":
                    temp_data_dict[temp_column] = None
                elif temp_column == "TurnoverVolume":
                    temp_data_dict[temp_column] = None
                else:
                    temp_data_dict[temp_column] = None
            # temp_df = pd.DataFrame([temp_data_dict], columns=stock_kline_data_columns)
            # stock_kline_data_df.append(temp_df, ignore_index=True)
            stock_kline_data_df = stock_kline_data_df.append(temp_data_dict, ignore_index=True)
        stock_kline_data_df = stock_kline_data_df.sort_values(by=["TradingDay"])
        stock_kline_data_df = stock_kline_data_df.reset_index(drop=True)
        #stock_kline_data_df = stock_kline_data_df.fillna(method='ffill')
        stock_kline_data_df[['ClosePrice', 'HighPrice', 'LowPrice']] = stock_kline_data_df[['ClosePrice', 'HighPrice', 'LowPrice']].fillna(method='bfill')
        stock_kline_data_df[['ClosePrice', 'HighPrice', 'LowPrice']] = stock_kline_data_df[['ClosePrice', 'HighPrice', 'LowPrice']].fillna(method='ffill')

        '''
        for i in range(len(lack_date_list)):
            lack_date = lack_date_list[i]
            stock_kline_data_df.loc[stock_kline_data_df.TradingDay == lack_date, "TurnoverValue"] = None
            stock_kline_data_df.loc[stock_kline_data_df.TradingDay == lack_date, "TurnoverVolume"] = None
        '''
        return stock_kline_data_df






if __name__=="__main__":
    obj = GetVPGlobalVariable()
    company_code = 1000546
    futu_secucode = 700
    inner_code = 1000546
    search_start_time = "2006-01-01"
    search_end_time = "2018-01-01"
    security = "HK.800000"
    #kk = obj.get_stock_financial_data(company_code, search_start_time, search_end_time)
    #print(kk)
    #obj.get_index_data(search_start_time, search_end_time)
    #print(obj.get_stock_sharestru_data(company_code, search_start_time, search_end_time))
    #kk1 = obj.get_stock_data_info(futu_secucode, company_code, search_start_time, search_end_time)
    #kk2 = obj.get_hs_index_data(search_start_time, search_end_time)
    #obj.stock_yield_rate_compare_index_tow_day_negative(kk1, kk2)
    #trade_day = pd.Timestamp(2015,1,6)
    #days_num = 252
    #obj.get_yearly_monthes_yield_rate(futu_secucode, trade_day, 5)
    #obj.get_num_monthly_rate(kk1, trade_day, 3)
    #kk = obj.oil_market_data(trade_day, days_num)
    #print(kk)
    #kk = obj.get_sp_index_data(search_start_time, search_end_time)
    #print(kk)
    #kk = obj.half_life_weight_list(63, 252)
    #print(kk)
    #stock_data_info_df = obj.get_stock_data_info(futu_secucode, company_code, search_start_time, search_end_time)
    #index_info_df = obj.get_hs_index_data(search_start_time, search_end_time)
    #kk = obj.get_stock_data_info_add_wls(stock_data_info_df, index_info_df, "2017-09-30")
    #obj.adjust_stock_kline_data()
    kk = obj.get_stock_market_data_info(inner_code, company_code, search_start_time, search_end_time)
    print(kk.dtypes)

