#-*- coding: utf-8 -*-

# 为基本面数据服务，输入市场（目前是香港市场），开始时间，结束时间
# 根据股票获得相关信息
# 输出一个Dataframe，基本信息有市场，Innercode,comanyCode, FutuSeccode,Start_time, end_time
# 优先在Full中取数据，如果没有的话在CN中取

from sqlalchemy import create_engine, text
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.exc import DisconnectionError, OperationalError, SQLAlchemyError
import pandas as pd
import numpy as np

class StockBasicInfo():
    def __init__(self, market_code, start_time, end_time, secu_code):
        self.market_code = market_code
        self.start_time = start_time
        self.end_time = end_time
        self.secu_code = secu_code

    def get_stock_basic_info(self):
        engine_template = "mysql+pymysql://{usr}:{pwd}@{host}:{port}/{db}?charset={charset}"
        engine_str = engine_template.format(usr="fina_reader", pwd="fina_reader@", host="172.24.31.179", port="13357",
                                            db="jydb", charset="utf8mb4")
        sql_stmt = text("SELECT InnerCode, CompanyCode, SecuCode, ListedState, ListedDate  FROM SecuMain " \
                       "where SecuCategory = 1 and ListedSector in (1,2,6) and ListedDate <= Date('{end_time}') and SecuCode='{secu_code}' limit 100;".format(end_time=self.end_time, secu_code=self.secu_code))
        engine = create_engine(engine_str)
        df_result = pd.read_sql(sql_stmt, engine)
        return df_result

if __name__=="__main__":
    market_code = 'CN'
    secu_code = '000001'
    start_time = "2006-01-01"
    end_time = "2018-01-01"
    obj = StockBasicInfo(market_code, start_time, end_time, secu_code)
    result_df = obj.get_stock_basic_info()
    result_df.to_csv("../test_files/stock_info_pa.csv", index=False)
    print(result_df)
