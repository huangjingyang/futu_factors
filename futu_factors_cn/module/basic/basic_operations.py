#-*- coding: utf-8 -*-
import math
class BasicOperations(object):
    def __init__(self, val):
        self.val = val


    def format_value(self, value):
        try:
            temp_value_float = float(value)
        except:
            temp_value_float = 0
        if math.isnan(temp_value_float):
            temp_value_float = 0
        return temp_value_float

    def __sub__(self, other):
        if self.val == None and other.val == None:
            return BasicOperations(None)
        else:
            return BasicOperations(self.format_value(self.val) - self.format_value(other.val))

    def __add__(self, other):
        if self.val == None and other.val == None:
            return BasicOperations(None)
        else:
            return BasicOperations(self.format_value(self.val) + self.format_value(other.val))

    def __mul__(self, other):
        if self.val == None or other.val == None:
            return BasicOperations(None)
        else:
            return BasicOperations(self.format_value(self.val) * self.format_value(other.val))

    def __truediv__(self, other):
        if self.val == None or other.val == None:
            return BasicOperations(None)
        else:
            if other.val == 0:
                return BasicOperations(None)
            result = None
            try:
                result = self.val / other.val
            except:
                result = None
            return BasicOperations(result)

    def abs_value(self):
        try:
            temp_value_abs = abs(self.val.val)
        except:
            temp_value_abs = None
        return BasicOperations(temp_value_abs)


if __name__=="__main__":
    k1 = BasicOperations(1)
    k2 = BasicOperations(0)
    kk2 = k1 / k2
    print(kk2.val)