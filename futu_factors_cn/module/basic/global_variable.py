#-*- coding: utf-8 -*-

import pandas as pd

class GlobalVariable():
    currency_data_dict = dict()
    sp_data_info_df = pd.DataFrame(columns=["date", "open", "close", "high", "volumn", "yield_rate"])
    hs300_data_info_df = pd.DataFrame(columns=["date", "open", "close", "high", "volumn", "yield_rate"])
    stock_data_info_df = pd.DataFrame(
        columns=["date", "open", "close", "high", "volumn", "market_value", "turnover_rate", "yield_rate"])
    stock_financial_data_dict = dict()
    cyd = "caoyadong"