#-*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import math
import pymysql.cursors
import configparser
# 保存所有数据到database

# 基本思路就是利用Dataframe 的列于表格的列名，来做逐笔插入
# 为了提升效率，可以用

class HandleDB():
    def __init__(self):
        pass


    def save_data_to_db(self, table_name, data_df):
        # 决定存储的DB
        config = configparser.ConfigParser()
        config.read("config/config.ini")
        save_db = str(config["global"]["save_db"])

        connection = pymysql.connect(host='172.28.249.6',
                                     port=13357,
                                     user='stock_writer',
                                     password='stock_writer@',
                                     db=save_db,
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)
        try:
            with connection.cursor() as cursor:
                for k in range(data_df.shape[0]):
                    data_df = data_df.round(6)
                    columns = data_df.columns
                    sql = "REPLACE INTO {table_name} (`".format(table_name=table_name)
                    sql += '`,`'.join(columns.values) + "`) " + "VALUES" + " ("
                    for i in range(len(columns)):
                        column_name = columns[i]
                        value = data_df[column_name][k]
                        if column_name == "EndDate" or column_name=="InfoPublDate" or column_name=="Trading_Day":
                            sql += "Date('" + str(value) + "')"
                            sql += ","
                        elif column_name in ["InnerCode", "CompanyCode", "SecuCode", "DateTypeCode", "SW_Industry_name"]:
                            sql += "'" + str(value) + "'"
                            sql += ","
                        else:
                            if value == None or value == np.nan or math.isnan(float(value)):
                                sql += 'NULL'
                                sql += ","
                            else:
                                if isinstance(value, int) or isinstance(value, float):
                                    value = round(float(value),6)
                                sql += "'"+str(value) +  "'"
                                sql += ","
                    sql = sql.rstrip(',')
                    sql += ");"
                    #print(sql)
                    cursor.execute(sql)
            connection.commit()
        except Exception as err:
            raise
        finally:
            connection.close()

    # Cashflow
    def save_cashflow_data(self, data_df):
        table_name="LC_Cashflow"
        self.save_data_to_db(table_name, data_df)

    # HK_Debtpayingability
    def save_debtpayingability_data(self, data_df):
        table_name="LC_Debtpayingability"
        self.save_data_to_db(table_name, data_df)

    # HK_Growthability
    def save_growthability_data(self, data_df):
        table_name="LC_Growthability"
        self.save_data_to_db(table_name, data_df)

    # HK_MarketPerformance
    def save_marketperformance_data(self, data_df):
        table_name = "LC_MarketPerformance"
        self.save_data_to_db(table_name, data_df)

    # HK_Operationability
    def save_operationability_data(self, data_df):
        table_name = "LC_Operationability"
        self.save_data_to_db(table_name, data_df)

    # HK_Profitability
    def save_profitability_data(self, data_df):
        table_name = "LC_Profitability"
        self.save_data_to_db(table_name, data_df)

    # HK_Resolutionability
    def save_resolutionability_data(self, data_df):
        table_name = "LC_Resolutionability"
        self.save_data_to_db(table_name, data_df)


    # HK_Volumn_Price_Common
    def save_volumn_price_common(self, data_df):
        table_name = "LC_Volumn_Price_Common"
        self.save_data_to_db(table_name, data_df)


if __name__=="__main__":
    data_df = pd.read_csv(r"D:\runfactors\futu_factors_cn\test\Profitability.600519.csv")
    obj = HandleDB()
    obj.save_volumn_price_common(data_df)