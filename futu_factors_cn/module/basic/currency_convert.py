#-*- coding: utf-8 -*-

# 货币转换，对于香港市场统一转换为港元
# 港币不用替换
import pandas as pd
import datetime
from module.basic.global_variable import *
import configparser

class CurrencyConvertHKD():
    def __init__(self):
        #self.currency_unit_code = currency_unit_code
        #self.convert_time = convert_time
        #self.currency_data_dict = GlobalVariable().currency_data_dict
        #self.test = dict()
        pass

    @classmethod
    def get_currency_info(self, currency_unit_code, convert_time):
        result_value = 1
        config = configparser.ConfigParser()
        config.read("config/config.ini")
        currency = str(config["global"]["currency"])
        if str(currency) == "0":
            return result_value
        if str(currency_unit_code) == "1100":
            return result_value
        currency_data_dict = GlobalVariable().currency_data_dict
        try:
            currency_code = ''
            path_currency = "config/Currency.csv"
            currency_df = pd.read_csv(path_currency)
            currency_code_series = currency_df.loc[currency_df['CurrencyUnitCode'] == int(currency_unit_code), 'CurrencyCode']
            if currency_code_series.shape[0] > 0:
                currency_code = currency_code_series.iloc[0]
            else:
                raise Exception('Error!')

            # currency_code = currency_code.reset_index(drop=True)
            path_currency_data = "config/CurrencyData.xlsx"
            currency_code_convert = currency_code + "HKD"
            if currency_code_convert in currency_data_dict.keys():
                target_currency_data_df = currency_data_dict[currency_code_convert]
            else:
                target_currency_data_df = pd.read_excel(path_currency_data, sheet_name=currency_code_convert, header=None,
                                                 names=['Date', 'Value'])
                currency_data_dict[currency_code_convert] = target_currency_data_df

            result_value = target_currency_data_df.loc[target_currency_data_df['Date'] <= convert_time, 'Value'].iloc[-1]
        except Exception as err:
            raise Exception(currency_unit_code, convert_time, str(err))
        return result_value

    @classmethod
    def get_currency_info1(self, currency_unit_code, convert_time):
        currency_data_dict = GlobalVariable().currency_data_dict
        try:
            currency_code = ''
            path_currency = "../../config/Currency.csv"
            currency_df = pd.read_csv(path_currency)
            currency_code_series = currency_df.loc[
                currency_df['CurrencyUnitCode'] == int(currency_unit_code), 'CurrencyCode']
            if currency_code_series.shape[0] > 0:
                currency_code = currency_code_series.iloc[0]
            else:
                raise Exception('Error!')

            # currency_code = currency_code.reset_index(drop=True)
            path_currency_data = "../../config/CurrencyData.xlsx"
            currency_code_convert = currency_code + "HKD"
            if currency_code_convert in currency_data_dict.keys():
                target_currency_data_df = currency_data_dict[currency_code_convert]
            else:
                target_currency_data_df = pd.read_excel(path_currency_data, sheet_name=currency_code_convert,
                                                        header=None,
                                                        names=['Date', 'Value'])
                currency_data_dict[currency_code_convert] = target_currency_data_df

            result_value = target_currency_data_df.loc[target_currency_data_df['Date'] <= convert_time, 'Value'].iloc[-1]
        except:
            raise Exception(currency_unit_code, convert_time)
        return result_value

    def get_currency_info_test(self, currency_unit_code, convert_time):
        currency_code = ''
        path_currency = "../../config/Currency.csv"
        currency_df = pd.read_csv(path_currency)
        currency_code_series = currency_df.loc[currency_df['CurrencyUnitCode'] == int(currency_unit_code), 'CurrencyCode']
        if currency_code_series.shape[0] > 0:
            currency_code = currency_code_series.iloc[0]
        else:
            raise Exception('Error!')
        #currency_code = currency_code.reset_index(drop=True)
        path_currency_data = "../../config/CurrencyData.xlsx"
        sheet_name = currency_code + "HKD"
        currency_data_df = pd.read_excel(path_currency_data, sheet_name=sheet_name, header=None, names=['Date', 'Value'])
        result_value = currency_data_df.loc[currency_data_df['Date'] == convert_time, 'Value'].iloc[0]
        return result_value

def test():
    path_currency_data = "../../config/CurrencyData.xlsx"
    wb = openpyxl.load_workbook(path_currency_data)
    sheetnames = wb.sheetnames
    print(sheetnames)


if __name__=="__main__":
    '''
    path = "../../config/Currency.csv"
    df = pd.read_csv(path)
    print(df)
    '''

    currency_unit_code= "1000"
    convert_time_str = "2017-06-30"
    convert_time = datetime.datetime.strptime(convert_time_str, "%Y-%m-%d")
    obj = CurrencyConvertHKD()
    kk = obj.get_currency_info1(currency_unit_code, convert_time)
    print(GlobalVariable().currency_data_dict)
    #print(obj.test)
    #kk = CurrencyConvertHKD().get_currency_info(currency_unit_code, convert_time)
    print(kk)







